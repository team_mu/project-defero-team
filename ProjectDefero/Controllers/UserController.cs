﻿using ProjectDefero.Business;
using ProjectDefero.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectDefero.Utilities;
using System.Web.Security;
using System.IO;

namespace ProjectDefero.Controllers
{
    public class UserController : Controller
    {
        private UserBusiness userBus;
        private readonly string[] supportedBrowsers = { "Chrome", "Firefox" };

        public UserController()
        {
            userBus = new UserBusiness();
        }

        /// <summary>
        /// Obtains 3 contact lists for the current user.
        /// contacts: a list of users where this user and each user in the list have added each other to their contacts list.
        /// contactRequests: a list of users where they have added this user to their contact list, but this user has not reciprocated.
        /// pendingContacts: a list of users where this user has added them to his/her contact list, but they have not reciprocated.
        /// </summary>
        /// <returns>Whether or not the operation was successful, as well as the specified 3 lists. These lists are empty if unsuccessful.</returns>
        public JsonResult GetContacts()
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = true,
                    contacts = new List<UserModel>(),
                    contactRequests = new List<UserModel>()
                });
            }
            else
            {
                Tuple<ResultModel, ContactsModel> result = userBus.GetContacts(userId);

                return ControllerUtilities.Json(new
                {
                    success = result.Item1.Success,
                    contacts = result.Item2.Contacts,
                    contactRequests = result.Item2.ContactRequests,
                    pendingContacts = result.Item2.PendingContacts
                });
            }
            
        }

        /// <summary>
        /// Adds the user specified by the contactId to the current user's contact list.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult AddContact(int contactId)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guest users cannot add contacts"
                });
            }
            else
            {
                ResultModel result = userBus.AddContact(userId, contactId);

                return ControllerUtilities.Json(new
                {
                    success = result.Success,
                    message = result.DisplayMessage
                });
            }
        }


        /// <summary>
        /// Accepts the contact request of the user specified by the contactId.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult AcceptContactRequest(int contactId)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guest users cannot add contacts"
                });
            }
            else
            {
                ResultModel result = userBus.AcceptContactRequest(userId, contactId);

                return ControllerUtilities.Json(new
                {
                    success = result.Success,
                    message = result.DisplayMessage
                });
            }
        }

        /// <summary>
        /// Rejects the contact request of the user specified by the contactId.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult RejectContactRequest(int contactId)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guest users cannot reject contact request"
                });
            }
            else
            {
                ResultModel result = userBus.RejectContactRequest(userId, contactId);

                return ControllerUtilities.Json(new
                {
                    success = result.Success,
                    message = result.DisplayMessage
                });
            }
        }

        /// <summary>
        /// Removes the user specified by the contactId from the current user's contact list.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult RemoveContact(int contactId)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guest users cannot remove contacts"
                });
            }
            else
            {
                ResultModel result = userBus.RemoveContact(userId, contactId);

                return ControllerUtilities.Json(new
                {
                    success = result.Success,
                    message = result.DisplayMessage
                });
            }
        }

        /// <summary>
        /// Creates a user account with the specified information.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult CreateUserAccount(string username, string email, string password)
        {
            ResultModel result = userBus.CreateUserAccount(username, email, password);

            return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
        }

        /// <summary>
        /// Validates the login information provided.
        /// If successful, the current user is authenticated according to these credentials.
        /// If rememberMe == true, creates an authentication cookie which persists for a long period of time
        /// which will automatically authenticate the user when they visit the website.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult ValidateUser(string email, string password, bool rememberMe = false)
        {
            //Handle unsupported browsers
            string browser = Request.Browser.Browser;
            if (!supportedBrowsers.Contains(browser))
            {
                return ControllerUtilities.Json(
                    new { success = false, message = "Your browser is not supported"}
                    );
            }

            UserModel user = userBus.ValidateUser(email, password);

            object result = new { success = (user.UserId != 0 ? true : false), message = "Make sure your email and password are correct." };


            if (user.UserId != 0)
            {
                //We validated this user, put their UserId into the session.
                //Now we can use it everywhere.
                HttpContext.Session["UserId"] = user.UserId;
                HttpContext.Session["Username"] = user.Username;
                HttpContext.Session["IsGuest"] = false;

                if (rememberMe)
                {
                    //Set up an authentication cookie that has a long expiration.
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                        1,
                        user.Username,
                        DateTime.Now,
                        DateTime.Now.AddYears(1),
                        true,
                        user.UserId.ToString()
                        );

                    string encryptedTicket = FormsAuthentication.Encrypt(ticket);

                    HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    authCookie.Expires = DateTime.Now.AddYears(1);
                    authCookie.HttpOnly = false; //We need it to be available to AJAX requests

                    if (!System.Diagnostics.Debugger.IsAttached)
                    {
                        authCookie.Secure = true; //restrict to HTTPS if not in debug mode
                    }

                    Response.Cookies.Add(authCookie);
                }
            }

            return ControllerUtilities.Json(result);
        }

        /// <summary>
        /// Authenticates the current user as a Guest with limited permissions in the app.
        /// The provided username has "Guest-" appended to it.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult ValidateGuest(string username)
        {
            //Handle unsupported browsers
            string browser = Request.Browser.Browser;
            if (!supportedBrowsers.Contains(browser))
            {
                return ControllerUtilities.Json(
                    new { success = false, message = "Your browser is not supported" }
                    );
            }

            UserModel user = userBus.ValidateGuest(username);

            object result = new { success = (user.UserId != 0 ? true : false) };

            if (user.UserId != 0)
            {
                HttpContext.Session["UserId"] = user.UserId;
                HttpContext.Session["Username"] = user.Username;
                HttpContext.Session["IsGuest"] = true;
            }

            return ControllerUtilities.Json(result);
        }

        /// <summary>
        /// Obtains information about the current user.
        /// </summary>
        /// <returns>The current user's Id, username, and whether or not they are a Guest user.</returns>
        public JsonResult GetCurrentUser()
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            string username = (string)HttpContext.Session["Username"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (userId == null)
            {
                username = "Guest";
            }

            return ControllerUtilities.Json(new { username = username, userId = userId, isGuestUser = isGuest });

        }

        /// <summary>
        /// Obtains basic information about the user specified by userId.
        /// </summary>
        /// <returns>Username, UserId, online status, and whether or not they are a Guest user</returns>
        public JsonResult GetBasicUserInfo(int userId)
        {
            int? requesterId = (int?)HttpContext.Session["UserId"];

            Tuple<ResultModel, UserModel> result = userBus.GetBasicUserInfo(userId, requesterId);

            return ControllerUtilities.Json(new
            {
                success = result.Item1.Success,
                message = result.Item1.DisplayMessage,
                user = result.Item2
            });
        }

        /// <summary>
        /// Obtains more detailed information about the user specified by userId.
        /// </summary>
        /// <returns>The information specified by GetBasicUserInfo, as well as additional profile information.</returns>
        public JsonResult GetUserProfile(int userId)
        {
            int? requesterId = (int?)HttpContext.Session["UserId"];

            Tuple<ResultModel, UserProfileModel> result = userBus.GetUserProfile(userId, requesterId);

            return ControllerUtilities.Json(new
            {
                success = result.Item1.Success,
                message = result.Item1.DisplayMessage,
                profile = result.Item2
            });
        }

        /// <summary>
        /// Uploads the file posted with this request to Images/profile/.
        /// Profile pictures should be images, at most 200x200 pixels in size.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult UploadProfilePicture()
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guests cannot upload profile pictures"
                });
            }
            else
            {
                HttpPostedFileBase image = null;

                if (Request.Files.Count > 0)
                {
                    image = Request.Files[0];
                }

                ResultModel result = userBus.UploadProfilePicture(image, userId);

                return ControllerUtilities.Json(new
                {
                    success = result.Success,
                    message = result.DisplayMessage
                });
            }
        }

        /// <summary>
        /// Changes the current user's profile information to the provided information.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult EditProfile(string username, string realName, string aboutMe, string settings)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guests can't edit their profiles"
                });
            }
            else
            {
                ResultModel result = userBus.EditProfile(username, realName, aboutMe, settings, userId);

                if (result.Success)
                {
                    HttpContext.Session["Username"] = username;
                }

                return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
            }
        }

        /// <summary>
        /// Changes the current user's password from oldpassword to newpassword.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult ChangePassword(string oldpassword, string newpassword)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guests can't change their password"
                });
            }
            else
            {
                ResultModel result = userBus.ChangePassword(oldpassword, newpassword, userId);

                return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
            }
        }

        /// <summary>
        /// If there is a user account tied to the given email address, sends a Forgot Password email to them.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult SendForgotPasswordEmail(string email)
        {
            ResultModel result = userBus.SendForgotPasswordEmail(email, Url);

            return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
        }

        /// <summary>
        /// Verifies that the given email/password reset parameters are valid (that is, they can be used to change the password
        /// for the account tied to the given email).
        /// </summary>
        /// <returns>Whether or not the email/password reset parameter pair is valid.</returns>
        public JsonResult VerifyForgotPasswordLink(string email, string passwordReset)
        {
            ResultModel result = userBus.VerifyForgotPasswordLink(email, passwordReset);

            return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
        }

        /// <summary>
        /// Changes the password for the user tied to the given email to the provided password,
        /// if the email/passwordReset combo is valid.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult ChangeForgottenPassword(string email, string passwordReset, string newPassword)
        {
            ResultModel result = userBus.ChangeForgottenPassword(email, passwordReset, newPassword);

            return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
        }

        /// <summary>
        /// Obtains the profile picture for the user specified by userId,
        /// or a default profile picture if the specified user has not uploaded one.
        /// </summary>
        /// <returns>An image file, at most 200x200 pixels.</returns>
        public FileResult ProfilePicture(int userId)
        {
            int? requesterId = (int?)HttpContext.Session["UserId"];

            FileStream image = userBus.GetProfilePicture(userId, requesterId);

            return File(image, "image/png");
        }

        /// <summary>
        /// Signs the current user out of the app.
        /// Clears the user's authentication cookie if it exists,
        /// and terminates their session.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult SignOut()
        {
            try
            {
                //Clear the authentication cookie.
                HttpCookie cookie = Response.Cookies[FormsAuthentication.FormsCookieName];
                if (cookie != null)
                {
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(cookie);
                }

                //Clear the session
                HttpContext.Session.Abandon();

                return ControllerUtilities.Json(new { success = true, message = "Signed out successfully." });
            }
            catch (Exception)
            {
                return ControllerUtilities.Json(new { success = false, message = "There was an error signing you out." });
            }
        }

        /// <summary>
        /// Searches the database for a user whose email and/or username matches the query string.
        /// </summary>
        /// <returns>A list of users who match the query string.</returns>
        public JsonResult UserSearch(string query)
        {

            int? userId = (int?)HttpContext.Session["UserId"];

            Tuple<ResultModel, List<UserModel>> result = userBus.UserSearch(userId, query);

            return ControllerUtilities.Json(new
            {
                success = result.Item1.Success,
                message = result.Item1.DisplayMessage,
                queryResult = result.Item2
            });
        }


    }
}
