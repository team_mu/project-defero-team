﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectDefero.Controllers
{
    /// <summary>
    /// A controller for serving up Angular templates.
    /// </summary>
    public class TemplateController : Controller
    {
        public PartialViewResult ContactList()
        {
            return PartialView();
        }

        public PartialViewResult OnlineUsers()
        {
            return PartialView();
        }

        public PartialViewResult Person()
        {
            return PartialView();
        }

        public PartialViewResult Room ()
        {
            return PartialView();
        }

        public PartialViewResult Rooms()
        {
            return PartialView();
        }

        public new PartialViewResult User()
        {
            return PartialView();
        }
        public PartialViewResult Auth()
        {
            return PartialView();
        }
        public PartialViewResult ChatBox()
        {
            return PartialView();
        }

        public PartialViewResult Login()
        {
            return PartialView();
        }

        public PartialViewResult Register()
        {
            return PartialView();
        }
        public PartialViewResult VideoStream()
        {
            return PartialView();
        }
        public PartialViewResult AddRoom()
        {
            return PartialView();
        }
        public PartialViewResult Feedback()
        {
            return PartialView();
        }

        public PartialViewResult UserProfile()
        {
            return PartialView();
        }

        public PartialViewResult LogInGuest()
        {
            return PartialView();        
        }

        public PartialViewResult EditProfileInfo()
        {
            return PartialView();
        }

        public PartialViewResult About()
        {
            return PartialView();
        }

        public PartialViewResult EditRoom()
        {
            return PartialView();
        }
    }
}
