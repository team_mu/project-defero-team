﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectDefero.Controllers
{
    public class InstallGuideController : Controller
    {
        //
        // GET: /InstallGuide/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult win8InstallGuide(string returnUrl)
        {
            return View();
        }

        public ActionResult winServerInstallGuide(string returnUrl)
        {
            return View();
        }
    }
}
