﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectDefero.Utilities;
using ProjectDefero.Models;

namespace ProjectDefero.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// The main page of the website.
        /// </summary>
        public ActionResult Index(string returnUrl)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            return View(new BrowserModel() { SignedIn = (userId != null) } );
        }


        /// <summary>
        /// Obtains the current time from the server in milliseconds since the Unix epoch (January 1, 1970).
        /// Also computes the travel time of the packet in the client->server direction.
        /// travelTime is roughly the time it takes for the server to receive the request, in milliseconds.
        /// </summary>
        public JsonResult GetServerTimeMillis(long clientTime)
        {
            TimeSpan millis = DateTime.UtcNow - new DateTime(1970, 1, 1);



            return ControllerUtilities.Json(new
            {
                time = millis.TotalMilliseconds,
                travelTime = millis.TotalMilliseconds - clientTime
            });
        }
    }
}