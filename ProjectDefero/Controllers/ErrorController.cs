﻿using ProjectDefero.Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace ProjectDefero.Controllers
{
    public class ErrorController : Controller
    {
        private string appDataFolder;
        private ErrorBusiness errorBus;

        public ErrorController()
        {
            appDataFolder = HostingEnvironment.MapPath("~/App_Data");
            errorBus = new ErrorBusiness();
        }
        
        public void LogError(string message)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                errorBus.LogErrorToFile(appDataFolder + "/errorlog.txt", message);
            }
            else
            {
                errorBus.LogErrorToWjtask(message);
            }
        }

        /// <summary>
        /// Displays a 404 page.
        /// </summary>
        public ViewResult PageNotFound()
        {
            return View();
        }

        public ViewResult UnsupportedBrowser(string browser = "")
        {
            return View();
        }
    }
}
