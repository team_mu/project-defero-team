﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectDefero.Utilities;
using ProjectDefero.Business;
using ProjectDefero.Models;

namespace ProjectDefero.Controllers
{
    public class RoomController : Controller
    {
        private RoomBusiness roomBus = new RoomBusiness();

        /// <summary>
        /// Obtains the current user's rooms.
        /// </summary>
        /// <returns>Whether or not the operation was successful.
        /// If successful, the rooms list will be nonempty and will contain information about each room, 
        /// such as its current users.</returns>
        public JsonResult GetCurrentRooms()
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            Tuple<ResultModel, List<RoomModel>> result = roomBus.GetRooms(userId);

            return ControllerUtilities.Json(
                new { 
                    success = result.Item1.Success, 
                    rooms = result.Item2, 
                    message = result.Item1.DisplayMessage });
        }

        /// <summary>
        /// Creates a room with the specified name.
        /// </summary>
        /// <returns>Whether or not the operation was successful. 
        /// If successful, the current user automatically joins the room.</returns>
        public JsonResult CreateRoom(String roomName)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new 
                { 
                    success = false, 
                    message = "Guests cannot create rooms"
                });
            }
            else
            {
                Tuple<ResultModel, RoomModel> result = roomBus.CreateRoom(userId, roomName);

                if (result.Item1.Success == true)
                {
                    ResultModel joinResult = roomBus.JoinRoom(userId, result.Item2.RoomId, asAdmin: true);

                    return ControllerUtilities.Json(new { success = joinResult.Success, message = joinResult.DisplayMessage });
                }
                else
                {
                    return ControllerUtilities.Json(new { success = result.Item1.Success, message = result.Item1.DisplayMessage });
                }
            
            }
        }
        
        /// <summary>
        /// Adds the current user to the room specified by the given inviteKey.
        /// Invite Keys are provided automatically to users who are in a room via the GetCurrentRooms API.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult JoinRoom(string inviteKey)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            ResultModel result = roomBus.JoinRoom(userId, inviteKey);

            return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
        }

        /// <summary>
        /// Removes the current user from the room specified by the given roomId.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult LeaveRoom(int roomId)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            ResultModel result = roomBus.LeaveRoom(userId, roomId);

            return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
        }

        /// <summary>
        /// Kicks the user specified by kickedUserId from the room specified by roomId.
        /// Kicks only work if the current user is an administrator for the specified room.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult KickUser(int roomId, int kickedUserId)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guests cannot kick users"
                });
            }
            else
            {
                ResultModel result = roomBus.KickUser(roomId, kickedUserId, userId);

                return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
            }
        }

        /// <summary>
        /// Changes the user specified by changedUserId to the role specified by roleName
        /// for the room specified by roomId.
        /// Role changes only work if the current user is an administrator for the specified room.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult ChangeUserRole(int roomId, int changedUserId, string roleName)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guests cannot change users' roles"
                });
            }
            else
            {
                ResultModel result = roomBus.ChangeUserRole(roomId, changedUserId, userId, roleName);

                return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
            }
        }

        /// <summary>
        /// Obtains a URL for the room specified by roomId.
        /// Room URLs can only be obtained for rooms the current user is in.
        /// </summary>
        /// <returns>Whether or not the operation was successful.
        /// If successful, url is a fully-qualified URL for the app which, when followed, will allow the person
        /// who follows it to join the specified room.</returns>
        public JsonResult GetRoomUrl(int roomId)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guests cannot get room URLs"
                });
            }
            else
            {
                Tuple<ResultModel, string> result = roomBus.GetRoomUrl(roomId, Url, userId);

                return ControllerUtilities.Json(new { success = result.Item1.Success, message = result.Item1.DisplayMessage, url = result.Item2 });
            }
        }

        /// <summary>
        /// Obtains information about the room specified by roomKey.
        /// This information can be obtained regardless of whether the current user is in the room.
        /// It is intended for display when a user clicks on a room URL so that they
        /// can verify that they do want to join it.
        /// </summary>
        /// <returns>Whether or not the operation was successful.
        /// If successful, room is an object containing information about the room, such as name and current users in the room.</returns>
        public JsonResult GetRoomInfoByKey(string roomKey)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            Tuple<ResultModel, RoomModel> result = roomBus.GetRoomInfoByKey(roomKey, userId);

            return ControllerUtilities.Json(new { success = result.Item1.Success, message = result.Item1.DisplayMessage, room = result.Item2 });
        }


        /// <summary>
        /// Adds the current user to the room specified by roomKey.
        /// This API should be called when a user clicks on a Room URL and confirms
        /// that they want to join the room.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult JoinRoomByRoomKey(string roomKey)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            ResultModel result = roomBus.JoinRoomByRoomKey(userId, roomKey);

            return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
        }

        /// <summary>
        /// Edits the information for the room specified by roomId.
        /// For now, just the room's name.
        /// </summary>
        /// <returns>Whether or not the operation was successful.</returns>
        public JsonResult EditRoom(int roomId, string name)
        {
            int? userId = (int?)HttpContext.Session["UserId"];

            bool? isGuest = (bool?)HttpContext.Session["IsGuest"];

            if (isGuest == true)
            {
                return ControllerUtilities.Json(new
                {
                    success = false,
                    message = "Guests cannot edit rooms"
                });
            }
            else
            {
                ResultModel result = roomBus.EditRoom(roomId, name, userId);

                return ControllerUtilities.Json(new { success = result.Success, message = result.DisplayMessage });
            }
        }
    }
}
