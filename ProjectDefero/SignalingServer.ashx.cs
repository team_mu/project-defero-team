﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Web.WebSockets;
using System.Web.SessionState;

namespace ProjectDefero
{
    /// <summary>
    /// Summary description for SignalingServer
    /// </summary>
    public class SignalingServer : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            //check if the request is a websocket request
            if (context.IsWebSocketRequest)
            {
                //if websocket request. Accept it then create a MicrosoftWebSocket passing it the session.
                context.AcceptWebSocketRequest(new MicrosoftWebSockets(context.Session));
            }
        }

        //Don't know what this does, it was here when the file was created.
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}