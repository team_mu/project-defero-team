﻿using System.Web;
using System.Web.Mvc;

namespace ProjectDefero
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //this is supposed to force any http request to our site to redirect to https.
            #if !DEBUG
                filters.Add(new RequireHttpsAttribute());
            #endif
        }
    }
}