﻿using DeferoEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectDefero.Utilities;

namespace ProjectDefero
{
    /// <summary>
    /// Contains methods that should be run on initialization of the web service
    /// to handle things like required database entries.
    /// </summary>
    public static class Initialization
    {
        /// <summary>
        /// Initializes the database to have proper room roles should they not already exist.
        /// </summary>
        public static void InitializeRoomRoles()
        {
            DeferoDB context = new DeferoDB();

            if (context.VTRoomRoles.Where(x => x.Name == "Administrator").Count() == 0) 
            {
                VTRoomRole adminRole = new VTRoomRole()
                {
                    Name = "Administrator"
                };

                context.VTRoomRoles.Add(adminRole);

                context.SaveChanges();
            }

            if (context.VTRoomRoles.Where(x => x.Name == "User").Count() == 0)
            {
                VTRoomRole userRole = new VTRoomRole()
                {
                    Name = "User"
                };

                context.VTRoomRoles.Add(userRole);

                context.SaveChanges();
            }
        }
    }
}