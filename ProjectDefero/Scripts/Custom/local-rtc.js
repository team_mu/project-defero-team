﻿function LocalRTC()
{
    var LOGGING = false;
    //callbacks
    this.onError = null;

    //private variables
    var onLocalStream = {}; //deprecated and never called.
    //var userMediaConstraints = {audio: {optional: {echoCancellation: true}}, video: false}; //does not seem to do anything
    var localStream = null;

    var error = function (_error) {
        if (typeof (this.onError) === "function") {
            this.onError(_error);
        }
    }.bind(this);

    //Call this if you want to get a stream that only has audio
    this.getMicrophoneStream = function () {
        return new Promise(function (successCallback, failedCallback) {
            if (LOGGING) {
                console.log("LocalRTC: requesting microphone only stream");
            }
            var mediaConstraints = { audio: { mandatory: { echoCancellation: true } }, video: false };
            navigator.getUserMedia(mediaConstraints, function (_stream) {
                //if we have stream, stop it before getting new one.
                if (localStream) {
                    localStream.stop();
                }

                localStream = _stream; successCallback(_stream);
            }, failedCallback);
        });
    }

    this.getVideoStream = function(){
        return new Promise(function (successCallback, failedCallback) {
            if (LOGGING) {
                console.log("LocalRTC: requesting video only stream");
            }
            var mediaConstraints = { audio: false, video: true };
            navigator.getUserMedia(mediaConstraints, function (_stream) {
                //if we have stream, stop it before getting new one.
                if (localStream) {
                    localStream.stop();
                }

                localStream = _stream; successCallback(_stream);
            }, failedCallback);
        });
    }

    //Call this if you to get a stream that has video and microphone
    this.getMicVideoStream = function () {
        return new Promise(function (successCallback, failedCallback) {
            if (LOGGING) {
                console.log("LocalRTC: requesting microphone and video stream");
            }
            var mediaConstraints = { audio: { mandatory: { echoCancellation: true } }, video: true };
            navigator.getUserMedia(mediaConstraints, function (_stream) {
                //if we have stream. Stop it before getting new one.
                if (localStream) {
                    localStream.stop();
                }

                localStream = _stream; successCallback(_stream);
            }, failedCallback);;
        });
    }

    this.getStream = function (){
        return localStream;
    }

    this.removeStream = function () {
        if (localStream) {
            localStream.stop();
        }
        localStream = null;
    }
}