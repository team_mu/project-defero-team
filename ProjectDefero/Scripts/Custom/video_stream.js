﻿(
function () {
	'use strict';
	var app = angular.module('video_stream', []);

	app.directive(
	  'videostream',
	  function () {
		  return {
			  restrict: 'E',
			  templateUrl: '/Template/VideoStream',
			  link: function (scope, element, attributes) {
				  scope.$watch("stream.stream", function () {
					  var video = element[0].getElementsByTagName('video')[0];

					  if (scope.stream.stream) {
						  //video.setAttribute("autoplay", true);
						  attachMediaStream(video, scope.stream.stream);
						  video.load()
						  video.play();
					  }
					  else {
						  video.pause();
					  }
				  });

				  scope.$watchGroup(['stream.audioEnabled', 'stream.videoEnabled'], function (newValues, oldValues, scope) {
					  var video = element[0].getElementsByTagName('video')[0];
					  if (video) {
						  if (scope.stream.audioEnabled && scope.stream.videoEnabled) {
							  video.volume = 1;
						  }
						  else if (scope.stream.audioEnabled) {
							  video.volume = 1;
						  }
						  else if (scope.stream.videoEnabled) {
							  video.volume = 0;
						  }
						  else {
							  video.volume = 0;
						  }
					  }
				  });

				  var getUserName = function (_userName) {
				      scope.stream.name = _userName;
				  }.bind(this);

				  scope.$watch("inCall", function () {
				      if (scope.stream.name === '') {
				          scope.$root.users.get_user(scope.id, function (ret) { getUserName(ret.Username); });
				      }
				  });
			  },
			  controller: ['$scope', '$element', '$rootScope', '$interval', function ($scope, $element, $rootScope, $interval) {
				  var roomID = $scope.roomID;
				  var peerID = $scope.id;

				  $scope.hasVideoStream = function () {
					  //console.log($scope.stream.stream && $scope.stream.stream.getVideoTracks());
					  return $scope.stream.stream && $scope.stream.stream.getVideoTracks().length;
				  };

				  $scope.hasAudioStream = function () {
					  //console.log($scope.stream.stream && $scope.stream.stream.getAudioTracks());
					  return $scope.stream.stream && $scope.stream.stream.getAudioTracks().length;
				  };

				  var sendSignal = function (_type) {
					  console.log("Want to " + _type + " for " + peerID);
					  //$rootScope.signal.send({ destination: peerID, type: _type, room: roomID, data: null });
				  }

				  /**
				   *Enables the audio for the provided peer, if id is user id, then sends audio.
				   **/
				  $scope.enableAudio = function (_id) {
					  $scope.stream.audioEnabled = true;
					  sendSignal("enableAudio");
				  };

				  /**
				  *Enables the video for the provided peer, if id is user id, then sends audio.
				  **/
				  $scope.enableVideo = function (_id) {
					  $scope.stream.videoEnabled = true;
					  sendSignal("enableVideo");
				  };

				  /**
				  *Enables the audio and video for the provided peer, if id is user id, then sends audio.
					*I don't think we need this function
				  **/
				  $scope.enabaleMicAndVideo = function (_id) {
					  $scope.stream.audioEnabled = true;
					  $scope.stream.videoEnabled = true;
					  sendSignal("enableAudioAndVideo");
				  }

				  /**
				  *Disables the audio for the provided peer, if id is user id, then sends audio.
				  **/
				  $scope.disableAudio = function (_id) {
					  $scope.stream.audioEnabled = false;
					  sendSignal("disableAudio");
				  }

				  /**
				  *Disables the video for the provided peer, if id is user id, then sends audio.
				  **/
				  $scope.disableVideo = function (_id) {
					  $scope.stream.videoEnabled = false;
					  sendSignal("disableVideo");
				  }

				  $scope.peerConnectionStats = {receivedPerSecond: 0, sentPerSecond: 0, audioReceived: 0, audioSent: 0, videoReceived: 0, videoSent: 0, totalReceived: 0, totalSent: 0 };

				  //in seconds
				  var updateInterval = 2;

				  $scope.statUpdate = $interval(function () {
					  if ($rootScope.peerConnections.getPeerConnection(peerID)) {
						  $rootScope.peerConnections.getPeerConnection(peerID).peerConnection.getStats().then(function (_stats) {
							  //calculate bytes per second.
							  $scope.peerConnectionStats.receivedPerSecond = ((((_stats.totalReceived - $scope.peerConnectionStats.totalReceived) / updateInterval) / 1024) * 8).toFixed(3);
							  $scope.peerConnectionStats.sentPerSecond = ((((_stats.totalSent - $scope.peerConnectionStats.totalSent) / updateInterval) / 1024) * 8).toFixed(3);
							  //update rest of the stats so we can computer future
							  $scope.peerConnectionStats.audioReceived = _stats.audioReceived;
							  $scope.peerConnectionStats.audioSent = _stats.audioSent;
							  $scope.peerConnectionStats.videoReceived = _stats.videoReceived;
							  $scope.peerConnectionStats.videoSent = _stats.videoSent;
							  $scope.peerConnectionStats.totalReceived = _stats.totalReceived;
							  $scope.peerConnectionStats.totalSent = _stats.totalSent;
						  }, null);
					  }
				  }, (updateInterval * 1000));

				  //listen for when this element is destoyed, and when it is, cancel the interval event.
				  $element.on('$destroy', function () {
				      $interval.cancel($scope.statUpdate);

                      //Get video element.
				      var video = $element[0].getElementsByTagName('video')[0];
                      //check if it still exists, and if it does stop the video.
				      if (video) {
				          video.pause;
				          video.src = "";
				      }
				  });
			  }
			  ],
			  controllerAs: 'videostream'
		  };
	  }
	);
}
)();
