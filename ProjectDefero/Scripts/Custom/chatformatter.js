﻿function chatFormatter(sanitizer) {

    this.sanitize = sanitizer;
    //bold/italics/underline/strike/superscript/subscript/code
    this.tokens = "** -- __ ~~ ^^ `` ##".split(" ");
    this.tokenMap =
        {
            "**": ["<b>", "</b>"],
            "--": ["<i>", "</i>"],
            "__": ["<u>", "</u>"],
            "~~": ["<s>", "</s>"],
            "^^": ["<sup>", "</sup>"],
            "``": ["<sub>", "</sub>"],
            "##": ["<pre>", "</pre>"]
        };

    //Not all of these will necessarily be in the final release. We'll see what I can make.
    this.emoticons = (":angel: :banned: :bash: :blink: :chin: :cool: :deadpan: :dry: :dunce: :eyeroll: :goodgrief: :happyno: :happyyes: :hug: " +
                      ":huh: :interest: :jawdrop: :kiss: :laugh: :locked: :lol: :love: :mad: :nudgewink: :ohmy: :ouch: :outtahere: :pcwhack: " +
                      ":pirate: :poke: :popcorn: :rant: :rekt: :rip: :sad: :sadno: :smile: :smug: :spock: :sweat: :tears: :therethere: " + 
                      ":thumbsup: :tongue: :unsure: :wave: :what: :wink: :orz:")
        .split(" ");

    this.getImageTag = function (emoticon) {
        emoticon = emoticon.substring(1, emoticon.length - 1);
        return '<img src="/Images/emoticons/' + emoticon + '.gif" class="emoticon" title=":' + emoticon + ':">';

        //for debugging and testing purposes:
        //return "!" + emoticon + "!";
    }

    //Determines whether two tags are mismatched (eg, firstTag is <u> and secondTag is </i> instead of </u>)
    this.mismatchedOpenCloseTags = function (firstTag, secondTag) {
        if (secondTag.charAt(1) != "/" || (firstTag.charAt(1) == "/" && secondTag.charAt(1) == "/")) {
            return false;
        }

        //firstTag is an opening tag, secondTag is a closing tag
        if (firstTag.substring(1, firstTag.length - 1) == secondTag.substring(2, secondTag.length - 1)) {
            return false;
        }
        else {
            return true;
        }
    }

    //Gets the closing tag for an opening tag
    this.getClosingTag = function (openingTag) {
        return "</" + openingTag.substring(1);
    }

    this.getEmoticons = function () {
        return this.emoticons.map(function (current) {
            return current.substring(1, current.length - 1);
        });
    }
}

//Formats the given text, replacing formatting characters and emoticons
chatFormatter.prototype.format = function (text) {

    //First replace formatting characters.
    for (var i = 0; i < this.tokens.length; i++) {

        var formatMark = this.tokens[i];

        //Find all the indices at which this particular formatting character occurs
        var count = 0;
        for (var j = 0; j < text.length - 1; j++) {
            if (text.substring(j, j + 2) == formatMark) {
                count++;

                //Increment j an extra time to skip over the 2-character formatting mark.
                //Normally in the string "***t", it would find a match at 0, increment j to 1, and find another match there.
                //Incrementing twice makes it go from a looking for match at index 0 to looking for a match at index 2, 
                //which is the string "*t", which is not a match.
                j++;
            }
        }

        //Iterated over the whole string, we now know how many of the current formatting mark there are.

        if (count % 2 == 1) {
            //We have an uneven number of formatting marks, ignore the last one.
            count--;
        }

        //Replace the marks in batches of 2.
        for (; count > 0; count -= 2) {
            text = text.replace(formatMark, this.tokenMap[formatMark][0]);
            text = text.replace(formatMark, this.tokenMap[formatMark][1]);
        }
    }

    //Do a pass to fix poorly nested tags 
    //(i.e., "**bold --italics and bold** just italics--" wouldn't have the "just italics" portion in italics due to badly nested HTML tags

    //Remove link tags first and insert placeholders.
    var anchorPattern = /<a[\s]+([^>]+)>((?:.(?!\<\/a\>))*.)<\/a>/g; //thanks http://stackoverflow.com/questions/9158346/regular-expression-for-anchor-tag-whith-all-attibutes
    var links = text.match(anchorPattern);
    text = text.replace(anchorPattern, "<<linkplaceholder>>");

    var tagPattern = /<\/?[a-z]>/g;
    var tags = text.match(tagPattern);         
    var subsequences = text.split(tagPattern); 
    var tagStack = [];

    if (tags != null) {

        //Begin rebuilding the string.
        var result = subsequences[0];
        var push = true;
        for (var i = 0; i < tags.length; i++) {

            result = result + tags[i] + subsequences[i + 1]; 

            //We want to push opening tags only.
            if (tags[i].charAt(1) != "/" && push) {
                tagStack.push(tags[i]); 
            }
            
            push = true;

            //Check if the next tag is a closing tag and matches the current one.
            if (tags[i + 1] != null && tags[i + 1].charAt(1) == "/") { 

                var topTag = tagStack.pop();

                if (this.mismatchedOpenCloseTags(topTag, tags[i + 1])) {
                    //tags mismatched: update tags[i + 1] to close and reopen the mismatched tag, and don't push next loop cycle.
                    tags[i + 1] = this.getClosingTag(topTag) + tags[i + 1] + topTag;
                    tagStack.push(topTag);
                    push = false; //if we pushed, we'd end up with a "tag" that looked like "</i><u><i>" when we just want the "<i>" in the stack.
                }
            }
            else {
                tagStack.push(tags[i])
            }

            //result = result + tags[i] + subsequences[i + 1];
            //if (mismatchedOpenCloseTags(tags[i], tags[i + 1])) {
            //    //Update the next tags entry to properly close and reopen the html tags.
            //    tags[i + 1] = getClosingTag(tags[i]) + tags[i + 1] + tags[i];
            //}
        }

        text = result;
    }

    //Reinsert links
    if (links != null) {
        for (var i = 0; i < links.length; i++) {
            text = text.replace("<<linkplaceholder>>", links[i]);
        }
    }


    //Next replace emoticons
    var emotePattern = /:[a-zA-Z]+:/g;
    var usedEmoticons = text.match(emotePattern);

    if (usedEmoticons != null) {

        var emoticonSet = [];

        //Remove any duplicates since we're going to do a single replace call to replace every emoticon of the same type.
        //If we don't remove duplicates, we'll get an Html parse error because the replacement inserts an img tag with the :emoticon:
        //inside the title text of the tag and the regex would replace that with an img tag.
        for (var i = 0; i < usedEmoticons.length; i++) {
            if (emoticonSet.indexOf(usedEmoticons[i]) == -1) {
                emoticonSet.push(usedEmoticons[i]);
            }
        }

        for (var i = 0; i < emoticonSet.length; i++) {
            var r = new RegExp(emoticonSet[i], "g"); 
            if (this.emoticons.indexOf(emoticonSet[i]) != -1) { //valid emoticon
                text = text.replace(r, this.getImageTag(emoticonSet[i]));
            }
        }
    }

    

    return text;
}