﻿// chat_box
// John D. Wissler

(
  function () {
      var app = angular.module('chat_box', ['ngSanitize']);

      app.directive(
        'chatBox',
        function () {
            return {
                restrict: 'E',
                templateUrl: '/Template/ChatBox',
                link: function (scope, element, attr) {
                    scope.initRoom();
                },
                controller: ['$scope', '$element', 'chatFormatter', '$rootScope', function ($scope, $element, chatFormatter, $rootScope) {
                    //Get the messages passed through the scope.
                    var chatContent = $element[0].getElementsByClassName("message_list")[0];
                   
                    $scope.auto_scroll = true;
                    
                    $scope.accept_attachment = function (chatFileMetadata) {
                        chatFileMetadata.offerState = ChatFileMetadata.STATE.OFFER_ACCEPTED;
                        $scope.chat_model.fileOfferResponse(chatFileMetadata);
                    };

                    $scope.reject_attachment = function (chatFileMetadata) {
                        chatFileMetadata.offerState = ChatFileMetadata.STATE.OFFER_REJECTED;
                        $scope.chat_model.fileOfferResponse(chatFileMetadata);
                    };

                    $scope.cancel_attachment_send = function (chatFileMetadata) {
                        chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
                        $scope.chat_model.cancelFileTransmission(chatFileMetadata);
                    };

                    $scope.cancel_attachment_receive = function (chatFileMetadata) {
                        chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
                        $scope.chat_model.cancelFileRetrieval(chatFileMetadata);
                    };

                    $scope.am_sender = function (message) {
                        return +message.authorUserId === +$rootScope.user_id;
                    }

                    $scope.can_download = function (message) {
                        return message.chatFileMetadata.offerState === ChatFileMetadata.STATE.OFFER_PENDING;
                    };

                    $scope.can_cancel = function (message) {
                        if (message.chatFileMetadata.offerState === ChatFileMetadata.STATE.ARCHIVED) {
                            return false;
                        }

                        var canCancel = message.chatFileMetadata.offerState !== ChatFileMetadata.STATE.OFFER_EXPIRED
                            && message.chatFileMetadata.offerState !== ChatFileMetadata.STATE.TRANSMISSION_FAILED
                            && message.chatFileMetadata.offerState !== ChatFileMetadata.STATE.TRANSMISSION_SUCCEEDED
                            && message.chatFileMetadata.offerState !== ChatFileMetadata.STATE.TRANSMISSION_CANCELLED
                            && message.chatFileMetadata.offerState !== ChatFileMetadata.STATE.OFFER_REJECTED;

                        if (!$scope.am_sender(message)) {
                            canCancel = canCancel && message.chatFileMetadata.offerState !== ChatFileMetadata.STATE.OFFER_PENDING;
                        }

                        return canCancel;
                    };

                    $scope.can_progress = function (message) {
                        return message.chatFileMetadata.offerState === ChatFileMetadata.STATE.TRANSMISSION_IN_PROGRESS;
                    };

                    $scope.is_attachment = function (message) {
                        return message.messageType === ChatTextMessage.TYPE.FILE_TRANSMISSION;
                    };
                    
                    $(chatContent).on('scroll', function () {
                        if ($scope.searching) {
                            return;
                        }
                        var from_bottom = (chatContent.scrollHeight - chatContent.scrollTop) - chatContent.clientHeight;
                        $scope.auto_scroll = Math.abs(from_bottom) <= 5; // Account for floating point errors?
                        // Scroll through previous messages.
                        if (chatContent.scrollTop === 0) {
                            $scope.chat_model.setChatBufferLength($scope.chat_model.getChatBufferLength() + 150);
                            setTimeout(function () {
                                    chatContent.scrollTop = 1; // Need a little hack to make sure you can keep scrolling...            
                            });
                        }
                    });

                    $scope.show_emoticons = false;

                    $scope.emoticons = chatFormatter.getEmoticons();

                    $scope.toggle_emoticons = function () {
                        $scope.show_emoticons = !$scope.show_emoticons;
                    };

                    $scope.insertEmoticon = function (emoticon) {
                        var input = $($element[0]).find('.chatbox_input');
                        var text = input.val();
                        var preSelection = text.substring(0, input[0].selectionStart);
                        var postSelection = text.substring(input[0].selectionEnd, text.length);
                        input.val(preSelection + ":" + emoticon + ":" + postSelection);
                        input.focus();
                        input[0].selectionStart = preSelection.length + emoticon.length + 2;
                        input[0].selectionEnd = preSelection.length + emoticon.length + 2;
                    }

                    $scope.getProfilePicture = function (user_id) {
                        return $rootScope.users.getUserProfilePicture(user);
                    };
                                       
                    //Now we want to watch for when the variable changes.
                    //and make sure the box moves.
                    $scope.$watchCollection("messages", function () {

                        if ($scope.auto_scroll) {
                            setTimeout(function () {
                                chatContent.scrollTop = chatContent.scrollHeight;
                            }.bind(this), 10); // Just make sure to wait till the DOM is updated
                        }
                    });

                    var onMessageToBeSent = $scope.onMessageToBeSent;

                    var chatInput = $element[0].getElementsByClassName("chatbox_input")[0];

                    chatInput.addEventListener("keypress", function (_event) {
                        //check if the key that was press is the "enter" key
                        if (_event.keyCode == 13) {
                            //if shift key is not pressed, then only enter was and message is to be sent. 
                            //Otherwise, shift was press and we want it to act like an enter.
                            if (!_event.shiftKey)
                            {
                                //prevent the default event for "enter" key
                                _event.preventDefault();

                                var textArea = $element[0].getElementsByClassName("chatbox_input")[0];
                                var text = textArea.value;

                                //set the textInput to be empty
                                textArea.value = "";
                                if (typeof (onMessageToBeSent) === "function") {
                                    onMessageToBeSent(text);
                                }
                            }
                        }
                    });


                    /* Handle file dragging and dropping into the chat text areas.
                     */

                    var onFilesToBeSent = $scope.onFilesToBeSent;

                    function handleFileDragover(evt) {
                        evt.stopPropagation();
                        evt.preventDefault();
                        evt.dataTransfer.dropEffect = 'copy';
                    }

                    function handleFileSelect(evt) {
                        evt.stopPropagation();
                        evt.preventDefault();

                        if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
                            return;
                        }

                        try {
                            var files = evt.dataTransfer.files;
                            var output = [];

                            for (var i = 0, f; f = files[i]; i++) {
                                if (typeof (onFilesToBeSent) === "function") {
                                    onFilesToBeSent(f);
                                }
                            }
                        }
                        catch (ex) { }
                    }

                    // Drag and drop files into the message input area.
                    chatInput.addEventListener("dragover", handleFileDragover, false);
                    chatInput.addEventListener("drop", handleFileSelect, false);

                    var chatOutput = $element[0].getElementsByClassName("message_list")[0];

                    // Drag and drop files into the message output area.
                    chatOutput.addEventListener("dragover", handleFileDragover, false);
                    chatOutput.addEventListener("drop", handleFileSelect, false);
                  }
                ],
                controllerAs: 'chat_box'
            };
        }
      );
  }
)();