(
	function () {
	    'use strict';
	    var app = angular.module('room', ['local_video_stream', 'video_stream', 'ngMaterial', 'chat_box', 'users_sidenav']);

	    app.filter('not_self', function () {
	        return function (input, user_id) {
	            var arr = [];
	            input.forEach(function (user) {
	                if (user.UserId !== user_id) arr.push(user);
	            });
	            return arr;
	        };
	    });


	    app.filter('focused', function () {
	        return function (rtc, focused_object) {
	            var arr = {}, i;
	            for (i in rtc) {
	                if (rtc[i] === focused_object) arr[i] = rtc[i];
	            }

	            return arr;
	        };
	    });

	    app.filter('not_focused', function () {
	        return function (rtc, focused_object) {
	            var arr = {}, i;
	            for (i in rtc) {
	                if (rtc[i] !== focused_object) arr[i] = rtc[i];
	            }

	            return arr;
	        };
	    });


	    app.directive(
				'room',
				function () {
				    return {
				        restrict: 'E',
				        templateUrl: '/Template/Room',
				        link: function () {
				        },
				        controller: [
				'$http', '$element', '$rootScope', '$scope', '$mdDialog', '$mdSidenav', '$rooms_controller',
				'$mdComponentRegistry', '$timeout', '$window',
				function ($http, $element, $rootScope, $scope, $mdDialog, $mdSidenav, $rooms_controller, $mdComponentRegistry, $timeout, $window) {
				    var that = this;
				    this.roomID = $scope.roomID = $scope.$parent.room_json.RoomId;  //This should be cleaned up... 
				    $scope.el = $($element[0]);
				    //Dictionary, hash,whatever javascript calls it of all the peers that are in the call.
				    $scope.peersInCall = {};
				    $scope.focusedPeer = {};
				    $scope.inCall = false;
				    $scope.am_in_call = function () {
				        return $scope.inCall;
				    };
				    $scope.callInProgress = false;
				    $scope.messages = [];
				    $scope.videos_shown = true;
				    $scope.messages_shown = true;
				    $scope.show_messages = function () { $scope.messages_shown = true; }
				    $scope.hide_messages = function () { $scope.messages_shown = false; }
				    $scope.show_videos = function () { $scope.videos_shown = true; }
				    $scope.hide_videos = function () { $scope.videos_shown = false; }
				    $scope.focused = -1;

				    $scope.toggle_search = function () {
				        $scope.show_search = !$scope.show_search;
				        $scope.search_input = '';
				        $scope.update_search();
				    }

				    $scope.am_admin = function () {
				        var user = $rootScope.users.get_user_room($rootScope.user_id, this.roomID);
				        return user && user.Role === 'Administrator';
				    }.bind(this);

				    $scope.share_room = function () {
				        $http.post(
							'/Room/GetRoomUrl',
							{
							    roomId: this.roomID
							}
						).then(
							function (ret) {
							    window.prompt("Copy URL to clipboard: Ctrl+C, Enter", ret.data.url);
							}
						);
				    };

				    //variable that changes if we are logging or not.
				    var ROOMLOGGING = false;

				    $scope.isFocused = function (id) {
				        return id === $scope.focused;
				    };

				    $scope.setFocused = function (id) {
				        if (id === $scope.focused) {
				            $scope.focused_object = null;
				            $scope.focused = -1;
				        } else {
				            $scope.focused_object = $scope.peersInCall[id];
				            $scope.focused = id;
				        }
				    };

				    $scope.unsetFocused = function (id) {
				        $scope.focused = -1;
				        $scope.focused_object = null;

				    };

				    $scope.search_input = '';

				    $scope.update_search = function () {
				        var myFilter = new ChatTextMessageFilter();
				        $scope.searching = true;
				        myFilter.setFilter_messageBody($scope.search_input, ChatTextMessageFilter.FILTER_CLAUSE.CONTAINS);
				        chatModel.loadChatHistory(myFilter, function () { $scope.searching = false; });
				    };

				    var chatModel = $scope.chat_model = new ChatModel($rootScope.user_id, $scope.roomID, $rootScope, $rooms_controller);

				    //gets the username associated with the id, then call the provided method.
				    var getUserName = function (user_id, callback) {
				        $rootScope.users.get_user(user_id, function (ret) { callback(ret.Username); });
				    };

				    this.get_room_json = function () {
				        return $rooms_controller.get_room($scope.roomID);
				    };

				    this.get_users = function () {
				        return this.get_room_json() && this.get_room_json().Users;
				    }.bind(this);

				    /**
					*
					**/
				    var addPeerStreamToList = function (_data) {
				        if ($scope.peersInCall[_data.id]) {
				            $scope.peersInCall[_data.id].stream = _data.stream;
				        }

				        if (!$scope.$$phase) {
				            $scope.$apply();
				        }
				    }.bind(this);

				    /**
					*
					**/
				    var removePeerStreamFromList = function (user_id) {
				        $scope.peersInCall[user_id].stream = null;
				        if (!$scope.$$phase) {
				            $scope.$apply();
				        }
				    }.bind(this);

				    /**
					 *Adds new messages or updates existing messages in the chatcontainer.
					 **/
				    var updateChatBox = function (buffer_changes) {
				        $scope.messages = buffer_changes;

				        if (that.has_updated_messages && (!$rootScope.focused || $rootScope.current_room_id !== that.roomID)) {
				            $($element[0]).find('textarea').one('focus', function () {
				                $rooms_controller.has_message[that.roomID] = false;
				                if (!$scope.$$phase) {
				                    $scope.$apply();
				                }
				            })
				        }
				        that.has_updated_messages = true;
				        if (!$scope.$$phase) {
				            $scope.$apply();
				        }
				    }

				    //set up a function to handle when we get a ChatTextMessage from the chat model.
				    chatModel.registerOnMessageHandler(function (_chatTextMessageDisplayIndex, _chatTextMessage) {
				        updateChatBox(_chatTextMessageDisplayIndex, _chatTextMessage);
				    });


                    //Displays a notification for the given chat message with the given title and body.
				    var displayNotification = function (_chatTextMessage, title, notificationBody) {
				        var notification = new Notification(title,
                                                            { body: notificationBody });

				        notification.onshow = function () {
				            $timeout(function () { notification.close() }, 5000);
				        }

				        //Make the notification, when clicked, cause the user to focus on the window and view the room
                        //where the message was received.
				        notification.onclick = function () {
				            $window.focus();
				            $rootScope.setRoom(Number(_chatTextMessage.roomId));
				        }
				    }

				    // Make a sound or perform some sort of action when a new live message is received.
				    chatModel.registerOnLiveMessageNotifyHandler(function (_chatTextMessage) {
				        $('#message_sound')[0].play();
				        $rooms_controller.has_message[that.roomID] = true;

				        if ("Notification" in window && (!($rootScope.windowFocus === true) || $rootScope.current_room_id != Number(_chatTextMessage.roomId))) {

				            var notificationBody = $rootScope.users.get_user(Number(_chatTextMessage.authorUserId)).Username + ": "
                                + _chatTextMessage.messageBody;
				            var notificationTitle = $rooms_controller.get_room(Number(_chatTextMessage.roomId)).Name;

				            displayNotification(_chatTextMessage, notificationTitle, notificationBody);
				        }
				    });

				    // Make a sound or perform some sort of action when a file share offer is received.
				    chatModel.registerOnFileOfferNotifyHandler(function (_chatTextMessage) {
				        $('#file_sound')[0].play();
				        $rooms_controller.has_message[that.roomID] = true;

				        if ("Notification" in window && (!($rootScope.windowFocus === true) || $rootScope.current_room_id != Number(_chatTextMessage.roomId))) {

				            var notificationBody = $rootScope.users.get_user(Number(_chatTextMessage.authorUserId)).Username + ": "
                                + _chatTextMessage.chatFileMetadata.fileName;
				            var notificationTitle = $rooms_controller.get_room(Number(_chatTextMessage.roomId)).Name;

				            displayNotification(_chatTextMessage, notificationTitle, notificationBody);
				        }
				    });

				    this.toggle_users = function () {
				        $scope.users_nav.toggle();
				    };

				    this.leave_room = function (ev) {
				        var confirm = $mdDialog.confirm()
											.title('Leave Room?')
											.content('It will permanently be removed from your Rooms list.')
											.ariaLabel('Leave room')
											.ok('Yes')
											.cancel('No')
											.targetEvent(ev);

				        $mdDialog.show(confirm).then(function () {
				            if ($rootScope.signal) {
				                $rootScope.signal.deregisterRoom($scope.roomID);
				            }
				            $rooms_controller.leave_room($scope.roomID);
				        }, function () { });
				    };

				    this.edit_name = function (ev) {
				        $mdDialog.show({
				            controller: function ($scope, $mdDialog) {
				                setTimeout(function () { $('textarea[name=room_name]').focus(); });

				                $scope.hide = function () {
				                    $mdDialog.hide();
				                };

				                $scope.cancel = function () {
				                    $mdDialog.cancel();
				                };

				                $scope.answer = function (edit) {
				                    if (edit) {
				                        $http.post(
											'/Room/EditRoom',
											{
											    roomId: that.roomID,
											    name: $scope.room_name
											}
										 ).then(function () {
										     $mdDialog.hide();
										     $rooms_controller.update();
										 });
				                    } else {
				                        $mdDialog.hide();
				                    }
				                }.bind(this)
				            },
				            templateUrl: '/Template/EditRoom',
				            targetEvent: ev,
				        });
				    };

				    /**
					 *GUI calls this when text needs to be sent to all connected PeerRTCs
					 **/
				    var sendText = function (_text) {
				        chatModel.broadcastChatTextMessage(_text);
				    }

				    /**
					 * GUI calls this when files are to be sent to all connected PeerRTCs.
					 * Parameter _windowFile: window.File object.
					 **/
				    var sendFile = function (_windowFile) {
				        chatModel.broadcastFile(_windowFile);
				    }

				    /**
					 *This will check if this rooms still has a call in progress.
					 **/
				    var updateCallStatus = function () {
				        if ((Object.keys($scope.peersInCall).length === 0) && !$scope.inCall) {
				            $rooms_controller.in_call[$scope.roomID] = false;
				            $scope.callInProgress = false;
				        }
				        else {
				            $rooms_controller.in_call[$scope.roomID] = true;
				            $scope.callInProgress = true;
				        }

				        if (!$scope.$$phase) {
				            $scope.$apply();
				        }
				    }.bind(this);

				    /**
					 *Event handler for when PeerRTC has a remote stream from the peer that we need to add.
					 *                           {sourceID, stream}
					 *_data: JSON object in form {id, stream}
					 **/
				    var receivedRemoteStream = function (_data) {
				        addPeerStreamToList(_data);
				    }

				    /**
					 *Event handler for when peer removes a stream, we need to update our UI.
					 *                   {sourceID}
					 *_data: JSON object {id}
					 **/
				    var peerRemovedStream = function (_data) {
				        if ($scope.peersInCall[_data.id]) {
				            if ($scope.peersInCall[_data.id].stream === _data.stream) {
				                removePeerStreamFromList(_data.id);
				            }
				        }
				    }

				    /**
                    *This function cleans up anything the room has with the peerRTC object. Generally happens when the peer is to be deleted.
                    **/
				    var cleanUpPeerRTC = function (_peerID) {
				        chatModel.removePeerRTCInstance($rootScope.peerConnections.getPeerConnection(_peerID).peerConnection);

				        //remove peer from list of users in call if in call.
				        if ($scope.peersInCall[_peerID]) {
				            delete $scope.peersInCall[_peerID];
				            updateCallStatus();
				        }
				    }

				    /**
				     *Event handler for when a chat data channel is setup between peer.
				     *                   {sourceID}
				     *_data: JSON object {id}
				     **/
				    var peerEnabledChat = function (_data) {
				        //log in developer tools
				        if (ROOMLOGGING) {
				            console.log("Room " + $scope.roomID + ": enabled chat with: " + _data.id);
				        }

				        // Perform chat history synchronization.
				        chatModel.sendChatMessageHistorySyncRequest($rootScope.peerConnections.getPeerConnection(_data.id).peerConnection);
				    }

				    /**
					 *This is a method that handles signals for "startingCall" and "InCall"
					 **/
				    var callSignalHandler = function (_source) {
				        $scope.peersInCall[_source] = { name: "", stream: null, audioEnabled: true, videoEnabled: true };

				        //get the name of user that is connecting and then add them to our data stucture.
				        getUserName(_source, function (_name) {
				            //Add the peer to the list of people currently in the call.
				            $scope.peersInCall[_source].name = _name;
				        }.bind(this));

				        //Update the flags that track room call status.
				        updateCallStatus();
				    }.bind(this);

				    /**
					 *This function will determine which signal was sent through websockets and perform the appropriate action.
					 **/
				    var onSignalHandler = function (_signal) {
				        var source = _signal.source;

				        if (_signal.type === "inRoom") {
				            if (ROOMLOGGING) {
				                console.log("room " + $scope.roomID + ": peer " + source + " is in room.");
				            }

				            if ($rootScope.peerConnections.getPeerConnection(source)) {
				                if (ROOMLOGGING) {
				                    console.log("room " + $scope.roomID + ": already connected with peer " + source + ".");
				                }

				                /* Add the new PeerRTC connection to the chat model.
								 * The chat model will register the PeerRTC.onMessage handler.
								 * If the chat model already contains a PeerRTC instance with
								 * this PeerRTC's id, then the chat model will remove the old
								 * PeerRTC instance and replace it with this one.
								 */
				                chatModel.addPeerRTCInstance($rootScope.peerConnections.getPeerConnection(source).peerConnection);

				                //register the room with the peer connection.
				                $rootScope.peerConnections.registerRoomWithPeer($scope.roomID, source, { chatEnabled: peerEnabledChat, dataEnabled: null, receivedChatMessage: null, receivedDataMessage: null }, { peerRTCCleanUp: cleanUpPeerRTC });
				            }
				            else {
				                //now lets determine local browser type.
				                var localBrowser = { browser: "unkown" };
				                if (navigator.mozGetUserMedia) {
				                    localBrowser.browser = "firefox";
				                }
				                else if (navigator.webkitGetUserMedia) {
				                    localBrowser.browser = "chrome";
				                }

				                //send message back to source telling them our browser.
				                if (!$rootScope.signal.send({ destination: source, type: "peerBrowser", room: null, data: localBrowser })) {
				                    console.log("Failed to send browser info to: " + source);
				                }

				                //get the browser type of peer
				                var peerBrowser = _signal.data.browser;

				                if (ROOMLOGGING) {
				                    console.log("room " + $scope.roomID + ": creating new connection to peer " + source + ".");
				                }

				                //create peer connection
				                $rootScope.peerConnections.createNewPeerConnection(source, peerBrowser);

				                /* Add the new PeerRTC connection to the chat model.
								* The chat model will register the PeerRTC.onMessage handler.
								* If the chat model already contains a PeerRTC instance with
								* this PeerRTC's id, then the chat model will remove the old
								* PeerRTC instance and replace it with this one.
								*/
				                chatModel.addPeerRTCInstance($rootScope.peerConnections.getPeerConnection(source).peerConnection);

				                //register the room with the peer connection.
				                $rootScope.peerConnections.registerRoomWithPeer($scope.roomID, source, { chatEnabled: peerEnabledChat, dataEnabled: null, receivedChatMessage: null, receivedDataMessage: null }, { peerRTCCleanUp: cleanUpPeerRTC });

				                //create data channels
				                $rootScope.peerConnections.getPeerConnection(source).peerConnection.createDataChannels();
				            }

				            //if we are participating in the call for this room, then notify person who just connected.
				            if ($scope.inCall) {
				                //update the eventhandlers
				                $rootScope.peerConnections.changeStreamEventHandlers({ receivedRemoteStream: receivedRemoteStream, peerRemovedStream: peerRemovedStream });
				                $rootScope.signal.send({ destination: source, type: "inCall", room: $scope.roomID, data: null });
				            }
				        }
				        else if (_signal.type === "startingCall") {
				            if (source) {
				                //If not in call, notify user that a call is starting.
				                if (!$scope.inCall) {
				                    var reset = function () {
				                        $('#call_sound')[0].pause();
				                        $('#call_sound')[0].currentTime = 0;
				                    }, cancel_timeout, confirm = $mdDialog.confirm()
									 .title('A call is starting in room: ' + $rooms_controller.get_room(that.roomID).Name)
									 .content('Would you like to join?')
									 .ok('Join')
									 .cancel('Dismiss');
				                    $('#call_sound')[0].play();
				                    $mdDialog.show(confirm).then(function () {
				                        that.joinCall();
				                        console.log('clear')
				                        $rootScope.setRoom(that.roomID);
				                        clearTimeout(cancel_timeout);
				                        reset();
				                    }, function () {
				                        console.log('clear')
				                        clearTimeout(cancel_timeout);
				                        reset();
				                    });

				                    cancel_timeout = $timeout(function () {
				                        $mdDialog.cancel();
				                        reset();
				                    }, 10000)
				                }
				                //If logging, then log.
				                if (ROOMLOGGING) {
				                    console.log("room " + $scope.roomID + ": peer " + source + " is starting a call.");
				                }
				                //Call the method that handles call Signals.
				                callSignalHandler(source);
				            }
				        }
				        else if (_signal.type === "inCall") {
				            if (source) {
				                //If logging, then log.
				                if (ROOMLOGGING) {
				                    console.log("room " + $scope.roomID + ": peer " + source + " is participating in the call.");
				                }

				                //Call the method that handles call Signals.
				                callSignalHandler(source);

				                //if we are in the call, then start streaming.
				                if ($scope.inCall) {
				                    //if we are in a call we may have a local stream, if so then add it to peer connection.
				                    if ($rootScope.local_rtc.getStream()) {
				                        $rootScope.peerConnections.getPeerConnection(source).peerConnection.addLocalStream($rootScope.local_rtc.getStream());
				                    }

				                    $rootScope.peerConnections.getPeerConnection(source).peerConnection.renegotiate();
				                }
				            }
				        }
				        else if (_signal.type === "leavingCall") {
				            if (ROOMLOGGING) {
				                console.log("Room " + $scope.roomID + ": " + _signal.source + " is leaving the call.");
				            }
				            //stop transmitting audio to this peer.
				            $rootScope.peerConnections.getPeerConnection(source).peerConnection.removeLocalStream();
				            $rootScope.peerConnections.getPeerConnection(source).peerConnection.renegotiate();

				            //now remove them from our list.
				            delete $scope.peersInCall[source];

				            //make the room update its flags and information displayed to user. 
				            updateCallStatus();
				        }
				    }

				    /**
					 *This function is to notify the users that are in the room we are now in the call.
					 *If the room does not have a call in progress, then we send a starting call notification instead.
					 *
					 **/
				    var notifyPeersInCall = function () {
				        var message = null;

				        //if no one is in a call, then we are starting the call.
				        if (Object.keys($scope.peersInCall).length === 0) {
				            message = { destination: null, type: "startingCall", room: $scope.roomID, data: null };
				        }
				        else {
				            message = { destination: null, type: "inCall", room: $scope.roomID, data: null };
				        }

				        var peerConnections = $rootScope.peerConnections.getAllPeerConnections();
				        for (var key in peerConnections) {
				            //give the peerRTC the stream, but it is not used yet shared with peer at this point.
				            if ($rootScope.local_rtc.getStream()) {
				                peerConnections[key].addLocalStream($rootScope.local_rtc.getStream());
				            }

				            //now add the destination.
				            message.destination = key;

				            //notify the peer we are in call.
				            $rootScope.signal.send(message);
				        }

				    }.bind(this);

				    /**
					 *Go through every peer in the call and removes the stream from the connection, then sends a signal to notify that peer.
					 **/
				    var removeStreamFromAllPeersInCall = function () {
				        //although only the peers that are in call should have a stream, remove it from all...
				        var peerConnections = $rootScope.peerConnections.getAllPeerConnections();
				        for (var key in peerConnections) {
				            $rootScope.peerConnections.getPeerConnection(key).peerConnection.removeLocalStream();
				            $rootScope.signal.send({ destination: key, type: "leavingCall", room: $scope.roomID, data: null });
				        }
				    }

				    /**
					 *Call this when user wants to join the call, it will get your local audio, and then call the appropriate methods to 
					 *notify all peers currently online that you are joining the call.
					 **/
				    this.joinCall = function () {
				        //since we are joining, we don't have stream yet. Get one and attach to everyone in call.
				        if ($rootScope.roomInCall) {
				            $rootScope.roomInCall.leaveCall();
				        }
				        $rootScope.roomInCall = this;

				        window.onbeforeunload = function () {
				            return 'You are in a Call.';
				        };

				        //add the event handlers for this room to the peerRTC since it is now in the call.
				        var allPeers = $rootScope.peerConnections.getAllPeerConnections();
				        for (var key in allPeers) {
				            $rootScope.peerConnections.changeStreamEventHandlers({ receivedRemoteStream: receivedRemoteStream, peerRemovedStream: peerRemovedStream });
				        }

				        //request a stream only containing microphone
				        $rootScope.local_rtc.getMicrophoneStream().then(function (_stream) {
				            $scope.callInProgress = true;
				            $scope.inCall = true;
				            //notify all peers we are joining the call.
				            notifyPeersInCall();
				        }, function (_error) {
				            $scope.callInProgress = true;
				            $scope.inCall = true;

				            //see if the error is caused by user denying permission or something else
				            if (_error.name === "PermissionDeniedError") {
				                //make sure the user is aware they denied permission to use the mic. 
				                $mdDialog.show(
							        $mdDialog.alert()
							        .title('Microphone Access Denied')
							        .content("You chose not to allow access to your microphone, you may join the room but others will not hear you.")
							        .ariaLabel('Microphone Access Denied')
							        .ok('Continue')
							        .targetEvent()
						        );
				            }
				            else {
				                //since we were unable to get a stream with just audio, notify user.
				                $mdDialog.show(
							        $mdDialog.alert()
							        .title('Error Accessing Microphone')
							        .content("We were unable to access your microphone, you may join the room but others will not hear you.")
							        .ariaLabel('Error Accessing Microphone')
							        .ok('Continue')
							        .targetEvent()
						        );
				            }
				            //notify all peers we are joining the call.
				            notifyPeersInCall();
				        });
				    }.bind(this)

				    /**
					 *Call this function when the user wants to leave the call. 
					 *It will stop streaming all audio to peers, and send a signal over the websocket telling them to stop streaming to you.
					 **/
				    this.leaveCall = function () {
				        $scope.inCall = false;
				        window.onbeforeunload = null;
				        updateCallStatus();
				        $rootScope.roomInCall = undefined;
				        removeStreamFromAllPeersInCall();
				        $rootScope.local_rtc.removeStream();
				    }.bind(this)

				    /**
					 *send a signal to all peers that are online. This lets them know you are in the room.
					 **/
				    var connectToOnlinePeers = function () {
				        //get the list of users who are online and connect to them all.
				        var usersInRoom = $rooms_controller.get_room($scope.roomID).Users;
				        var myID = $rootScope.user_id;

				        for (var i = 0; i < usersInRoom.length; i++) {
				            var peerID = usersInRoom[i].UserId;
				            if (!(peerID === myID) && (usersInRoom[i].Online)) {
				                var peerConnection = $rootScope.peerConnections.getPeerConnection(peerID);
				                //get local browser info.
				                var localBrowser = { browser: "unkown" };

				                if (navigator.mozGetUserMedia) {
				                    localBrowser.browser = "firefox";
				                }
				                else if (navigator.webkitGetUserMedia) {
				                    localBrowser.browser = "chrome";
				                }

				                //check if we already have a peer connection
				                if (peerConnection) {
				                    if (ROOMLOGGING) {
				                        console.log("room " + $scope.roomID + ": already have a connection with " + peerID);
				                    }
				                }
				                else {
				                    //we don't have a connection, so we create one.
				                    $rootScope.peerConnections.createNewPeerConnection(peerID, null);

				                    //get the peer connection we just created.
				                    peerConnection = $rootScope.peerConnections.getPeerConnection(peerID);

				                    //send a signal to the peer letting them know we are in this room.
				                    if (!$rootScope.signal.send({ destination: peerID, type: "connectionRequest", room: $scope.roomID, data: localBrowser })) {
				                        console.log("room " + $scope.roomID + ": failed to send connectionRequest signal to: " + peerID);
				                    }
				                }

				                /* Add the new PeerRTC connection to the chat model.
								* The chat model will register the PeerRTC.onMessage handler.
								* If the chat model already contains a PeerRTC instance with
								* this PeerRTC's id, then the chat model will remove the old
								* PeerRTC instance and replace it with this one.
								*/
				                chatModel.addPeerRTCInstance(peerConnection.peerConnection);

				                //register the room with the peer connection.
				                $rootScope.peerConnections.registerRoomWithPeer($scope.roomID, peerID, { chatEnabled: peerEnabledChat, dataEnabled: null, receivedChatMessage: null, receivedDataMessage: null }, { peerRTCCleanUp: cleanUpPeerRTC });

				                if (ROOMLOGGING) {
				                    console.log("room " + $scope.roomID + ": sending inRoom to peer " + peerID);
				                }

				                //send a signal to the peer letting them know we are in this room.
				                if (!$rootScope.signal.send({ destination: peerID, type: "inRoom", room: $scope.roomID, data: localBrowser })) {
				                    console.log("room " + $scope.roomID + ": failed to send inRoom signal to: " + peerID);
				                }
				            }
				        }
				    }

				    /**
					 *This is called once the room has been rendered and we can start doing work.
					 **/
				    $scope.initRoom = function () {
				        // Load cached chat messages.
				        chatModel.loadChatHistory();

				        //connect to the peers that are currently online.
				        connectToOnlinePeers();
				    }

				    //Add a variable to the scope wich points to the method that needs to be called when we send chat in room.
				    $scope.onMessageToBeSent = sendText;
				    $scope.onFilesToBeSent = sendFile;

				    //check if we are signed in.
				    //though the room should not be created if we are not signed in.
				    if ($rootScope.logged_in) {
				        //register myself with signaling object. Pass a room ID of 1 for now,
				        //will need to dynamically grab once more than a single room is implmented.
				        $rootScope.signal.registerRoom($scope.roomID, onSignalHandler);
				    }
				    else {
				        console.log("room " + $scope.roomID + ": is loading before logged in.");
				    }

				    //detect when the element is destroyed, and perfom some actions. This is likely only caused when the user is kicked...
				    $element.on('$destroy', function () {
				        if($rootScope.signal){
				            $rootScope.signal.deregisterRoom($scope.roomID);
				        }

				        var peerConnections = $rootScope.peerConnections.getAllPeerConnections();
				        for (var key in peerConnections) {
				            $rootScope.peerConnections.removeRoomFromPeers($scope.roomID);
				        }

                        //check if we are in call, if so then leave the call.
				        if ($scope.inCall) {
				            this.leaveCall();
				        }
				    }.bind(this));
				}
				        ],
				        controllerAs: 'room'
				    };
				}
		);
	}
)();
