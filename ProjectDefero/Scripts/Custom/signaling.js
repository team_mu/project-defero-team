﻿/**
*This is how we communicate with the signaling server
*It uses websockets.
**/

function SignalConnection(_rootScope, callback)
{
    var LOGGING = false;
    var rootScope = _rootScope;
    //Holds all the call backs in key/value pairs
    var messageCallBacks = {};
    var onConnectCallBack = callback;
    this.connected = false;

    //var url = "wss://projectdefero.com/signalingServer.ashx?userID=" + window.userId;
    var url = location.protocol.replace("http", "ws") + "//" + document.location.host + "/signalingServer.ashx";
    var socket = new WebSocket(url);

    //setup event when socket is closed.
    socket.onclose = function (_object) {
        if (LOGGING) {
            console.log("Signaling: disconnected from server.");
        }
        
        //update the logged in status and reload the page.
        rootScope.logged_in = false;
        //location.reload();
    }

    //setup event handler for when socket receives a message.
    socket.onmessage = function (_object) {
        //convert the data back into a JSON object.
        var messageObject = JSON.parse(_object.data);
        var source = messageObject.source;

        //determine if the message is just a connection notice or a message for somebody else.
        if (messageObject.type === "connected") {
            this.connected = true;
            if (LOGGING) {
                console.log("Signaling: connected to server");
            }
            
            if (typeof (onConnectCallBack) === "function")
            {
                onConnectCallBack();
            }
        }
        else if (messageObject.type === "connectionRequest") {
            rootScope.peerConnections.connectionRequest(messageObject);
        }
        else if (messageObject.type === "stillConnected") {
            rootScope.peerConnections.stillConnectedMessage(messageObject);
        }
        else if (messageObject.type === "peerNotConnected") {
            rootScope.peerConnections.peerIsDisconnected(messageObject);
        }
        else if (messageObject.type === "peerBrowser"){
            rootScope.peerConnections.updatePeerBrowser(messageObject);
        }
        else if (messageObject.type === "signal") {
            rootScope.peerConnections.peerRTCSignal(messageObject);
        }
        else {
            //get the inteded room this signal is for.
            var destinationRoom = messageObject.room;

            //grab the associated callback for the room
            var callBack = messageCallBacks[destinationRoom];

            //make sure we actually have a callback. and that it is a function.
            if (typeof (callBack) === "function") {
                callBack(messageObject);
            }
            else if (messageObject.type === "invite")
            {
                if (LOGGING) {
                    console.log("Signaling: user is not in room " + destinationRoom + "; or a function was not provided as callback.");
                }
                
                callBack = messageCallBacks["default"];

                if (typeof(callBack) === "function")
                {
                    callBack(messageObject);
                }
            }
        }
    }.bind(this);

    /**
    *Adds room and its callback to signalObject
    **/
    this.registerRoom = function(_roomID, _callback)
    {
        if (_roomID in messageCallBacks)
        {
            if (LOGGING) {
                console.log("Signaling: room " + _roomID + "is already registered, removing.");
            }
        }

        //if the key is already in there, this overwrites existing. 
        messageCallBacks[_roomID] = _callback;

        if (LOGGING) {
            console.log("Signaling: registered room " + _roomID);
        }
    }

    /**
    *Removes a room and its associated callback from signalObject.
    **/
    this.deregisterRoom = function(_roomID)
    {
        if (_roomID in messageCallBacks)
        {
            delete messageCallBacks[_roomID];
        }
        else
        {
            if (LOGGING) {
                console.log("Signaling: Unable to deregister room " + _roomID + ", it was not in list.");
            }
        }
    }

    var createSignalMessage = function(_data)
    {
        //signal looks like {source, destination, type, room, data}
        return {source:rootScope.user_id, destination:_data.destination, type:_data.type, room:_data.room, data:_data.data, user: _data.user};
    }

    /**
    *Sends supplied data to the websocket.
    **/
    this.send = function(_data)
    {
        var sent = false;
        var tryCount = 0;

        while ((sent === false) && (tryCount < 3))
        {
            if (this.connected) {
                //make sure user id is not null.
                if (rootScope.user_id) {
                    socket.send(JSON.stringify(createSignalMessage(_data)));
                    sent = true;
                }
                else {
                    if (LOGGING) {
                        console.log("Signaling: rootscope.user_id is null.");
                        break;
                    }
                }
            }

            tryCount++;
        }

        if ((sent === false) && LOGGING) {
            console.log("Signaling: not connected to signal server, can't send signal");
        }

        return sent;
    }
}