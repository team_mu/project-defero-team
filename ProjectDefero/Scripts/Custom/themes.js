﻿(
function () {
    var themes = {
        pink: ['pink', 'blue'],
        indigo: ['indigo', 'pink'],
        brown: ['brown', 'blue'],
        blue: ['blue', 'red'],
        grey: ['grey', 'blue-grey']
    };
    angular.module('defero').factory('$themes', [
        '$http',
        function ($http) {
            return {
                current_theme: localStorage.theme || 'indigo',
                get_themes: function () { return themes; },
                set_theme: function (theme) {
                    localStorage.theme = theme;
                    this.current_theme = theme;
                }
            };
        }]);
       
    angular.module('defero').config(function ($mdThemingProvider) {       
        for (var i in themes) {
            $mdThemingProvider.theme(i).primaryPalette(themes[i][0]).accentPalette(themes[i][1]);
        }

        $mdThemingProvider.alwaysWatchTheme(true);
    });
})
();