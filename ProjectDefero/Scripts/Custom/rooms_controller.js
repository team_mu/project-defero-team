﻿(
function () {    
    angular.module('defero').factory('$rooms_controller', [
        '$http', '$rootScope', '$mdDialog', '$timeout', 
        function ($http, $rootScope, $mdDialog, $timeout) {        
            return {
                rooms: [],
                in_call: {},
                _has_message: {},
                has_message: function (rooms_controller, id) {
                    return rooms_controller._has_message[id];
                },

                notify_message: function () {
                    
                },

                get_room_index: function (room_id) {
                    for (var i = 0; i < this.rooms.length; ++i) {
                        if (this.rooms[i].RoomId === room_id) {
                            return i;
                        }
                    }
                },

                room_has_user: function (room_id, user_id) {
                    var room = this.get_room(room_id), users = room && room.Users, i, max;
                    if (users) {
                        for (i = 0, max = users.length; i < max; ++i) {
                            if (users[i].UserId === user_id) return true;
                        }
                    }
                    return false;
                },

                get_room: function (room_id) {
                    for (var i = 0; i < this.rooms.length; ++i) {
                        if (this.rooms[i].RoomId === room_id) {
                            return this.rooms[i];
                        }
                    }
                },

                add_room: function (ev) {
                    var that = this;
                    $mdDialog.show({
                        clickOutsideToClose: false,
                        controller: function ($scope, $mdDialog) {
                            setTimeout(function () { $('input[name=room_name]').focus(); });

                            $scope.hide = function () {
                                $mdDialog.hide();
                            };

                            $scope.cancel = function () {
                                $mdDialog.cancel();
                            };
                            $scope.answer = function (create_room) {
                                $scope.logging_in = true;
                                $scope.error = '';
                                if (create_room) {
                                    $http.post('/Room/CreateRoom',
                                    {
                                        roomName: $scope.name
                                    }
                                     ).success(
                                          function (message) {
                                              if (message.success) {
                                                  $mdDialog.hide();
                                                  $http.post('/Room/GetCurrentRooms').success(
                                                  function (data) {
                                                      if (data.success) {
                                                          that.rooms.push(data.rooms[data.rooms.length - 1]);
                                                      }
                                                  }.bind(this)
                                                 );
                                              }
                                          }.bind(this)
                                );
                                } else {
                                    $mdDialog.hide();
                                }
                            }.bind(this)
                        },
                        templateUrl: '/Template/AddRoom',
                        targetEvent: ev,
                    });
                },

                join_room: function (invite_key) {
                    var that = this;

                    $http.post('/Room/JoinRoom',
                               {
                                   inviteKey: invite_key
                               }
                                ).success(
                                     function (message) {
                                         if (message.success) {
                                             $http.post('/Room/GetCurrentRooms').success(
                                             function (data) {
                                                 if (data.success) {
                                                     that.rooms.push(data.rooms[data.rooms.length - 1]);
                                                 }
                                             }.bind(this)
                                            );
                                         }
                                     }.bind(this)
                           );
                },

                leave_room: function (room_id) {
                    var that = this;
                    $http.post('/Room/LeaveRoom',
                           {
                               roomId: room_id
                           }
                       ).success(
                       function (message) {
                           if (message.success) {
                               for (var i = 0; i < that.rooms.length; ++i) {
                                   if (that.rooms[i].RoomId === room_id) {
                                       that.rooms.splice(i, 1);
                                       return;
                                   }
                               }
                           }
                       }.bind(this)
                  );
                },

                is_active: function (id) {
                    return $rootScope.current_room_id === id;
                },

                update: function (callback) {
                    $http.post('/Room/GetCurrentRooms').success(
                       function (data) {
                           if (data.success) {
                               this.rooms = data.rooms;
                               this.rooms.forEach(
                                   function (room) {
                                       $rootScope.users.update_users_room(room.Users, room.RoomId);
                                   }
                               );
                               if (callback) callback();
                           }
                       }.bind(this)
                     );
                }
            };        
        }]);
})();