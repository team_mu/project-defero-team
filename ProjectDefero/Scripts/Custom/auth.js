﻿(
function () {
    var app = angular.module('auth', ['ngMaterial']);

    app.directive(
    'auth',
    function () {
        return {
            restrict: 'E',
            templateUrl: '/Template/Auth',
            controller: [
              '$http', '$mdDialog', '$rootScope', '$scope', '$rooms_controller', '$interval', '$timeout', '$themes',
              function ($http, $mdDialog, $rootScope, $scope, $rooms_controller, $interval, $timeout, $themes) {
                  var that = this;
                  this.name = '';
                  this.email = '';
                  this.password = '';

                  $scope.get_profile_picture = function (id) {
                      return 'background-image:url(/User/ProfilePicture?userId=' + id + ')';
                  };

                  this.log_in_guest = function (ev) {
                      $mdDialog.show({
                          controller: function ($scope, $mdDialog) {
                              setTimeout(function () { $('input[name=username]').focus(); });
                              $scope.hide = function () {
                                  $mdDialog.hide();
                              };

                              $scope.cancel = function () {
                                  $mdDialog.cancel();
                              };
                              $scope.answer = function (do_login) {
                                  $scope.logging_in = true;
                                  $scope.error = '';
                                  if (do_login) {
                                      $http.post(
                                          '/User/ValidateGuest',
                                          {
                                              username: $scope.username
                                          }
                                          ).then(
                                          function (resp) {
                                              $scope.logging_in = false;

                                              if (resp.data.success) {
                                                  location.reload();
                                              } else {
                                                  $scope.error = 'There was an error logging in. ' + resp.data.message;
                                              }
                                          });
                                  } else {
                                      $mdDialog.hide();
                                  }
                              }
                          },
                          templateUrl: '/Template/LogInGuest',
                          targetEvent: ev,
                      });
                  }

                  $scope.giveFeedback = function (ev) {
                      $mdDialog.show({
                          controller: function ($scope, $mdDialog) {
                              setTimeout(function () { $('textarea[name=feedback]').focus(); });

                              $scope.hide = function () {
                                  $mdDialog.hide();
                              };

                              $scope.cancel = function () {
                                  $mdDialog.cancel();
                              };

                              $scope.answer = function (do_feedback) {
                                  if (do_feedback) {
                                      window.wjtask_on_error("**Feedback from User**\n\n" + $scope.feedback);

                                      $mdDialog.hide();
                                      $mdDialog.show(
                                        $mdDialog.alert()
                                          .title('Feedback')
                                          .content('Thank you for your Feedback on Defero.')
                                          .ariaLabel('Feedback')
                                          .ok('OK')
                                          .targetEvent(ev)
                                      );

                                  } else {
                                      $mdDialog.hide();
                                  }
                              }
                          },
                          templateUrl: '/Template/Feedback',
                          targetEvent: ev,
                      });
                  };

                  $scope.showAbout = function (ev) {
                      location.hash = "about";
                      $mdDialog.show({
                          clickOutsideToClose: false,
                          escapeToClose: false,
                          controller: function ($scope, $mdDialog) {
                              $scope.hide = function () {
                                  location.hash = "";
                                  $mdDialog.hide();
                              };

                          },
                          templateUrl: '/Template/About',
                          targetEvent: ev,
                      });
                  };

                  this.log_in = function (email, password, remember, callback) {
                      $http.post('/User/ValidateUser',
                          {
                              email: email,
                              password: password,
                              rememberMe: remember
                          }
                           ).success(callback);

                      this.name = '';
                      this.email = '';
                      this.password = '';
                  };

                  this.sign_up = function (name, email, password, callback) {
                      $http.post('/User/CreateUserAccount',
                          {
                              username: name,
                              email: email,
                              password: password
                          }
                           ).success(callback);

                      this.name = '';
                      this.password = '';
                      this.email = '';
                  };

                  this.logout = function () {
                      $http.post('/User/SignOut',
                          {
                              username: this.name,
                              email: this.email,
                              password: this.password
                          }
                           ).success(function (resp) {
                               $rootScope.logged_in = false;
                               location.reload();
                               localStorage.clear();
                           }.bind(this)
                      );
                  };

                  this.show_sign_up = function (ev) {
                      $mdDialog.show({
                          controller: function ($scope, $mdDialog) {
                              setTimeout(function () { $('input[name=name]').focus(); });

                              $scope.hide = function () {
                                  $mdDialog.hide();
                              };

                              $scope.cancel = function () {
                                  $mdDialog.cancel();
                              };

                              $scope.answer = function (do_signup) {
                                  $scope.signing_up = true;
                                  $scope.error = '';
                                  if (do_signup) {
                                      if ($scope.confirm_password !== $scope.password) {
                                          $scope.error = 'Passwords must be equal.';
                                          $scope.signing_up = false;
                                          return;
                                      }
                                      that.sign_up($scope.name, $scope.email, $scope.password, function (resp) {
                                          $scope.signing_up = false;

                                          if (resp.success) {
                                              $mdDialog.hide();
                                              $mdDialog.show(
                                                $mdDialog.alert()
                                                  .title('Signed up')
                                                  .content('Your account has successfully been created!')
                                                  .ariaLabel('Signup notification')
                                                  .ok('OK')
                                                  .targetEvent(ev)
                                              );
                                          } else {
                                              $scope.error = 'Error: ' + resp.message;
                                          }
                                      });
                                  } else {
                                      $mdDialog.hide();
                                  }
                              }
                          },
                          templateUrl: '/Template/Register',
                          targetEvent: ev,
                      });
                  };

                  this.show_log_in = function (ev) {
                      $mdDialog.show({
                          controller: function ($scope, $mdDialog) {
                              setTimeout(function () { $('input[name=email]').focus(); });
                              $scope.hide = function () {
                                  $mdDialog.hide();
                              };

                              $scope.cancel = function () {
                                  $mdDialog.cancel();
                              };

                              $scope.log_in_guest = function (ev) {
                                  $scope.cancel();
                                  that.log_in_guest(ev);
                              }

                              $scope.answer = function (do_login) {
                                  $scope.logging_in = true;
                                  $scope.error = '';
                                  if (do_login) {
                                      that.log_in($scope.email, $scope.password, $scope.remember, function (resp) {
                                          $scope.logging_in = false;

                                          if (resp.success) {
                                              location.reload();
                                          } else {
                                              $scope.error = 'There was an error logging in. ' + resp.message;
                                          }
                                      });
                                  } else {
                                      $mdDialog.hide();
                                  }
                              }
                          },
                          templateUrl: '/Template/Login',
                          targetEvent: ev,
                      });
                  };

                  $scope.show_profile = function (ev) {
                      $mdDialog.show({
                          controller: function ($scope, $mdDialog) {
                              $scope.theme = $themes.current_theme;

                              $scope.get_user_id = function () { return $rootScope.user_id; }

                              $scope.am_logged_in = function () { return true; }

                              $scope.get_username = function () {
                                  var user = $rootScope.users.get_user($rootScope.user_id);
                                  return (user && user.Username) || "Not set";
                              };

                              $scope.get_name = function () {
                                  var user = $rootScope.users.get_user($rootScope.user_id);
                                  return (user && user.RealName) || "Not set";
                              };

                              $scope.get_about = function () {
                                  var user = $rootScope.users.get_user($rootScope.user_id);
                                  return (user && user.AboutMe) || "Not set";
                              };

                              $scope.edit = function (ev, type) {
                                  $mdDialog.show({
                                      controller: function ($scope, $mdDialog) {
                                          setTimeout(function () { $('input[name=text]').focus(); });
                                          $scope.hide = function () { $mdDialog.hide(); };

                                          $scope.cancel = function () { $mdDialog.cancel(); };

                                          $scope.get_default = function () {
                                              var user = $rootScope.users.get_user($rootScope.user_id);
                                              return user && user[type];
                                          };

                                          $scope.get_input_name = function () {
                                              return { RealName: "Name", AboutMe: "About me", Username: "Username" }[type];
                                          };

                                          $scope.answer = function () {
                                              var obj = {};
                                              obj[type] = $scope.input;
                                              $rootScope.users.update_user_profile($rootScope.user_id, obj);
                                              $mdDialog.hide();
                                          }
                                      },
                                      templateUrl: '/Template/EditProfileInfo',
                                      targetEvent: ev,
                                  });
                              };

                              $scope.get_themes = function () { return $themes.get_themes(); };

                              $scope.get_current_theme = function () { return $themes.current_theme; }

                              $scope.update = function () {
                                  $rootScope.users.update_user_profile($rootScope.user_id, { Settings: JSON.stringify({ theme: this.theme }) });
                                  $themes.set_theme(this.theme);
                              };
                              setTimeout(function () { $('input[name=email]').focus(); });
                              $scope.hide = function () {
                                  $mdDialog.hide();
                              };

                              $scope.cancel = function () {
                                  $mdDialog.cancel();
                              };
                              $scope.upload_profile = function () {
                                  var formdata = new FormData(); //FormData object
                                  var fileInput = document.getElementById('upload_file');
                                  //Iterating through each files selected in fileInput
                                  for (i = 0; i < fileInput.files.length; i++) {
                                      //Appending each file to FormData object
                                      formdata.append(fileInput.files[i].name, fileInput.files[i]);
                                  }
                                  //Creating an XMLHttpRequest and sending
                                  var xhr = new XMLHttpRequest();
                                  xhr.open('POST', '/User/UploadProfilePicture');
                                  xhr.send(formdata);
                                  xhr.onreadystatechange = function () {
                                      if (xhr.readyState == 4 && xhr.status == 200) {
                                          var alert = $mdDialog.alert()
                      .title('Upload image')
                      .content(JSON.parse(xhr.responseText).message)
                      .ok('OK')
                      .clickOutsideToClose(false);
                                          $mdDialog.show(alert).then(function () { });
                                      }
                                  }
                              }
                          },
                          templateUrl: '/Template/UserProfile',
                          targetEvent: ev,
                      });
                  };

                  var defaultSignalHandler = function (_signal) {
                      $rootScope.users.get_user(_signal.user, function (user) {
                          if (!$rooms_controller.room_has_user(_signal.data.RoomId, $rootScope.user_id)) {
                              var confirm = $mdDialog.confirm()
                                  .title('Invite request from ' + user.Username)
                                  .content('Would you like to join Room: \'' + _signal.data.Name + '\'?')
                                  .ariaLabel('Join room')
                                  .ok('Yes')
                                  .cancel('No')
                                  .clickOutsideToClose(false);

                              $mdDialog.show(confirm).then(function () {
                                  $rooms_controller.join_room(_signal.data.InviteKey);
                              }, function () {

                              });
                          }
                      });
                  }

                  var onSignalingConnected = function () {
                      var room_key;
                      $rootScope.logged_in = true;
                      $rooms_controller.update();
                      $rootScope.contact_list_data.update();
                      $interval(function () {
                          $rooms_controller.update();
                          $rootScope.contact_list_data.update();
                      }, 5000);
                      if (location.search.match(/roomKey=/)) {
                          room_key = location.search.split('=')[1];
                          $http.post(
                              '/Room/GetRoomInfoByKey',
                              {
                                  roomKey: room_key
                              }
                          ).then(function (ret) {
                              $rooms_controller.update(
                                  function () {
                                      var join_room = function () {
                                          $http.post(
                                                '/Room/JoinRoomByRoomKey',
                                                {
                                                    roomKey: room_key
                                                }
                                            ).then(function () {
                                                $rooms_controller.update(
                                                    function () {
                                                        $timeout(function () {
                                                            if ($rootScope._guest_user_tmp) location.reload();
                                                            $rootScope.setRoom(ret.data.room.RoomId);
                                                        });
                                                    }
                                                );
                                            });
                                      };
                                      if (!$rooms_controller.room_has_user(ret.data.room.RoomId, $rootScope.user_id)) {
                                          if ($rootScope._guest_user_tmp) {
                                              join_room();
                                          } else {
                                              var confirm = $mdDialog.confirm()
                                                  .title('Join room')
                                                  .content('Would you like to join Room: \'' + ret.data.room.Name + '\'?')
                                                  .ariaLabel('Join room')
                                                  .ok('Yes')
                                                  .cancel('No')
                                                  .clickOutsideToClose(false);

                                              $mdDialog.show(confirm).then(join_room, function () {

                                              });
                                          }
                                      } else {
                                          $rootScope.setRoom(ret.data.room.RoomId);
                                      }
                                  }
                              );
                              
                          });
                      }
                  }

                  this.update = function (first) {
                      $http.post('/User/GetCurrentUser').success(function (resp) {
                          if (first && location.hash.match(/about/)) $scope.showAbout();

                          if (resp.username !== 'Guest') {
                              if (first) {
                                  $interval(this.update.bind(this, false), 5000);

                                  $rootScope.user_id = resp.userId;
                                  this.name = resp.username;

                                  $rootScope.users.get_user(
                                      resp.userId, function (r) {
                                          $rootScope.users.get_user_settings(
                                              $rootScope.user_id,
                                              function (settings) {
                                                  if (settings.theme) $themes.set_theme(settings.theme);
                                              })
                                      })

                                  $rootScope._guest_user_tmp = resp.isGuestUser;

                                  //Now that we are logged in, create a LocalRTC object.
                                  $rootScope.local_rtc = new LocalRTC();
                                  //Create our global list of peerConnections
                                  $rootScope.peerConnections = new PeerRTCConnections($rootScope);

                                  //Now that we are logged in, create a websocket connection.
                                  $rootScope.signal = new SignalConnection($rootScope, onSignalingConnected);
                                  $rootScope.signal.registerRoom("default", defaultSignalHandler);

                                  //if (DEBUG) onSignalingConnected(); // JDW: This is a hack so we can debug locally. In theory only in DEBUG mode...
                              }
                          } else if (!first && !$scope.notified_auth) {
                              $scope.notified_auth = true;
                              var alert = $mdDialog.alert()
                                  .title('Logged out')
                                  .content('Your session has timed out.')
                                  .ok('OK')
                                  .clickOutsideToClose(false);
                              $mdDialog.show(alert).then(function () { location.reload(); });
                          } else if (location.search.match('roomKey=')) {
                              var confirm = $mdDialog.confirm()
                                  .title('Not logged in')
                                  .content('You need to log in before visiting this link')
                                  .ariaLabel('login')
                                  .ok('OK')
                                  .cancel('Cancel')
                                  .clickOutsideToClose(false);

                              $mdDialog.show(confirm).then(function () {
                                  that.show_log_in();
                              }, function () {

                              });
                          }
                      }.bind(this)
                                );
                  }

                  this.update(true);
              }
            ],
            controllerAs: 'auth'
        };
    }
    );
}
)();
