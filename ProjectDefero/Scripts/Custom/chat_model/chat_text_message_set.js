﻿/* ChatTextMessageSet
 *
 * Author(s): Ryan Finn
 */

/* ChatTextMessageSet stores a set of ChatTextMessages and is designed to be a
 * ChatClientMessage payload.  This allows for sending ChatTextMessages in batches
 * instead of just one at a time.  Sending ChatTextMessages in batches makes sense
 * during message history synchronization between clients.
 */
function ChatTextMessageSet() {

    // Stores set of ChatTextMessages.
    var chatTextMessages = {};

    // Stores the ChatTextMessage count.
    var chatTextMessagesCount = 0;

    /* Method adds the supplied ChatTextMessage to the ChatTextMessageSet.
     */
    this.addChatTextMessage = function (chatTextMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessageSet.addChatTextMessage: Supplied ChatTextMessage is corrupt.");
            return;
        }

        if (!(chatTextMessage.uniqueIdentifier in chatTextMessages)) {
            chatTextMessagesCount++;
        }

        chatTextMessages[chatTextMessage.uniqueIdentifier] = chatTextMessage.getCopy();
    };

    /* Method iterates over all the ChatTextMessages and passes them to the supplied
     * callback function.
     *
     * Paramters:
     *   callbackFunction(ChatTextMessage): Callback function that will be passed
     *                                      each ChatTextMessage during iteration.
     */
    this.processAllChatTextMessages = function (callbackFunction) {
        ChatUtility.iterateAssociativeArray(
            chatTextMessages,
            function (key, message) {
                callbackFunction(message.getCopy());
            }, null);
    };

    /* Method returns a count of the ChatTextMessages in the set.
     */
    this.getChatTextMessageCount = function () {
        return chatTextMessagesCount;
    };

    /* Method returns the JSON representation of the ChatTextMessageSet.
     */
    this.stringify = function () {
        var chatTextMessagesJSON = [];

        ChatUtility.iterateAssociativeArray(
            chatTextMessages,
            function (key, message) {
                chatTextMessagesJSON.push(message.stringify());
            }, null);

        return JSON.stringify({ chatTextMessages: chatTextMessagesJSON });
    };

    /* Method returns a deep copy of the ChatTextMessageSet.
     */
    this.getCopy = function () {
        var chatTextMessageSet = new ChatTextMessageSet();

        ChatUtility.iterateAssociativeArray(
            chatTextMessageSet,
            function (key, message) {
                chatTextMessageSet.addChatTextMessage(message);
            }, null);

        return chatTextMessageSet;
    };
}

ChatTextMessageSet.prototype = {

    /* Method returns a ChatTextMessageSet representative of the supplied JSON version
     * of a ChatTextMessageSet.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatTextMessageSet.
     *
     * Returns:
     *   ChatTextMessageSet constructed from values in the supplied JSON string.
     */
    parse: function (chatTextMessageSetAsJSON) {
        var chatTextMessageSet = new ChatTextMessageSet();
        var jsonObj = null;

        try {
            jsonObj = JSON.parse(chatTextMessageSetAsJSON);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessageSet.prototype.parse: Exception thrown calling JSON.parse:"
                + "\n  " + ex.message);
            return chatTextMessageSet;
        }

        var parseValue = null;

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = jsonObj.chatTextMessages)) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatTextMessageSet.prototype.parse: chatTextMessages is null or undefined. Defaulting to value {}.");
            }
            else {
                for (var i = 0; i < parseValue.length; i++) {
                    chatTextMessageSet.addChatTextMessage(ChatTextMessage.prototype.parse(parseValue[i]));
                }
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessageSet.prototype.parse: Exception thrown parsing chatTextMessages:"
                + "\n  " + ex.message);
        }

        return chatTextMessageSet;
    }
}