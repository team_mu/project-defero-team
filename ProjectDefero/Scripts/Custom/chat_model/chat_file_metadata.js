﻿/* ChatFileMetadata
 *
 * Author(s): Ryan Finn
 */

/* ChatFileMetadata is the chat model's representation of a digital computer
 * file's metadata. This class does not interact with the web browser file API.
 */
function ChatFileMetadata() {
    // Key that uniquely identifies the ChatFile instance.
    this._uniqueIdentifier = ChatUtility.getNewChatUniqueIdentifier();

    // Name of the file.
    this._fileName = "";

    // Expected file size in bytes. May be different than actual bytes curretnly in possession by an object.
    this._fileSizeBytes = 0;

    // User Id of the file sender.
    this._senderUserId = "-1";

    // Id of the Room in which the file transfer is taking place.
    this._roomId = "-1";

    // User Id of intended file recipient, assuming there is a recipient.
    this._recipientUserId = "-1";

    // Current file offer state.
    this._offerState = ChatFileMetadata.STATE.OFFER_PENDING;

    // Indicates what data chunk index this ChatFileMetadata instance represents.
    this._chunkSequentialIndex = -1;

    // Indicates the total number of sequential chunks required to reconstruct the file.
    this._totalChunkSequenceCount = 0;

    // Checksum calculated pre transmission.
    this._preTransmissionChecksum = 0;

    // Checksum calculated post transmission.
    this._postTransmissionChecksum = 0;

    // Raw file data in a ArrayBuffer.
    this._fileData = null;

    /* Returns the String representation of the ChatFileSender instance.
     */
    this.toString = function () {
        var resultVal
            = "ChatFileMetadata:"
            + "\n  Unique identifier: " + this.uniqueIdentifier
            + "\n  File name: " + this.fileName
            + "\n  File size bytes: " + this.fileSizeBytes
            + "\n  Sender user id: " + this.senderUserId
            + "\n  Recipient user id: " + this.recipientUserId
            + "\n  Room id: " + this.roomId
            + "\n  Current offer state: " + this.offerState + ": " + this.offerStateMessage
            + "\n  Current chunk index: " + this.chunkSequentialIndex
            + "\n  Total sequential chunks: " + this.totalChunkSequenceCount
            + "\n  Transfer progress: " + (this.progressPercent * 100.0) + "%"
            + "\n  Pre transmission checksum: " + this.preTransmissionChecksum
            + "\n  Post transmission checksum: " + this.postTransmissionChecksum
            + "\n  Checksum match: " + this.validateChecksum();

        return resultVal;
    };

    this.validateChecksum = function () {
        return this.preTransmissionChecksum === this.postTransmissionChecksum;
    };

    /* Method returns a shallow copy of this ChatFileMetadata instance.
     * The resulting copy will not contain file data.
     */
    this.getCopy = function () {
        var copyChatFileMetadata = new ChatFileMetadata();

        copyChatFileMetadata._uniqueIdentifier = this.uniqueIdentifier;
        copyChatFileMetadata.senderUserId = this.senderUserId;
        copyChatFileMetadata.recipientUserId = this.recipientUserId;
        copyChatFileMetadata.roomId = this.roomId;
        copyChatFileMetadata.fileName = this.fileName;
        copyChatFileMetadata.fileSizeBytes = this.fileSizeBytes;
        copyChatFileMetadata.offerState = this.offerState;
        copyChatFileMetadata.chunkSequentialIndex = this.chunkSequentialIndex;
        copyChatFileMetadata.totalChunkSequenceCount = this.totalChunkSequenceCount;
        copyChatFileMetadata.preTransmissionChecksum = this.preTransmissionChecksum;
        copyChatFileMetadata.postTransmissionChecksum = this.postTransmissionChecksum;

        return copyChatFileMetadata;
    };

    /* Method returns the JSON representation of the ChatFileMetadata.
     */
    this.stringify = function () {
        var jsonString = null;

        try {
            jsonString = JSON.stringify(this.toJSON());
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.stringify: Exception thrown calling JSON.stringify: "
                + "\n  " + ex.message);

            jsonString = "";
        }

        return jsonString;
    };

    /* Method returns the pre-serialized JSON object representation of the ChatFileMetadata.
     */
    this.toJSON = function () {
        var jsonReadyChatFileMetadata = {
            uniqueIdentifier: this.uniqueIdentifier,
            fileName: this.fileName,
            fileSizeBytes: this.fileSizeBytes,
            senderUserId: this.senderUserId,
            recipientUserId: this.recipientUserId,
            roomId: this.roomId,
            offerState: this.offerState,
            chunkSequentialIndex: this.chunkSequentialIndex,
            totalChunkSequenceCount: this.totalChunkSequenceCount,
            preTransmissionChecksum: this.preTransmissionChecksum,
            postTransmissionChecksum: this.postTransmissionChecksum,
            fileData: Array.apply(null, new Uint8Array(this.fileData))
        };

        return jsonReadyChatFileMetadata;
    };
}

// Enumeration of file offer states.
ChatFileMetadata.STATE = {
    OFFER_ACCEPTED: 0, // A file offer was accepted.
    OFFER_REJECTED: 1, // A file offer was rejected.
    OFFER_PENDING: 2,  // A file offer response has not been chosen.
    OFFER_EXPIRED: 3,  // A file offer received no response and has expired.
    TRANSMISSION_IN_PROGRESS: 4, // File bytes are being transmitted.
    TRANSMISSION_FAILED: 5,      // File transmission somehow failed.
    TRANSMISSION_SUCCEEDED: 6,   // Full file transmission succeeded.
    TRANSMISSION_CANCELLED: 7,   // File transmission was cancelled an is now abandoned.
    ARCHIVED: 8                  // File transmission is inactive and archived in chat database.
}

ChatFileMetadata.resolveStateMessage = function (state) {
    switch (state) {
        case ChatFileMetadata.STATE.OFFER_ACCEPTED:
            return "File offer accepted, waiting for data transfer to begin."
            break;
        case ChatFileMetadata.STATE.OFFER_REJECTED:
            return "File offer rejected.";
            break;
        case ChatFileMetadata.STATE.OFFER_PENDING:
            return "File offer sent, waiting for recipient's response.";
            break;
        case ChatFileMetadata.STATE.OFFER_EXPIRED:
            return "File offer response was never received, file offer has expired.";
            break;
        case ChatFileMetadata.STATE.TRANSMISSION_FAILED:
            return "File transmission has somehow failed.";
            break;
        case ChatFileMetadata.STATE.TRANSMISSION_SUCCEEDED:
            return "File transmission succeeded!";
            break;
        case ChatFileMetadata.STATE.TRANSMISSION_CANCELLED:
            return "File transmission canceled.";
            break;
        case ChatFileMetadata.STATE.ARCHIVED:
            return "Archived file transmission operation."
            break;
        default:
            return "Unknown file transfer state.";
    }
};

ChatFileMetadata.prototype = {
    // Public getter for private uniqueIdentifier.
    get uniqueIdentifier() {
        if (ChatUtility.isNullOrUndefined(this._uniqueIdentifier)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.uniqueIdentifier is null or undefined. Defaulting to value "
                + ChatTextMessage.DEFAULT.UNIQUE_IDENTIFIER);

            this._uniqueIdentifier = ChatTextMessage.DEFAULT.UNIQUE_IDENTIFIER;
        }

        return this._uniqueIdentifier;
    },

    // Public getter fileName.
    get fileName() {
        if (ChatUtility.isNullOrUndefined(this._fileName)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._fileTargetSizeBytes is corrupt. Defaulting to empty string.");

            this._fileName = "";
        }

        return this._fileName;
    },

    // Public setter for private fileName.
    set fileName(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.fileName is null or undefined. Defaulting to empty string.");

            this._fileName = "";
            return;
        }

        this._fileName = String(value);
    },

    // Public getter for fileSizeBytes.
    get fileSizeBytes() {
        if (ChatUtility.isNullOrUndefined(Number(this._fileSizeBytes))) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._fileSizeBytes is null or undefined. Defaulting to 0.");

            this._fileSizeBytes = 0;
        }

        return Number(this._fileSizeBytes);
    },

    // Public setter for private fileSizeBytes.
    set fileSizeBytes(value) {
        if (ChatUtility.isNullOrUndefined(Number(value))) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.fileSizeBytes is null or undefined. Defaulting to 0.");

            this._fileSizeBytes = 0;
            return;
        }

        this._fileSizeBytes = Number(value);
    },

    // Public getter senderUserId.
    get senderUserId() {
        if (ChatUtility.isNullOrUndefined(this._senderUserId)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._senderUserId is null or undefined. Defaulting to -1.");

            this._senderUserId = "-1";
        }

        return this._senderUserId;
    },

    // Public setter for private recipientUserId.
    set senderUserId(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.senderUserId is null or undefined. Defaulting to -1.");

            this._senderUserId = "-1";
            return;
        }

        this._senderUserId = String(value);
    },

    // Public getter roomId.
    get roomId() {
        if (ChatUtility.isNullOrUndefined(this._roomId)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._roomId is null or undefined. Defaulting to -1.");

            this._roomId = "-1";
        }

        return this._roomId;
    },

    // Public setter for private roomId.
    set roomId(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.roomId is null or undefined. Defaulting to -1.");

            this._roomId = "-1";
            return;
        }

        this._roomId = String(value);
    },

    // Public getter recipientUserId.
    get recipientUserId() {
        if (ChatUtility.isNullOrUndefined(this._recipientUserId)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._recipientUserId is null or undefined. Defaulting to -1.");

            this._recipientUserId = "-1";
        }

        return this._recipientUserId;
    },

    // Public setter for private recipientUserId.
    set recipientUserId(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.recipientUserId is null or undefined. Defaulting to -1.");

            this._recipientUserId = "-1";
            return;
        }

        this._recipientUserId = String(value);
    },

    // Public getter offerState.
    get offerState() {
        if (ChatUtility.isNullOrUndefined(this._offerState)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._offerState is null or undefined. Defaulting to "
                + ChatFileMetadata.STATE.OFFER_PENDING);

            this._offerState = ChatFileMetadata.STATE.OFFER_PENDING;
        }

        return this._offerState;
    },

    // Public setter for private recipientUserId.
    set offerState(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._offerState is null or undefined. Defaulting to "
                + ChatFileMetadata.STATE.OFFER_PENDING);

            this._offerState = ChatFileMetadata.STATE.OFFER_PENDING;
            return;
        }

        this._offerState = Number(value);
    },

    // Public getter chunkSequentialIndex.
    get chunkSequentialIndex() {
        if (ChatUtility.isNullOrUndefined(this._chunkSequentialIndex)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._chunkSequentialIndex is null or undefined. Defaulting to -1.");

            this._chunkSequentialIndex = -1;
        }

        return this._chunkSequentialIndex;
    },

    // Public getter offerStateMessage.
    get offerStateMessage() {
        return ChatFileMetadata.resolveStateMessage(this._offerState);
    },

    // Public getter progressPercent.
    get progressPercent() {
        return (this.chunkSequentialIndex + 1) / Math.max(1.0, this.totalChunkSequenceCount);
    },

    // Public setter for private chunkSequentialIndex.
    set chunkSequentialIndex(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._chunkSequentialIndex is null or undefined. Defaulting to -1.");

            this._chunkSequentialIndex = -1;
            return;
        }

        this._chunkSequentialIndex = Number(value);
    },

    // Public getter totalChunkSequenceCount.
    get totalChunkSequenceCount() {
        if (ChatUtility.isNullOrUndefined(this._totalChunkSequenceCount)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._totalChunkSequenceCount is null or undefined. Defaulting to 0.");

            this._totalChunkSequenceCount = 0;
        }

        return this._totalChunkSequenceCount;
    },

    // Public setter for private totalChunkSequenceCount.
    set totalChunkSequenceCount(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._totalChunkSequenceCount is null or undefined. Defaulting to 0.");

            this._totalChunkSequenceCount = 0;
            return;
        }

        this._totalChunkSequenceCount = Number(value);
    },

    // Public getter preTransmissionChecksum.
    get preTransmissionChecksum() {
        if (ChatUtility.isNullOrUndefined(this._preTransmissionChecksum)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._preTransmissionChecksum is null or undefined. Defaulting to 0.");

            this._preTransmissionChecksum = 0;
        }

        return this._preTransmissionChecksum;
    },

    // Public setter for private preTransmissionChecksum.
    set preTransmissionChecksum(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._preTransmissionChecksum is null or undefined. Defaulting to 0.");

            this._preTransmissionChecksum = 0;
            return;
        }

        this._preTransmissionChecksum = Number(value);
    },

    // Public getter postTransmissionChecksum.
    get postTransmissionChecksum() {
        if (ChatUtility.isNullOrUndefined(this._postTransmissionChecksum)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._postTransmissionChecksum is null or undefined. Defaulting to 0.");

            this._postTransmissionChecksum = 0;
        }

        return this._postTransmissionChecksum;
    },

    // Public setter for private preTransmissionChecksum.
    set postTransmissionChecksum(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._postTransmissionChecksum is null or undefined. Defaulting to 0.");

            this._postTransmissionChecksum = 0;
            return;
        }

        this._postTransmissionChecksum = Number(value);
    },

    // Public getter for fileData.
    get fileData() {
        if (ChatUtility.isNullOrUndefined(this._fileData)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._fileData is null or undefined. Defaulting to ArrayBuffer(0).");

            this._fileData = new ArrayBuffer(0);
        }

        return this._fileData;
    },

    // Public setter for private fileData.
    set fileData(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata._fileData is null or undefined. Defaulting to ArrayBuffer(0).");

            this._fileData = new ArrayBuffer(0);
            return;
        }

        this._fileData = value;
    },

    /* Method returns a ChatFileMetadata representative of the supplied
     * JSON version of a ChatFileMetadata.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatFileMetadata.
     *
     * Returns:
     *   ChatFileMetadata constructed from values in the supplied JSON string.
     */
    parse: function (chatFileMetadataAsJSON) {
        try {
            return ChatFileMetadata.prototype.reconstructor(JSON.parse(chatFileMetadataAsJSON));
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.prototype.parse: Exception thrown calling JSON.parse: "
                + "\n  " + ex.message);
        }

        return new ChatFileMetadata();
    },

    /* Method converts the JSON-deserialized representation of a ChatFileMetadata
     * instance into an actual ChatFileMetadata instance.
     *
     * Parameters:
     *   jsonParseResult: JSON-deserialized representation of a ChatFileMetadata instance.
     *
     * Returns:
     *   New ChatFileMetadata instance built from the values in the supplied object.
     */
    reconstructor: function (jsonParseResult) {
        var chatFileMetadata = new ChatFileMetadata();

        try {
            chatFileMetadata._uniqueIdentifier = jsonParseResult.uniqueIdentifier;
            chatFileMetadata.fileName = jsonParseResult.fileName;
            chatFileMetadata.fileSizeBytes = jsonParseResult.fileSizeBytes;
            chatFileMetadata.senderUserId = jsonParseResult.senderUserId;
            chatFileMetadata.recipientUserId = jsonParseResult.recipientUserId;
            chatFileMetadata.roomId = jsonParseResult.roomId;
            chatFileMetadata.offerState = jsonParseResult.offerState;
            chatFileMetadata.chunkSequentialIndex = jsonParseResult.chunkSequentialIndex;
            chatFileMetadata.totalChunkSequenceCount = jsonParseResult.totalChunkSequenceCount;
            chatFileMetadata.preTransmissionChecksum = jsonParseResult.preTransmissionChecksum;
            chatFileMetadata.postTransmissionChecksum = jsonParseResult.postTransmissionChecksum;
            chatFileMetadata.fileData = new Uint8Array(jsonParseResult.fileData).buffer;
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileMetadata.prototype.reconstructor: Exception thrown: "
                + "\n  " + ex.message);
        }

        return chatFileMetadata;
    },
}