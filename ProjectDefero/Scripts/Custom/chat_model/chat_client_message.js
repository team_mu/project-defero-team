﻿/* ChatClientMessage
 *
 * Author(s): Ryan Finn
 */

/* ChatClientMessage is a general purpose message that contains a payload. The payload
 * could be a ChatTextMessage, for example. Sending ChatClientMessages with different
 * payload types simplifies the message recipient's job of determing what type message
 * they have received.
 *
 * Parameters:
 *   roomIdParam: Room id of the Room in which this chat originated and is destined for.
 *
 *   messagePayloadTypeParam:  Enumeration that indicats what type object the messagePayload
 *                             parameter is.  See PAYLOAD_TYPE.
 *
 *   messagePayloadParam:  Object that can be identified using parameter messagePayloadType,
 *                         and that implements the following functions:
 *
 *                         // Returns JSON representation of itself.
 *                         String instance.stringify()
 *
 *                         // Returns a deep copy of itself.
 *                         <messageObject> instance.getCopy()
 *
 *                         ...and for which a function exists like such:
 *
 *                         // Method takes a JSON representation of the message object and 
 *                         // returns the represented message object.
 *                         <messageObject> globalScope.<functionName>(<JSON representation>)
 */
function ChatClientMessage(roomIdParam, messagePayloadTypeParam, messagePayloadParam) {
    // FNV1a hash of the payload after being stringified. We are using if for checksumming.
    this._stringifyPayload_FNV1a_32bit_hash = 0;

    // FNV1a hash of the payload after being parsed. We are using if for checksumming.
    this._parsePayload_FNV1a_32bit_hash = 0;

    // Room id of the Room in which this chat originated and is destined for.
    this._roomId = "-1";

    // Indicates the type of payload the ChatClientMessage carries.
    this._messagePayloadType = ChatClientMessage.PAYLOAD_TYPE.UNKNOWN;

    // ChatClientMessage payload object.
    this._messagePayload = null;

    if (ChatUtility.isNullOrUndefined(roomIdParam)) {
        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "Null or undefined room id supplied to ChatClientMessage: " + roomIdParam);
    }
    else {
        this._roomId = String(roomIdParam);
    }

    if (ChatUtility.isNullOrUndefined(messagePayloadTypeParam)) {
        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "Null or undefined messagePayloadTypeParam supplied to ChatClientMessage: " + messagePayloadTypeParam);
    }
    else {
        this._messagePayloadType = messagePayloadTypeParam;
    }

    if (ChatUtility.isNullOrUndefined(messagePayloadParam)) {
        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "Null or undefined messagePayloadParam supplied to ChatClientMessage: " + messagePayloadParam);
    }
    else {
        this._messagePayload = messagePayloadParam;
    }


    /* Method returns the JSON representation of the ChatClientMessage.
     */
    this.stringify = function () {
        var payloadJSON = "";

        if (!ChatUtility.isNullOrUndefined(this.messagePayload)) {
            if (ChatUtility.isFunction(this.messagePayload.stringify)) {
                payloadJSON = this.messagePayload.stringify();
            }
            else {
                payloadJSON = String(this.messagePayload);
            }
        }

        this._stringifyPayload_FNV1a_32bit_hash = ChatUtility.FNV1a_32bit_hash(payloadJSON);

        var jsonRepresentation = {
            roomId: this.roomId,
            stringifyPayload_FNV1a_32bit_hash: this._stringifyPayload_FNV1a_32bit_hash,
            messagePayloadType: this.messagePayloadType,
            messagePayload: payloadJSON
        };

        return JSON.stringify(jsonRepresentation);
    };

    /* Returns a new ChatClientMessage instance that is a deep copy of
     * this ChatClientMessage instance.
     */
    this.getCopy = function () {
        var payloadCopy = null;

        if (ChatUtility.isFunction(this.messagePayload.getCopy)) {
            payloadCopy = this.messagePayload.getCopy();
        }
        else {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatClientMessage.getCopy: messagePayload.getCopy is null or undefined.");
            payloadCopy = null;
        }

        var resultChatClientMessage = new ChatClientMessage(this.roomId, this.messagePayloadType, payloadCopy);

        resultChatClientMessage.stringifyPayload_FNV1a_32bit_hash = this._stringifyPayload_FNV1a_32bit_hash;
        resultChatClientMessage.parsePayload_FNV1a_32bit_hash = this._parsePayload_FNV1a_32bit_hash;

        return resultChatClientMessage;
    };

    /* Method returns Boolean representing whether or not the FNV1a 32-bit hash generated
     * from the stringified payload and the parsed payload are a match.
     */
    this.validChecksum = function () {
        return !ChatUtility.isNullOrUndefined(this._stringifyPayload_FNV1a_32bit_hash)
            && !ChatUtility.isNullOrUndefined(this._parsePayload_FNV1a_32bit_hash)
            && (this._stringifyPayload_FNV1a_32bit_hash === this._parsePayload_FNV1a_32bit_hash);
    };
}

/* Enumeration of payload types recognized by ChatClientMessage.
 */
ChatClientMessage.PAYLOAD_TYPE = {
    UNKNOWN: 0,                       // Payload type is unknown and somebody messed up.
    CHAT_TEXT_MESSAGE: 1,             // Payload type is ChatTextMessage.
    CHAT_HISTORY_REQUEST: 2,          // Payload type is ChatMessageHistoryRequest.
    CHAT_HISTORY_TEXT_MESSAGE: 3  ,   // Payload type is ChatTextMessage as a response to a ChatMessageHistoryRequest.
    CHAT_HISTORY_TEXT_MESSAGE_SET: 4, // Payload type is ChatTextMessageSet as a response to a ChatMessageHistoryRequest.

    /* Payload type is a ChatTextMessage.uniqueIdentifier. This is an acknowledgment
     * that your sent ChatTextMessage was received cleanly.
     */
    CHAT_TEXT_MESSAGE_RECEIVED_ACK: 5,

    /* Payload type is a ChatTextMessageAcknowledgmentSet. This is a set of acknowledgments
     * that your sent ChatTextMessages were received cleanly.
     */
    CHAT_TEXT_MESSAGE_RECEIVED_ACK_SET: 6,

    /* Chat client is offering to send a file to the recipient. Will the recipient accept?
     */
    CHAT_FILE_OFFER: 7,

    /* Chat client is responding to an offer from another client to send a file.
     */
    CHAT_FILE_OFFER_RESPONSE: 8,

    /* Payload is a chunk of file data.
     */
    CHAT_FILE_DATA_CHUNK: 9,

    /* Payload is an ACK, positive or negative, concerning a file chunk.
     */
    CHAT_FILE_DATA_CHUNK_ACK: 10,

    /* Chat client file receiver is canceling a file transfer.
     */
    CHAT_FILE_RECEIVER_CANCEL_TRANSFER: 11,

    /* Chat client file sender is canceling a file transfer.
     */
    CHAT_FILE_SENDER_CANCEL_TRANSFER: 12,
};

ChatClientMessage.prototype = {

    // Public getter for private messagePayloadType.
    get messagePayloadType() {
        if (ChatUtility.isNullOrUndefined(this._messagePayloadType)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatClientMessage.stringify: messagePayloadType is null or undefined."
                + " Defaulting to value PAYLOAD_TYPE.UNKNOWN.");

            this._messagePayloadType = ChatClientMessage.PAYLOAD_TYPE.UNKNOWN;
        }

        return this._messagePayloadType;
    },

    // Public getter for private messagePayload.
    get messagePayload() {
        if (ChatUtility.isNullOrUndefined(this._messagePayload)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatClientMessage.stringify: messagePayload is null or undefined."
                + " Defaulting to value null.");

            this._messagePayload = null;
        }

        return this._messagePayload;
    },

    // Public getter for private roomId.
    get roomId() {
        if (ChatUtility.isNullOrUndefined(this._roomId)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatClientMessage.stringify: _roomId is null or undefined."
                + " Defaulting to string -1.");

            this._roomId = "-1";
        }

        return this._roomId;
    },

    /* Method returns a ChatClientMessage representative of the supplied JSON version
     * of a ChatClientMessage.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatClientMessage.
     *
     * Returns:
     *   ChatClientMessage constructed from values in the supplied JSON string.
     */
    parse: function (chatClientMessageAsJSON) {
        var chatClientMessage = new ChatClientMessage("-1", ChatClientMessage.PAYLOAD_TYPE.UNKNOWN, { dummy: "placeholder"});
        var jsonObj = null;

        try {
            jsonObj = JSON.parse(chatClientMessageAsJSON);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatClientMessage.prototype.parse: Exception thrown calling JSON.parse: "
                + "\n  " + ex.message);
            return chatClientMessage;
        }

        var parseValue = null;

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = Number(jsonObj.roomId))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatClientMessage.prototype.parse: roomId is null or undefined.");
                return chatClientMessage;
            }
            else {
                chatClientMessage._roomId = String(parseValue);
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatClientMessage.prototype.parse: Exception thrown: "
                + "\n  " + ex.message);
        }

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = Number(jsonObj.stringifyPayload_FNV1a_32bit_hash))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatClientMessage.prototype.parse: stringifyPayload_FNV1a_32bit_hash is null or undefined.");
                return chatClientMessage;
            }
            else {
                chatClientMessage._stringifyPayload_FNV1a_32bit_hash = parseValue;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatClientMessage.prototype.parse: Exception thrown: "
                + "\n  " + ex.message);
        }

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = Number(jsonObj.messagePayloadType))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatClientMessage.prototype.parse: _messagePayloadType is null or undefined."
                    + " Defaulting to value PAYLOAD_TYPE.UNKNOWN. and _messagePayload = null.");
                return chatClientMessage;
            }
            else {
                chatClientMessage._messagePayloadType = parseValue;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatClientMessage.prototype.parse: Exception thrown: "
                + "\n  " + ex.message);
        }

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = String(jsonObj.messagePayload))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatClientMessage.prototype.parse: messagePayload is null or undefined. Defaulting to value null.");
            }
            else {
                var payload = null;
                chatClientMessage._parsePayload_FNV1a_32bit_hash = ChatUtility.FNV1a_32bit_hash(parseValue);

                switch (chatClientMessage.messagePayloadType) {
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE:
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE_RECEIVED_ACK:
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_TEXT_MESSAGE:
                        payload = ChatTextMessage.prototype.parse(parseValue);
                        break;
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_REQUEST:
                        payload = ChatMessageHistoryRequest.prototype.parse(parseValue);
                        break;
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_TEXT_MESSAGE_SET:
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE_RECEIVED_ACK_SET:
                        payload = ChatTextMessageSet.prototype.parse(parseValue);
                        break;
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_OFFER:
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_OFFER_RESPONSE:
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_DATA_CHUNK:
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_DATA_CHUNK_ACK:
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_RECEIVER_CANCEL_TRANSFER:
                    case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_SENDER_CANCEL_TRANSFER:
                        payload = ChatFileMetadata.prototype.parse(parseValue);
                        payload.preTransmissionChecksum = chatClientMessage._stringifyPayload_FNV1a_32bit_hash;
                        payload.postTransmissionChecksum = chatClientMessage._parsePayload_FNV1a_32bit_hash;
                        break;
                    case ChatClientMessage.PAYLOAD_TYPE.UNKNOWN:
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatClientMessage.prototype.parse: _messagePayloadType is PAYLOAD_TYPE.UNKNOWN.");
                        break;
                    default:
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatClientMessage.prototype.parse: _messagePayloadType is not recognized."
                            + " Changing its value to PAYLOAD_TYPE.UNKNOWN. and messagePayload = null.");
                        chatClientMessage._messagePayloadType = ChatClientMessage.PAYLOAD_TYPE.UNKNOWN;
                        payload = null;
                        break;
                }

                chatClientMessage._messagePayload = payload;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatClientMessage.prototype.parse: Exception thrown: "
                + "\n  " + ex.message);
        }

        return chatClientMessage;
    }
}