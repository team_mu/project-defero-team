﻿/* ChatFileSender
 *
 * Author(s): Ryan Finn
 */

/* ChatFileSender manages the nogotiations and actions of sending a file in
 * a sequential sequence of data chunks.
 *
 * Paramters:
 *   inputFile: A window.File object with a handle on the file to send.
 *   senderUserId: User Id of the user who is sending the file.
 *   roomId: Id of room in which the file sending is taking place.
 *   recipientUserId: User Id of the user who is receiving the file.
 */
function ChatFileSender(inputFile, senderUserId, roomId, recipientUserId) {
    var self = this;

    // window.File object instance.
    this._fileHandle = inputFile;

    /* ChatFileMetadata will be used tracking metadata and for communicating
     * metadata to event listeners.
     */
    var chatFileMetadata = new ChatFileMetadata();
    chatFileMetadata.senderUserId = String(senderUserId);
    chatFileMetadata.roomId = String(roomId);
    chatFileMetadata.recipientUserId = String(recipientUserId);

    /* PeerRTC instance used by the ChatFileSender to send data.
     */
    this._peerRTCInstance = null;

    /* Method initializes the window.File-related components ChatMetadata object.
     */
    var initializeMetadata = function () {
        if (ChatUtility.isFileObjectCorrupt(this._fileHandle)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender._fileHandle is corrupt.");
            return;
        }

        chatFileMetadata.chunkSequentialIndex = 0;
        chatFileMetadata.fileName = this._fileHandle.name;
        chatFileMetadata.fileSizeBytes = this._fileHandle.size;

        if (chatFileMetadata.fileSizeBytes === 0) {
            chatFileMetadata.totalChunkSequenceCount = 0;
        }
        else {
            chatFileMetadata.totalChunkSequenceCount = Math.ceil(chatFileMetadata.fileSizeBytes / ChatFileSender.MAX_DATA_CHUNK_SIZE_BYTES);
        }
    }.bind(this);

    /* Queue of callback functions called when the ChatFileSender removes itself
     * from the ChatClient's upload queue.
     */
    var selfCleanupNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when the ChatFileSender has sent a file
     * download invitation to the recipient and is now waiting for a reply.
     */
    var waitingForFileOfferResponseNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file offer recipient has responded
     * with a file offer rejection.
     */
    var fileOfferRejectedNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file offer recipient has accepted the
     * file offer.
     */
    var fileOfferAcceptedNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file offer response wait period
     * has expired with no reponse from the recipient.
     */
    var fileOfferNoResponseNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file data chunk transmission fails.
     */
    var fileTransmissionFailureNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file data chunk transmission succeeds.
     */
    var fileTransmissionProgressNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a full file transmission completes.
     */
    var fileTransmissionCompleteNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file transfer is cancelled by the receiver.
     */
    var fileRetrievalCancellationNotifyQueue = new ChatCallbackFunctionList();

    /* This is the timing mechanism we will be using for file offer timeouts and retries.
     */
    var protocolTimer_FileOffer = new ChatTimer();
    protocolTimer_FileOffer.setTimeoutMsec(1000 * 5 * 60); // 1000 * 5 * 60 = 5 minutes

    /* This is the timing mechanism we will be using for data chunk transmission timeouts and retries.
     */
    var protocolTimer_dataChunkACK = new ChatTimer();

    /* Returns the String representation of the ChatFileSender instance.
     */
    this.toString = function () {
        var resultVal
            = "ChatFileSender:"
            + "\n  File API browser support: " + ChatUtility.hasAllFileAPISupport()
            + "\n  File object is corrupt: " + ChatUtility.isFileObjectCorrupt(this._fileHandle)
            + "\n " + chatFileMetadata.toString();

        return resultVal;
    };

    /* Returns the uniqueId that identifies this file sending procedure.
     */
    this.getUniqueId = function () {
        return chatFileMetadata.uniqueIdentifier;
    };

    /* Returns a copy of this ChatFileSender's ChatFileMetadata.
     */
    this.getMetadata = function () {
        return chatFileMetadata.getCopy();
    };

    /* Method sends a file offer to another client.
     *
     * Paramters:
     *   peerRTCInstance: PeerRTC instance used for the actual data transfer.
     */
    this.sendFileOffer = function () {
        chatFileMetadata.offerState = ChatFileMetadata.STATE.OFFER_PENDING;

        var sendAction = function () {
            if (!ChatUtility.isNullOrUndefined(self.peerRTCInstance)) {
                // Send the file offer.
                sendChatClientMessage(
                    new ChatClientMessage(chatFileMetadata.roomId, ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_OFFER, chatFileMetadata.stringify()));
            }
            else {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatFileSender.sendFileOffer: peerRTCInstance is null or undefined.");
            }

            // Start the response timeout.
            protocolTimer_FileOffer.startNewTimeoutAlarm();
        };

        protocolTimer_FileOffer.addTimeoutCallback(
            function (msec) {
                ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatFileSender.sendFileOffer: Timeout period elapsed when waiting for file offer response."
                    + "\n Attempt " + protocolTimer_FileOffer.totalTimeoutAlarms
                    + " of " + ChatFileSender.FILE_OFFER_MAX_RETRIES
                    + "\n " + chatFileMetadata.toString());

                if (protocolTimer_FileOffer.totalTimeoutAlarms < ChatFileSender.FILE_OFFER_MAX_RETRIES) {
                    sendAction();
                }
                else {
                    // No offer response received, permenantly give up.
                    chatFileMetadata.offerState = ChatFileMetadata.STATE.OFFER_EXPIRED;
                    fileOfferNoResponseNotifyQueue.processAll(self.getMetadata());
                    var canceledChatFileMetaData = chatFileMetadata.getCopy();
                    canceledChatFileMetaData.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
                    sendChatClientMessage(
                        new ChatClientMessage(canceledChatFileMetaData.roomId, ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_SENDER_CANCEL_TRANSFER, canceledChatFileMetaData.stringify()));
                    selfCleanupNotifyQueue.processAll(self.getMetadata());
                }
            });

        sendAction();

        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "ChatFileSender.sendFileOffer: Waiting for recipient " + chatFileMetadata.recipientUserId
            + " to accept offer for file [" + chatFileMetadata.fileName + "].");

        waitingForFileOfferResponseNotifyQueue.processAll(this.getMetadata());
    };

    /* Method processes a file offer response that had better be a response to a file offer made
     * by this ChatFileSender instance.
     *
     * Paramters:
     *   chatFileMetadata: ChatFileMetadata object from the file offer responder.
     */
    this.processFileOfferResponse = function (incomingChatFileMetadata) {
        if (ChatUtility.isNullOrUndefined(incomingChatFileMetadata)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.processFileOfferResponse: incomingChatFileMetadata is null or undefined.");
            return;
        }

        if (incomingChatFileMetadata.uniqueIdentifier !== this.getUniqueId()) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.processFileOfferResponse: incomingChatFileMetadata.uniqueIdentifier is incorrect."
                + "\nExpected: " + this.getUniqueId()
                + "\nFound: " + incomingChatFileMetadata.uniqueIdentifier);
            return;
        }

        if (chatFileMetadata.offerState !== ChatFileMetadata.STATE.OFFER_PENDING) {
            // We have already received a file offer response, so we don't care about this one.
            return;
        }

        protocolTimer_FileOffer.stopTimeoutSilent();

        if (incomingChatFileMetadata.offerState === ChatFileMetadata.STATE.OFFER_ACCEPTED) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.processFileOfferResponse: File offer was accepted:"
                + "\n " + incomingChatFileMetadata.toString());

            chatFileMetadata.offerState = ChatFileMetadata.STATE.OFFER_ACCEPTED;
            fileOfferAcceptedNotifyQueue.processAll(chatFileMetadata.getCopy());
            sendDataChunk();
        }
        else if (incomingChatFileMetadata.offerState === ChatFileMetadata.STATE.OFFER_REJECTED) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.processFileOfferResponse: File offer was rejected:"
                + "\n " + incomingChatFileMetadata.toString());

            chatFileMetadata.offerState = ChatFileMetadata.STATE.OFFER_REJECTED;
            fileOfferRejectedNotifyQueue.processAll(chatFileMetadata.getCopy());
            selfCleanupNotifyQueue.processAll(chatFileMetadata.getCopy());
        }
        else {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.processFileOfferResponse: Received file offer response that neither"
                + " accepts nor rejects the offer: "
                + "\n " + incomingChatFileMetadata.toString());
            chatFileMetadata.offerState = ChatFileMetadata.STATE.OFFER_REJECTED;
            fileOfferRejectedNotifyQueue.processAll(chatFileMetadata.getCopy());
            selfCleanupNotifyQueue.processAll(chatFileMetadata.getCopy());
        }
    };

    /* Sends the next single chunk of data to another client.
     */
    var sendDataChunk = function () {
        protocolTimer_dataChunkACK.setTimeoutMsec(ChatFileSender.DATA_CHUNK_ACK_TIMEOUT);
        protocolTimer_dataChunkACK.resetTotalTimeoutAlarms();
        protocolTimer_dataChunkACK.clearTimeoutCallbackQueue();
        chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_IN_PROGRESS;
        var chunk = chatFileMetadata.getCopy();

        var sendAction = function () {
            if (!ChatUtility.isNullOrUndefined(self.peerRTCInstance)) {
                // Send the data chunk.
                sendChatClientMessage(
                    new ChatClientMessage(chunk.roomId, ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_DATA_CHUNK, chunk.stringify()));
            }
            else {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatFileSender.sendDataChunk: peerRTCInstance is null or undefined.");
            }

            // Start the response timeout.
            protocolTimer_dataChunkACK.startNewTimeoutAlarm();
        };

        protocolTimer_dataChunkACK.addTimeoutCallback(
            function (msec) {
                if (protocolTimer_dataChunkACK.totalTimeoutAlarms < ChatFileSender.MAX_DATA_CHUNK_RETRIES) {
                    // Double the timeout before resending.
                    protocolTimer_dataChunkACK.setTimeoutMsec(protocolTimer_dataChunkACK.getTimeoutMsec() * 2);
                    sendAction();
                }
                else {
                    // Permenantly give up.
                    chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_FAILED;
                    chunk.offerState = ChatFileMetadata.STATE.TRANSMISSION_FAILED;
                    fileTransmissionFailureNotifyQueue.processAll(chunk.getCopy());
                    selfCleanupNotifyQueue.processAll(chunk.getCopy());
                }
            });

        try {
            // Add data to the chunk!
            var uint8;
            var fileReader = new FileReader();

            fileReader.onload = function () {
                chunk.fileData = this.result;
                sendAction();
            };
            fileReader.readAsArrayBuffer(
                this._fileHandle.slice(
                  chunk.chunkSequentialIndex * ChatFileSender.MAX_DATA_CHUNK_SIZE_BYTES
                , (chunk.chunkSequentialIndex + 1) * ChatFileSender.MAX_DATA_CHUNK_SIZE_BYTES));
        }
        catch (ex) { /* Wait for timeout. */ }
    }.bind(this);

    /* Method processes a data chunk ACK from the data chunk recipient. If the ACK's checksum is
     * valid and the sequential index is the expected value, then we send the next chunk.
     */
    this.processDataChunkACK = function (incomingChatFileMetadata) {
        if (incomingChatFileMetadata.validateChecksum()
            && chatFileMetadata.chunkSequentialIndex === incomingChatFileMetadata.chunkSequentialIndex) {
            protocolTimer_dataChunkACK.stopTimeoutSilent();
            chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_IN_PROGRESS;
            incomingChatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_IN_PROGRESS;

            setTimeout(function () {
                fileTransmissionProgressNotifyQueue.processAll(incomingChatFileMetadata.getCopy());
            }, 0);

            if (Number(chatFileMetadata.chunkSequentialIndex) >= Math.max(chatFileMetadata.totalChunkSequenceCount - 1, 0)) {
                // All chunks have been sent.
                chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_SUCCEEDED;
                incomingChatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_SUCCEEDED;
                fileTransmissionCompleteNotifyQueue.processAll(incomingChatFileMetadata.getCopy());
                selfCleanupNotifyQueue.processAll(incomingChatFileMetadata.getCopy());
                return;
            }

            chatFileMetadata.chunkSequentialIndex = chatFileMetadata.chunkSequentialIndex + 1;
            sendDataChunk();
        }
    };

    /* Method sends a cancellation message to the file receiver and performs
     * self cleanup.
     */
    this.cancelFileTransmission = function (chatFileMetadataParam) {
        protocolTimer_FileOffer.stopTimeoutSilent();
        protocolTimer_dataChunkACK.stopTimeoutSilent();
        chatFileMetadataParam.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
        chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
        fileRetrievalCancellationNotifyQueue.processAll(chatFileMetadataParam.getCopy());
        sendChatClientMessage(
            new ChatClientMessage(chatFileMetadataParam.roomId, ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_SENDER_CANCEL_TRANSFER, chatFileMetadataParam.stringify()));
        selfCleanupNotifyQueue.processAll(chatFileMetadataParam.getCopy());
    };

    /* Processes the file receiver's file transmission cancellation.
     */
    this.processReceiverCancel = function (chatFileMetadataParam) {
        protocolTimer_FileOffer.stopTimeoutSilent();
        protocolTimer_dataChunkACK.stopTimeoutSilent();
        chatFileMetadataParam.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
        chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
        fileRetrievalCancellationNotifyQueue.processAll(chatFileMetadataParam.getCopy());
        selfCleanupNotifyQueue.processAll(chatFileMetadataParam.getCopy());
    };

    /* Sends a ChatClientMessage to a single endpoint.
     *
     * Parameters:
     *   chatClientMessage: A ChatClientMessage object that is to be sent through the
     *                      peerRTCInstance.
     * Returns:
     *  True if ChatClientMessage was sent successfully.
     *  False, otherwise.
     */
    var sendChatClientMessage = function (chatClientMessage) {
        if (ChatUtility.isNullOrUndefined(chatClientMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.sendChatClientMessage: chatClientMessage was null or undefined.");
            return;
        }

        if (ChatUtility.isNullOrUndefined(this.peerRTCInstance)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.sendChatClientMessage: PeerRTC instance was null or undefined.");
            return;
        }

        try {
            this.peerRTCInstance.sendMessage(chatClientMessage);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.sendChatClientMessage: Exception was thrown during message send:"
                + "\n  " + ex.message);
        }
    }.bind(this);

    /* Adds a callback function that will be called when the ChatFileSender needs
     * to remove itself from the ChatModel's upload queue.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addSelfCleanupCallback = function (callbackFunction) {
        selfCleanupNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when the ChatFileSender has sent
     * a file download invitation to the recipient and is now waiting for a reply.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addWaitingForFileOfferResponseCallback = function (callbackFunction) {
        waitingForFileOfferResponseNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when the ChatFileSender receives
     * a file offer rejection.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileOfferRejectedCallback = function (callbackFunction) {
        fileOfferRejectedNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when the ChatFileSender receives
     * an acceptance for a file offer.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileOfferAcceptedCallback = function (callbackFunction) {
        fileOfferAcceptedNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file offer response waits
     * period has expired with no reponse from the recipient.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileOfferNoResponseCallback = function (callbackFunction) {
        fileOfferNoResponseNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file transmission fails.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileTransmissionFailureCallback = function (callbackFunction) {
        fileTransmissionFailureNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file transmission fails.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileTransmissionProgressCallback = function (callbackFunction) {
        fileTransmissionProgressNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a full file transmission completes.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileTransmissionCompleteCallback = function (callbackFunction) {
        fileTransmissionCompleteNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file transfer is cancelled by the receiver.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileRetrievalCancellationCallback = function (callbackFunction) {
        fileRetrievalCancellationNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    initializeMetadata();
}

// Maximum number of file offer attempts after failures or timeouts ChatFileSender will make before giving up.
ChatFileSender.FILE_OFFER_MAX_RETRIES = 3;

// Maximum size in bytes of a single data chunk to send to PeerRTC for transmission.
ChatFileSender.MAX_DATA_CHUNK_SIZE_BYTES = 1024 * 64; // 1024 * 64 = 64 kilobyte chunks.

// Maximum number of times we will re-send the same data chunk.
ChatFileSender.MAX_DATA_CHUNK_RETRIES = 4;

/* Initial span of time to wait for a data chunk ACK.
 * (Chunk size / 3600) * 1000 = best case-ish 28.8K modem conditions.
 */
ChatFileSender.DATA_CHUNK_ACK_TIMEOUT = Math.floor((ChatFileSender.MAX_DATA_CHUNK_SIZE_BYTES / 3600) * 1000);

ChatFileSender.prototype = {
    // Public getter peerRTCInstance.
    get peerRTCInstance() {
        if (ChatUtility.isNullOrUndefined(this._peerRTCInstance)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender._peerRTCInstance is corrupt. Defaulting to null.");

            return null;
        }

        return this._peerRTCInstance;
    },

    // Public setter for private peerRTCInstance.
    set peerRTCInstance(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileSender.fileName is null or undefined. Defaulting to null.");

            this._peerRTCInstance = null;
            return;
        }

        this._peerRTCInstance = value;
    },
}