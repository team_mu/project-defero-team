﻿/* ChatFileReceiver
 *
 * Author(s): Ryan Finn
 */

/* ChatFileReceiver manages the nogotiations and actions of receiving a file in
 * a sequential sequence of data chunks.
 */
function ChatFileReceiver(senderChatFileMetadata) {
    var self = this;

    /* ChatFileMetadata will be used tracking metadata and for communicating
     * metadata to event listeners.
     */
    var chatFileMetadata = senderChatFileMetadata;
    chatFileMetadata.chunkSequentialIndex = 0;

    /* PeerRTC instance used by the ChatFileReceiver to send data.
     */
    this._peerRTCInstance = null;

    /* Uint8Array that stores the incoming data.
     */
    this._uint8Array = null;

    /* Tracks how many file bytes have been received.
     */
    this._bytesReceivedCount = 0;

    /* Queue of callback functions called when the ChatFileReceiver removes itself
     * from the ChatClient's download queue.
     */
    var selfCleanupNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a ChatFileSender has received a file
     * download offer.
     */
    var fileOfferReceivedNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file data chunk retrieval fails.
     */
    var fileRetrievalFailureNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a full file retrieval is complete.
     */
    var fileRetrievalCompleteNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file data chunk retrieval succeeds.
     */
    var fileRetrievalProgressNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file transfer is cancelled by the sender.
     */
    var fileTransmissionCancellationNotifyQueue = new ChatCallbackFunctionList();

    /* This is the timing mechanism we will be using for data chunk transmission timeouts.
     */
    var protocolTimer_dataChunk = new ChatTimer();
    protocolTimer_dataChunk.setTimeoutMsec(ChatFileReceiver.DATA_CHUNK_TIMEOUT);

    /* Returns a copy of this ChatFileReceiver's ChatFileMetadata.
     */
    this.getMetadata = function () {
        return chatFileMetadata.getCopy();
    };

    /* Returns the String representation of the ChatFileReceiver instance.
     */
    this.toString = function () {
        var resultVal
            = "ChatFileReceiver:"
            + "\n " + chatFileMetadata.toString();

        return resultVal;
    };

    /* Returns the uniqueId that identifies this file sending procedure.
     */
    this.getUniqueId = function () {
        return chatFileMetadata.uniqueIdentifier;
    };

    /* Method sends a ChatFileMetadata as a file offer response. The recipient to
     * the response is the user who sent the file offer.
     *
     * Paramters:
     *   chatFileMetadata: ChatFileMetadata that contains the file offer reponse
     *                     and that defines who made the file offer.
     */
    this.sendFileOfferResponse = function (chatFileMetadataParam) {
        chatFileMetadata.offerState = chatFileMetadataParam.offerState;
        sendChatClientMessage(
            new ChatClientMessage(chatFileMetadata.roomId, ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_OFFER_RESPONSE, chatFileMetadata.stringify()));

        if (chatFileMetadataParam.offerState !== ChatFileMetadata.STATE.OFFER_ACCEPTED) {
            fileTransmissionCancellationNotifyQueue.processAll(chatFileMetadata.getCopy());
            selfCleanupNotifyQueue.processAll(chatFileMetadata.getCopy());
            return;
        }

        protocolTimer_dataChunk.resetTotalTimeoutAlarms();
        protocolTimer_dataChunk.clearTimeoutCallbackQueue();

        protocolTimer_dataChunk.addTimeoutCallback(
            function (msec) {
                // Give up on waiting for data.
                chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_FAILED;
                fileRetrievalFailureNotifyQueue.processAll(chatFileMetadata.getCopy());
                selfCleanupNotifyQueue.processAll(chatFileMetadata.getCopy());
            });
        protocolTimer_dataChunk.startNewTimeoutAlarm();
    };

    /* Method processes an incoming file offer.
     *
     * Paramters:
     *   chatFileMetadata: ChatFileMetadata that contains the file offer request
     *                     and that defines who made the file offer.
     */
    this.processFileOffer = function (incomingChatFileMetadata) {
        if (ChatUtility.isNullOrUndefined(incomingChatFileMetadata)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver.processFileOffer: incomingChatFileMetadata is null or undefined.");
            return;
        }

        if (incomingChatFileMetadata.uniqueIdentifier !== this.getUniqueId()) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver.processFileOffer: incomingChatFileMetadata.uniqueIdentifier is incorrect."
                + "\nExpected: " + this.getUniqueId()
                + "\nFound: " + incomingChatFileMetadata.uniqueIdentifier);
            return;
        }

        chatFileMetadata.offerState = ChatFileMetadata.STATE.OFFER_PENDING;
        fileOfferReceivedNotifyQueue.processAll(incomingChatFileMetadata.getCopy());
    };

    /* Method processes an incomming data chunk. If the incomming chunk's checksum is
     * valid and the chunk has the expected sequential index, then we repsond with a
     * positive ACK, otherwise we ignore it and wait for another chunk or our timeout
     * period to expire.
     *
     * Parameters:
     *   incomingChatFileMetadata: Incomming file data chunk and associated metadata.
     */
    this.processDataChunk = function (incomingChatFileMetadata) {
        if (ChatUtility.isNullOrUndefined(incomingChatFileMetadata)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver.processDataChunk: incomingChatFileMetadata is null or undefined.");
            return;
        }

        if (incomingChatFileMetadata.uniqueIdentifier !== this.getUniqueId()) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver.processDataChunk: incomingChatFileMetadata.uniqueIdentifier is incorrect."
                + "\nExpected: " + this.getUniqueId()
                + "\nFound: " + incomingChatFileMetadata.uniqueIdentifier);
            return;
        }

        if (incomingChatFileMetadata.validateChecksum()
            && chatFileMetadata.chunkSequentialIndex === incomingChatFileMetadata.chunkSequentialIndex) {
            protocolTimer_dataChunk.stopTimeoutSilent();

            chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_IN_PROGRESS;
            incomingChatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_IN_PROGRESS;

            setTimeout(function () {
                fileRetrievalProgressNotifyQueue.processAll(incomingChatFileMetadata.getCopy());
            }, 0);

            // Don't ACK the actual data chunk; use a shallow copy of the metadata instead.
            sendChatClientMessage(
                new ChatClientMessage(
                      incomingChatFileMetadata.roomId
                    , ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_DATA_CHUNK_ACK
                    , incomingChatFileMetadata.getCopy().stringify()));

            var incomingUint8Array = new Uint8Array(incomingChatFileMetadata.fileData);

            // Store the received data.
            if (ChatUtility.isNullOrUndefined(this._uint8Array)) {
                this._uint8Array = new Uint8Array(chatFileMetadata.fileSizeBytes);
            }

            try {
                this._uint8Array.set(incomingUint8Array, Math.max(this._bytesReceivedCount, 0));
            }
            catch (ex) {
                // If we have a failure here, we must abort the file transfer.
                protocolTimer_dataChunk.stopTimeoutSilent();
                chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_FAILED;

                selfCleanupNotifyQueue.processAll(chatFileMetadata.getCopy());
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatFileReceiver.processDataChunk: Exception occurred when adding bytes to this._uint8Array:"
                    + "\n  " + ex.message);
                return;
            }

            this._bytesReceivedCount = this._bytesReceivedCount + incomingUint8Array.length;

            if (Number(chatFileMetadata.chunkSequentialIndex) >= Math.max(chatFileMetadata.totalChunkSequenceCount - 1, 0)) {
                // All chunks have been received.
                chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_SUCCEEDED;

                setTimeout(function () {
                    fileRetrievalCompleteNotifyQueue.processAll(chatFileMetadata.getCopy());
                }, 0);

                try {
                    var a = document.createElement('a');
                    a.download = chatFileMetadata.fileName;
                    var blob = new Blob([this._uint8Array], { type: 'application/octet-binary' });
                    a.setAttribute('href', window.URL.createObjectURL(blob));
                    document.body.appendChild(a);
                    a.click();
                    window.URL.revokeObjectURL(blob);
                }
                catch (ex) { }

                selfCleanupNotifyQueue.processAll(chatFileMetadata.getCopy());
                return;
            }

            chatFileMetadata.chunkSequentialIndex = chatFileMetadata.chunkSequentialIndex + 1;
            protocolTimer_dataChunk.startNewTimeoutAlarm();
        }
    };

    /* Method sends a cancellation message to the file sender and performs
     * self cleanup.
     */
    this.cancelFileRetrieval = function (chatFileMetadataParam) {
        protocolTimer_dataChunk.stopTimeoutSilent();
        chatFileMetadataParam.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
        chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
        fileTransmissionCancellationNotifyQueue.processAll(chatFileMetadata.getCopy());
        sendChatClientMessage(
            new ChatClientMessage(chatFileMetadataParam.roomId, ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_RECEIVER_CANCEL_TRANSFER, chatFileMetadataParam.stringify()));
        selfCleanupNotifyQueue.processAll(chatFileMetadataParam.getCopy());
    };

    /* Processes the file sender's file transmission cancellation.
     */
    this.processSenderCancel = function (chatFileMetadataParam) {
        protocolTimer_dataChunk.stopTimeoutSilent();
        chatFileMetadataParam.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
        chatFileMetadata.offerState = ChatFileMetadata.STATE.TRANSMISSION_CANCELLED;
        fileTransmissionCancellationNotifyQueue.processAll(chatFileMetadataParam.getCopy());
        selfCleanupNotifyQueue.processAll(chatFileMetadataParam.getCopy());
    };

    /* Sends a ChatClientMessage to a single endpoint.
     *
     * Parameters:
     *   chatClientMessage: A ChatClientMessage object that is to be sent through the
     *                      peerRTCInstance.
     *
     * Returns:
     *  True if ChatClientMessage was sent successfully.
     *  False, otherwise.
     */
    var sendChatClientMessage = function (chatClientMessage) {
        if (ChatUtility.isNullOrUndefined(chatClientMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver.sendChatClientMessage: chatClientMessage was null or undefined.");
            return;
        }

        if (ChatUtility.isNullOrUndefined(this.peerRTCInstance)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver.sendChatClientMessage: PeerRTC instance was null or undefined.");
            return;
        }

        try {
            this.peerRTCInstance.sendMessage(chatClientMessage);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver.sendChatClientMessage: Exception was thrown during message send:"
                + "\n  " + ex.message);
        }
    }.bind(this);

    /* Adds a callback function that will be called when the ChatFileReceiver needs
     * to remove itself from the ChatModel's download queue.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addSelfCleanupCallback = function (callbackFunction) {
        selfCleanupNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when the ChatFileReceiver has received
     * a file download offer to the recipient and is now waiting for a reply.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileOfferReceivedCallback = function (callbackFunction) {
        fileOfferReceivedNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file retrieval fails.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileRetrievalFailureCallback = function (callbackFunction) {
        fileRetrievalFailureNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a full file retrieval completes.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileRetrievalCompleteCallback = function (callbackFunction) {
        fileRetrievalCompleteNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file chunk retrieval completes.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addRetrievalProgressCallback = function (callbackFunction) {
        fileRetrievalProgressNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file transfer is cancelled by the sender.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileTransmissionCancellationCallback = function (callbackFunction) {
        fileTransmissionCancellationNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    this.addSelfCleanupCallback(function (c) { self._uint8Array = null; /* Return this memory to the garbage collector. */ });
}

// Time span to wait for data chunk before giving up.
ChatFileReceiver.DATA_CHUNK_TIMEOUT
    = ChatFileSender.DATA_CHUNK_ACK_TIMEOUT * Math.pow(2, ChatFileSender.MAX_DATA_CHUNK_RETRIES);

ChatFileReceiver.prototype = {
    // Public getter peerRTCInstance.
    get peerRTCInstance() {
        if (ChatUtility.isNullOrUndefined(this._peerRTCInstance)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver._peerRTCInstance is corrupt. Defaulting to null.");

            return null;
        }

        return this._peerRTCInstance;
    },

    // Public setter for private peerRTCInstance.
    set peerRTCInstance(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatFileReceiver.fileName is null or undefined. Defaulting to null.");

            this._peerRTCInstance = null;
            return;
        }

        this._peerRTCInstance = value;
    },
}