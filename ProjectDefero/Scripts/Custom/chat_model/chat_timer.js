﻿/* ChatTimer
 *
 * Author(s): Ryan Finn
 */

/* ChatTimer is less like a stopwatch and more like an alarm clock.
 */
function ChatTimer() {
    var self = this;
    var timeoutAlarm_msec = 0;              // Millisecond timespan until alarm will sound.
    var timeoutStartDate = 0;               // UTC milliseconds when the timer started.
    var currentState = ChatTimer.STATE.OFF; // Current state of the timer.
    var windowTimeoutFuntion = null;        // Pointer to window.setTimeout function.
    var windowIntervalFuntion = null;       // Pointer to window.setInterval function.

    // Statistics:
    this._totalStarts = 0;        // Total times the timer has been started (manually).
    this._totalManualStops = 0;   // Total times the timer has been stopped manually.
    this._totalTimeoutAlarms = 0; // Total times a timeout alarm has fired.

    // Callback functions that will be called when the timeout expires.
    var timoutCallbackNotifyQueue = new ChatCallbackFunctionList();


    /* Method sets the alarm timeout based on the supplied milliseconds input.
     * For now, using this method will not affect a currently-running timer.
     *
     * Parameters:
     *   milliseconds: Integer representation of milliseconds.
     */
    this.setTimeoutMsec = function (msec) {
        timeoutAlarm_msec = msec;
    };

    /* Method returns the alarm timeout setting value.
     */
    this.getTimeoutMsec = function () {
        return timeoutAlarm_msec;
    };

    /* Resests the total alarm timeout count to 0.
     */
    this.resetTotalTimeoutAlarms = function () {
        this._totalTimeoutAlarms = 0;
    };

    /* Resests the total timeout start count to 0.
     */
    this.resetTotalStarts = function () {
        this._totalStarts = 0;
    };

    /* Method starts the window.setTimeout function fresh with no previously elapsed time.
     */
    this.startNewTimeoutAlarm = function () {
        this.stopTimeoutSilent();

        var timeout = timeoutAlarm_msec;
        timeoutStartDate = ChatTimer.getClientUTCmsec();
        currentState = ChatTimer.STATE.RUNNING;
        windowTimeoutFuntion = setTimeout(
            function () {
                currentState = ChatTimer.STATE.EXPIRED;
                self._totalTimeoutAlarms++;
                timoutCallbackNotifyQueue.processAll(ChatTimer.getClientUTCmsec() - timeoutStartDate);
            }, timeout);

        this._totalStarts++;
    };

    /* Method starts the window.setInterval function fresh with no previously elapsed time.
     */
    this.startNewRepeatingTimoutAlarm = function () {
        this.stopTimeoutSilent();

        var timeout = timeoutAlarm_msec;
        timeoutStartDate = ChatTimer.getClientUTCmsec();
        currentState = ChatTimer.STATE.RUNNING;
        windowIntervalFuntion = setInterval(
            function () {
                currentState = ChatTimer.STATE.EXPIRED;
                self._totalTimeoutAlarms++;
                timoutCallbackNotifyQueue.processAll(ChatTimer.getClientUTCmsec() - timeoutStartDate);
            }, timeout);

        this._totalStarts++;
    };

    /* Method stops the window.setTimeout function without triggering the timeout
     * callback notification queue.
     */
    this.stopTimeoutSilent = function () {
        clearTimeout(windowTimeoutFuntion);
        clearInterval(windowIntervalFuntion);
        currentState = ChatTimer.STATE.OFF;
        this._totalManualStops++;
    };

    /* Method stops the window.setTimeout function without triggering the timeout
     * callback notification queue.
     */
    this.stopTimeoutWithAlarm = function () {
        this.stopTimeoutSilent();
        timoutCallbackNotifyQueue.processAll(ChatTimer.getClientUTCmsec() - timeoutStartDate);
    };


    /* Adds a callback function that will be called when the ChatTimer alarm fires.
     *
     * Parameters:
     *   callbackFunction(milliseconds<int>): Callback function that will be called.
     */
    this.addTimeoutCallback = function (callbackFunction) {
        timoutCallbackNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Clears the callback function queue.
     */
    this.clearTimeoutCallbackQueue = function () {
        timoutCallbackNotifyQueue = new ChatCallbackFunctionList();
    };

    /* Method returns the time span in milliseconds until the running timer will expire.
     */
    this.getMsecUntilTimeout = function () {
        return Math.max(timeoutAlarm_msec - (ChatTimer.getClientUTCmsec() - timeoutStartDate), 0);
    };
}

/* ChatTimer states.
 */
ChatTimer.STATE = {
    OFF: 0,     // ChatTimer is not running.
    RUNNING: 1, // ChatTimer is running.
    EXPIRED: 2  // ChatTimer ran and has now expired.
}

/* Returns UTC time in milliseconds as reported by the local environment.
 */
ChatTimer.getClientUTCmsec = function () {
    return Math.floor(Number(new Date().getTime()));
};

/* ChatTimer's reference to $rootScope. Someone with access to $rootScope needs
 * assign this reference. It's down to the wire around here.
 */
ChatTimer.rootScopeRef = null;

/* Returns UTC time in milliseconds as reported by a Defero server.
 */
ChatTimer.getDeferoUTCmsec = function () {
    if (ChatUtility.isNullOrUndefined(ChatTimer.serverTime)) {
        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "ChatTimer.serverTime is null or undifined. Defaulting to client time.");

        return ChatTimer.getClientUTCmsec();
    }

    return Math.floor(ChatTimer.getClientUTCmsec() + ChatTimer.serverTime.timeDifference);
};

ChatTimer.prototype = {
    // Public getter for private totalStarts.
    get totalStarts() {
        if (ChatUtility.isNullOrUndefined(this._totalStarts)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTimer.totalStarts is null or undefined. Defaulting to value 0.");

            this._totalStarts = 0;
        }

        return this._totalStarts;
    },

    // Public getter for private totalManualStops.
    get totalManualStops() {
        if (ChatUtility.isNullOrUndefined(this._totalManualStops)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTimer.totalManualStops is null or undefined. Defaulting to value 0.");

            this._totalManualStops = 0;
        }

        return this._totalManualStops;
    },

    // Public getter for private totalTimeoutAlarms.
    get totalTimeoutAlarms() {
        if (ChatUtility.isNullOrUndefined(this._totalTimeoutAlarms)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTimer.totalTimeoutAlarms is null or undefined. Defaulting to value 0.");

            this._totalTimeoutAlarms = 0;
        }

        return this._totalTimeoutAlarms;
    }
}

/* Server time object.
 */
ChatTimer.serverTime = {
    time: ChatTimer.getClientUTCmsec(),
    travelTime: 0,
    timeDifference: 0
};

/* Method asynchronously updates the ChatTimer.serverTime object.
 */
ChatTimer.initializeServerTime = function (msec) {
    if (ChatUtility.isNullOrUndefined(ChatTimer.rootScopeRef)) {
        return ChatTimer.serverTime;
    }

    ChatTimer.rootScopeRef.get_server_time(
        function (serverTime) {
            ChatTimer.serverTime = serverTime;

            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTimer.serverTime recalculated. Server/client time offset msec: "
                + ChatTimer.serverTime.timeDifference);
        });
};

// Periodically update the ChatTimer.serverTime object. Note that this approach doesn't scale well.
ChatTimer.deferoUTCping = new ChatTimer();
ChatTimer.deferoUTCping.setTimeoutMsec(1000 * 60); // 1000 * 60 = 1 minute
ChatTimer.deferoUTCping.addTimeoutCallback(ChatTimer.initializeServerTime);
ChatTimer.deferoUTCping.startNewRepeatingTimoutAlarm();