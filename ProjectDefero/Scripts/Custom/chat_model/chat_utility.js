﻿/* Defero chat model utilities.
 *
 * Author(s): Ryan Finn
 */

// Utility functions.
function ChatUtility() { }

// Enumeration of Defero Chat console logger modes.
ChatUtility.LOG_MODE = {
    PRODUCTION_ERROR: 0, // Only print errors to console that we want users to have access to seeing.
    PRODUCTION_INFO: 1,  // Only print info-level messages to console that we want users to have access to seeing.
    DEVELOPER_DEBUG: 2   // Print everything a chat developer wishes to see and don't show it to users.
};

// Enumeration of Defero Chat release modes.
ChatUtility.RELEASE_MODE = {
    PRODUCTION: 0,           // Most appropriate mode for production.
    DEVELOPER_DEBUG: 1,      // Useful for developers working on chat functionality.
    DEVELOPER_DEBUG_ERROR: 2 // Useful for Defero developers as only chat-related errors will be shown.
};
var globalDeferoChatReleaseMode = ChatUtility.RELEASE_MODE.DEVELOPER_DEBUG_ERROR; // TODO: Set this to PRODUCTION when appropriate.

/* Console message logger.
 *
 * Parameters:
 *   logMode: ChatUtility.LOG_MODE enumeration instance indicating the logging mode.
 *   message: Text sto print to the console.
 */
ChatUtility.log = function (logMode, message) {
    var prependMessage = "\n";

    if (globalDeferoChatReleaseMode !== ChatUtility.RELEASE_MODE.PRODUCTION) {
        prependMessage = "\nChat: ";
    }

    switch (logMode) {
        case ChatUtility.LOG_MODE.DEVELOPER_DEBUG:
            if (globalDeferoChatReleaseMode === ChatUtility.RELEASE_MODE.DEVELOPER_DEBUG) {
                console.log(prependMessage + message);
            }
            break;
        case ChatUtility.LOG_MODE.PRODUCTION_ERROR:
            console.log(prependMessage + message);
            break;
        case ChatUtility.LOG_MODE.PRODUCTION_INFO:
            console.log(prependMessage + message);
            break;
        default:
            if (globalDeferoChatReleaseMode !== ChatUtility.RELEASE_MODE.PRODUCTION) {
                console.log(prependMessage + "Unrecognized log mode used: " + logMode);
                console.log(prependMessage + message);
            }
            break;
    }
};

/* Console error message logger.
 *
 * Parameters:
 *   logMode: ChatUtility.LOG_MODE enumeration instance indicating the logging mode.
 *   message: Text sto print to the console.
 */
ChatUtility.error = function (logMode, message) {
    var prependMessage = "\n";

    if (globalDeferoChatReleaseMode !== ChatUtility.RELEASE_MODE.PRODUCTION) {
        prependMessage = "\nChat: ";
    }

    switch (logMode) {
        case ChatUtility.LOG_MODE.DEVELOPER_DEBUG:
            if (globalDeferoChatReleaseMode !== ChatUtility.RELEASE_MODE.PRODUCTION) {
                console.error(prependMessage + message);
            }
            break;
        case ChatUtility.LOG_MODE.PRODUCTION_ERROR:
            console.error(prependMessage + message);
            break;
        case ChatUtility.LOG_MODE.PRODUCTION_INFO:
            console.error(prependMessage + message);
            break;
        default:
            if (globalDeferoChatReleaseMode !== ChatUtility.RELEASE_MODE.PRODUCTION) {
                console.error(prependMessage + "Unrecognized log mode used: " + logMode);
                console.error(prependMessage + message);
            }
            break;
    }
};

/* Returns a pseudo-random string that is random enough to be used as
 * a chat message unique identifier.  The string is the current UTC milliseconds
 * since the Unix epoch, and some pseudo-random alpha-numeric characters.
 */
ChatUtility.getNewChatUniqueIdentifier = function () {
    return String(ChatTimer.getClientUTCmsec()) + "-" + Math.random().toString(36).substr(2, 15);
};

/* Determines if the object parameter is null or undefined.
 *
 * Parameters:
 *   object:  The object subject to the test.
 *
 * Returns:
 *   True if the object is null or undefined.
 *   False, otherwise.
 */
ChatUtility.isNullOrUndefined = function (object) {
    return (typeof object === 'undefined' || object === null);
};

/* Determines if the object parameter is of type "function."
 *
 * Parameters:
 *   Object:  The object subject to the test.
 *
 * Returns:
 *   True if the object is a funtion.
 *   False, otherwise.
 */
ChatUtility.isFunction = function (object) {
    return (typeof (object) === "function");
};

/* Determines if the supplied associative array is empty or not.
 *
 * Paramters:
 *   assArray: The associative array to test.
 *
 * Returns:
 *   True if the associative array is empty. False, otherwise.
 */
ChatUtility.isAssociativeArrayEmpty = function (assArray) {
    var isEmtpy = true;

    ChatUtility.iterateAssociativeArrayKeys(
        assArray,
        function (key) { isEmtpy = false; },
        function () { return !isEmtpy; });

    return isEmtpy;
};

/* Method iterates over the supplied associative array. Each item revealed during
 * iteration is passed as an argument to the supplied processing function. Before
 * each iteration, the stop condition function is called.  If the stop condition
 * function returns true, the iteration is stopped and this function returns.
 * Otherwise, the iteration will proceed.
 *
 * Paramters:
 *   assArray: Associative array that is to be iterated over.
 *   processingFunction(key, value): Function that will be passed the key and value
 *                                   revealed on each iteration loop.
 *   stopConidtionFunction(): Function with Boolean return value that indicates with
 *                            return value true to discontinue iteration, and indicates
 *                            with return value false to continue iteration.
 */
ChatUtility.iterateAssociativeArray = function (assArray, processingFunction, stopConidtionFunction) {
    ChatUtility.iterateAssociativeArrayKeys(
        assArray,
        function (key) {
            processingFunction(key, assArray[key]);
        },
        stopConidtionFunction);
};

/* Method iterates over the supplied associative array. Each key revealed during
 * iteration is passed as an argument to the supplied processing function. Before
 * each iteration, the stop condition function is called.  If the stop condition
 * function returns true, the iteration is stopped and this function returns.
 * Otherwise, the iteration will proceed.
 *
 * Paramters:
 *   assArray: Associative array that is to be iterated over.
 *   processingFunction(key): Function that will be passed the key revealed on each
 *                            iteration loop.
 *   stopConidtionFunction(): Function with Boolean return value that indicates with
 *                            return value true to discontinue iteration, and indicates
 *                            with return value false to continue iteration.
 */
ChatUtility.iterateAssociativeArrayKeys = function (assArray, processingFunction, stopConidtionFunction) {
    if (ChatUtility.isNullOrUndefined(assArray)) {
        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatUtility.iterateAssociativeArrayKeys: Associative array is null or undefined.");
        return;
    }

    if (!ChatUtility.isFunction(processingFunction)) {
        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatUtility.iterateAssociativeArrayKeys: processingFunction is not a function.");
        return;
    }

    if (!ChatUtility.isFunction(stopConidtionFunction)) {
        stopConidtionFunction = function () { return false; }
    }

    for (var key in assArray) {
        if (stopConidtionFunction()) { return; }

        if (assArray.hasOwnProperty(key)) {
            processingFunction(key);
        }
    }
}

/* Method returns Boolean value representing whether or not the calling web browser
 * supports all the File APIs.
 */
ChatUtility.hasAllFileAPISupport = function () {
    return (ChatUtility.isFunction(window.File)
        && ChatUtility.isFunction(window.FileReader)
        && ChatUtility.isFunction(window.FileList)
        && ChatUtility.isFunction(window.Blob));
};

/* Method returns Boolean value representing whether or not supplied window.File
 * object is corrupt.
 */
ChatUtility.isFileObjectCorrupt = function (fileObject) {
    return ChatUtility.hasAllFileAPISupport()
        && ChatUtility.isFunction(fileObject.name)
        && ChatUtility.isFunction(fileObject.size);
};

/* Method calculates and returns the 32-bit FNV1a hash of the supplied string.
 * The FNV1a hash is NOT suitable for use in cryptography.
 * http://www.isthe.com/chongo/tech/comp/fnv/index.html#FNV-param
 *
 * Parameters:
 *   inputString: String for which the hash will be calculated and returned.
 *
 * Returns:
 *   FNV1a 32-bit hash for the given input paramters.
 */
ChatUtility.FNV1a_32bit_hash = function (inputString) {
    return ChatUtility.FNV1a_hash(inputString, 0x811C9DC5, 0x1000193);
};

/* Method calculates and returns the FNV1a hash of the supplied string for
 * the supplied offset_basis and FNV_prime. The FNV1a hash is NOT suitable
 * for use in cryptography.
 * http://www.isthe.com/chongo/tech/comp/fnv/index.html#FNV-param
 *
 * Parameters:
 *   inputString: String for which the hash will be calculated and returned.
 *   offset_basis: offset_basis variable of the FNV1a algorithm.
 *   FNV_prime: FNV_prime variable to the FNV1a algorithm.
 *
 * Returns:
 *   FNV1a hash for the given input paramters.
 */
ChatUtility.FNV1a_hash = function (inputString, offset_basis, FNV_prime) {
    if (ChatUtility.isNullOrUndefined(inputString)
        || ChatUtility.isNullOrUndefined(offset_basis)
        || ChatUtility.isNullOrUndefined(FNV_prime)) {
        return 1;
    }

    var hash = offset_basis;
    var length = inputString.length;

    for (var i = 0; i < length; i++) {
        hash ^= inputString.charCodeAt(i);
        hash = (hash * FNV_prime) >>> 0; // Simulates unsigned 32-bit overflow.
    }

    return hash;
};