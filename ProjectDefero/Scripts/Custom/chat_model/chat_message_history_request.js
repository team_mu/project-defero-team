﻿/* ChatMessageHistoryRequest
 *
 * Author(s): Ryan Finn
 */

/* ChatMessageHistoryRequest allows chat client A to request chat client B's
 * ChatMessage history starting chronologically from a particular ChatMessage.
 */
function ChatMessageHistoryRequest()
{
    this._roomId = "-1";      // Room id scope of the requested ChatMessage history.
    this._senderUserId = "-1" // User id of the ChatMessageHistoryRequest sender.
                              // Message's UTC creation date in milliseconds.
    this._timestampUTC = ChatTimer.getDeferoUTCmsec();

    /* Method returns the JSON representation of the ChatMessageHistoryRequest.
     */
    this.stringify = function () {
        if (ChatUtility.isNullOrUndefined(this._roomId)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatMessageHistoryRequest.stringify: roomId is null or undefined. Defaulting to value \"-1\".");

            this._roomId = "-1";
        }

        if (ChatUtility.isNullOrUndefined(this._senderUserId)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatMessageHistoryRequest.stringify: senderUserId is null or undefined. Defaulting to value \"-1\".");

            this._senderUserId = "-1";
        }

        if (ChatUtility.isNullOrUndefined(this._timestampUTC)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatMessageHistoryRequest.stringify: timestampUTC is null or undefined. Defaulting to current UTC time.");

            this._timestampUTC = ChatTimer.getDeferoUTCmsec();
        }

        var jsonRepresentation = {
            roomId: this._roomId,
            senderUserId: this._senderUserId,
            timestampUTC: this._timestampUTC
        };

        return JSON.stringify(jsonRepresentation);
    };

    /* Returns a string representation of the object.
     */
    this.toString = function () {
        var resultVal
            = "ChatMessageHistoryRequest:"
            + "\n  UTC timestamp: " + this._timestampUTC + " (" + new Date(this._timestampUTC).toString() + ")"
            + "\n  room id: " + this._roomId
            + "\n  sender user id: " + this._senderUserId;

        return resultVal;
    };
}

ChatMessageHistoryRequest.prototype = {

    // Public getter for private roomId.
    get roomId() {
        return this._roomId;
    },

    // Public setter for private roomId.
    set roomId(val) {
        this._roomId = val;
    },

    // Public getter for private senderUserId.
    get senderUserId() {
        return this._senderUserId;
    },

    // Public setter for private senderUserId.
    set senderUserId(val) {
        this._senderUserId = val;
    },

    // Public getter for private timestampUTC.
    get timestampUTC() {
        return this._timestampUTC;
    },

    // Public setter for private timestampUTC.
    set timestampUTC(val) {
        this._timestampUTC = val;
    },

    /* Method returns a ChatMessageHistoryRequest representative of the supplied
     * JSON version of a ChatMessageHistoryRequest.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatMessageHistoryRequest.
     *
     * Returns:
     *   ChatMessageHistoryRequest constructed from values in the supplied JSON string.
     */
    parse: function (chatMessageHistoryRequestAsJSON) {
        var chatMessageHistoryRequest = new ChatMessageHistoryRequest();
        var jsonObj = null;

        try {
            jsonObj = JSON.parse(chatMessageHistoryRequestAsJSON);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatMessageHistoryRequest.prototype.parse: Exception thrown calling JSON.parse:"
                + "\n  " + ex.message);
            return chatMessageHistoryRequest;
        }

        var parseValue = null;
        chatMessageHistoryRequest._roomId = "-1";
        chatMessageHistoryRequest._senderUserId = "-1";
        chatMessageHistoryRequest._timestampUTC = ChatTimer.getDeferoUTCmsec();

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = String(jsonObj.roomId))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatMessageHistoryRequest.prototype.parse: roomId is null or undefined. Defaulting to value -1.");
            }
            else {
                chatMessageHistoryRequest._roomId = parseValue;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatMessageHistoryRequest.prototype.parse: Exception thrown: "
                + "\n  " + ex.message);
        }

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = String(jsonObj.senderUserId))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatMessageHistoryRequest.prototype.parse: senderUserId is null or undefined. Defaulting to value -1.");
            }
            else {
                chatMessageHistoryRequest._senderUserId = parseValue;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatMessageHistoryRequest.prototype.parse: Exception thrown: "
                + "\n  " + ex.message);
        }

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = Number(jsonObj.timestampUTC))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatMessageHistoryRequest.prototype.parse: timestampUTC is null or undefined. Defaulting to current UTC.");
            }
            else {
                chatMessageHistoryRequest._timestampUTC = parseValue;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatMessageHistoryRequest.prototype.parse: Exception thrown: "
                + "\n  " + ex.message);
        }

        return chatMessageHistoryRequest;
    }
}