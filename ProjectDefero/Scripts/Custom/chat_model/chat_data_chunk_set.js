﻿/* ChatDataChunkSet
 *
 * Author(s): Ryan Finn
 */

/* Data chunks sent from endpoint to endpoint over WebRTC's data channel must be of a minimum size,
 * or part of the data in the chunk will simply not be make it to the the receiving client. (This minimum
 * size is still changing as of the time of this writing, but that doesn't affect the nature of class
 * ChatDataChunkSet.) Class ChatDataChunkSet manages a set of data chunks that are to be sent endpoint-
 * to-endpoint with the assumption that each chunk is small enuough to be sent without issue.
 * ChatDataChunkSet provides information for clients sending and receiving sets of chunks that are to be
 * reconstructed into something and then passed along to the higher-level application. This class will
 * provide answer questions such as, "Have I sent all my chunks?" and "Have I received all my chunks"
 * while ignoring the order in which chunks are sent or arive.
 */
function ChatDataChunkSet() {
    /* Associative array in the form { chunkIndex<in>: chunkData<String> }
     */
    var dataChunkSet = {};

    /* Tracks how many uniquely-indexed chunks are currently in the set.
     */
    var uniquelyIndexedChunkCount = 0;

    /* Stores the number of chunks that were in the set the last time the set
     * was initialized.
     */
    var lastInitializationChunkCount = 0;

    /* Adds a chunk of data to the set at a chosen index. The method caller gets to
     * choose how large the chunk is, so the method caller better know about WebRTC's
     * data chunk size restrictions.
     *
     * Paramters:
     *   chunkIndex<int>: Index of the data chunk. The index affects where the chunk
     *                    will appear if and when the set is eventually reconstructed
     *                    as a single string of data.
     *   dataString<String>: Chunk of data that, with all the other chunks, can be reconstructed
     *                       into something meaningful to the higher-level application.
     */
    this.addChunk = function (chunkIndex, dataString) {
        var id = String(chunkIndex);

        if (!(id in dataChunkSet)) {
            uniquelyIndexedChunkCount++;
        }

        dataChunkSet[id] = dataString;
    };

    /* Takes a string of data and converts it to a set of chunks. The ChatDataChunkSet instance
     * is now a chunked representation of the input string.
     *
     * Parameters:
     *   dataString: String of data to be chunked and represented by the ChatDataChunkSet instance.
     */
    this.initializeSet = function (dataString) {
        this.initializeSetByChunkSize(dataString, ChatDataChunkSet.MAX_MESSAGE_CHUNK_BYTE_COUNT);
    };

    /* Takes a string of data and converts it to a set of chunks based on the chunk size paramter.
     * The ChatDataChunkSet instance is now a chunked representation of the input string.
     *
     * Parameters:
     *   dataString: String of data to be chunked and represented by the ChatDataChunkSet instance.
     *   chunkSize: Size of the data chunks to create.
     */
    this.initializeSetByChunkSize = function (dataString, chunkSize) {
        uniquelyIndexedChunkCount = 0;
        lastInitializationChunkCount = 0;
        var dataStringIndex = 0;
        var datasetIndexCounter = 0;
        var dataStringLength = dataString.length;
        var safetyLoopBreaker = dataString.length;

        while (safetyLoopBreaker-- > 0 && dataStringIndex < dataStringLength) {
            this.addChunk(datasetIndexCounter, dataString.slice(dataStringIndex, dataStringIndex + chunkSize));
            dataStringIndex += chunkSize;
            datasetIndexCounter++;
        }

        lastInitializationChunkCount = uniquelyIndexedChunkCount;
    };

    /* Returns the current chunk count of the set.
     */
    this.getCurrentChunkCount = function () {
        return uniquelyIndexedChunkCount;
    };

    /* Returns the number of chunks that were in the set the last time the set
     * was initialized.
     */
    this.getChunkInitializationCount = function () {
        return lastInitializationChunkCount;
    };

    /* Method returns the chunk string stored at the supplied index. If the set index
     * is not found, then an emtpy string is returned.
     *
     * Paramters:
     *   chunkIndex<int>: Set index of the data chunk to return.
     */
    this.getChunkByIndex = function (chunkIndex) {
        var id = String(chunkIndex);

        if (id in dataChunkSet) {
            return dataChunkSet[id];
        }

        return "";
    };

    /* Method returns the chunk string stored at the supplied index and then removes
     * it from the set. If the set index is not found, then an emtpy string is returned.
     *
     * Paramters:
     *   chunkIndex<int>: Set index of the data chunk to return.
     */
    this.popChunkByIndex = function (chunkIndex) {
        var id = String(chunkIndex);
        var returnValue = this.getChunkByIndex(id);

        if (id in dataChunkSet) {
            delete dataChunkSet[id];
            uniquelyIndexedChunkCount--;
        }

        return returnValue;
    };

    /* Concatenates all the data chunks in the set in order into a single string and returns it.
     */
    this.getReconstructedData = function () {
        // Find the minimum and maximum indexes.
        var minIndex = 0;
        var maxIndex = 0;

        ChatUtility.iterateAssociativeArrayKeys(
            dataChunkSet,
            function (index) {
                var indexInt = Number(index);
                if (indexInt < minIndex) { minIndex = indexInt; }
                if (indexInt > maxIndex) { maxIndex = indexInt; }
            }, null);

        var reconstructedString = [];

        for (var i = minIndex; i <= maxIndex; i++) {
            reconstructedString.push(this.getChunkByIndex(i));
        }

        return reconstructedString.join('');
    }
}

/* Max number of message bytes to transmit at a time. If we start seeing dropped data,
 * then we will need to re-evaluate this constant an likely shrink its value.
 */
ChatDataChunkSet.MAX_MESSAGE_CHUNK_BYTE_COUNT = 8 * 1024;