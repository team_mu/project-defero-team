﻿/* ChatTextMessageBuffer
 *
 * Author(s): Ryan Finn
 */

/* ChatTextMessageBuffer works on behalf of the U.I. and handles the sorting of
 * ChatTextMessages that is necessary due to the psuedo-asynchronous environment
 * and handles the ignoring of ChatTextMessage duplicate instances that can arrive
 * as part of chat history synchronization exercises with other chat clients.
 */
function ChatTextMessageBuffer() {
    var self = this;

    /* ChatTextMessage buffer. We must keep this in sorted order.
     * Newer messages have higher numbers. There must be no empty
     * slots.
     */
    var chatTextMessageBuffer = [];

    /* Used for detecting duplicate ChatTextMessage instances.
     */
    var chatTextMessageIndex = {};

    /* Queue of functions of form function(index, ChatTextMessage) that are called every
     * time a value in the ChatTextMessageBuffer changes.
     */
    var onBufferChangeHandlerQueue = new ChatCallbackFunctionList();

    /* Queue of buffer changes in key/value form buffer index : ChatTextMessage
     */
    var bufferChangesQueue = {};

    /* Maximum length of the buffer.
     */
    this._chatBufferLength = ChatTextMessageBuffer.DEFAULT_BUFFER_LENGTH;

    /* Method inserts a ChatTextMessage into the ChatTextMessageBuffer, assuming there
     * is not already an instance of the same ChatTextMessage present in the buffer or
     * the supplied ChatTextMessage is too old to fall within the range of ChatTextMessages
     * already in the buffer.
     *
     * ChatTextMessages are inserted into buffer locations based on newest-to-oldest
     * chronological ordering using the ChatTextMessage.timestampUTC values.
     *
     * Callback functions registered with the ChatTextMessageBuffer will be called for
     * each of the ChatTextMessageBuffer changes resulting from the method call and for
     * each of the queued ChatTextMessageBuffer changes that have not yet been supplied
     * to the registered callback functions.
     *
     * Parameters:
     *   chatTextMessage: ChatTextMessage insertion candidate.
     */
    this.insertMessageNotifyChanges = function (chatTextMessage) {
        self.insertMessageNoNotify(chatTextMessage);
        self.processOnBufferChangeHandlerQueue();
    };

    /* Method inserts a ChatTextMessage into the ChatTextMessageBuffer, assuming there
     * is not already an instance of the same ChatTextMessage present in the buffer or
     * the supplied ChatTextMessage is too old to fall within the range of ChatTextMessages
     * already in the buffer.
     *
     * Callback functions registered with the ChatTextMessageBuffer will not be called
     * as a result of calling this method.
     *
     * ChatTextMessages are inserted into buffer locations based on newest-to-oldest
     * chronological ordering using the ChatTextMessage.timestampUTC values.
     *
     * Parameters:
     *   chatTextMessage: ChatTextMessage insertion candidate.
     */
    this.insertMessageNoNotify = function (chatTextMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessageBuffer.insertMessage: ChatTextMessage is corrupt.");
            return;
        }

        if (chatTextMessage.uniqueIdentifier in chatTextMessageIndex) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessageBuffer.insertMessage: ChatTextMessage duplicate detected and ignored.");
            return;
        }

        // Is this the first message insertion?
        if (chatTextMessageBuffer.length <= 0) {
            insertMessageAtIndex(0, chatTextMessage);
            return;
        }

        var minIndex = 0;
        var maxIndex = Math.max(chatTextMessageBuffer.length - 1, 0);
        var midIndex = minIndex;
        var matchFound = false;

        // Protect the wider application from an accidental endless loop.
        var safetyStop = self._chatBufferLength;

        // Rough binary insertion location selection.
        while (safetyStop-- > 0
                && !matchFound
                && maxIndex >= minIndex) {
            midIndex = minIndex + Math.floor((maxIndex - minIndex) / 2);

            if (chatTextMessage.timestampUTC > chatTextMessageBuffer[midIndex].timestampUTC) {
                minIndex = midIndex + 1;
            }
            else if (chatTextMessage.timestampUTC < chatTextMessageBuffer[midIndex].timestampUTC) {
                maxIndex = midIndex - 1;
            }
            else // ==
            {
                matchFound = true;
            }
        }

        // Locate exact insertion location using the rough insertion location as a starting point.
        if (chatTextMessage.timestampUTC >= chatTextMessageBuffer[midIndex].timestampUTC) {
            if (chatTextMessageBuffer.length >= self._chatBufferLength) {
                var insertionIndex = midIndex;

                // Shift everything smaller down to make room, then insert.
                shiftBufferDown(insertionIndex, 0);
                insertMessageAtIndex(insertionIndex, chatTextMessage);
            }
            else {
                var insertionIndex = Math.min(midIndex + 1, self._chatBufferLength - 1);

                // Shift everything larger up to make room, then insert.
                shiftBufferUp(insertionIndex, chatTextMessageBuffer.length);
                insertMessageAtIndex(insertionIndex, chatTextMessage);
            }
        }
        else if (chatTextMessage.timestampUTC < chatTextMessageBuffer[midIndex].timestampUTC) {
            var insertionIndex = midIndex - 1;

            while (insertionIndex >= 0
                && chatTextMessage.timestampUTC <= chatTextMessageBuffer[insertionIndex].timestampUTC) {
                insertionIndex--;
            }

            insertionIndex = Math.max(insertionIndex, 0);

            // Try to push everything up to fit in.
            if (chatTextMessage.timestampUTC < chatTextMessageBuffer[insertionIndex].timestampUTC) {
                // At this point we can only insert if there's room in the buffer.
                if (chatTextMessageBuffer.length >= self._chatBufferLength) {
                    return;
                }

                // Shift everything larger up to make room, then insert.
                shiftBufferUp(insertionIndex, chatTextMessageBuffer.length);
                insertMessageAtIndex(insertionIndex, chatTextMessage);
            }
            else if (chatTextMessage.timestampUTC >= chatTextMessageBuffer[insertionIndex].timestampUTC) {
                if (chatTextMessageBuffer.length >= self._chatBufferLength) {
                    // Shift everything smaller down to make room, then insert.
                    shiftBufferDown(insertionIndex, 0);
                    insertMessageAtIndex(insertionIndex, chatTextMessage);
                }
                else {
                    insertionIndex++;

                    // Shift everything larger up to make room, then insert.
                    shiftBufferUp(insertionIndex, chatTextMessageBuffer.length);
                    insertMessageAtIndex(insertionIndex, chatTextMessage);
                }
            }
        }
    };

    /* Method inserts the supplied ChatTextMessage into the ChatTextMessageBuffer
     * at the supplied index.
     *
     * Parameters:
     *   index: Index of ChatTextMessageBuffer where the supplied ChatTextMessage
     *          will be inserted.
     *   chatTextMessage: ChatTextMessage that will be inserted into the
     *                    ChatTextMessageBuffer at the supplied index.
     */
    var insertMessageAtIndex = function (index, chatTextMessage) {
        chatTextMessageBuffer[index] = chatTextMessage;
        chatTextMessageIndex[chatTextMessage.uniqueIdentifier] = true;
        bufferChangesQueue[index] = chatTextMessage;
    };

    /* Method shifts the values in the message buffer up from a starting
     * index to an ending index.
     *
     * Parameters:
     *   shiftStartIndex: Index where upward value shifting starts.
     *   shiftToIndex: Index where upward shifting stops.
     */
    var shiftBufferUp = function (shiftStartIndex, shiftToIndex) {
        var overwrittenChatTextMessage = null;
        var overwrittenCaptured = false;

        for (var i = shiftToIndex; i > shiftStartIndex; i--) {
            if (!overwrittenCaptured) {
                overwrittenChatTextMessage = chatTextMessageBuffer[i];
                overwrittenCaptured = true;
            }

            chatTextMessageBuffer[i] = chatTextMessageBuffer[i - 1];
            bufferChangesQueue[i] = chatTextMessageBuffer[i];
        }

        if (overwrittenCaptured
            && !ChatUtility.isNullOrUndefined(overwrittenChatTextMessage))
        {
            delete chatTextMessageIndex[overwrittenChatTextMessage.uniqueIdentifier];
        }
    };

    /* Method shifts the values in the message buffer down from a starting
     * index to an ending index.
     *
     * Parameters:
     *   shiftStartIndex: Index where downward value shifting starts.
     *   shiftToIndex: Index where downward shifting stops.
     */
    var shiftBufferDown = function (shiftStartIndex, shiftToIndex) {
        var overwrittenChatTextMessage = null;
        var overwrittenCaptured = false;

        for (var i = shiftToIndex; i < shiftStartIndex; i++) {
            if (!overwrittenCaptured) {
                overwrittenChatTextMessage = chatTextMessageBuffer[i];
                overwrittenCaptured = true;
            }

            chatTextMessageBuffer[i] = chatTextMessageBuffer[i + 1];
            bufferChangesQueue[i] = chatTextMessageBuffer[i];
        }

        if (overwrittenCaptured
            && !ChatUtility.isNullOrUndefined(overwrittenChatTextMessage)) {
            delete chatTextMessageIndex[overwrittenChatTextMessage.uniqueIdentifier];
        }
    };

    /* Registers a function that will be called when value in the ChatTextMessageBuffer
     * changes.
     *
     * Parameters:
     *   callbackFunction([]{ index:<int>, message:<ChatTextMessage> }): Callback function
     *   that will be enqueued and called every time the ChatModel receives a ChatTextMessage.
     *   When called, the callback function will be supplied an array of objects each containing
     *   the index where the buffer change occurred and the ChatTextMessage resulting from the
     *   change.
     */
    this.registerOnBufferChangeHandlerQueue = function (callbackFunction) {
        if (!ChatUtility.isFunction(callbackFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "registerOnBufferChangeHandlerQueue: Supplied callback function parameter is not a function.");
            return;
        }

        onBufferChangeHandlerQueue.addCallbackFuntion(callbackFunction);
    };

    /* Calls each callback function stored in onMessageHandlerQueue and supplies it with the
     * enqueued buffer changes.
     */
    this.processOnBufferChangeHandlerQueue = function () {
        /*
        // Get a deep copy of the bufferChangesQueue to return.
        var returnBufferChangesQueue = {};
        ChatUtility.iterateAssociativeArray(
            bufferChangesQueue,
            function (bufferChangesQueueIndex, chatTextMessage) {
                returnBufferChangesQueue[bufferChangesQueueIndex] = chatTextMessage.getCopy();
            }, null);
        */

        /*
        // Get a deep copy of the entire buffer to return.
        var returnChatTextMessageBuffer = [];
        for (var i = 0; i < chatTextMessageBuffer.length; i++) {
            returnChatTextMessageBuffer[i] = chatTextMessageBuffer[i].getCopy();
        }

        onBufferChangeHandlerQueue.processAll(returnChatTextMessageBuffer);
        bufferChangesQueue = {};
        */

        // This is so hacky it makes me sick.
        onBufferChangeHandlerQueue.processAll(chatTextMessageBuffer);
    };

    /* Method iterates over all the ChatTextMessages currently in the buffer and
     * passes them to the supplied callback function.
     *
     * Paramters:
     *   callbackFunction(ChatTextMessage): Callback function that will be passed
     *                                      each ChatTextMessage during iteration.
     */
    this.processAllChatTextMessages = function (callbackFunction) {
        for (var i = 0; i < chatTextMessageBuffer.length; i++) {
            //callbackFunction(chatTextMessageBuffer[i].getCopy());
            callbackFunction(chatTextMessageBuffer[i]);
        }
    };

    /* Method returns the buffer size currently in use.
     */
    this.getChatBufferLength = function () {
        return this._chatBufferLength;
    };

    /* Method sets the buffer size of the ChatTextMessageBuffer using the supplied
     * input value. If the supplied value results in a larger buffer size, then the
     * chat message history will be automatically loaded.
     *
     * Parameter:
     *   length: New buffer size.
     */
    this.setChatBufferLength = function (length) {
        var newValue = Number(length);
        var oldValue = this._chatBufferLength;

        if (ChatUtility.isNullOrUndefined(newValue)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessageBuffer.setChatBufferLength: length is null or undefined. Defaulting to value "
                + ChatTextMessageBuffer.DEFAULT_BUFFER_LENGTH);

            newValue = ChatTextMessageBuffer.DEFAULT_BUFFER_LENGTH;
        }

        this._chatBufferLength = newValue;

        // Shrink the buffer.
        if (chatTextMessageBuffer.length > newValue) {
            bufferChangesQueue = {};
            chatTextMessageIndex = {};

            var counter = 0;
            for (var i = chatTextMessageBuffer.length - newValue; i < chatTextMessageBuffer.length; i++, counter++) {
                if (!(chatTextMessageBuffer[i].uniqueIdentifier in chatTextMessageIndex)) {
                    chatTextMessageBuffer[counter] = chatTextMessageBuffer[i];
                    bufferChangesQueue[counter] = chatTextMessageBuffer[i];
                }

                chatTextMessageIndex[chatTextMessageBuffer[i].uniqueIdentifier];
            }

            chatTextMessageBuffer.length = newValue;
        }
    };

    /* Method clears out the buffer.
     */
    this.clearBuffer = function () {
        chatTextMessageBuffer = [];
        chatTextMessageIndex = {};
        bufferChangesQueue = {};
        this.processOnBufferChangeHandlerQueue();
    };
}

// Default maximum length of the buffer.
ChatTextMessageBuffer.DEFAULT_BUFFER_LENGTH = 150;