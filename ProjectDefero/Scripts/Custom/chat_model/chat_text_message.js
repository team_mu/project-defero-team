﻿/* ChatTextMessage
 *
 * Author(s): Ryan Finn
 */

/* ChatTextMessage represents a chat room text message that may be broadcast
 * from one sender client to many recipient clients.
 */
function ChatTextMessage() {
    this._uniqueIdentifier = ChatUtility.getNewChatUniqueIdentifier(); // Message's unique identifier.
    this._timestampUTC = ChatTimer.getDeferoUTCmsec();                 // Message's UTC creation date in milliseconds.
    this._authorUserId = ChatTextMessage.DEFAULT.DATA_ID;              // UserId of the message author.
    this._senderUserId = ChatTextMessage.DEFAULT.DATA_ID;              // UserId of the message sender.
    this._recipientUserId = ChatTextMessage.DEFAULT.DATA_ID;           // UserId of the message recipient.
    this._roomId = ChatTextMessage.DEFAULT.DATA_ID;                    // RoomId of the message.
    this._messageBody = ChatTextMessage.DEFAULT.MESSAGE_BODY;          // Message body.
    this._messageState = ChatTextMessage.DEFAULT.MESSAGE_STATE;        // ChatTextMessage state.
    this._messageType = ChatTextMessage.DEFAULT.MESSAGE_TYPE;          // ChatTextMessage state.
    this._databaseVersion = ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION; // ChatTextMessate storage version.

    /* ChatFileMetadata object. This is used when the ChatTextMessage is involved in a file transmission.
     */
    this._chatFileMetadata = null;

    /* Method returns the JSON representation of the ChatTextMessage.
     */
    this.stringify = function () {
        var jsonRepresentation = {
            uniqueIdentifier: this.uniqueIdentifier,
            timestampUTC: this.timestampUTC,
            authorUserId: this.authorUserId,
            senderUserId: this.senderUserId,
            recipientUserId: this.recipientUserId,
            roomId: this.roomId,
            messageBody: this.messageBody,
            messageState: this.messageState,
            messageType: this.messageType,
            databaseVersion: this.databaseVersion,
        };

        if (!ChatUtility.isNullOrUndefined(this.chatFileMetadata)
            && ChatUtility.isFunction(this.chatFileMetadata.toJSON)) {
            jsonRepresentation["chatFileMetadata"] = this.chatFileMetadata.toJSON();
        }

        return JSON.stringify(jsonRepresentation);
    };

    /* Returns a new ChatTextMessage instance that is a copy of this ChatTextMessage instance.
     * I don't know how to do a copy contructor in Javascript.
     */
    this.getCopy = function () {
        var resultChatTextMessage = new ChatTextMessage();

        resultChatTextMessage._uniqueIdentifier = this.uniqueIdentifier;
        resultChatTextMessage._timestampUTC = this.timestampUTC;
        resultChatTextMessage.authorUserId = this.authorUserId;
        resultChatTextMessage.senderUserId = this.senderUserId;
        resultChatTextMessage.recipientUserId = this.recipientUserId;
        resultChatTextMessage.roomId = this.roomId;
        resultChatTextMessage.messageBody = this.messageBody;
        resultChatTextMessage.messageState = this.messageState;
        resultChatTextMessage.messageType = this.messageType;
        resultChatTextMessage.databaseVersion = this.databaseVersion;

        if (!ChatUtility.isNullOrUndefined(this.chatFileMetadata)
            && ChatUtility.isFunction(this.chatFileMetadata.getCopy)) {
            resultChatTextMessage.chatFileMetadata = this.chatFileMetadata.getCopy();
        }
        else {
            resultChatTextMessage.chatFileMetadata = null;
        }

        return resultChatTextMessage;
    };

    /* Returns a string representation of the object.
     */
    this.toString = function () {
        var resultVal
            = "ChatTextMessage:"
            + "\n  Unique identifier: " + this.uniqueIdentifier
            + "\n  UTC timestamp: " + this.timestampUTC + " (" + new Date(this.timestampUTC).toString() + ")"
            + "\n  author user id: " + this.authorUserId
            + "\n  sender user id: " + this.senderUserId
            + "\n  recipient user id: " + this.recipientUserId
            + "\n  room id: " + this.roomId
            + "\n  message body non-empty: " + (this.messageBody.length > 0)
            + "\n  message state: " + this.messageState
            + "\n  message type: " + this.messageType
            + "\n  message database version: " + this.databaseVersion;

        if (!ChatUtility.isNullOrUndefined(this.chatFileMetadata)
            && ChatUtility.isFunction(this.chatFileMetadata.toString)) {
            resultVal += "\n " + this.chatFileMetadata.toString();
        }

        return resultVal;
    };

    /* Method determines if supplied ChatTextMessage and this ChatTextMessage
     * have the same content and ownership, and ignores the uniqueIdentifier,
     * timestampUTC, and state.
     *
     * Returns:
     *   True if this ChatTextMessage's content and ownership matches that of
     *   the supplied ChatTextMessage.
     *   False, otherwise.
     */
    this.isDuplicate = function (comparatorChatTextMessage) {
        return (!ChatUtility.isNullOrUndefined(comparatorChatTextMessage)
            && this.authorUserId === comparatorChatTextMessage.authorUserId
            && this.recipientUserId === comparatorChatTextMessage.recipientUserId
            && this.roomId === comparatorChatTextMessage.roomId
            && this.messageBody === comparatorChatTextMessage.messageBody
            && this.messageType === comparatorChatTextMessage.messageType);
        // Do not compare database versions.
    };

    /* Sets the messageBody to an empty string and sets the chatFileMetadata to null.
     */
    this.setToEmpty = function () {
        this.messageBody = "";
        this.chatFileMetadata = null;
    };

    //ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG, "ChatTextMessage instantiated.");
}

// Enumeration of ChatTextMessage states.
ChatTextMessage.STATE = {

    NEW_UNDREAD: 0, // ChatTextMessage is new and unread.

    /* ChatTextMessage is old and unread. This could be a message received
     * during synchronization retrieval of missed messages.
     */
    OLD_UNREAD: 1,

    /* ChatTextMessage is old and read. This could be a message pulled
     * from local storage message history.
     */
    OLD_READ: 2,

    /* Message is in a bad enough sate that is should be discarded.
     */
    CORRUPT: 3
};

// Enumeration of ChatTextMessage types.
ChatTextMessage.TYPE = {
    TEXT_MESSAGE: 0,      // Standard text message.
    FILE_TRANSMISSION: 1  // Represents a file transmission process.
};

// ChatTextMessage component default values.
ChatTextMessage.DEFAULT = {
    DATA_ID: "-1",
    MESSAGE_BODY: "",
    MESSAGE_STATE: ChatTextMessage.STATE.NEW_UNDREAD,
    MESSAGE_TYPE: ChatTextMessage.TYPE.TEXT_MESSAGE,
    UNIQUE_IDENTIFIER: "-1",
    UTC_TIME: 0,
    DATABASE_VERSION: 0
};

ChatTextMessage.prototype = {

    // Public getter for private uniqueIdentifier.
    get uniqueIdentifier() {
        if (ChatUtility.isNullOrUndefined(this._uniqueIdentifier)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.uniqueIdentifier is null or undefined. Defaulting to value "
                + ChatTextMessage.DEFAULT.UNIQUE_IDENTIFIER);

            this._uniqueIdentifier = ChatTextMessage.DEFAULT.UNIQUE_IDENTIFIER;
        }

        return this._uniqueIdentifier;
    },

    // Public getter for private timestampUTC.
    get timestampUTC() {
        if (ChatUtility.isNullOrUndefined(this._timestampUTC)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.timestampUTC is null or undefined. Defaulting to value "
                + ChatTextMessage.DEFAULT.UTC_TIME);

            this._timestampUTC = ChatTextMessage.DEFAULT.UTC_TIME;
        }

        return Math.floor(this._timestampUTC);
    },

    // Public setter for private timestampUTC.
    set timestampUTC(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.timestampUTC is null or undefined. Defaulting to current UTC.");

            this._timestampUTC = ChatTimer.getDeferoUTCmsec();
            return;
        }

        this._timestampUTC = String(value);
    },

    // Public getter for private authorUserId.
    get authorUserId() {
        return this._authorUserId;
    },

    // Public setter for private authorUserId.
    set authorUserId(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.authorUserId is null or undefined. Defaulting to value "
                + ChatTextMessage.DEFAULT.DATA_ID);

            this._authorUserId = ChatTextMessage.DEFAULT.DATA_ID;
            return;
        }

        this._authorUserId = String(value);
    },

    // Public getter for private senderUserId.
    get senderUserId() {
        return this._senderUserId;
    },

    // Public setter for private senderUserId.
    set senderUserId(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.senderUserId is null or undefined. Defaulting to value "
                + ChatTextMessage.DEFAULT.DATA_ID);

            this._senderUserId = ChatTextMessage.DEFAULT.DATA_ID;
            return;
        }

        this._senderUserId = String(value);
    },

    // Public getter for private recipientUserId.
    get recipientUserId() {
        return this._recipientUserId;
    },

    // Public setter for private recipientUserId.
    set recipientUserId(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.recipientUserId is null or undefined. Defaulting to value "
                + ChatTextMessage.DEFAULT.DATA_ID);

            this._recipientUserId = ChatTextMessage.DEFAULT.DATA_ID;
            return;
        }

        this._recipientUserId = String(value);
    },

    // Public getter for private roomId.
    get roomId() {
        return this._roomId;
    },

    // Public setter for private roomId.
    set roomId(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.roomId is null or undefined. Defaulting to value "
                + ChatTextMessage.DEFAULT.DATA_ID);

            this._roomId = ChatTextMessage.DEFAULT.DATA_ID;
            return;
        }

        this._roomId = String(value);
    },

    // Public getter for private messageBody.
    get messageBody() {
        return this._messageBody;
    },

    // Public setter for private messageBody.
    set messageBody(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.messageBody is null or undefined. Defaulting to empty string.");

            this._messageBody = ChatTextMessage.DEFAULT.MESSAGE_BODY;
            return;
        }

        this._messageBody = String(value);
    },

    // Public getter for private messageState.
    get messageState() {
        return this._messageState;
    },

    // Public setter for private messageBody.
    set messageType(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.messageType is null or undefined. Defaulting to: "
                + ChatTextMessage.DEFAULT.MESSAGE_TYPE);

            this._messageType = ChatTextMessage.DEFAULT.MESSAGE_TYPE;
            return;
        }

        this._messageType = value;
    },

    // Public getter for private messageType.
    get messageType() {
        return this._messageType;
    },

    // Public setter for private messageState.
    set messageState(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.messageState is null or undefined. Defaulting to "
                + ChatTextMessage.DEFAULT.MESSAGE_STATE);

            this._messageState = ChatTextMessage.DEFAULT.MESSAGE_STATE;
            return;
        }

        this._messageState = Number(value);
    },

    // Public getter for private databaseVersion.
    get databaseVersion() {
        return this._databaseVersion;
    },

    set databaseVersion(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.databaseVersion is null or undefined. Defaulting to "
                + ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION);

            this._databaseVersion = ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION;
            return;
        }

        this._databaseVersion = value;
    },

    // Public setter for private _chatFileMetadata.
    set chatFileMetadata(value) {
        if (ChatUtility.isNullOrUndefined(value)) {
            this._chatFileMetadata = null;
            return;
        }

        this._uniqueIdentifier = value.uniqueIdentifier;
        this._chatFileMetadata = value;
    },

    // Public getter for private chatFileMetadata.
    get chatFileMetadata() {
        return this._chatFileMetadata;
    },

    /* Method returns a ChatTextMessage representative of the supplied JSON version
     * of a ChatTextMessage.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatTextMessage.
     *
     * Returns:
     *   ChatTextMessage constructed from values in the supplied JSON string.
     */
    parse: function (chatTextMessageAsJSON) {
        var chatTextMessage = new ChatTextMessage();
        var jsonObj = null;

        try {
            jsonObj = JSON.parse(chatTextMessageAsJSON);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown calling JSON.parse:"
                + "\n  " + ex.message);
            return chatTextMessage;
        }

        var parseValue = null;
        chatTextMessage._uniqueIdentifier = ChatTextMessage.DEFAULT.UNIQUE_IDENTIFIER;
        chatTextMessage.databaseVersion = ChatTextMessage.DEFAULT.DATABASE_VERSION;

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = String(jsonObj.uniqueIdentifier))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatTextMessage.prototype.parse: uniqueIdentifier is null or undefined. Defaulting to value "
                    + ChatTextMessage.DEFAULT.UNIQUE_IDENTIFIER);
            }
            else {
                chatTextMessage._uniqueIdentifier = parseValue;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing uniqueIdentifier: "
                + "\n  " + ex.message);
        }

        try {
            if (ChatUtility.isNullOrUndefined(parseValue = Number(jsonObj.timestampUTC))) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatTextMessage.prototype.parse: timestampUTC is null or undefined. Defaulting to value "
                    + ChatTextMessage.DEFAULT.UTC_TIME);
            }
            else {
                chatTextMessage._timestampUTC = parseValue;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing timestampUTC:"
                + "\n  " + ex.message);
        }

        try {
            chatTextMessage.authorUserId = jsonObj.authorUserId;
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing authorUserId:"
                + "\n  " + ex.message);
        }

        try {
            chatTextMessage.senderUserId = jsonObj.senderUserId;
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing senderUserId:"
                + "\n  " + ex.message);
        }

        try {
            chatTextMessage.recipientUserId = jsonObj.recipientUserId;
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing recipientUserId:"
                + "\n  " + ex.message);
        }

        try {
            chatTextMessage.roomId = jsonObj.roomId;
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing roomId:"
                + "\n  " + ex.message);
        }

        try {
            chatTextMessage.messageBody = jsonObj.messageBody;
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing messageBody:"
                + "\n  " + ex.message);
        }

        try {
            chatTextMessage.messageState = Number(jsonObj.messageState);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing messageState:"
                + "\n  " + ex.message);
        }

        try {
            chatTextMessage._messageType = Number(jsonObj.messageType);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing messageType:"
                + "\n  " + ex.message);
        }

        try {
            chatTextMessage.databaseVersion = Number(jsonObj.databaseVersion);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessage.prototype.parse: Exception thrown parsing databaseVersion:"
                + "\n  " + ex.message);
        }

        if (chatTextMessage.messageType === ChatTextMessage.TYPE.FILE_TRANSMISSION) {
            try {
                if (!ChatUtility.isNullOrUndefined(jsonObj.chatFileMetadata)) {
                    chatTextMessage.chatFileMetadata = ChatFileMetadata.prototype.reconstructor(jsonObj.chatFileMetadata);
                }
            }
            catch (ex) {
                chatTextMessage.chatFileMetadata = null;
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatTextMessage.prototype.parse: Exception thrown parsing chatFileMetadata:"
                    + "\n  " + ex.message);
            }
        }

        return chatTextMessage;
    },

    /* Method determines if supplied ChatTextMessage should be considered to be
     * in a corrupt state.
     *
     * Returns:
     *   True if this ChatTextMessage should be considered to be in a corrupt
     *   state.
     *   False, otherwise.
     */
    isCorrupt: function (chatTextMessage) {
        try {
            if (ChatUtility.isNullOrUndefined(chatTextMessage)) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._uniqueIdentifier)
                || chatTextMessage._uniqueIdentifier === ChatTextMessage.DEFAULT.UNIQUE_IDENTIFIER) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._timestampUTC)
                || isNaN(chatTextMessage._timestampUTC)
                || chatTextMessage._timestampUTC === ChatTextMessage.DEFAULT.UTC_TIME) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._authorUserId)
                || chatTextMessage._authorUserId === ChatTextMessage.DEFAULT.DATA_ID) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._senderUserId)
                || chatTextMessage._senderUserId === ChatTextMessage.DEFAULT.DATA_ID) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._recipientUserId)
                || chatTextMessage._recipientUserId === ChatTextMessage.DEFAULT.DATA_ID) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._roomId)
                || chatTextMessage._roomId === ChatTextMessage.DEFAULT.DATA_ID) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._messageBody)) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._messageState)) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._messageType)) {
                return true;
            }

            if (ChatUtility.isNullOrUndefined(chatTextMessage._databaseVersion)) {
                return true;
            }
        }
        catch (ex) {
            return true;
        }

        return false;
    }
}