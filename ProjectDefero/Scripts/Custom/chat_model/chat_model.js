﻿/* Defero chat model.
 *
 * Author(s): Ryan Finn
 */

/* Constructor
 *
 * Parameters:
 *   userId: Id of the user.
 *   roomId: Id of the chat room the user is involved.
 *   rootScope: Reference to the $rootScope object. ChatModel needs this so that we can
 *              $rootScope's methods to get information about users involved in the chat.
 *   roomsController: Reference to the $rooms_controller object. Chatmodel needs this to
 *                    determine if our PeerRTC instances are associated to users online
 *                    this ChatModel's room.
 *
 * TODO: Consider re-working things so that we're passing around User objects that
 *       contain everything we need to know about individual users and that contain
 *       the means to communicate with them.
 */
function ChatModel(userId, roomId, rootScope, roomsController) {
    ChatTimer.rootScopeRef = rootScope;
    ChatTimer.initializeServerTime(0);

    var self = this;
    var myUserId = "-1"; // Id of user currently using this ChatModel instance.
    var myRoomId = "-1"; // Id of Room currently using this ChatModel instance.
    var myRootScope = rootScope; // Reference to $rootScope.
    var myRoomsControllerRef = roomsController;

    if (ChatUtility.isNullOrUndefined(userId)) {
        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "Invalid user id supplied to ChatModel: " + userId);
    }
    else {
        myUserId = String(userId); // Id of user currently using this ChatModel instance.
    }

    if (ChatUtility.isNullOrUndefined(roomId)) {
        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "Invalid room id supplied to ChatModel: " + roomId);
    }
    else {
        myRoomId = String(roomId);
    }

    /* ChatTextMessageFilter that filters out ChatTextMessages that should not be seen
     * by the current user in the current room.  The filter catches ChatTextMessages
     * that we should see in our U.I. message list.
     */
    var principleMessageFilter = new ChatTextMessageFilter();
    principleMessageFilter.setFilter_recipientUserId(myUserId, ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO);
    principleMessageFilter.setFilter_roomId(myRoomId, ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO);
    principleMessageFilter.setFilter_messageBody("", ChatTextMessageFilter.FILTER_CLAUSE.NOT_EQUAL_TO);


    /* Stores a user-defined ChatTextMessageFilter that gets applied when loading chat history.
     */
    var userSuppliedFilter = new ChatTextMessageFilter();

    /* Stores key/value pairs in form PeerRTC.id : PeerRTC.
     * Let's hope the PeerRTC's id value doesn't change!
     */
    var peerRTCInstances = {};

    /* Queue of functions of form function(ChatTextMessage) that are called every
     * time the ChatModel receives a LIVE ChatTextMessage. "Live" means it was not
     * sent as part of a local storage loading process or a synchronization process,
     * but instead sent from a client during a live chat session.
     */
    var onLiveMessageNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of functions of form function(ChatTextMessage) that are called every
     * time the ChatModel receives a new file share offer.
     */
    var onFileOfferNotifyQueue = new ChatCallbackFunctionList();

    /* Client-side chat model database.
     */
    var chatDatabase = new ChatDatabase();

    /* ChatTextMessage buffer abstraction that handles things like chronological
     * ordering of displayed messages and prevention of duplicate message displays.
     */
    var chatTextMessageBuffer = new ChatTextMessageBuffer();

    /* Stores a set of active ChatFileSenders.
     */
    var activeChatFileSenderSet = {};

    /* Stores a set of active ChatReceivers.
     */
    var activeChatFileReceiverSet = {};

    /* Queue of callback functions called when a ChatFileSender has sent a file
     * download invitation to the recipient and is now waiting for a reply.
     */
    var waitingForFileOfferResponseNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a ChatFileSender has received a file
     * download invitation.
     */
    var fileOfferReceivedNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file offer recipient has responded
     * with a file offer rejection.
     */
    var fileOfferRejectedNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file offer recipient has accepted the
     * file offer.
     */
    var fileOfferAcceptedNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file offer response wait period
     * has expired with no reponse from the recipient.
     */
    var fileOfferNoResponseNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file data chunk transmission fails.
     */
    var fileTransmissionFailureNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file data chunk transmission succeeds.
     */
    var fileTransmissionProgressNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file data chunk retrieval fails.
     */
    var fileRetrievalFailureNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a full file transmission completes.
     */
    var fileTransmissionCompleteNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a full file retrieval is complete.
     */
    var fileRetrievalCompleteNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file data chunk retrieval succeeds.
     */
    var fileRetrievalProgressNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file transfer is cancelled by the sender.
     */
    var fileTransmissionCancellationNotifyQueue = new ChatCallbackFunctionList();

    /* Queue of callback functions called when a file transfer is cancelled by the receiver.
     */
    var fileRetrievalCancellationNotifyQueue = new ChatCallbackFunctionList();


    /* Adds PeerRTC instance to the ChatModel's list of PeerRTC objects.
     * If the supplied PeerRTC instance is already stored in the ChatModel,
     * then the supplied PeerRTC instance will overwrite the existing instance.
     *
     * Parameters:
     *   peerRtcInstance: PeerRTC instance that ChatModel will send to and receive text
     *                    chats from.
     *
     * Returns:
     *   True if the PeerRTC instance was successfully added to the ChatModel.
     *   False, otherwise.
     */
    this.addPeerRTCInstance = function (peerRtcInstance) {
        var debugMessage = "addPeerRTCInstance: Adding PeerRTC instance...";

        if (ChatUtility.isNullOrUndefined(peerRtcInstance)) {
            debugMessage += "\n  ERROR: Supplied PeerRTC instance was null or undefined.";
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG, debugMessage);
            return false;
        }

        if (ChatUtility.isNullOrUndefined(peerRtcInstance.id)) {
            debugMessage += "\n  ERROR: Supplied PeerRTC.id was null or undefined.";
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG, debugMessage);
            return false;
        }

        if (peerRtcInstance.id in peerRTCInstances) {
            debugMessage += "\n  WARNING: ChatModel already contained a PeerRTC instance"
                + " with PeerRTC.id = " + peerRtcInstance.id;

            this.removePeerRTCInstance(peerRtcInstance);
        }

        peerRTCInstances[peerRtcInstance.id] = peerRtcInstance;
        debugMessage += "\n  PeerRTC.id = " + peerRtcInstance.id + " added.";

        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG, debugMessage);

        var result = registerPeerRTCInstance(peerRtcInstance);

        /* Determine if any of our ChatFileSenders already had a PeerRTC instance
         * with the same PeerRTC.id and therefor need it replaced with this new
         * one.
         */
        var chatFileSenderInstanceFound = false;
        ChatUtility.iterateAssociativeArray(
            activeChatFileSenderSet,
            function (key, chatFileSender) {
                if (ChatUtility.isNullOrUndefined(chatFileSender)) { return; }

                if (chatFileSender.getMetadata().recipientUserId === String(peerRtcInstance.id)) {
                    chatFileSender.peerRTCInstance = peerRtcInstance;
                    chatFileSenderInstanceFound = true;
                }
            },
            function () { return chatFileSenderInstanceFound; });

        /* Determine if any of our ChatFileReceivers already had a PeerRTC instance
         * with the same PeerRTC.id and therefor need it replaced with this new
         * one.
         */
        var chatFileReceiverInstanceFound = false;
        ChatUtility.iterateAssociativeArray(
            activeChatFileReceiverSet,
            function (key, chatFileReceiver) {
                if (ChatUtility.isNullOrUndefined(chatFileReceiver)) { return; }

                if (chatFileReceiver.getMetadata().recipientUserId === String(peerRtcInstance.id)) {
                    chatFileReceiver.peerRTCInstance = peerRtcInstance;
                    chatFileReceiverInstanceFound = true;
                }
            },
            function () { return chatFileReceiverInstanceFound; });

        return result;
    };

    /* Removes PeerRTC instance from the ChatModel.
     *
     * Parameters:
     *   peerRtcInstance: PeerRTC instance to be removed from ChatModel.
     *
     * Returns:
     *   True if PeerRTC instance is removed successfully.
     *   False, otherwise.
     */
    this.removePeerRTCInstance = function (peerRtcInstance) {
        if (ChatUtility.isNullOrUndefined(peerRtcInstance)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "removePeerRTCInstance: PeerRTC instance was null or undefined. Let's try to remove it anyway.");
        }
        else if (ChatUtility.isNullOrUndefined(peerRtcInstance.id)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "removePeerRTCInstance: PeerRTC.id null or undefined. Let's try to remove it anyway.");
        }

        try {
            delete peerRTCInstances[peerRtcInstance.id];
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "removePeerRTCInstance: PeerRTC instance removed. id: " + peerRtcInstance.id);
            return unRegisterPeerRTCInstance(peerRtcInstance);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "removePeerRTCInstance: Exception occured while trying to remove PeerRTC instance:"
                + "\n  " + ex.message);
            return false;
        }
    };

    /* Registers PeerRTC event listeners.
     *
     * Parameters:
     *  peerRtcInstance: PeerRTC instance whose event handlers will be registered with.
     *
     * Returns:
     *   True if the event handlers were successfully registered.
     *   False, otherwise.
     */
    var registerPeerRTCInstance = function (peerRtcInstance) {
        var debugMessage = "registerPeerRTCInstance: Registering PeerRTC event listeners...";
        var resultValue = true;

        if (ChatUtility.isNullOrUndefined(peerRtcInstance)) {
            debugMessage += "\n  ERROR: Supplied PeerRTC instance was null or undefined.";
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG, debugMessage);
            return false;
        }

        try {
            peerRtcInstance.addReceivedChatMessageEventHandler(myRoomId, onMessagePeerRTC);
            resultValue = true;
            debugMessage += "\n  PeerRTC.id = " + peerRtcInstance.id + " registered.";
        }
        catch (ex) {
            resultValue = false;
            debugMessage += "\n  Failed to register PeerRTC instance:"
                + "\n  " + ex.message;
        }

        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG, debugMessage);
        return resultValue;
    };

    /* Un-registers PeerRTC event listeners.
     *
     * Parameters:
     *  peerRtcInstance: PeerRTC instance whose event handlers will be un-registered.
     *
     * Returns:
     *   True if the event handlers were successfully un-registered.
     *   False, otherwise.
     */
    var unRegisterPeerRTCInstance = function (peerRtcInstance) {
        if (ChatUtility.isNullOrUndefined(peerRtcInstance)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "unRegisterPeerRTCInstance: PeerRTC instance was null or undefined.");
            return false;
        }

        try {
            peerRtcInstance.removeReceivedChatMessageEventHandler(myRoomId);
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "unRegisterPeerRTCInstance: PeerRTC instance unregistered. id: " + peerRtcInstance.id);
            return true;
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "unRegisterPeerRTCInstance: Exception occured while trying to unregister PeerRTC instance:"
                + "\n  " + ex.message);
            return false;
        }
    };

    /* Registers a function that will be called when the ChatModel receives an
     * incoming ChatTextMessage that results in a change inside the ChatTextMessageBuffer.
     * An incoming ChatTextMessage that can result in a change inside the
     * ChatTextMessageBuffer could be a new message, a message loaded from local
     * storage, or a message resulting in a synchronization request made to another
     * client.
     *
     * Every time this function is called, the supplied callback function will be enqueued.
     * All enqueued functions will be called when a change occurs inside the
     * ChatTextMessageBuffer.
     *
     * Parameters:
     *   callbackFunction([]{ index:<int>, message:<ChatTextMessage> }): Callback function
     *   that will be enqueued and called every time the ChatModel receives a ChatTextMessage.
     *   When called, the callback function will be supplied an array of objects each containing
     *   the index where the buffer change occurred and the ChatTextMessage resulting from the
     *   change.
     */
    this.registerOnMessageHandler = function (callbackFunction) {
        if (!ChatUtility.isFunction(callbackFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "registerOnMessageHandler: Supplied callback function parameter is not a function.");
            return;
        }

        chatTextMessageBuffer.registerOnBufferChangeHandlerQueue(callbackFunction);
    };

    /* Registers a function that will be called when the ChatModel receives a
     * live ChatTextMessage. A "live" ChatTextMessage means it was not sent as
     * part of a local storage loading process or a synchronization process,
     * but instead sent from a client during a live chat session.
     *
     * Parameters:
     *   callbackFunction(ChatTextMessage): Callback function that will be enqueued
     *   and called every time the ChatModel receives a live ChatTextMessage.
     */
    this.registerOnLiveMessageNotifyHandler = function (callbackFunction) {
        if (!ChatUtility.isFunction(callbackFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "registerOnLiveMessageNotifyHandler: Supplied callback function parameter is not a function.");
            return;
        }

        onLiveMessageNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Registers a function that will be called when the ChatModel receives a
     * file share offer.
     *
     * Parameters:
     *   callbackFunction(ChatTextMessage): Callback function that will be enqueued
     *   and called every time the ChatModel receives a file share offer.
     */
    this.registerOnFileOfferNotifyHandler = function (callbackFunction) {
        if (!ChatUtility.isFunction(callbackFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "registerOnFileOfferNotifyHandler: Supplied callback function parameter is not a function.");
            return;
        }

        onFileOfferNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /*
    this.broadcastChatTextMessage = function (messageBody) {
        var max = Number(window.prompt("Number of [" + messageBody + "] messages to create:", "1"));
        for (var i = 0; i < max; i++) {
            this.broadcastChatTextMessage2(
                "Message of unfathomable importance "
                + (i + 1) + " of " + max
                + " broadcast at UTC "
                + (new Date().getTime())
                + " to room id " + myRoomId
                + ", then carefully buffered and streamed to your web browser via Project Defero: "
                + messageBody);
        }
    };
    */

    /* Method broadcasts a chat message to all the PeerRTC instances.
     *
     * Parameters:
     *   messageBody: A text message body that is to be broadcast to
     *                all the PeerRTC instances held by this ChatModel.
     */
    this.broadcastChatTextMessage = function (messageBody) {
        if (ChatUtility.isNullOrUndefined(messageBody)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "broadcastChatTextMessage: Message body is null or undefined.");
            return;
        }

        if (messageBody.length <= 0) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "broadcastChatTextMessage: Message body is empty, so we're not going to send it.");
            return;
        }

        chatTextMessage = new ChatTextMessage();

        chatTextMessage.authorUserId = myUserId;
        chatTextMessage.senderUserId = myUserId;
        chatTextMessage.recipientUserId = myUserId;
        chatTextMessage.roomId = myRoomId;
        chatTextMessage.messageBody = messageBody;
        processArrivingChatMessage(chatTextMessage.getCopy());  // This one is to myself!

        // Tracks count of successful chat message broadcasts.
        var broadcastSuccessCount = 0;
        var broadcastFailureCount = 0;

        iteratePeerRTCInstances(
            function (peerRTCInstance) {
                if (sendChatTextMessage(chatTextMessage.getCopy(), peerRTCInstance)) {
                    broadcastSuccessCount++;
                }
                else {
                    broadcastFailureCount++;
                }
            });

        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "broadcastChatTextMessage: " + broadcastSuccessCount + " successful broadcasts, "
            + broadcastFailureCount + " failed broadcasts.");
    };

    /* Method broadcasts a file to all the PeerRTC instances.
     *
     * Paramters:
     *   inputFile: A window.File object instance that defines the file to be broadcast.
     */
    this.broadcastFile = function (windowFile) {
        if (ChatUtility.isFileObjectCorrupt(windowFile)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatModel.broadCastFile: windowFile is corrupt.");
            return;
        }

        if (globalDeferoChatReleaseMode === ChatUtility.RELEASE_MODE.DEVELOPER_DEBUG) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatModel.broadCastFile:" + "\n " + (new ChatFileSender(windowFile, myUserId, myRoomId, "-1")).toString());
        }

        /* Returns true if the supplied PeerRTC instance is associated with the Room
         * we're in and is associated with an online user.
         */
        var isPeerOnline = function (peerRTCInstance) {
            try {
                var usersInRoom = myRoomsControllerRef.get_room(Number(myRoomId)).Users;
                for (var i = 0; i < usersInRoom.length; i++) {
                    if (usersInRoom[i].UserId === peerRTCInstance.id
                        && usersInRoom[i].Online) {
                        return true;
                    }
                }

                return false;
            }
            catch (ex) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatModel.broadCastFile: Exception occured when checking if"
                    + " PeerRTC instance is associated with an online member of this Room:"
                    + "\n  " + ex.message);

                return false;
            }
        };

        // Prevent sending he same file mroe than once to the same user.
        var duplicatePeerPreventionIndex = {};

        iteratePeerRTCInstances(
            function (peerRTCInstance) {
                if (isPeerOnline(peerRTCInstance) && !(String(peerRTCInstance.id) in duplicatePeerPreventionIndex)) {
                    duplicatePeerPreventionIndex[String(peerRTCInstance.id)] = true;
                    var chatFileSender = new ChatFileSender(windowFile, myUserId, myRoomId, String(peerRTCInstance.id));
                    chatFileSender.peerRTCInstance = peerRTCInstance;
                    activeChatFileSenderSet[chatFileSender.getUniqueId()] = chatFileSender;

                    var chatTextMessageFileTransmission = new ChatTextMessage();

                    chatTextMessageFileTransmission.authorUserId = myUserId;
                    chatTextMessageFileTransmission.senderUserId = myUserId;
                    chatTextMessageFileTransmission.recipientUserId = myUserId;
                    chatTextMessageFileTransmission.roomId = myRoomId;
                    chatTextMessageFileTransmission.chatFileMetadata = chatFileSender.getMetadata();
                    chatTextMessageFileTransmission.messageType = ChatTextMessage.TYPE.FILE_TRANSMISSION;

                    // Assign callbacks to update the ChatTextMessage's ChatFileMetadata object.
                    var chatTextMessageCallback = function (metadata) {
                        chatTextMessageFileTransmission.chatFileMetadata = metadata;

                        switch (chatTextMessageFileTransmission.chatFileMetadata.offerState) {
                            case ChatFileMetadata.STATE.OFFER_ACCEPTED:
                                chatTextMessageFileTransmission.messageBody = "File offer accepted. Starting upload...";
                                break;
                            case ChatFileMetadata.STATE.OFFER_REJECTED:
                                chatTextMessageFileTransmission.messageBody = "Your file offer was rejected.";
                                break;
                            case ChatFileMetadata.STATE.OFFER_PENDING:
                                chatTextMessageFileTransmission.messageBody = "Waiting for file offer recipient's response.";
                                break;
                            case ChatFileMetadata.STATE.OFFER_EXPIRED:
                                chatTextMessageFileTransmission.messageBody = "File offer has expired.";
                                break;
                            case ChatFileMetadata.STATE.TRANSMISSION_IN_PROGRESS:
                                chatTextMessageFileTransmission.messageBody
                                    = "Uploading file...";
                                break;
                            case ChatFileMetadata.STATE.TRANSMISSION_FAILED:
                                chatTextMessageFileTransmission.messageBody = "File upload failed.";
                                break;
                            case ChatFileMetadata.STATE.TRANSMISSION_SUCCEEDED:
                                chatTextMessageFileTransmission.messageBody = "File upload complete.";
                                break;
                            case ChatFileMetadata.STATE.TRANSMISSION_CANCELLED:
                                chatTextMessageFileTransmission.messageBody = "File upload canceled.";
                                break;
                        }
                    };
                    var chatTextMessageCallback_updateLocalDatabase = function (metadata) {
                        chatTextMessageCallback(metadata);

                        var archivedMessage = chatTextMessageFileTransmission.getCopy();
                        archivedMessage.chatFileMetadata.offerState = ChatFileMetadata.STATE.ARCHIVED;
                        chatDatabase.persistMessageOverwrite(archivedMessage);
                    };
                    chatFileSender.addWaitingForFileOfferResponseCallback(chatTextMessageCallback_updateLocalDatabase);
                    chatFileSender.addFileOfferRejectedCallback(chatTextMessageCallback_updateLocalDatabase);
                    chatFileSender.addFileOfferAcceptedCallback(chatTextMessageCallback_updateLocalDatabase);
                    chatFileSender.addFileOfferNoResponseCallback(chatTextMessageCallback_updateLocalDatabase);
                    chatFileSender.addFileTransmissionFailureCallback(chatTextMessageCallback_updateLocalDatabase);
                    chatFileSender.addFileTransmissionCompleteCallback(chatTextMessageCallback_updateLocalDatabase);
                    chatFileSender.addFileRetrievalCancellationCallback(chatTextMessageCallback_updateLocalDatabase);
                    chatFileSender.addFileTransmissionProgressCallback(chatTextMessageCallback); // Don't update the local database with this one!

                    // Add function that removes the ChatFileSender from the ChatModel.activeChatFileSenderSet.
                    chatFileSender.addSelfCleanupCallback(
                        function (chatFileMetadata) {
                            var key = chatFileMetadata.uniqueIdentifier;
                            if (key in activeChatFileSenderSet) {
                                delete activeChatFileSenderSet[key];
                                ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                    , "ChatFileSender deleted from activeChatFileSenderSet:"
                                    + "\n " + chatFileMetadata.toString());
                            }
                        });

                    // Assign others' callbacks
                    waitingForFileOfferResponseNotifyQueue.iterateAll(
                        function (callback) {
                            chatFileSender.addWaitingForFileOfferResponseCallback(callback);
                        });

                    fileOfferRejectedNotifyQueue.iterateAll(
                        function (callback) {
                            chatFileSender.addFileOfferRejectedCallback(callback);
                        });

                    fileOfferAcceptedNotifyQueue.iterateAll(
                        function (callback) {
                            chatFileSender.addFileOfferAcceptedCallback(callback);
                        });

                    fileOfferNoResponseNotifyQueue.iterateAll(
                        function (callback) {
                            chatFileSender.addFileOfferNoResponseCallback(callback);
                        });

                    fileTransmissionFailureNotifyQueue.iterateAll(
                        function (callback) {
                            chatFileSender.addFileTransmissionFailureCallback(callback);
                        });

                    fileTransmissionCompleteNotifyQueue.iterateAll(
                        function (callback) {
                            chatFileSender.addFileTransmissionCompleteCallback(callback);
                        });

                    fileTransmissionProgressNotifyQueue.iterateAll(
                        function (callback) {
                            chatFileSender.addFileTransmissionProgressCallback(callback);
                        });

                    fileRetrievalCancellationNotifyQueue.iterateAll(
                        function (callback) {
                            chatFileSender.addFileRetrievalCancellationCallback(callback);
                        });

                    chatFileSender.sendFileOffer();
                    processArrivingChatMessage(chatTextMessageFileTransmission);
                }
            });
    };

    /* Adds a callback function that will be called when a ChatFileSender has sent
     * a file download offer to the recipient and is now waiting for a reply.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addWaitingForFileOfferResponseCallback = function (callbackFunction) {
        waitingForFileOfferResponseNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when the ChatModel has received
     * a file download offer.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileOfferReceivedCallback = function (callbackFunction) {
        fileOfferReceivedNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when the ChatModel receives
     * a file offer rejection.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileOfferRejectedCallback = function (callbackFunction) {
        fileOfferRejectedNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when the file offer recipient
     * accepts the file offer.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileAcceptedCallback = function (callbackFunction) {
        fileOfferAcceptedNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file offer response wait
     * period has expired with no reponse from the recipient.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileOfferNoResponseCallback = function (callbackFunction) {
        fileOfferNoResponseNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a full file transmission completes.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileTransmissionCompleteCallback = function (callbackFunction) {
        fileTransmissionCompleteNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a full file retrieval completes.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileRetrievalCompleteCallback = function (callbackFunction) {
        fileRetrievalCompleteNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file transmission fails.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileTransmissionFailureCallback = function (callbackFunction) {
        fileTransmissionFailureNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file retrieval fails.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileRetrievalFailureCallback = function (callbackFunction) {
        fileRetrievalFailureNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file transmission fails.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileTransmissionProgressCallback = function (callbackFunction) {
        fileTransmissionProgressNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file chunk retrieval completes.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addRetrievalProgressCallback = function (callbackFunction) {
        fileRetrievalProgressNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file transfer is cancelled by the sender.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileTransmissionCancellationCallback = function (callbackFunction) {
        fileTransmissionCancellationNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Adds a callback function that will be called when a file transfer is cancelled by the receiver.
     *
     * Parameters:
     *   callbackFunction(ChatFileMetadata): Callback function that will be called.
     */
    this.addFileRetrievalCancellationCallback = function (callbackFunction) {
        fileRetrievalCancellationNotifyQueue.addCallbackFuntion(callbackFunction);
    };

    /* Method sends a ChatFileMetadata as a file offer response. The recipient to
     * the response is the user who sent the file offer.
     *
     * Paramters:
     *   chatFileMetadata: ChatFileMetadata that contains the file offer reponse
     *                     and that defines who made the file offer.
     */
    this.fileOfferResponse = function (chatFileMetadata) {
        var uniqueIdentifier = String(chatFileMetadata.uniqueIdentifier);
        if (uniqueIdentifier in activeChatFileReceiverSet) {
            activeChatFileReceiverSet[uniqueIdentifier].sendFileOfferResponse(chatFileMetadata);

            if (chatFileMetadata.offerState === ChatFileMetadata.STATE.OFFER_REJECTED) {
                delete activeChatFileReceiverSet[uniqueIdentifier];

                ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatFileSender deleted from activeChatFileSenderSet:"
                    + "\n " + chatFileMetadata.toString());
            }
        }
    };

    /* Method sends a cancellation message to the file receiver.
     */
    this.cancelFileTransmission = function (chatFileMetadata) {
        var uniqueIdentifier = String(chatFileMetadata.uniqueIdentifier);
        if (uniqueIdentifier in activeChatFileSenderSet) {
            activeChatFileSenderSet[uniqueIdentifier].cancelFileTransmission(chatFileMetadata);
        }
    };

    /* Method sends a cancellation message to the file sender.
     */
    this.cancelFileRetrieval = function (chatFileMetadata) {
        var uniqueIdentifier = String(chatFileMetadata.uniqueIdentifier);
        if (uniqueIdentifier in activeChatFileReceiverSet) {
            activeChatFileReceiverSet[uniqueIdentifier].cancelFileRetrieval(chatFileMetadata);
        }
    };

    /* Method iterates over the PeerRTC instances stored in ChatModel.peerRTCInstances
     * and passes them to the supplied callback function.
     *
     * Parameters:
     *   callbackFunction(PeerRTC): Callback function that will be supplied PeerRTC
     *                              instances from ChatModel.peerRTCInstances.
     */
    var iteratePeerRTCInstances = function (callbackFunction) {
        // Stores PeerRTC instances identified as removal candidates during iteration.
        var peerRTCDRemovalCandidates = [];

        ChatUtility.iterateAssociativeArray(
            peerRTCInstances,
            function (key, peerInstance) {
                if (ChatUtility.isNullOrUndefined(peerInstance)
                    || ChatUtility.isNullOrUndefined(peerInstance.id)) {
                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "iteratePeerRTCInstances: A null or undefined PeerRTC instance was discovered"
                        + " and will be removed from the ChatModel's list.");

                    // Queue the problem instance for removal.
                    peerRTCDRemovalCandidates.push(peerInstance);
                }
                else {
                    callbackFunction(peerInstance);
                }
            }, null);

        // Remove any trouble makers identified during broadcast.
        var length = peerRTCDRemovalCandidates.length;
        for (var i = 0; i < length; i++) {
            this.removePeerRTCInstance(peerRTCDRemovalCandidates[i]);
        }
    }.bind(this);

    /* Sends a ChatTextMessage to a single endpoint.
     *
     * Parameters:
     *
     *   chatTextMessage: A ChatTextMessage object that is to be sent through the
     *                    peerRTCInstance.
     *
     *   peerRTCInstance: PeerRTC instance through which the message will be sent.
     *
     * Returns:
     *  True if ChatTextMessage was sent successfully.
     *  False, otherwise.
     */
    var sendChatTextMessage = function (chatTextMessage, peerRTCInstance) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatTextMessage: ChatTextMessage was corrupt.");
            return false;
        }

        if (ChatUtility.isNullOrUndefined(peerRTCInstance)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatTextMessage: PeerRTC instance was null or undefined.");
            return false;
        }

        if (ChatUtility.isNullOrUndefined(peerRTCInstance.id)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatTextMessage: PeerRTC.id was null or undefined.");
            return false;
        }

        chatTextMessage.roomId = myRoomId;
        chatTextMessage.senderUserId = myUserId;
        chatTextMessage.recipientUserId = String(peerRTCInstance.id);

        var result = sendChatClientMessage(
              new ChatClientMessage(myRoomId, ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE, chatTextMessage.getCopy())
            , peerRTCInstance);

        if (result) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatTextMessage: Message sent: " + chatTextMessage.toString());
        }
        else {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatTextMessage: Failed to send message: " + chatTextMessage.toString());
        }

        return result;
    };

    /* Method forces ChatModel to send ChatMessageHistoryRequests to all the PeerRTC instances
     * currently known to this ChatModel instance whether or not synchronization through those
     * PeerRTC instances has already taken place.
     */
    this.forceChatMessageHistorySync = function () {
        iteratePeerRTCInstances(
            function (peerRTCInstance) {
                this.sendChatMessageHistorySyncRequest(peerRTCInstance);
            });
    };

    /* Sends a ChatMessageHistoryRequest to a single endpoint.
     *
     * Parameters:
     *   peerRTCInstance: PeerRTC instance through which the message will be sent.
     */
    this.sendChatMessageHistorySyncRequest = function (peerRTCInstance) {
        if (ChatUtility.isNullOrUndefined(peerRTCInstance)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatMessageHistorySyncRequest: PeerRTC instance was null or undefined.");
            return;
        }

        if (ChatUtility.isNullOrUndefined(peerRTCInstance.id)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatMessageHistorySyncRequest: PeerRTC.id was null or undefined.");
            return;
        }

        var peerId = String(peerRTCInstance.id);

        if (ChatUtility.isNullOrUndefined(chatDatabase)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatMessageHistorySyncRequest: chatDatabase is null or undefined: cannot request ChatMessages from "
                + peerId);
            return;
        }

        chatDatabase.registerOnChatLocalProfileEncapsulationInitializedHandler(
            function () {
                myRootScope.users.get_user_room(
                    myUserId,
                    myRoomId,
                    function (retUser) {
                        var roomJoinTimestampUTC = Number(retUser.JoinTime);

                        if (ChatUtility.isNullOrUndefined(roomJoinTimestampUTC)) {
                            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                , "sendChatMessageHistorySyncRequest: roomJoinTimestampUTC is null or undefined. Defaulting to current UTC milliseconds.");

                            roomJoinTimestampUTC = ChatTimer.getDeferoUTCmsec();
                        }

                        var chatMessageHistoryRequest = new ChatMessageHistoryRequest()
                        chatMessageHistoryRequest.senderUserId = myUserId;
                        chatMessageHistoryRequest.roomId = myRoomId;

                        var lastChatMessageReceived = chatDatabase.getChatLastMessageReceived(myUserId, peerId, myRoomId);
                        var lastChatMessageSent = chatDatabase.getChatLastMessageSent(myUserId, peerId, myRoomId);

                        var lastChatMessageReceived_timestampUTC = roomJoinTimestampUTC;
                        var lastChatMessageSent_timestampUTC = roomJoinTimestampUTC;

                        if (!ChatUtility.isNullOrUndefined(lastChatMessageReceived)) {
                            lastChatMessageReceived_timestampUTC = lastChatMessageReceived.timestampUTC;
                        }

                        if (!ChatUtility.isNullOrUndefined(lastChatMessageSent)) {
                            lastChatMessageSent_timestampUTC = lastChatMessageSent.timestampUTC;
                        }

                        if (lastChatMessageReceived_timestampUTC < roomJoinTimestampUTC) {
                            lastChatMessageReceived_timestampUTC = roomJoinTimestampUTC;
                        }

                        if (lastChatMessageSent_timestampUTC < roomJoinTimestampUTC) {
                            lastChatMessageSent_timestampUTC = roomJoinTimestampUTC;
                        }

                        /* Consider the timestamp of the last message received from the requestee
                         * the timestamp of the last message I sent to the requestee and use the
                         * older of the two.
                         *
                         * If either of these timestamps is older than my room join date, them use
                         * my room join date.
                         */

                        chatMessageHistoryRequest.timestampUTC = Math.min(lastChatMessageReceived_timestampUTC, lastChatMessageSent_timestampUTC);

                        var result = sendChatClientMessage(
                            new ChatClientMessage(
                                  myRoomId
                                , ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_REQUEST
                                , chatMessageHistoryRequest)
                            , peerRTCInstance);

                        if (result) {
                            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                , "sendChatMessageHistorySyncRequest: ChatMessageHistoryRequest sent: "
                                + chatMessageHistoryRequest.toString());
                        }
                        else {
                            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                , "sendChatMessageHistorySyncRequest: Failed to send message: "
                                + chatMessageHistoryRequest.toString());
                        }
                    });
            });
    };

    /* Sends a ChatClientMessage to a single endpoint.
     *
     * Parameters:
     *
     *   chatClientMessage: A ChatClientMessage object that is to be sent through the
     *                      peerRTCInstance.
     *
     *   peerRTCInstance: PeerRTC instance throught which the ChatClientMessage will
     *                    be sent.
     *
     * Returns:
     *  True if ChatClientMessage was sent successfully.
     *  False, otherwise.
     */
    var sendChatClientMessage = function (chatClientMessage, peerRTCInstance) {
        if (ChatUtility.isNullOrUndefined(chatClientMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatClientMessage: ChatTextMessage was null or undefined.");
            return false;
        }

        if (ChatUtility.isNullOrUndefined(peerRTCInstance)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatClientMessage: PeerRTC instance was null or undefined.");
            return false;
        }

        try {
            return peerRTCInstance.sendMessage(chatClientMessage);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "sendChatClientMessage: Exception was thrown during message send:"
                + "\n  " + ex.message);
            return false;
        }

        return false;
    };

    /* Processes onMessage callbacks from PeerRTC.
     *
     * Parameters:
     *   rtcMessage: onMessage object from PeerRTC.onMessage.
     */
    var onMessagePeerRTC = function (rtcMessage) {
        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "onMessagePeerRTC: Message received.");

        if (ChatUtility.isNullOrUndefined(rtcMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "onMessagePeerRTC: rtcMessage parameter is null or undefined.");
            return;
        }

        if (ChatUtility.isNullOrUndefined(rtcMessage.message)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "onMessagePeerRTC: rtcMessage.message parameter is null or undefined.");
            return;
        }

        var chatClientMessage = rtcMessage.message;

        if (ChatUtility.isNullOrUndefined(chatClientMessage.messagePayload)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "onMessagePeerRTC: chatClientMessage.messagePayload is null or undefined.");
            return;
        }

        if (chatClientMessage.roomId !== myRoomId) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "onMessagePeerRTC: chatClientMessage.roomId !== ChatModel.myRoomId:"
                + "\n  (" + chatClientMessage.roomId + " !== " + myRoomId + ")");
            return;
        }

        var checksumValid = false;

        try { checksumValid = chatClientMessage.validChecksum(); }
        catch (ex) { }

        if (!checksumValid) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatModel.onMessagePeerRTC: ClientMessage checksum invalid.");
        }

        switch (chatClientMessage.messagePayloadType) {
            case ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE:
                processArrivingChatMessage(chatClientMessage.messagePayload);

                if (principleMessageFilter.filterChatTextMessage(chatClientMessage.messagePayload)) {
                    onLiveMessageNotifyQueue.processAll(chatClientMessage.messagePayload);
                }
                break;
            case ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_REQUEST:
                processArrivingChatMessageHistoryRequest(chatClientMessage.messagePayload);
                break;
            case ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_TEXT_MESSAGE:
                processArrivingChatMessage(chatClientMessage.messagePayload);
                break;


            case ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_TEXT_MESSAGE_SET:
                var chatTextMessageSet = chatClientMessage.messagePayload;
                var chatTextMessagePreBuffer = new ChatTextMessageBuffer();
                var chatTextMessageAcknowledgmentSet = new ChatTextMessageSet();
                chatTextMessagePreBuffer.setChatBufferLength(chatTextMessageBuffer.getChatBufferLength());

                chatTextMessageSet.processAllChatTextMessages(
                    function (chatTextMessage) {
                        chatTextMessagePreBuffer.insertMessageNoNotify(chatTextMessage);

                        // Only add chat messages to the database if their checksum is valid.
                        if (checksumValid) {
                            // Add the ChatTextMessage to local database.
                            if (principleMessageFilter.filterChatTextMessage(chatTextMessage)) {
                                chatDatabase.persistMessage(
                                    chatTextMessage.getCopy(),
                                    function (err) {
                                        if (ChatUtility.isNullOrUndefined(err)) {
                                            var emptyBodyChatTextMessage = chatTextMessage.getCopy();
                                            emptyBodyChatTextMessage.setToEmpty();
                                            emptyBodyChatTextMessage.senderUserId = myUserId;
                                            emptyBodyChatTextMessage.recipientUserId = rtcMessage.source;
                                            chatTextMessageAcknowledgmentSet.addChatTextMessage(emptyBodyChatTextMessage);

                                            if (chatTextMessageAcknowledgmentSet.getChatTextMessageCount() === chatTextMessageSet.getChatTextMessageCount()
                                                && rtcMessage.source in peerRTCInstances) {
                                                sendChatClientMessage(
                                                      new ChatClientMessage(
                                                          myRoomId
                                                        , ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE_RECEIVED_ACK_SET
                                                        , chatTextMessageAcknowledgmentSet)
                                                    , peerRTCInstances[rtcMessage.source]);
                                            }
                                        }
                                    });
                            }
                        }
                    });

                // Display messages whether or not their checksums are valid.
                chatTextMessagePreBuffer.processAllChatTextMessages(chatTextMessageBuffer.insertMessageNoNotify);
                chatTextMessageBuffer.processOnBufferChangeHandlerQueue();
                break;

            // A client who received the message you sent him is responding with an ACK.
            case ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE_RECEIVED_ACK:
                var chatTextMessage = chatClientMessage.messagePayload;
                chatTextMessage.setToEmpty();
                chatDatabase.persistChatLastMessageSentSet(myUserId, chatTextMessage);
                break;

            // A client who received the message set you sent him is responding with an ACK set.
            case ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE_RECEIVED_ACK_SET:
                if (checksumValid) {
                    var chatTextMessageAcknowledgmentSet = chatClientMessage.messagePayload;

                    chatTextMessageAcknowledgmentSet.processAllChatTextMessages(
                        function (chatTextMessage) {
                            chatTextMessage.setToEmpty();
                            chatDatabase.persistChatLastMessageSentSet(myUserId, chatTextMessage);
                        });
                }
                break;

            /* We've received a file offer.
             */
            case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_OFFER:
                ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatModel.onMessagePeerRTC: Received chat file offer:\n"
                    + chatClientMessage.messagePayload.toString());

                /* We can ignore this offer if we've already received an offer for the same file
                 * from the same sender.
                 */
                var chatFileMetadata = chatClientMessage.messagePayload;
                if (!(chatFileMetadata.uniqueIdentifier in activeChatFileReceiverSet)) {
                    if (chatFileMetadata.senderUserId in peerRTCInstances) {
                        chatFileReceiver = new ChatFileReceiver(chatFileMetadata);
                        chatFileReceiver.peerRTCInstance = peerRTCInstances[chatFileMetadata.senderUserId];
                        activeChatFileReceiverSet[chatFileReceiver.getUniqueId()] = chatFileReceiver;

                        var chatTextMessageFileTransmission = new ChatTextMessage();

                        chatTextMessageFileTransmission.authorUserId = chatFileMetadata.senderUserId;
                        chatTextMessageFileTransmission.senderUserId = chatFileMetadata.senderUserId;
                        chatTextMessageFileTransmission.recipientUserId = myUserId;
                        chatTextMessageFileTransmission.roomId = myRoomId;
                        chatTextMessageFileTransmission.chatFileMetadata = chatFileReceiver.getMetadata();
                        chatTextMessageFileTransmission.messageType = ChatTextMessage.TYPE.FILE_TRANSMISSION;

                        // Assign callbacks to update the ChatTextMessage's ChatFileMetadata object.
                        var chatTextMessageCallback = function (metadata) {
                            chatTextMessageFileTransmission.chatFileMetadata = metadata;

                            switch (chatTextMessageFileTransmission.chatFileMetadata.offerState) {
                                case ChatFileMetadata.STATE.OFFER_ACCEPTED:
                                    chatTextMessageFileTransmission.messageBody = "File offer accepted. Starting download...";
                                    break;
                                case ChatFileMetadata.STATE.OFFER_REJECTED:
                                    chatTextMessageFileTransmission.messageBody = "File offer rejected.";
                                    break;
                                case ChatFileMetadata.STATE.OFFER_PENDING:
                                    chatTextMessageFileTransmission.messageBody = "Please accept or reject the file offer.";
                                    break;
                                case ChatFileMetadata.STATE.OFFER_EXPIRED:
                                    chatTextMessageFileTransmission.messageBody = "File offer has expired.";
                                    break;
                                case ChatFileMetadata.STATE.TRANSMISSION_IN_PROGRESS:
                                    chatTextMessageFileTransmission.messageBody
                                        = "Downloading file...";
                                    break;
                                case ChatFileMetadata.STATE.TRANSMISSION_FAILED:
                                    chatTextMessageFileTransmission.messageBody = "File download failed.";
                                    break;
                                case ChatFileMetadata.STATE.TRANSMISSION_SUCCEEDED:
                                    chatTextMessageFileTransmission.messageBody = "File download complete.";
                                    break;
                                case ChatFileMetadata.STATE.TRANSMISSION_CANCELLED:
                                    chatTextMessageFileTransmission.messageBody = "File download canceled.";
                                    break;
                            }
                        };
                        var chatTextMessageCallback_updateLocalDatabase = function (metadata) {
                            chatTextMessageCallback(metadata);

                            var archivedMessage = chatTextMessageFileTransmission.getCopy();
                            archivedMessage.chatFileMetadata.offerState = ChatFileMetadata.STATE.ARCHIVED;
                            chatDatabase.persistMessageOverwrite(archivedMessage);
                        };
                        chatFileReceiver.addFileOfferReceivedCallback(chatTextMessageCallback_updateLocalDatabase);
                        chatFileReceiver.addFileRetrievalFailureCallback(chatTextMessageCallback_updateLocalDatabase);
                        chatFileReceiver.addFileRetrievalCompleteCallback(chatTextMessageCallback_updateLocalDatabase);
                        chatFileReceiver.addFileTransmissionCancellationCallback(chatTextMessageCallback_updateLocalDatabase);
                        chatFileReceiver.addRetrievalProgressCallback(chatTextMessageCallback); // Don't update the local database with this one!

                        // Add function that removes a ChatFileReceiver from the ChatModel.activeChatFileReceiverSet.
                        chatFileReceiver.addSelfCleanupCallback(
                            function (chatFileMetadata) {
                                var key = chatFileMetadata.uniqueIdentifier;
                                if (key in activeChatFileReceiverSet) {
                                    delete activeChatFileReceiverSet[key];
                                    ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                        , "ChatFileReceiver deleted from activeChatFileReceiverSet:"
                                        + "\n " + chatFileMetadata.toString());
                                }
                            });

                        // Assign others' callbacks.
                        fileOfferReceivedNotifyQueue.iterateAll(
                            function (callback) {
                                chatFileReceiver.addFileOfferReceivedCallback(callback);
                            });

                        fileRetrievalFailureNotifyQueue.iterateAll(
                            function (callback) {
                                chatFileReceiver.addFileRetrievalFailureCallback(callback);
                            });

                        fileRetrievalCompleteNotifyQueue.iterateAll(
                            function (callback) {
                                chatFileReceiver.addFileRetrievalCompleteCallback(callback);
                            });

                        fileRetrievalProgressNotifyQueue.iterateAll(
                            function (callback) {
                                chatFileReceiver.addRetrievalProgressCallback(callback);
                            });

                        fileTransmissionCancellationNotifyQueue.iterateAll(
                            function (callback) {
                                chatFileReceiver.addFileTransmissionCancellationCallback(callback);
                            });

                        chatFileReceiver.processFileOffer(chatFileMetadata);
                        onFileOfferNotifyQueue.processAll(chatTextMessageFileTransmission);
                        processArrivingChatMessage(chatTextMessageFileTransmission);
                    }
                }
                break;

            case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_OFFER_RESPONSE:
                ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatModel.onMessagePeerRTC: Received chat file offer response:");

                var fileUniqueId = chatClientMessage.messagePayload.uniqueIdentifier;
                if (fileUniqueId in activeChatFileSenderSet) {
                    activeChatFileSenderSet[fileUniqueId].processFileOfferResponse(chatClientMessage.messagePayload);
                }
                break;

            // We have received a chunk of file transfer data.
            case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_DATA_CHUNK:
                var chatFileMetadata = chatClientMessage.messagePayload;
                if (chatFileMetadata.uniqueIdentifier in activeChatFileReceiverSet) {
                    activeChatFileReceiverSet[chatFileMetadata.uniqueIdentifier].processDataChunk(chatFileMetadata);
                }
                break;

            case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_DATA_CHUNK_ACK:
                var chatFileMetadata = chatClientMessage.messagePayload;
                if (chatFileMetadata.uniqueIdentifier in activeChatFileSenderSet) {
                    activeChatFileSenderSet[chatFileMetadata.uniqueIdentifier].processDataChunkACK(chatFileMetadata);
                }
                break;

            case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_SENDER_CANCEL_TRANSFER:
                var chatFileMetadata = chatClientMessage.messagePayload;
                if (chatFileMetadata.uniqueIdentifier in activeChatFileReceiverSet) {
                    activeChatFileReceiverSet[chatFileMetadata.uniqueIdentifier].processSenderCancel(chatFileMetadata);
                }
                break;

            case ChatClientMessage.PAYLOAD_TYPE.CHAT_FILE_RECEIVER_CANCEL_TRANSFER:
                var chatFileMetadata = chatClientMessage.messagePayload;
                if (chatFileMetadata.uniqueIdentifier in activeChatFileSenderSet) {
                    activeChatFileSenderSet[chatFileMetadata.uniqueIdentifier].processReceiverCancel(chatFileMetadata);
                }
                break;

            default:
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatModel.onMessagePeerRTC: ChatClientMessage.PAYLOAD_TYPE = ["
                    + chatClientMessage.messagePayloadType + "] is not known to ChatModel.");
                break;
        }
    };

    /* Processes arriving ChatTextMessage objects.
     *
     * Parameters:
     *   chatTextMessage: Arriving ChatTextMessage to be processed.
     */
    var processArrivingChatMessage = function (chatTextMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "processArrivingChatMessage: chatTextMessage parameter is corrupt.");
            return;
        }

        /* Proceed no further if this message isn't meant for display by the
         * current user or current chat room.
         */
        if (!principleMessageFilter.filterChatTextMessage(chatTextMessage)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatModel.processArrivingChatMessage: Ignoring someone else's message:\n"
                + chatTextMessage.toString());
            return;
        }

        var unAdjustedUTC_chatTextMessage = chatTextMessage.getCopy();
        chatTextMessage.timestampUTC = ChatTimer.getDeferoUTCmsec();
        chatTextMessageBuffer.insertMessageNotifyChanges(chatTextMessage);

        // Add the ChatTextMessage to local database.
        if (!ChatUtility.isNullOrUndefined(chatDatabase)) {
            chatDatabase.persistMessage(
                unAdjustedUTC_chatTextMessage.getCopy(),
                function (err) {
                    if (ChatUtility.isNullOrUndefined(err)
                        && unAdjustedUTC_chatTextMessage.senderUserId in peerRTCInstances) {
                        unAdjustedUTC_chatTextMessage.setToEmpty();
                        sendChatClientMessage(
                              new ChatClientMessage(myRoomId, ChatClientMessage.PAYLOAD_TYPE.CHAT_TEXT_MESSAGE_RECEIVED_ACK, unAdjustedUTC_chatTextMessage)
                            , peerRTCInstances[unAdjustedUTC_chatTextMessage.senderUserId]);
                    }
                });
        }
    };

    /* Processes arriving ChatMessageHistoryRequest objects.
     *
     * Parameters:
     *   chatMessageHistoryRequest: Arriving ChatMessageHistoryRequest to be processed.
     */
    var processArrivingChatMessageHistoryRequest = function (chatMessageHistoryRequest) {
        if (ChatUtility.isNullOrUndefined(chatMessageHistoryRequest)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "processArrivingChatMessageHistoryRequest: chatMessageHistoryRequest parameter is null or undefined.");
            return;
        }

        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "processArrivingChatMessageHistoryRequest: " + chatMessageHistoryRequest.toString());

        myRootScope.users.get_user_room(
            chatMessageHistoryRequest.senderUserId,
            myRoomId,
            function (retUser) {
                var requesterRoomJoinTimestampUTC = Number(retUser.JoinTime);

                if (ChatUtility.isNullOrUndefined(requesterRoomJoinTimestampUTC)) {
                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "processArrivingChatMessageHistoryRequest: requesterRoomJoinTimestampUTC is null or undefined. Defaulting to current UTC milliseconds.");

                    requesterRoomJoinTimestampUTC = ChatTimer.getDeferoUTCmsec();
                }

                var chatTextMessageSet = new ChatTextMessageSet();
                var listCount = 0;

                var callbackProcess =
                    function (key, chatTextMessage) {
                        if (!principleMessageFilter.filterChatTextMessage(chatTextMessage)
                            || chatMessageHistoryRequest.roomId !== chatTextMessage.roomId
                            || chatMessageHistoryRequest.timestampUTC >= chatTextMessage.timestampUTC
                            || requesterRoomJoinTimestampUTC >= chatTextMessage.timestampUTC) {
                            return;
                        }

                        chatTextMessage.senderUserId = myUserId;
                        chatTextMessage.recipientUserId = chatMessageHistoryRequest.senderUserId;

                        chatTextMessageSet.addChatTextMessage(chatTextMessage);
                        listCount++;

                        if (listCount >= ChatModel.DEFAULT_MAX_SYNCHRONIZATION_BATCH_SIZE) {
                            sendChatClientMessage(
                                new ChatClientMessage(
                                      myRoomId
                                    , ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_TEXT_MESSAGE_SET
                                    , chatTextMessageSet)
                                , peerRTCInstances[chatMessageHistoryRequest.senderUserId]);

                            listCount = 0;
                            chatTextMessageSet = new ChatTextMessageSet();
                        }
                    };

                var callbackComplete =
                    function () {
                        if (!(chatMessageHistoryRequest.senderUserId in peerRTCInstances)) {
                            return;
                        }

                        if (chatTextMessageSet.getChatTextMessageCount() > 0) {
                            sendChatClientMessage(
                                new ChatClientMessage(
                                      myRoomId
                                    , ChatClientMessage.PAYLOAD_TYPE.CHAT_HISTORY_TEXT_MESSAGE_SET
                                    , chatTextMessageSet)
                                , peerRTCInstances[chatMessageHistoryRequest.senderUserId]);
                        }
                    };

                chatDatabase.processAllChatTextMessages(callbackProcess, callbackComplete);
            });
    };

    /* Processes ChatTextMessage objects already in some kind of possession
     * of the ChatModel.
     *
     * Parameters:
     *   chatTextMessage: ChatTextMessage to be processed.
     */
    var processChatTextMessage = function (chatTextMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "processChatTextMessage: chatTextMessage parameter is corrupt.");
            return;
        }

        /* Proceed no further if this message isn't meant for display by the
         * current user or current chat room.
         */
        if (!principleMessageFilter.filterChatTextMessage(chatTextMessage)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatModel.processChatTextMessage: Ignoring someone else's message:\n"
                + chatTextMessage.toString());
            return;
        }
        else
        {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatModel.processChatTextMessage: "
                + chatTextMessage.toString());
        }

        chatTextMessageBuffer.insertMessageNotifyChanges(chatTextMessage);
    }.bind(this);

    /* Read and process all local ChatTextMessage history.
     *
     * Parameters:
     *   userSuppliedFilter: ChatTextMessageFilter object that will be applied in addition
     *                       to any default filters while loading chat history.
     */
    this.loadChatHistory = function (newUserSuppliedFilter, callback) {
        if (!ChatUtility.isNullOrUndefined(newUserSuppliedFilter)) {
            userSuppliedFilter = newUserSuppliedFilter;
        }
        else if (ChatUtility.isNullOrUndefined(userSuppliedFilter)) {
            userSuppliedFilter = new ChatTextMessageFilter();
        }

        var chatTextMessagePreBuffer = new ChatTextMessageBuffer();
        chatTextMessagePreBuffer.setChatBufferLength(chatTextMessageBuffer.getChatBufferLength());

        var callbackProcess =
            function (key, chatTextMessage) {
                /* Verify this ChatMessage is meant for display for this user in
                 * this chat room.
                 */
                if (principleMessageFilter.filterChatTextMessage(chatTextMessage)
                    && userSuppliedFilter.filterChatTextMessage(chatTextMessage)) {
                    chatTextMessagePreBuffer.insertMessageNoNotify(chatTextMessage);
                }
            };

        var callbackComplete =
            function () {
                chatTextMessageBuffer.clearBuffer();
                chatTextMessagePreBuffer.processAllChatTextMessages(chatTextMessageBuffer.insertMessageNoNotify);
                chatTextMessageBuffer.processOnBufferChangeHandlerQueue();
                if (callback) callback();
            };

        setTimeout(
            function () {
                chatDatabase.processAllChatTextMessages(callbackProcess, callbackComplete);
            }, 0);
    };

    /* Method returns the buffer size currently used by the ChatModel's ChatTextMessageBuffer.
     */
    this.getChatBufferLength = function () {
        return chatTextMessageBuffer.getChatBufferLength();
    };

    /* Method sets the buffer size of the ChatModel's ChatTextMessageBuffer using the supplied
     * input value. If the supplied value results in a larger buffer size, then the chat message
     * history will be automatically loaded.
     *
     * Parameter:
     *   length: New buffer size.
     */
    this.setChatBufferLength = function (length) {
        var previousLength = chatTextMessageBuffer.getChatBufferLength();
        chatTextMessageBuffer.setChatBufferLength(length);

        setTimeout(function () {
            if (previousLength < chatTextMessageBuffer.getChatBufferLength()) {
                self.loadChatHistory(userSuppliedFilter);
            }
        }, 0);
    };

    ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG, "ChatModel instantiated.");
}

// Maximum number of ChatTextMessages that can be sent in a batch during synchronization.
ChatModel.DEFAULT_MAX_SYNCHRONIZATION_BATCH_SIZE = 500;