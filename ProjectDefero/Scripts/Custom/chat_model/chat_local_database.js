﻿/* ChatDatabase
 *
 * Author(s): Ryan Finn
 */

/* ChatDatabase is the abstraction used by ChatModel for local storage.
 * In this case we are using someone's library named LocalForage.js.
 */
function ChatDatabase() {
    var self = this;

    /* Queue stores callback functions called when the ChatDatabase.ChatLocalProfileEncapsulation
     * becomes initialized or is already initialized.
     */
    var onChatLocalProfileEncapsulationInitializedQueue = new ChatCallbackFunctionList();

    /* Initializes the database.
     */
    var initializeLocalDatabase = function () {
        if (ChatDatabase.deferoChatLocalDBInitialized) { return; }

        var consoleMessage =
              "\n  DB name: " + ChatDatabase.DEFERO_CHAT_LOCAL_DB_NAME
            + "\n  DB version: " + ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION
            + "\n  DB description: " + ChatDatabase.DEFERO_CHAT_LOCAL_DB_DESCRIPTION;

        try {
            // This call is blocks until return.
            localforage.config({
                name: ChatDatabase.DEFERO_CHAT_LOCAL_DB_NAME,
                version: ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION,
                description: ChatDatabase.DEFERO_CHAT_LOCAL_DB_DESCRIPTION
            });

            ChatDatabase.deferoChatLocalDBInitialized = true;

            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase initialized: " + consoleMessage);
        }
        catch (ex) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , consoleMessage
                + "\n  " + ex.message);
        }

        if (ChatDatabase.deferoChatLocalDBInitialized) {
            setTimeout(function () {
                performGeneralMaintenance();
                performDatabaseUpgrade();
            }, 0);
        }

        initializeChatLocalProfileEncapsulation();
    };

    /* Method initializes the static ChatLocalProfileEncapsulation.
     * It is safe to call this method more than once.
     */
    var initializeChatLocalProfileEncapsulation = function () {
        if (ChatLocalProfileEncapsulation.chatLocalProfileEncapsulationGlobalState !== ChatLocalProfileEncapsulation.STATE.UNINITIALIZED) {
            return;
        }

        ChatLocalProfileEncapsulation.chatLocalProfileEncapsulationGlobalState = ChatLocalProfileEncapsulation.STATE.INITIALIZING;

        // Attempt load the ChatLocalProfileEncapsulation from local storage.
        localforage.getItem(
            ChatLocalProfileEncapsulation.STORAGE_KEY,

            function (err, chatLocalProfileEncapsulationJSON) {
                if (!ChatUtility.isNullOrUndefined(err)) {
                    ChatLocalProfileEncapsulation.chatLocalProfileEncapsulationGlobalState = ChatLocalProfileEncapsulation.STATE.UNINITIALIZED;

                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatDatabase.initializeChatLocalProfileEncapsulation: Failed to get ChatLocalProfileEncapsulation: "
                        + "\n  " + err);
                    return;
                }

                if (ChatLocalProfileEncapsulation.prototype.isCorrupt(
                        ChatDatabase.chatLocalProfileEncapsulation
                            = ChatLocalProfileEncapsulation.prototype.parse(chatLocalProfileEncapsulationJSON))) {
                    ChatLocalProfileEncapsulation.chatLocalProfileEncapsulationGlobalState = ChatLocalProfileEncapsulation.STATE.INITIALIZED;
                    onChatLocalProfileEncapsulationInitializedQueue.processAndRemoveAll(null);

                    if (globalDeferoChatReleaseMode === ChatUtility.RELEASE_MODE.DEVELOPER_DEBUG) {
                        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatLocalProfileEncapsulation initialized:"
                            + "\n" + ChatDatabase.chatLocalProfileEncapsulation.toString());
                    }
                }
                else {
                    // We found a bad record. Let's remove it now.
                    localforage.removeItem(
                        ChatLocalProfileEncapsulation.STORAGE_KEY,

                        function (err) {
                            if (!ChatUtility.isNullOrUndefined(err)) {
                                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                    , "ChatDatabase.initializeChatLocalProfileEncapsulation: Failed to remove bad record: "
                                    + err);
                            }
                            else {
                                ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                    , "ChatDatabase.initializeChatLocalProfileEncapsulation: Removed null or undifined record from local storage.");
                            }

                            // A ChatLocalProfileEncapsulation record is not in local storage. Let's set one now.

                            ChatDatabase.chatLocalProfileEncapsulation = new ChatLocalProfileEncapsulation();

                            localforage.setItem(
                                ChatLocalProfileEncapsulation.STORAGE_KEY,
                                ChatDatabase.chatLocalProfileEncapsulation.stringify(),

                                function (err, value) {
                                    if (!ChatUtility.isNullOrUndefined(err)) {
                                        ChatLocalProfileEncapsulation.chatLocalProfileEncapsulationGlobalState = ChatLocalProfileEncapsulation.STATE.UNINITIALIZED;

                                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                            , "ChatDatabase.initializeChatLocalProfileEncapsulation: Failed to persist ChatLocalProfileEncapsulation: "
                                            + "\n  " + err);
                                    }
                                    else {
                                        ChatLocalProfileEncapsulation.chatLocalProfileEncapsulationGlobalState = ChatLocalProfileEncapsulation.STATE.INITIALIZED;
                                        onChatLocalProfileEncapsulationInitializedQueue.processAndRemoveAll(null);

                                        if (globalDeferoChatReleaseMode === ChatUtility.RELEASE_MODE.DEVELOPER_DEBUG) {
                                            message = ChatDatabase.chatLocalProfileEncapsulation.toString();
                                            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                                , "ChatLocalProfileEncapsulation initialized:"
                                                + "\n" + ChatDatabase.chatLocalProfileEncapsulation.toString());
                                        }
                                    }
                                });
                        });
                }
            });
    };

    /* Registers a function that will be called ChatDatabase.ChatLocalProfileEncapsulation
     * becomes initialized or is already initialized.  The function will only be called once
     * and then removed from the queue.
     *
     * Parameters:
     *   callbackFunction(): Callback function that will be enqueued and called
     *   when the ChatDatabase.ChatLocalProfileEncapsulation becomes initialized
     *   or is already initialized.
     */
    this.registerOnChatLocalProfileEncapsulationInitializedHandler = function (callbackFunction) {
        if (!ChatUtility.isFunction(callbackFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "registerOnChatLocalProfileEncapsulationInitializedHandler: Supplied callback parameter is not a function.");
            return;
        }

        onChatLocalProfileEncapsulationInitializedQueue.addCallbackFuntion(callbackFunction);

        if (ChatLocalProfileEncapsulation.chatLocalProfileEncapsulationGlobalState === ChatLocalProfileEncapsulation.STATE.INITIALIZED) {
            onChatLocalProfileEncapsulationInitializedQueue.processAndRemoveAll(null);
        }
        else {
            initializeChatLocalProfileEncapsulation();
        }
    };

    /* Adds a ChatTextMessage to the database and makes a record of this being
     * the latest message received from the sender if the UTC timestamp is newer
     * than the last message received from the sender.
     *
     * If storage already contains the supplied ChatMessage's key and the associated
     * value is not null and not undefined, then the supplied ChatMessage will
     * not overwrite the existing record.  This will help protect us if someone
     * corrupts an existing message and then sends it to us.
     *
     * Parameters:
     *   chatTextMessage: ChatTextMessage object to be added to the database.
     *   callBackOnComplete(error<String>): Function that will be called when persistence
     *                                      attempt completes. If an error occurs, then
     *                                      the error parameter will not be null and will
     *                                      instead be an error message string.
     */
    this.persistMessage = function (chatTextMessage, callBackOnComplete) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.persistMessage: ChatTextMessage is corrupt.");
            return;
        }

        /* We believe it is safe to permanently lift this requirement.
         *
        if (chatTextMessage.databaseVersion !== ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.persistMessage: ChatMessage cannot be stored because ChatMessage.databaseVersion ["
                + " " + chatTextMessage.databaseVersion + "] !== " + ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION);
            return;
        }
        */

        if (!ChatUtility.isFunction(callBackOnComplete)) { callBackOnComplete = function (err) { }; }

        /* Make a record of this being the latest message received from the sender.
         */
        var recordLastMessageReceived = function () { self.persistChatLastMessageReceivedSet(chatTextMessage.recipientUserId, chatTextMessage); };

        try {
            localforage.getItem(
                chatTextMessage.uniqueIdentifier,

                function (err, chatTextMessageJSON) {
                    if (!ChatUtility.isNullOrUndefined(err)) {
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.persistMessage: Failed to check for pre-existing ChatTextMessage: "
                            + err);
                        callBackOnComplete(err);
                        return;
                    }

                    if (!ChatUtility.isNullOrUndefined(chatTextMessageJSON)
                        && !ChatTextMessage.prototype.isCorrupt(parseChatTextMessage(chatTextMessageJSON))) {
                        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.persistMessage: A record already exists for ChatMessage.uniqueIdentifier ["
                            + chatTextMessage.uniqueIdentifier + "]. We will not overwrite it.");

                        recordLastMessageReceived();
                        callBackOnComplete(null);
                        return;
                    }

                    localforage.setItem(
                        chatTextMessage.uniqueIdentifier,
                        chatTextMessage.stringify(),
                        function (err, value) {
                            if (!ChatUtility.isNullOrUndefined(err)) {
                                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                    , "ChatDatabase.persistMessage: Failed to persist ChatTextMessage: " + err
                                    + "\n  ChatTextMessage may be permanently lost: " + chatTextMessage.toString());
                                callBackOnComplete(err);
                                return;
                            }

                            recordLastMessageReceived();
                            callBackOnComplete(null);
                        });
                });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.persistMessage: Error occurred when persisting message:"
                + "\n  " + ex.message);
        }
    };

    /* Adds a ChatTextMessage to the database and makes a record of this being
     * the latest message received from the sender if the UTC timestamp is newer
     * than the last message received from the sender.
     *
     * A ChatTextMessage record with the same storage key already in storage will
     * be overwritten by the supplied ChatTextMessage.
     *
     * Parameters:
     *   chatTextMessage: ChatTextMessage object to be added to the database.
     *   callBackOnComplete(error<String>): Function that will be called when persistence
     *                                      attempt completes. If an error occurs, then
     *                                      the error parameter will not be null and will
     *                                      instead be an error message string.
     */
    this.persistMessageOverwrite = function (chatTextMessage, callBackOnComplete) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.persistMessageOverwrite: ChatTextMessage is corrupt.");
            return;
        }

        if (!ChatUtility.isFunction(callBackOnComplete)) { callBackOnComplete = function (err) { }; }

        /* Make a record of this being the latest message received from the sender.
         */
        var recordLastMessageReceived = function () { self.persistChatLastMessageReceivedSet(chatTextMessage.recipientUserId, chatTextMessage); };

        try {
            localforage.setItem(
                chatTextMessage.uniqueIdentifier,
                chatTextMessage.stringify(),
                function (err, value) {
                    if (!ChatUtility.isNullOrUndefined(err)) {
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.persistMessageOverwrite: Failed to persist ChatTextMessage: " + err
                            + "\n  ChatTextMessage may be permanently lost: " + chatTextMessage.toString());
                        callBackOnComplete(err);
                        return;
                    }

                    recordLastMessageReceived();
                    callBackOnComplete(null);
                });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.persistMessageOverwrite: Error occurred when persisting message:"
                + "\n  " + ex.message);
        }
    };

    /* Iterates over entire database and passes each ChatTextMessage in database
     * to the callback function provided as a parameter.
     *
     * Parameters:
     *
     *   callbackProcessFunction: Callback function that for each ChatTextMessage is
     *                            called with the ChatTextMessage as a parameter in the
     *                            form callbackProcess(key, ChatTextMessage).
     *
     *   callbackCompleteFunction: Callback function that is called when the ChatTextMessage
     *                             iteration has completed. Function is in the form
     *                             callbackCompleteFunction().
     */
    this.processAllChatTextMessages = function (callbackProcessFunction, callbackCompleteFunction) {
        if (!ChatUtility.isFunction(callbackProcessFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "processAllChatTextMessages: Valid callbackProcessFunction function was not provided.");
            return;
        }

        var corruptChatTextMessageRemovalQueue = [];

        try {
            localforage.iterate(
                function (chatTextMessageJSON, key) {
                    if (isChatTextMessageKey(key)) {
                        var chatTextMessage = null;
                        if (!ChatUtility.isNullOrUndefined(chatTextMessageJSON)
                            && !ChatTextMessage.prototype.isCorrupt(chatTextMessage = parseChatTextMessage(chatTextMessageJSON))) {
                            callbackProcessFunction(key, chatTextMessage);
                        }
                        else {
                            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                , "ChatDatabase.processChatTextMessageById: Bad record found. Let's remove it now.");

                            corruptChatTextMessageRemovalQueue.push(chatTextMessageUniqueIdentifier);
                        }
                    }
                }, function () {
                    for (var i = 0; i < corruptChatTextMessageRemovalQueue.length; i++) {
                        self.deleteEntry(corruptChatTextMessageRemovalQueue[i]);
                    }

                    if (!ChatUtility.isFunction(callbackCompleteFunction)) {
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "processAllChatTextMessages: Valid callbackCompleteFunction function was not provided.");
                        return;
                    }

                    callbackCompleteFunction();
                });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.processAllChatTextMessages: Exception occurred when using localforage.iterate: "
                + ex.message);
        }
    };

    /* Method performs a direct lookup of a ChatTextMessage and supplies it to the callback
     * function.
     *
     * Parameters:
     *
     *   chatTextMessageUniqueIdentifier:  String that uniquely identifies a ChatTextMessage
     *                                     instance.
     *
     *   callbackFunction:  Callback function that for each qualifying ChatTextMessage
     *                      is called with the ChatTextMessage as a parameter in the
     *                      form callbackFunction(key, ChatTextMessage).
     */
    this.processChatTextMessageById = function (chatTextMessageUniqueIdentifier, callbackFunction) {
        if (ChatUtility.isNullOrUndefined(chatTextMessageUniqueIdentifier)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "processChatTextMessageById: chatTextMessageUniqueIdentifier parameter was null or undefined.");
            return;
        }

        if (!ChatUtility.isFunction(callbackFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "processChatTextMessageById: callbackFunction is not a function.");
            return;
        }

        if (!isChatTextMessageKey(chatTextMessageUniqueIdentifier)) {
            return;
        }

        try {
            localforage.getItem(
                chatTextMessageUniqueIdentifier,

                function (err, chatTextMessageJSON) {
                    if (!ChatUtility.isNullOrUndefined(err)) {
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.processChatTextMessageById: Failed to get ChatTextMessage: "
                            + err);
                        return;
                    }

                    var chatTextMessage = null;
                    if (!ChatUtility.isNullOrUndefined(chatTextMessage = parseChatTextMessage(chatTextMessageJSON))) {
                        callbackFunction(chatTextMessageUniqueIdentifier, chatTextMessage);
                    }
                    else {
                        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.processChatTextMessageById: Bad record found. Let's remove it now.");

                        // We found a bad record. Let's remove it now.
                        self.deleteEntry(chatTextMessageUniqueIdentifier);
                    }
                });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.processChatTextMessageById: Exception occurred when reading ChatTextMessage key: "
                + chatTextMessageUniqueIdentifier
                + "\n  " + ex.message);
        }
    };

    /* Method attempts to parse the supplied ChatTextMessage JSON representation and
     * return the resulting ChatTextMessage.
     *
     * Parameters:
     *   chatTextMessageJSON: ChatTextMessage JSON representation that will be used
     *                        in the parse attempt.
     *
     * Returns:
     *   ChatTextMessage object if the parse was successful, null otherwise.
     */
    var parseChatTextMessage = function (chatTextMessageJSON) {
        var chatTextMessage = null;

        if (ChatUtility.isNullOrUndefined(chatTextMessageJSON)) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.tryParseChatTextMessage: chatTextMessageJSON is null or undefined.");
            return null;
        }

        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage = ChatTextMessage.prototype.parse(chatTextMessageJSON))) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.tryParseChatTextMessage: ChatTextMessage is corrupt.");
            return null;
        }

        return chatTextMessage;
    };

    /* Method deletes a record from the local storage.
     */
    this.deleteEntry = function (key) {
        if (!isChatTextMessageKey(key)) {
            return;
        }

        localforage.removeItem(
            key,

            function (err) {
                if (!ChatUtility.isNullOrUndefined(err)) {
                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatDatabase.deleteEntry: Failed remove record with key: " + key
                        + "\n  " + err);
                }
                else {
                    ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatDatabase.deleteEntry: Record deleted with key: " + key);
                }
            });
    };

    /* Method adds the supplied key/value pair to the set of settings corresponding
     * to the supplied userId. If the key is already in use, then the supplied value
     * will overwrite the old value.
     *
     * Parameters:
     *   userId: User id of the user whoes settings these are.
     *   key: key<String> in key/value pair.
     *   value: value<String> in key/value pair.
     */
    this.persistChatProfileSetting = function (userId, key, value) {
        this.registerOnChatLocalProfileEncapsulationInitializedHandler(
            function () {
                ChatDatabase.chatLocalProfileEncapsulation.setValue(userId, key, value);
                commitChatLocalProfileEncapsulation();
            });
    };

    /* Method adds the supplied ChatTextMessage to the user's ChatLastMessageReceivedSet.
     *
     * Parameters:
     *   userId: User Id of the owner of the ChatLastMessageSentSet object.
     *   chatTextMessage: ChatTextMessage to be added to the user's ChatLastMessageReceivedSet
     */
    this.persistChatLastMessageReceivedSet = function (userId, chatTextMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.persistChatLastMessageReceivedSet: chatTextMessage is null or undefined.");
            return;
        }

        var chatLastMessageReceivedSetAsJSON
            = this.getChatProfileSetting(userId, ChatLastMessageReceivedSet.STORAGE_KEY);

        var chatLastMessageReceivedSet = null;

        if (ChatUtility.isNullOrUndefined(chatLastMessageReceivedSetAsJSON)) {
            chatLastMessageReceivedSet = new ChatLastMessageReceivedSet();
        }
        else {
            chatLastMessageReceivedSet
                = ChatLastMessageReceivedSet.prototype.parse(chatLastMessageReceivedSetAsJSON);
        }

        chatLastMessageReceivedSet.setValue(chatTextMessage.getCopy());

        this.persistChatProfileSetting(
              userId
            , ChatLastMessageReceivedSet.STORAGE_KEY
            , chatLastMessageReceivedSet.stringify());
    };

    /* Method returns the last reported ChatTextMessage received in the context of the supplied
     * parameters.
     *
     * Parameters:
     *   myUserId: User Id of the owner of the ChatLocalProfileEncapsulation.
     *   senderUserId: User Id of the user who sent me the last ChatTextMessage.
     *   roomId: Room id where the ChatTextMessage was sent.
     */
    this.getChatLastMessageReceived = function (myUserId, senderUserId, roomId) {
        var chatLastMessageReceivedSetAsJSON
            = this.getChatProfileSetting(myUserId, ChatLastMessageReceivedSet.STORAGE_KEY);

        if (ChatUtility.isNullOrUndefined(chatLastMessageReceivedSetAsJSON)) {
            return null;
        }

        var chatLastMessageReceivedSet
            = ChatLastMessageReceivedSet.prototype.parse(chatLastMessageReceivedSetAsJSON);

        if (ChatUtility.isNullOrUndefined(chatLastMessageReceivedSet)) {
            return null;
        }

        return chatLastMessageReceivedSet.getValue(roomId, senderUserId);
    };

    /* Method returns the last reported ChatTextMessage I sent in the context of the supplied
     * parameters.
     *
     * Parameters:
     *   myUserId: User Id of the owner of the ChatLocalProfileEncapsulation.
     *   recipientUserId: User Id of the user who I sent the last ChatTextMessage to.
     *   roomId: Room id where the ChatTextMessage was sent.
     */
    this.getChatLastMessageSent = function (myUserId, recipientUserId, roomId) {
        var chatLastMessageSentSetAsJSON
            = this.getChatProfileSetting(myUserId, ChatLastMessageSentSet.STORAGE_KEY);

        if (ChatUtility.isNullOrUndefined(chatLastMessageSentSetAsJSON)) {
            return null;
        }

        var chatLastMessageSentSet
            = ChatLastMessageSentSet.prototype.parse(chatLastMessageSentSetAsJSON);

        if (ChatUtility.isNullOrUndefined(chatLastMessageSentSet)) {
            return null;
        }

        return chatLastMessageSentSet.getValue(roomId, recipientUserId);
    };

    /* Method adds the supplied ChatTextMessage to the user's ChatLastMessageSentSet.
     *
     * Parameters:
     *   userId: User Id of the owner of the ChatLastMessageSentSet object.
     *   chatTextMessage: ChatTextMessage to be added to the user's ChatLastMessageSentSet
     */
    this.persistChatLastMessageSentSet = function (userId, chatTextMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.persistChatLastMessageSentSet: chatTextMessage is null or undefined.");
            return;
        }

        var chatLastMessageSentSetAsJSON
            = this.getChatProfileSetting(userId, ChatLastMessageSentSet.STORAGE_KEY);

        var chatLastMessageSentSet = null;

        if (ChatUtility.isNullOrUndefined(chatLastMessageSentSetAsJSON)) {
            chatLastMessageSentSet = new ChatLastMessageSentSet();
        }
        else {
            chatLastMessageSentSet
                = ChatLastMessageSentSet.prototype.parse(chatLastMessageSentSetAsJSON);
        }

        chatLastMessageSentSet.setValue(chatTextMessage.getCopy());

        this.persistChatProfileSetting(
              userId
            , ChatLastMessageSentSet.STORAGE_KEY
            , chatLastMessageSentSet.stringify());
    };

    /* Method returns the value of a key/value pair from a set of settings corresponding
     * to the supplied userId.
     *
     * Parameters:
     *   userId: User id of the user whose settings these are.
     *   key: key in a key/value pair.
     *
     * Returns:
     *   String value in the key/value pair if the key is found. If the key is
     *   not found, then null is returned.
     */
    this.getChatProfileSetting = function (userId, key) {
        if (ChatUtility.isNullOrUndefined(ChatDatabase.chatLocalProfileEncapsulation)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.getChatProfileSetting:"
                + "\n  ChatLocalProfileEncapsulation is not initialized; could not get setting.");

            setTimeout(
                function () {
                    initializeChatLocalProfileEncapsulation();
                }, 0);
            return null;
        }

        return ChatDatabase.chatLocalProfileEncapsulation.getValue(userId, key);
    };

    /* Commits the global ChatLocalProfileEncapsulation in its current state
     * to persistence.
     */
    var commitChatLocalProfileEncapsulation = function () {
        localforage.setItem(
            ChatLocalProfileEncapsulation.STORAGE_KEY,
            ChatDatabase.chatLocalProfileEncapsulation.stringify(),

            function (err, value) {
                if (!ChatUtility.isNullOrUndefined(err)) {
                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatDatabase.commitChatLocalProfileEncapsulation:"
                        + " Failed to commit ChatLocalProfileEncapsulation changes: "
                        + "\n  " + err);
                }
            });
    };

    /* Method performs database general maintenance.
     */
    var performGeneralMaintenance = function () {
        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "ChatDatabase: Performing general maintenance...");
    };

    /* Method detects if database needs to be upgraded, and if so, upgrades it.
     */
    var performDatabaseUpgrade = function () {
        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "ChatDatabase: Performing database upgrade check...");

        try {
            localforage.getItem(
                ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION_STORAGE_KEY,

                function (err, currentVersion) {
                    if (!ChatUtility.isNullOrUndefined(err)) {
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.performDatabaseUpgrade: Failed to get current database version: "
                            + err);
                        return;
                    }

                    if (ChatUtility.isNullOrUndefined(currentVersion)
                        || ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION !== currentVersion) {
                        ChatUtility.log(ChatUtility.LOG_MODE.PRODUCTION_INFO
                            , "Upgrade to chat database version "
                            + String(ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION)
                            + " is required now. This may take some time, but you may continue to chat while it's working.");

                        // Perform all upgrades.
                        upgradeToVersion_1_1();
                        upgradeToVersion_1_2();
                        upgradeToVersion_1_3();
                        upgradeToVersion_1_4();
                        upgradeToVersion_1_5();
                        upgradeToVersion_1_6();

                        setChatTextMessageDatabaseVersion();

                        localforage.setItem(
                            ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION_STORAGE_KEY,
                            ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION,

                            function (err, value) {
                                if (!ChatUtility.isNullOrUndefined(err)) {
                                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                        , "ChatDatabase.performDatabaseUpgrade: Failed to persist database version "
                                        + ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION
                                        + ": \n" + err);
                                }
                            });
                    }
                    else if (ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION === currentVersion) {
                        // No upgrade required.
                        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.performDatabaseUpgrade: Database is up to date with version "
                            + ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION);
                    }
                });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.performDatabaseUpgrade: Error occurred:"
                + "\n  " + ex.message);
        }
    };

    /* Method upgrades database to version 1.1.  This upgrade involves detecting and
     * removing ChatTextMessages that may have matching keys and timestamps, but are
     * of the same content and ownership.
     */
    var upgradeToVersion_1_1 = function () {
        var upgradeVersion = 1.1;

        ChatUtility.log(ChatUtility.LOG_MODE.PRODUCTION_INFO
            , "ChatDatabase: Performing database version "
            + String(upgradeVersion)
            + " incremental upgrade...");

        var deletionKeys = {};
        var chatTextMessages = {};

        try {
            localforage.iterate(
                function (value, key) {
                    if (isChatTextMessageKey(key)) {
                        var chatTextMessage = parseChatTextMessage(value);

                        ChatUtility.iterateAssociativeArray(
                            chatTextMessages,
                            function (key, message) {
                                try {
                                    if (message.isDuplicate(chatTextMessage)) {
                                        deletionKeys[key] = true;
                                    }
                                }
                                catch (ex) { }
                            }, null);

                        chatTextMessages[key] = chatTextMessage;
                    }
                }, function () {
                    var count = 0;
                    ChatUtility.iterateAssociativeArrayKeys(
                        deletionKeys,
                        function (k) {
                            self.deleteEntry(k);
                            count++;
                        }, null);

                    ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatDatabase.upgradeToVersion_1_1: Deleting (" + count + ") duplicates.");
                });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.upgradeToVersion_1_1: Exception occurred when requesting list of database keys: "
                + ex.message);
        }
    }.bind(this);

    /* Method upgrades database to version 1.2.  This upgrade involves setting all the
     * database versions of the ChatTextMessates already in storage to that of the current
     * ChatLocalDatabase version.
     */
    var upgradeToVersion_1_2 = function () {
        var upgradeVersion = 1.2;

        ChatUtility.log(ChatUtility.LOG_MODE.PRODUCTION_INFO
            , "ChatDatabase: Performing database version "
            + String(upgradeVersion)
            + " incremental upgrade...");

        try {
            this.processAllChatTextMessages(
                function (key, chatTextMessage) {
                    if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.upgradeToVersion_1_2: Corrupt ChatTextMessage discovered:"
                            + "\n" + chatTextMessage.toString());
                        self.deleteEntry(key);
                    }
                    else if (chatTextMessage.uniqueIdentifier !== key) {
                        ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatDatabase.upgradeToVersion_1_2: ChatTextMessage.uniqueIdentifier !== its corresponding database key.");
                        self.deleteEntry(key);
                    }
                },
                function () { });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.upgradeToVersion_1_2: Exception occurred when upgrading: "
                + ex.message);
        }
    }.bind(this);

    /* Method upgrades database to version 1.3.  This upgrade involves setting all the
     * database versions of the ChatTextMessates already in storage to that of the current
     * ChatLocalDatabase version.
     */
    var upgradeToVersion_1_3 = function () {
        var upgradeVersion = 1.3;

        ChatUtility.log(ChatUtility.LOG_MODE.PRODUCTION_INFO
            , "ChatDatabase: Performing database version "
            + String(upgradeVersion)
            + " incremental upgrade...");

        try {
            this.processAllChatTextMessages(
                function (key, chatTextMessage) {
                    chatTextMessage.databaseVersion = ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION;
                    localforage.setItem(
                        key,
                        chatTextMessage.stringify(),
                        function (err, value) {
                            if (!ChatUtility.isNullOrUndefined(err)) {
                                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                    , "ChatDatabase.upgradeToVersion_1_3: Failed to persist ChatTextMessage: " + err
                                    + "\n  ChatTextMessage may be permanently lost: " + chatTextMessage.toString());
                            }
                        });
                },
                function () { });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.upgradeToVersion_1_3: Exception occurred when upgrading: "
                + ex.message);
        }
    }.bind(this);

    /* Method upgrades database to version 1.4.  This upgrade involves deleting the
     * ChatLocalProfileEncapsulation thereby forcing the creation of a new one. We may or
     * may not actually need to do this; a bug was fixed involving the persisting of
     * ChatLastMessageSent and Received objects, so we're clearing it out to be safe.
     * This will not cause anyone to lose data, it will just cause everyone to fully
     * synchronize again.
     */
    var upgradeToVersion_1_4 = function () {
        var upgradeVersion = 1.4;

        ChatUtility.log(ChatUtility.LOG_MODE.PRODUCTION_INFO
            , "ChatDatabase: Performing database version "
            + String(upgradeVersion)
            + " incremental upgrade...");

        try {
            localforage.removeItem(
            ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION_STORAGE_KEY,
            function (err) { });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.upgradeToVersion_1_4: Exception occurred when upgrading: "
                + ex.message);
        }
    }.bind(this);

    /* Method determines if the key is a ChatTextMessage type key.
     */
    var isChatTextMessageKey = function (key) {
        return (key !== ChatLocalProfileEncapsulation.STORAGE_KEY
            && key !== ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION_STORAGE_KEY);
    };

    /* Method upgrades database to version 1.5.  This upgrade involves detecting and
     * removing ChatTextMessages that have empty message bodies.
     */
    var upgradeToVersion_1_5 = function () {
        var upgradeVersion = 1.5;

        ChatUtility.log(ChatUtility.LOG_MODE.PRODUCTION_INFO
            , "ChatDatabase: Performing database version "
            + String(upgradeVersion)
            + " incremental upgrade...");

        var deletionCandidates = [];

        try {
            this.processAllChatTextMessages(
                function (key, chatTextMessage) {
                    chatTextMessage.databaseVersion = ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION;

                    if (chatTextMessage.messageBody.length <= 0) {
                        deletionCandidates.push(key);
                    }
                },
                function () {
                    for (var i = 0; i < deletionCandidates.length; i++) {
                        self.deleteEntry(deletionCandidates[i]);
                    }

                    ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatDatabase.upgradeToVersion_1_5: Deleting (" + deletionCandidates.length + ") messages with empty bodies.");
                });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.upgradeToVersion_1_5: Exception occurred when requesting list of database keys: "
                + ex.message);
        }
    }.bind(this);

    /* Method upgrades database to version 1.6.  This upgrade involves setting all ChatMetadata
     * ChatFileMetadata.STATEs to ChatFileMetadata.STATE.ARCHIVED.
     */
    var upgradeToVersion_1_6 = function () {
        var upgradeVersion = 1.6;

        ChatUtility.log(ChatUtility.LOG_MODE.PRODUCTION_INFO
            , "ChatDatabase: Performing database version "
            + String(upgradeVersion)
            + " incremental upgrade...");

        var overwriteCandidates = [];

        try {
            this.processAllChatTextMessages(
                function (key, chatTextMessage) {
                    chatTextMessage.databaseVersion = ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION;

                    if (chatTextMessage.messageType === ChatTextMessage.TYPE.FILE_TRANSMISSION) {
                        chatTextMessage.chatFileMetadata.offerState = ChatFileMetadata.STATE.ARCHIVED;
                        overwriteCandidates.push(chatTextMessage);
                    }
                },
                function () {
                    for (var i = 0; i < overwriteCandidates.length; i++) {
                        self.persistMessageOverwrite(overwriteCandidates[i]);
                    }

                    ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatDatabase.upgradeToVersion_1_6: Setting (" + overwriteCandidates.length
                        + ") ChatMetadata records to ChatFileMetadata.STATE.ARCHIVED.");
                });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.upgradeToVersion_1_6: Exception occurred when requesting list of database keys: "
                + ex.message);
        }
    }.bind(this);

    /* Method upgrades database to version 1.3.  This upgrade involves setting all the
     * database versions of the ChatTextMessates already in storage to that of the current
     * ChatLocalDatabase version.
     */
    var setChatTextMessageDatabaseVersion = function () {
        ChatUtility.log(ChatUtility.LOG_MODE.PRODUCTION_INFO
            , "ChatDatabase: Setting all ChatTextMessage database versions to "
            + ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION);

        try {
            this.processAllChatTextMessages(
                function (key, chatTextMessage) {
                    chatTextMessage.databaseVersion = ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION;
                    localforage.setItem(
                        key,
                        chatTextMessage.stringify(),
                        function (err, value) {
                            if (!ChatUtility.isNullOrUndefined(err)) {
                                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                                    , "ChatDatabase.setChatTextMessageDatabaseVersion: Failed to persist ChatTextMessage: "
                                    + err);
                            }
                        });
                },
                function () { });
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatDatabase.setChatTextMessageDatabaseVersion: Exception occurred when upgrading: "
                + ex.message);
        }
    }.bind(this);

    // Initialize!
    initializeLocalDatabase();
}

/* Name of localforage database used by the chat model.
 */
ChatDatabase.DEFERO_CHAT_LOCAL_DB_NAME = "DeferoChat";

/* Version of localforage database used by the chat model.
 * This must be a Javascript number.
 */
ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION = 1.7;

/* Key used to access the persisted Defero chat local storage version when
 * stored in a key/value pair.
 */
ChatDatabase.DEFERO_CHAT_LOCAL_DB_VERSION_STORAGE_KEY = "DeferoChatLocalDatabaseVersionStorageKey";

/* Description of the database.
 */
ChatDatabase.DEFERO_CHAT_LOCAL_DB_DESCRIPTION = "Client-side chat message storage.";

/* Inidicates whether or not the local database has been initialized.
 */
ChatDatabase.deferoChatLocalDBInitialized = false;

/* Static ChatLocalProfileEncapsulation. See documentation for class
 * ChatLocalProfileEncapsulation for more information.
 */
ChatDatabase.chatLocalProfileEncapsulation = null;