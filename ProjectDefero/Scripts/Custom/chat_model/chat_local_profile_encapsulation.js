﻿/* ChatLocalProfileEncapsulation
 *
 * Author(s): Ryan Finn
 */

/* ChatLocalProfileEncapsulation provides a means to store multiple ChatLocalProfile objects
 * under a single local storage key.  The ChatDatabase will be able to find the single
 * ChatLocalProfileEncapsulation instance that exists in the ChatDatabase by key, and then
 * access the individual ChatLocalProfiles inside that represent individual users.
 *
 * A big part of the reason we need this is due to the single-local-storage-per-domain
 * restriction we have to live with.
 */
function ChatLocalProfileEncapsulation() {
    /* Stores key/value pairs in form userId<string> : ChatLocalProfile.
     */
    var chatLocalProfiles = {};

    /* Method adds a ChatLocalProfile.
     *
     * Parameters:
     *   userId: User id of the user whoes settings these are.
     *   chatLocalProfile: ChatLocalProfile to be added.
     */
    this.addChatLocalProfile = function (userId, chatLocalProfile) {
        if (ChatUtility.isNullOrUndefined(userId)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.addChatLocalProfile: Supplied userId is null or undefined.");
            return;
        }

        if (ChatLocalProfile.prototype.isCorrupt(chatLocalProfile)) {
            delete chatLocalProfiles[userId];
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.addChatLocalProfile: Corrupt ChatLocalProfile ignored.");
            return;
        }

        chatLocalProfiles[userId] = chatLocalProfile;

        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
            , "ChatLocalProfileEncapsulation.addChatLocalProfile:"
            + "\n  ChatLocalProfile added for user id: " + userId);
    };

    /* Method adds the supplied key/value pair to the set of settings corresponding
     * to the supplied userId. If the key is already in use, then the supplied value
     * will overwrite the old value.
     *
     * Parameters:
     *   userId: User id of the user whoes settings these are.
     *   key: key in key/value pair.
     *   value: value in key/value pair.
     */
    this.setValue = function (userId, key, value) {
        if (ChatUtility.isNullOrUndefined(userId)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.setValue: Supplied userId is null or undefined.");
            return;
        }

        if (!(userId in chatLocalProfiles)) { this.addChatLocalProfile(userId, new ChatLocalProfile()); }

        if (ChatLocalProfile.prototype.isCorrupt(chatLocalProfiles[userId])) {
            delete chatLocalProfiles[userId];
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.setValue: Corrupt ChatLocalProfile discovered and deleted.");
            return;
        }

        chatLocalProfiles[userId].setValue(key, value);

        if (globalDeferoChatReleaseMode === ChatUtility.RELEASE_MODE.DEVELOPER_DEBUG) {
            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.setValue: Setting added:"
                + "\n  user id: " + userId
                + "\n  key:     " + key
                + "\n  value:   " + value);
        }
    };

    /* Method returns the value of a key/value pair from a set of settings corresponding
     * to the supplied userId.
     *
     * Parameters:
     *   userId: User id of the user whoes settings these are.
     *   key: key in a key/value pair.
     *
     * Returns:
     *   String value in the key/value pair if the key is found. If the key is
     *   not found, then null is returned.
     */
    this.getValue = function (userId, key) {
        if (ChatUtility.isNullOrUndefined(userId)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.getValue: Supplied userId is null or undefined.");
            return;
        }

        if (!(userId in chatLocalProfiles)) { this.addChatLocalProfile(userId, new ChatLocalProfile()); }

        if (ChatLocalProfile.prototype.isCorrupt(chatLocalProfiles[userId])) {
            delete chatLocalProfiles[userId];
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.getValue: Corrupt ChatLocalProfile discovered and deleted.");
            return;
        }

        return chatLocalProfiles[userId].getValue(key);
    };

    /* Method returns the JSON representation of the ChatLocalProfile.
     */
    this.stringify = function () {
        var jsonString = null;

        try {
            jsonString = JSON.stringify(this.toJSON());
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.stringify: Exception thrown calling JSON.parse: "
                + "\n  " + ex.message);

            jsonString = "";
        }

        return jsonString;
    };

    /* Method returns the pre-serialized JSON object representation of the ChatLocalProfile.
     */
    this.toJSON = function () {
        return chatLocalProfiles;
    };

    /* Returns a string representation of this object.
     */
    this.toString = function () {
        var result = "";

        ChatUtility.iterateAssociativeArray(
            chatLocalProfiles,
            function (key, profile) {
                result += "\n key: " + key + " value: " + profile.toString();
            }, null);

        return "ChatLocalProfileEncapsulation:" + result;
    };
}

/* Key used to access the ChatLocalProfileEncapsulation object when it's
 * stored in a key/value pair.
 */
ChatLocalProfileEncapsulation.STORAGE_KEY = "ChatLocalProfileEncapsulation";

/* Enumeration of the global ChatLocalProfileEncapsulation state.
 * this is useful for preventing re-initializations from inadvertently
 * overwriting ChatLocalProfileEncapsulation data.
 */
ChatLocalProfileEncapsulation.STATE = {
    UNINITIALIZED: 0,
    INITIALIZING: 1,
    INITIALIZED: 2
};
ChatLocalProfileEncapsulation.chatLocalProfileEncapsulationGlobalState = ChatLocalProfileEncapsulation.STATE.UNINITIALIZED;


ChatLocalProfileEncapsulation.prototype = {
    isCorrupt: function (chatLocalProfileEncapsulation) {
        try {
            if (!ChatUtility.isNullOrUndefined(chatLocalProfileEncapsulation)
                || !ChatUtility.isNullOrUndefined(chatLocalProfileEncapsulation.chatLocalProfiles)
                || !ChatUtility.isFunction(chatLocalProfileEncapsulation.addChatLocalProfile)
                || !ChatUtility.isFunction(chatLocalProfileEncapsulation.setValue)
                || !ChatUtility.isFunction(chatLocalProfileEncapsulation.getValue)) {
                return true;
            }
        }
        catch (ex) {
            return true;
        }

        return false;
    },

    /* Method returns a ChatLocalProfileEncapsulation representative of the supplied
     * JSON version of a ChatLocalProfileEncapsulation.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatLocalProfileEncapsulation.
     *
     * Returns:
     *   ChatLocalProfileEncapsulation constructed from values in the supplied JSON string.
     */
    parse: function(chatLocalProfileEncapsulationAsJSON)
    {
        try {
            return ChatLocalProfileEncapsulation.prototype.reconstructor(JSON.parse(chatLocalProfileEncapsulationAsJSON));
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.prototype.parse: Exception thrown calling JSON.parse: "
                + "\n  " + ex.message);
            return new ChatLocalProfileEncapsulation();
        }
    },

    /* Method converts the JSON-deserialized representation of a ChatLocalProfile instance
     * into an actual ChatLocalProfile instance.
     *
     * Parameters:
     *   jsonParseResult: JSON-deserialized representation of a ChatLocalProfile instance.
     *
     * Returns:
     *   New ChatLocalProfile instance built from the values in the supplied object.
     */
    reconstructor: function (jsonParseResult) {
        var chatLocalProfileEncapsulation = new ChatLocalProfileEncapsulation();

        if (ChatUtility.isNullOrUndefined(jsonParseResult)) {
            return chatLocalProfileEncapsulation;
        }

        try {
            ChatUtility.iterateAssociativeArray(
                jsonParseResult,
                function (userId, value) {
                    chatLocalProfileEncapsulation.addChatLocalProfile(
                      userId
                    , ChatLocalProfile.prototype.reconstructor(value));
                }, null);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfileEncapsulation.prototype.reconstructor: Exception thrown: "
                + "\n  " + ex.message);
        }

        return chatLocalProfileEncapsulation;
    }
}