﻿/* ChatLocalProfileEncapsulation
 *
 * Author(s): Ryan Finn
 */


/* ChatLocalProfile stores persistent user state. Persistence is supplied by local storage.
 * User state is stored as key/value pairs.
 */
function ChatLocalProfile() {
    /* Stores key/value pairs in form { String: String }.
     */
    var userSettings = {};

    /* Method returns the value of a key/value pair from a set of settings.
     *
     * Parameters:
     *   key: key in a key/value pair.
     *
     * Returns:
     *   String value in the key/value pair if the key is found. If the key is
     *   not found, then null is returned.
     */
    this.getValue = function (key) {
        if (ChatUtility.isNullOrUndefined(key)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfile.getValue: Supplied key is null or undefined.");
            return;
        }

        if (key in userSettings) { return userSettings[key]; }
        return null;
    };

    /* Method adds the supplied key/value pair to the set of settings. If the key
     * was already in use, then the supplied value will overwrite the old value.
     *
     * Parameters:
     *   key: key in key/value pair.
     *   value: value in key/value pair.
     */
    this.setValue = function (key, value) {
        if (ChatUtility.isNullOrUndefined(key)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfile.setValue: Supplied key is null or undefined.");
            return;
        }

        if (ChatUtility.isNullOrUndefined(value)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfile.setValue: Supplied value is null or undefined.");
            return;
        }

        userSettings[key] = value;
    };

    /* Method deletes a value by key from the set of settings.
     *
     * Parameters:
     *   key: key in key/value pair that is to be deleted.
     */
    this.deleteValue = function (key) {
        if (ChatUtility.isNullOrUndefined(key)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfile.deleteValue: Supplied key is null or undefined.");
            return;
        }

        delete userSettings[key];
    };

    /* Method determines if a value for the supplied key exists.
     *
     * Parameters:
     *   key: key in key/value pair that is to be deleted.
     *
     * Returns:
     *   True if a value associated to the key was discoverd. False, otherwise.
     */
    this.containsValue = function (key) {
        if (ChatUtility.isNullOrUndefined(key)) { return false; }

        if (key in userSettings) {
            if (ChatUtility.isNullOrUndefined(userSettings[key])) {
                delete userSettings[key];
                return false;
            }
            else {
                return true;
            }
        }
        
        return false;
    };

    /* Method returns the JSON string representation of the ChatLocalProfile.
     */
    this.stringify = function () {
        var jsonString = null;

        try {
            jsonString = JSON.stringify(this.toJSON());
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfile.stringify: Exception thrown calling JSON.parse: "
                + "\n  " + ex.message);

            jsonString = "";
        }

        return jsonString;
    };

    /* Method returns the pre-serialized JSON object representation of the ChatLocalProfile.
     */
    this.toJSON = function () {
        return userSettings;
    };

    /* Returns a string representation of this object.
     */
    this.toString = function () {
        var result = "";

        ChatUtility.iterateAssociativeArray(
            userSettings,
            function (key, setting) {
                result += "\n  " + key + ": ";

                if (ChatUtility.isFunction(setting.toString)) {
                    result += setting.toString();
                }
                else {
                    result += setting;
                }
            }, null);

        return "ChatLocalProfile:" + result;
    };
}

ChatLocalProfile.prototype = {

    /* Method returns a ChatLocalProfile representative of the supplied JSON version
     * of a ChatLocalProfile.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatLocalProfile.
     *
     * Returns:
     *   ChatLocalProfile constructed from values in the supplied JSON string.
     */
    parse: function (chatLocalProfileAsJSON) {
        try {
            return ChatLocalProfile.prototype.reconstructor(JSON.parse(chatLocalProfileAsJSON));
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfile.prototype.parse: Exception thrown calling JSON.parse: "
                + "\n  " + ex.message);
        }

        return new ChatLocalProfile();
    },

    /* Method converts the JSON-deserialized representation of a ChatLocalProfile instance
     * into an actual ChatLocalProfile instance.
     *
     * Parameters:
     *   jsonParseResult: JSON-deserialized representation of a ChatLocalProfile instance.
     *
     * Returns:
     *   New ChatLocalProfile instance built from the values in the supplied object.
     */
    reconstructor: function (jsonParseResult) {
        var chatLocalProfile = new ChatLocalProfile();

        try {
            ChatUtility.iterateAssociativeArray(
                jsonParseResult,
                function (key, value) {
                    chatLocalProfile.setValue(key, value);
                }, null);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfile.prototype.reconstructor: Exception thrown: "
                + "\n  " + ex.message);
        }

        return chatLocalProfile;
    },

    /* Method determines if the supplied ChatLocalProfile instance should be considered to be
     * in a corrupt state or not.
     *
     * Parameters:
     *   chatLocalProfile:  ChatLocalProfile instance to be inspected for corruption.
     *
     * Returns:
     *   True if the supplied ChatLocalProfile instance should be considered to be
     *   in a corrupt state.  False, otherwise.
     */
    isCorrupt: function (chatLocalProfile) {
        try {
            if (ChatUtility.isNullOrUndefined(chatLocalProfile)
                || !ChatUtility.isNullOrUndefined(chatLocalProfile.userSettings)
                || !ChatUtility.isFunction(chatLocalProfile.getValue)
                || !ChatUtility.isFunction(chatLocalProfile.stringify)
                || !ChatUtility.isFunction(chatLocalProfile.toJSON)
                || !ChatUtility.isFunction(chatLocalProfile.toString)) {
                return true;
            }
        }
        catch (ex) {
            return true;
        }

        return false;
    }
}