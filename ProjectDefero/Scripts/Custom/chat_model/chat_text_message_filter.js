﻿/* ChatTextMessageFilter
 *
 * Author(s): Ryan Finn
 */

/* ChatTextMessageFilter allows us to define filtering parameters relevant to filtering
 * ChatTextMessage objects. One example of where this is can be useful is locating ChatTextMessages
 * belonging to a specific user within the context of a specific room from a flat database of
 * ChatTextMessages belonging to many different users in the contexts of many different rooms.
 *
 * We're at the end of the semester, so we're just going to apply all the filter paramters using
 * the AND operator.
 */
function ChatTextMessageFilter() {
    this._uniqueIdentifier = null; // Filters message's unique identifier.
    this._uniqueIdentifier_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._timestampUTC_start = null; // Filters message's UTC creation date start range.
    this._timestampUTC_start_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._timestampUTC_end = null; // Filters message's UTC creation date end range.
    this._timestampUTC_end_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._authorUserId = null; // Filters userId of the message author.
    this._authorUserId_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._senderUserId = null; // Filters userId of the message sender.
    this._senderUserId_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._recipientUserId = null; // Filters userId of the message recipient.
    this._recipientUserId_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._roomId = null; // Filters roomId of the message.
    this._roomId_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._messageBody = null; // Filters message body.
    this._messageBody_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._messageState = null; // Filters ChatTextMessage state.
    this._messageState_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._messageType = null; // Filters ChatTextMessage state.
    this._messageType_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    this._databaseVersion = null; // Filters ChatTextMessate storage version.
    this._databaseVersion_clause = ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO;

    /* Method runs the supplied ChatTextMessage object through this ChatTextMessageFilter.
     *
     * Paramters:
     *   chatTextMessage: ChatTextMessage object to send through the filter.
     *
     * Returns:
     *   True if this ChatTextMessageFilter caught the ChatTextMessage, false otherwise.
     */
    this.filterChatTextMessage = function (chatTextMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatTextMessage)) {
            return false;
        }

        var resultAccumulator = true;

        if (!ChatUtility.isNullOrUndefined(this._uniqueIdentifier)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(String(this._uniqueIdentifier), String(chatTextMessage.uniqueIdentifier), this._uniqueIdentifier_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._timestampUTC_start)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(Number(this._timestampUTC_start), Number(chatTextMessage.timestampUTC), this._timestampUTC_start_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._timestampUTC_end)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(Number(this._timestampUTC_end), Number(chatTextMessage.timestampUTC), this._timestampUTC_end_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._authorUserId)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(String(this._authorUserId), String(chatTextMessage.authorUserId), this._authorUserId_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._senderUserId)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(String(this._senderUserId), String(chatTextMessage.senderUserId), this._senderUserId_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._recipientUserId)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(String(this._recipientUserId), String(chatTextMessage.recipientUserId), this._recipientUserId_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._roomId)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(String(this._roomId), String(chatTextMessage.roomId), this._roomId_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._messageBody)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(String(this._messageBody), String(chatTextMessage.messageBody), this._messageBody_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._messageState)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(Number(this._messageState), Number(chatTextMessage.messageBody), this._messageState_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._messageType)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(Number(this._messageType), Number(chatTextMessage.messageType), this._messageType_clause);
        }

        if (!ChatUtility.isNullOrUndefined(this._databaseVersion)) {
            resultAccumulator = resultAccumulator
                && this.compareOperands(Number(this._databaseVersion), Number(chatTextMessage.databaseVersion), this._databaseVersion_clause);
        }

        return resultAccumulator;
    };

    /* Method performs a comparison of the two supplied operands using the supplied
     * operator to define the comparison.
     *
     * Parameters:
     *   operandOne: Object that is to be compared with parameter operandTwo.
     *   operandTwo: Object that is to be compared with parameter operandOne.
     *   operator: ChatTextMessageFilter.FILTER_CLAUSE that defines the comparison approach.
     */
    this.compareOperands = function (operandOne, operandTwo, operator) {
        try {
            switch (operator) {
                case ChatTextMessageFilter.FILTER_CLAUSE.STARTS_WITH:
                    return operandTwo.indexOf(operandOne) === 0;
                case ChatTextMessageFilter.FILTER_CLAUSE.ENDS_WITH:
                    return operandTwo.indexOf(operandOne, operandTwo.length - operandOne.length) !== -1;
                case ChatTextMessageFilter.FILTER_CLAUSE.CONTAINS:
                    return operandTwo.indexOf(operandOne) > -1;
                case ChatTextMessageFilter.FILTER_CLAUSE.GREATER_THAN:
                    return operandTwo > operandOne;
                case ChatTextMessageFilter.FILTER_CLAUSE.GREATER_THAN_OR_EQUAL_TO:
                    return operandTwo >= operandOne;
                case ChatTextMessageFilter.FILTER_CLAUSE.LESS_THAN:
                    return operandTwo < operandOne;
                case ChatTextMessageFilter.FILTER_CLAUSE.LESS_THAN_OR_EQUAL_TO:
                    return operandTwo <= operandOne;
                case ChatTextMessageFilter.FILTER_CLAUSE.EQUAL_TO:
                    return operandTwo === operandOne;
                case ChatTextMessageFilter.FILTER_CLAUSE.NOT_EQUAL_TO:
                    return operandTwo !== operandOne;
                default:
                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatTextMessageFilter.compareOperands: Unknown operator encountered: "
                        + operator);
                    return false;
            }
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatTextMessageFilter.compareOperands: Exception occurred during comparison:"
                + "\n  " + ex.message);
            return false;
        }
    };

    /* Sets the uniqueIdentifier filter in accordance with the method input.
     *
     * Paramters:
     *   val: uniqueIdentifier filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_uniqueIdentifier = function (val, filterClause) {
        this._uniqueIdentifier = val;
        this._uniqueIdentifier_clause = filterClause;
    };

    /* Sets the timestampUTC_start filter in accordance with the method input.
     *
     * Paramters:
     *   val: timestampUTC_start filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_timestampUTC_start = function (val, filterClause) {
        this._timestampUTC_start = val;
        this._timestampUTC_start_clause = filterClause;
    };

    /* Sets the timestampUTC_end filter in accordance with the method input.
     *
     * Paramters:
     *   val: timestampUTC_end filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_timestampUTC_end = function (val, filterClause) {
        this._timestampUTC_end = val;
        this._timestampUTC_end_clause = filterClause;
    };

    /* Sets the authorUserId filter in accordance with the method input.
     *
     * Paramters:
     *   val: authorUserId filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_authorUserId = function (val, filterClause) {
        this._authorUserId = val;
        this._authorUserId_clause = filterClause;
    };

    /* Sets the senderUserId filter in accordance with the method input.
     *
     * Paramters:
     *   val: senderUserId filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_senderUserId = function (val, filterClause) {
        this._senderUserId = val;
        this._senderUserId_clause = filterClause;
    };

    /* Sets the recipientUserId filter in accordance with the method input.
     *
     * Paramters:
     *   val: recipientUserId filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_recipientUserId = function (val, filterClause) {
        this._recipientUserId = val;
        this._recipientUserId_clause = filterClause;
    };

    /* Sets the roomId filter in accordance with the method input.
     *
     * Paramters:
     *   val: roomId filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_roomId = function (val, filterClause) {
        this._roomId = val;
        this._roomId_clause = filterClause;
    };

    /* Sets the messageBody filter in accordance with the method input.
     *
     * Paramters:
     *   val: messageBody filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_messageBody = function (val, filterClause) {
        this._messageBody = val;
        this._messageBody_clause = filterClause;
    };

    /* Sets the messageState filter in accordance with the method input.
     *
     * Paramters:
     *   val: messageState filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_messageState = function (val, filterClause) {
        this._messageState = val;
        this._messageState_clause = filterClause;
    };

    /* Sets the messageType filter in accordance with the method input.
     *
     * Paramters:
     *   val: messageType filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_messageType = function (val, filterClause) {
        this._messageType = val;
        this._messageType_clause = filterClause;
    };

    /* Sets the databaseVersion filter in accordance with the method input.
     *
     * Paramters:
     *   val: databaseVersion filter value.
     *   filterClause: ChatTextMessageFilter.FILTER_CLAUSE that defines how the filter
     *                 value is compared with the comparator value.
     */
    this.setFilter_databaseVersion = function (val, filterClause) {
        this._databaseVersion = val;
        this._databaseVersion_clause = filterClause;
    };
}

/* ChatTextMessageFilter operands.
 */
ChatTextMessageFilter.FILTER_CLAUSE = {
    STARTS_WITH: 0,
    ENDS_WITH: 1,
    CONTAINS: 2,
    GREATER_THAN: 3,
    GREATER_THAN_OR_EQUAL_TO: 4,
    LESS_THAN: 5,
    LESS_THAN_OR_EQUAL_TO: 6,
    EQUAL_TO: 7,
    NOT_EQUAL_TO: 8
};