﻿/* ChatLastMessageReceivedSet
 *
 * Author(s): Ryan Finn
 */

/* ChatLastMessageReceivedSet stores a set of senderUserIds associated with
 * information about the last message received from each sender per room. The
 * following is an example of how ChatLastMessageReceivedSet can be used:
 *
 * The ChatModel receives a ChatTextMessage from senderUserId 3 while in roomId 1.
 * The ChatModel then makes a record in its ChatLastMessageReceivedSet that the
 * last ChatTextMessage it received from senderUserId 3 while in roomId 1 was as
 * such. This information will be useful when the ChatModel needs to request all
 * the ChatTextMessages it may have missed that senderUserId 3 might have in its
 * possesion without having to just ask for all of senderUserId 3's messages.
 */
function ChatLastMessageReceivedSet() {
    /* Stores key/value pairs in form { roomId<string> : { senderUserId<string> : ChatMessage } }.
     * We're going to set the ChatMessage's message body to and emtpy string
     * to save space.
     */
    var lastMessagesReceived = {};

    /* Only supply copies of ChatTextMessages to this method because it's
     * going to set the message body to an empty string.
     * 
     * Method sets a ChatLocalProfile.  If the ChatLastMessageReceivedSet
     * already contains a ChatTextMessage with the same senderUserId and the
     * supplied ChatTextMessage's UTC timestamp is newer than the existing
     * ChatMessage's UTC timestamp, then the supplied ChatTextMessage will
     * replace the existing ChatTextMessage.
     *
     * Parameters:
     *   chatMessage: ChatMessage to be added. The ChatMessage's message
     *                body will be set to an empty string to save space.
     *                We only need the metadata in here.
     */
    this.setValue = function (chatMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLastMessageReceivedSet.setValue: chatMessage is corrupt.");
            return;
        }

        this.setValueByRoom(chatMessage.roomId, chatMessage.senderUserId, chatMessage);
    };

    /* Only supply copies of ChatTextMessages to this method because it's going to
     * set the message body to an empty string.
     * 
     * Method sets a ChatLocalProfile.  If the ChatLastMessageReceivedSet already
     * contains a ChatTextMessage associated with the same userID and the supplied
     * ChatTextMessage's UTC timestamp is newer than the existing ChatTextMessage's
     * UTC timestamp, then the supplied ChatTextMessage will replace the existing
     * ChatTextMessage.
     *
     * Parameters:
     *   roomId: RoomId of the room in which the ChatTextMessage will have context.
     *   userId: UserId of the user in which the ChatTextMessage will have context.
     *   chatMessage: ChatMessage to be added. The ChatMessage's message
     *                body will be set to an empty string to save space.
     *                We only need the metadata in here.
     */
    this.setValueByRoom = function (roomId, userId, chatMessage) {
        if (ChatTextMessage.prototype.isCorrupt(chatMessage)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLastMessageReceivedSet.setValue: chatMessage is corrupt.");
            return;
        }

        chatMessage.setToEmpty();

        if (roomId in lastMessagesReceived) {
            var oldChatMessage = null;

            try {
                if (userId in lastMessagesReceived[roomId]) {
                    oldChatMessage = lastMessagesReceived[roomId][userId];

                    // Only add newer ChatMessages.
                    if (chatMessage.timestampUTC > oldChatMessage.timestampUTC) {
                        lastMessagesReceived[roomId][userId] = chatMessage;

                        ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                            , "ChatLastMessageReceivedSet.setValue:"
                            + "\n  ChatMessage overwritten for sender user id: " + userId);
                    }
                }
                else {
                    lastMessagesReceived[roomId][userId] = chatMessage;
                }
            }
            catch (ex) {
                try {
                    lastMessagesReceived[roomId][userId] = chatMessage;
                }
                catch (ex) { }

                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatLastMessageReceivedSet.setValue:"
                    + "\n  A corrupt ChatMessage was discovered and removed.");
            }
        }
        else {
            lastMessagesReceived[roomId] = {};
            lastMessagesReceived[roomId][userId] = chatMessage;

            ChatUtility.log(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLastMessageReceivedSet.setValue:"
                + "\n  ChatMessage added for sender user id: " + userId);
        }
    };

    /* Method returns the ChattextMessage corresponding to the supplied senderUserId
     * in then room context of the supplied roomId.
     *
     * Parameters:
     *   userId: The senderUserId of the ChatTextMessage.
     *   roomId: The room id where the ChatTextMessage lives.
     *
     * Returns:
     *   ChatMessage if the supplied senderUserId in the context of the supplied roomId
     *   yields a result. Null, otherwise.
     */
    this.getValue = function (roomId, userId) {
        if (roomId in lastMessagesReceived
            && userId in lastMessagesReceived[roomId]) {
            return lastMessagesReceived[roomId][userId];
        }

        return null;
    };

    /* Method returns the JSON representation of the ChatLastMessageReceivedSet.
     */
    this.stringify = function () {
        var jsonString = null;

        try {
            jsonString = JSON.stringify(this.toJSON());
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLastMessageReceivedSet.stringify: Exception thrown calling JSON.parse: "
                + "\n  " + ex.message);

            jsonString = "";
        }

        return jsonString;
    };

    /* Method returns the pre-serialized JSON object representation of the ChatLastMessageReceivedSet.
     */
    this.toJSON = function () {
        var jsonReadyChatLastMessageReceivedSet = {};

        ChatUtility.iterateAssociativeArrayKeys(
            lastMessagesReceived,
            function (roomId) {
                jsonReadyChatLastMessageReceivedSet[roomId] = {};

                ChatUtility.iterateAssociativeArrayKeys(
                    lastMessagesReceived[roomId],
                    function (senderId) {
                        jsonReadyChatLastMessageReceivedSet[roomId][senderId] = lastMessagesReceived[roomId][senderId].stringify();
                    }, null);
            }, null);

        return jsonReadyChatLastMessageReceivedSet;
    };
}

/* Key used to access the ChatLastMessageReceivedSet object when it's
 * stored in a key/value pair.
 */
ChatLastMessageReceivedSet.STORAGE_KEY = "ChatLastMessageReceivedSet",

ChatLastMessageReceivedSet.prototype = {

    /* Method returns a ChatLastMessageReceivedSet representative of the supplied
     * JSON version of a ChatLastMessageReceivedSet.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatLastMessageReceivedSet.
     *
     * Returns:
     *   ChatLastMessageReceivedSet constructed from values in the supplied JSON string.
     */
    parse: function (chatLastMessageReceivedSetAsJSON) {
        try {
            return ChatLastMessageReceivedSet.prototype.reconstructor(JSON.parse(chatLastMessageReceivedSetAsJSON));
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLastMessageReceivedSet.prototype.parse: Exception thrown calling JSON.parse: "
                + "\n  " + ex.message);
        }

        return new ChatLastMessageReceivedSet();
    },

    /* Method converts the JSON-deserialized representation of a ChatLastMessageReceivedSet
     * instance into an actual ChatLastMessageReceivedSet instance.
     *
     * Parameters:
     *   jsonParseResult: JSON-deserialized representation of a ChatLastMessageReceivedSet
     *                    instance.
     *
     * Returns:
     *   New ChatLastMessageReceivedSet instance built from the values in the supplied object.
     */
    reconstructor: function (jsonParseResult) {
        var chatLastMessageReceivedSet = new ChatLastMessageReceivedSet();

        try {
            ChatUtility.iterateAssociativeArray(
                jsonParseResult,
                function (roomLastMessageListKey, roomLastMessageList) {
                    ChatUtility.iterateAssociativeArray(
                        roomLastMessageList,
                        function (lastMessageListKey, lastMessageList) {
                            chatLastMessageReceivedSet.setValue(ChatTextMessage.prototype.parse(lastMessageList));
                        }, null);
                }, null);
        }
        catch (ex) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatLocalProfile.prototype.reconstructor: Exception thrown: "
                + "\n  " + ex.message);
        }

        return chatLastMessageReceivedSet;
    },
}