﻿/* ChatCallbackFunctionList
 *
 * Author(s): Ryan Finn
 */

/* ChatCallbackFunctionList is a general purpose callback function list that's useful when you want
 * more than one callback function associated with an event.
 */
function ChatCallbackFunctionList() {
    /* List of callback functions in the form function(eventParams<Object>)
     */
    var callbackList = [];

    /* Method adds a callback function to the list.
     *
     * Paramters:
     *   callbackFunction: Callback function in the form function(eventParams<Object>)
     *                     that will be added to the list.
     */
    this.addCallbackFuntion = function (callbackFunction) {
        if (!ChatUtility.isFunction(callbackFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                , "ChatCallbackFunctionList.addCallbackFuntion: callbackFunction is not a function.");

            return;
        }

        callbackList.push(callbackFunction);
    };

    /* Iterates over the list of callback functions, calling each one with the supplied
     * input. Null or undefined callback functions will be automatically removed from the
     * list. The other functions will remain in the list after processing.
     *
     * Parameters:
     *   eventParams: An object of some unkown kind (or perhaps nothing at all) that will
     *                be suppplied to each callback function in the callback list.
     */
    this.processAll = function (eventParams) {
        executeAllCallbackFunctions(eventParams);
    };

    /* Iterates over the list of callback functions, calling each one with the supplied
     * input. The list will be emptied after processing.
     *
     * Parameters:
     *   eventParams: An object of some unkown kind (or perhaps nothing at all) that will
     *                be suppplied to each callback function in the callback list.
     */
    this.processAndRemoveAll = function (eventParams) {
        this.processAll(eventParams);
        this.deleteList();
    };

    /* Iterates over the list of callback functions, passing each one to the supplied
     * function. Null or undefined callback functions will be automatically removed from the
     * list.
     */
    this.iterateAll = function (iterateCallbackFunction) {
        if (!ChatUtility.isFunction(iterateCallbackFunction)) {
            ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatCallbackFunctionList.iterateAll: iterateCallbackFunction is not a function.");
            return;
        }

        // Track all the useless callback functions.
        var badCallbackFunctionIndexes = [];

        for (var i = 0; i < callbackList.length; i++) {
            var callbackFunction = callbackList[i];

            if (!ChatUtility.isFunction(callbackFunction)) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatCallbackFunctionList.iterateAll: Bad callback function discovered; will be removed from list.");
                badCallbackFunctionIndexes.push(i);
            }
            else {
                try {
                    iterateCallbackFunction(callbackFunction);
                }
                catch (ex) {
                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatCallbackFunctionList.iterateAll: Exception occurred when calling supplied callback function:"
                        + "\n  " + ex.message);
                }
            }
        }

        // Remove the useless callback functions.
        for (var i = 0; i < badCallbackFunctionIndexes.length; i++) {
            callbackList.splice(badCallbackFunctionIndexes[i], 1);
        }
    };

    /* Iterates over the list of callback functions, calling each one with the supplied
     * input.  Null or undefined callback functions will be automatically removed from the
     * list.
     *
     * Parameters:
     *   eventParams: An object of some unkown kind (or perhaps nothing at all) that will
     *                be suppplied to each callback function in the callback list.
     */
    var executeAllCallbackFunctions = function (eventParams) {
        var badCallbackFunctionIndexes = [];

        for (var i = 0; i < callbackList.length; i++) {
            var callbackFunction = callbackList[i];

            if (!ChatUtility.isFunction(callbackFunction)) {
                ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                    , "ChatCallbackFunctionList.iterateAll: Bad callback function discovered; will be removed from list.");
                badCallbackFunctionIndexes.push(i);
            }
            else {
                try {
                    callbackFunction(eventParams);
                }
                catch (ex) {
                    //badCallbackFunctionIndexes.push(i);
                    ChatUtility.error(ChatUtility.LOG_MODE.DEVELOPER_DEBUG
                        , "ChatCallbackFunctionList.iterateAll: Exception occurred when calling callback function. Exception:"
                        + "\n  " + ex.message);
                }
            }
        }

        // Remove the bad callback functions.
        for (var i = 0; i < badCallbackFunctionIndexes.length; i++) {
            callbackList.splice(badCallbackFunctionIndexes[i], 1);
        }
    }.bind(this);

    /* Method deletes the list, returning it to an empty state.
     */
    this.deleteList= function () {
        callbackList = [];
    };
}