﻿/* ChatLastMessageSentSet
 *
 * Author(s): Ryan Finn
 */

/* ChatLastMessageSentSet stores a set of recipientUserId associated with
 * information about the last message sent to each recipientUserId per room.
 * The following is an example of how ChatLastMessageSentSet can be used:
 *
 * The ChatModel sends a ChatTextMessage to recipientUserId 3 while in roomId 1.
 * The ChatModel then makes a record in its ChatLastMessageSentSet that the
 * last ChatTextMessage it sent to recipientUserId 3 while in roomId 1 was as
 * such. This information will be useful when the ChatModel needs to request all
 * the ChatTextMessages it may have missed that recipientUserId 3 might have in its
 * possesion without having to just ask for all of recipientUserId 3's messages.
 */
function ChatLastMessageSentSet() {
    var chatLastMessageSentSet = new ChatLastMessageReceivedSet();

    /* Only supply copies of ChatTextMessages to this method because it's
     * going to set the message body to an empty string.
     * 
     * Method sets a ChatLocalProfile.  If the ChatLastMessageSentSet
     * already contains a ChatTextMessage with the same roomId and the
     * supplied ChatTextMessage's UTC timestamp is newer than the existing
     * ChatMessage's UTC timestamp, then the supplied ChatTextMessage will
     * replace the existing ChatTextMessage.
     *
     * Parameters:
     *   roomId: RoomId of the room in which the ChatTextMessage will have context.
     *   recipientUserId: UserId of the user in which the ChatTextMessage will have context.
     *   chatMessage: ChatMessage to be added. The ChatMessage's message
     *                body will be set to an empty string to save space.
     *                We only need the metadata in here.
     */
    this.setValue = function (chatMessage) {
        chatLastMessageSentSet.setValueByRoom(chatMessage.roomId, chatMessage.recipientUserId, chatMessage);
    };

    /* Method returns the ChattextMessage corresponding to the supplied senderUserId
     * in then room context of the supplied roomId.
     *
     * Parameters:
     *   recipientUserId: The recipientUserId of the ChatTextMessage.
     *   roomId: The room id where the ChatTextMessage lives.
     *
     * Returns:
     *   ChatMessage if the supplied senderUserId in the context of the supplied roomId
     *   yields a result. Null, otherwise.
     */
    this.getValue = function (roomId, recipientUserId) {
        return chatLastMessageSentSet.getValue(roomId, recipientUserId);
    };

    /* Method returns the JSON representation of the ChatLastMessageSentSet.
     */
    this.stringify = function () {
        return chatLastMessageSentSet.stringify();
    };

    /* Method returns the pre-serialized JSON object representation of the ChatLastMessageSentSet.
     */
    this.toJSON = function () {
        return chatLastMessageSentSet.toJSON();
    };
}

/* Key used to access the ChatLastMessageSentSet object when it's
 * stored in a key/value pair.
 */
ChatLastMessageSentSet.STORAGE_KEY = "ChatLastMessageSentSet",

ChatLastMessageSentSet.prototype = {

    /* Method returns a ChatLastMessageSentSet representative of the supplied
     * JSON version of a ChatLastMessageSentSet.
     *
     * Parameters:
     *   JSON string that parses to values compatible with ChatLastMessageReceivedSet.
     *
     * Returns:
     *   ChatLastMessageReceivedSet constructed from values in the supplied JSON string.
     */
    parse: function (chatLastMessageSentSetAsJSON) {
        return ChatLastMessageReceivedSet.prototype.reconstructor(JSON.parse(chatLastMessageSentSetAsJSON));
    },

    /* Method converts the JSON-deserialized representation of a ChatLastMessageSentSet
     * instance into an actual ChatLastMessageSentSet instance.
     *
     * Parameters:
     *   jsonParseResult: JSON-deserialized representation of a ChatLastMessageSentSet instance.
     *
     * Returns:
     *   New ChatLastMessageSentSet instance built from the values in the supplied object.
     */
    reconstructor: function (jsonParseResult) {
        var chatLastMessageSentSet = new ChatLastMessageSentSet();
        chatLastMessageSentSet.chatLastMessageSentSet = ChatLastMessageReceivedSet.prototype.reconstructor(jsonParseResult);

        return chatLastMessageSentSet;
    },
}