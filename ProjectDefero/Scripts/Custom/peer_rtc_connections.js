﻿function PeerRTCConnections(_rootScope) {
    var LOGGING = false;
    //a variable we need to access the rootscope object
    var rootScope = _rootScope;

    //This is a dictionary for every peer connection we have.
    var peerConnections = {};

    /**
    *Our current list of ice servers.
    *
    *This list currently only contains google's public STUN servers. 
    *For more reliable connections you would want to add some TURN servers.
    *
    *For better security, you would want to host your own ICE servers, 
    *remove the google ones and replace them with your own.
    *
    *Also, you will want to double check peer-rtc.js for the settings it is passing 
    *to the peerConnection object to make sure it is not limiting the type of ICE you can use.
    *
    **/
    var iceServers = [{ url: 'stun:stun.l.google.com:19302' },
								  { url: 'stun:stun1.l.google.com:19302' },
								  { url: 'stun:stun2.l.google.com:19302' },
								  { url: 'stun:stun3.l.google.com:19302' },
								  { url: 'stun:stun4.l.google.com:19302' }];

    /**
	*Event handler for when a PeerRTC object needs to send data through signaling server.
	*                              {destinationID, type, room, data}
	*_data:JSON object in the form {id, type, room, data}
	**/
    var sendPeerSignalData = function (_data) {
        var destination = _data.destination;
        var data = _data.data;
        //socket.send(JSON.stringify(createSignalMessage(_data.destination, "message", null, _data)));
        if (!rootScope.signal.send({ destination: destination, type: "signal", room: null, data: data })) {
            console.log("Failed to send signal to: " + destination);
        }
    }

    /**
	*Event handler for when a peer randomly loses connection.
	*
	*_data: JSON object {id}
	**/
    var peerDisconnected = function (_data) {
        var peerID = _data.id

        if (peerConnections[peerID]) {
            if (LOGGING) {
                console.log("Sending connection check to server.");
            }

            var message = { destination: peerID, type: "stillConnected", room: null, data: null };
            //if return value is false, then it failed to even send over websocket.
            if (!rootScope.signal.send(message)) {
                console.log("Failed to send signal to: " + peerID);
            }
        }
    }

    this.createNewPeerConnection = function (_peerID, _peerBrowser) {
        var peerBrowser = "unkown";

        if (_peerBrowser) {
            peerBrowser = _peerBrowser;
        }

        //Check if we already have a connection for this peer.
        if (peerConnections[_peerID]) {
            //close the peerconnection and chat channels
            peerConnections[_peerID].peerConnection.disconnect();
            //now initialize it again.
            peerConnections[_peerID].init();
        }
        else {
            //create a PeerRTC object for this connection.
            var newConnection = new PeerRTC(_peerID, iceServers, peerBrowser);

            //setup function to handle data that peerRTC needs to send through signaling
            newConnection.onDataToSend = sendPeerSignalData;

            //setup event handler for when the peer randomly disconnects.
            newConnection.onPeerDisconnected = peerDisconnected;

            //initialize the PeerRTC object now that we have it.
            newConnection.init();

            //add the new PeerRTC connection to our data structure.
            peerConnections[_peerID] = { peerConnection: newConnection, cleanup: {} };
        }
    }

    /**
	*This is a signal that comes when a peer is initiating the very first connection.
	**/
    this.connectionRequest = function (_signal) {
        var source = _signal.source;
        if (LOGGING) {
            console.log("Received connection request from " +source);
        }

        //If we have existing connection, clean it up and then delete it.
        if (peerConnections[source]) {
            if (LOGGING) {
                console.log("Have old peerConnection from " +source + ", cleaning up.");
            }

            //close all connections.
            peerConnections[source].peerConnection.disconnect();

            //see if rooms have provided a cleanup method to call.
            for (var key in peerConnections[source].cleanup) {
                if (typeof (peerConnections[source].cleanup[key]) === "function") {
                    peerConnections[source].cleanup[key](source);
                }
            }

            //remove the peerRTC object from our list.
            delete peerConnections[source];
        }
    }

    /**
	*This is called when we receive a signal to update the peer browser data.
	**/
    this.updatePeerBrowser = function (_signal) {
        var source = _signal.source;

        if (peerConnections[source]) {
            var peerBrowser = _signal.data.browser;
            peerConnections[source].peerConnection.setPeerBrowser(peerBrowser);
        }
        else {
            //error
        }
    }

    /**
	*Call this when a webRTC Signal needs to be sent to the PeerRTC object.
	**/
    this.peerRTCSignal = function (_signal) {
        var source = _signal.source;

        //make sure we actually have the peer connection.
        if (peerConnections[source]) {
            //now pass the message to the PeerRTC object.
            peerConnections[source].peerConnection.receivedMessage(_signal.data);
        }
        else {
            //error
        }
    }

    /**
    *Currently, this does not do anything but serve as a place to send the message.
    **/
    this.stillConnectedMessage = function(_signal){

    }

    /**
    *Call this when we there is a peerNotConnected signal.
    **/
    this.peerIsDisconnected = function (_signal) {
        var peerID;
        if (_signal.data) {
            peerID = _signal.data.peerID;
        }
        else {
            console.log("PeerID is not defined, this is bad and should not happen. ");
        }

        if (LOGGING) {
            console.log("Server verified that peer " + peerID + " disconnected.");
        }

        if (peerID && peerConnections[peerID]) {
            //If we are here, Cleanup the peerconnection.
            peerConnections[peerID].peerConnection.disconnect();

            //see if rooms have provided a cleanup method to call.
            for (var key in peerConnections[peerID].cleanup) {
                if (typeof (peerConnections[peerID].cleanup[key]) === "function") {
                    peerConnections[peerID].cleanup[key](peerID);
                }
            }

            //remove the peer from our list.
            delete peerConnections[peerID];
        }
    }

    /**
	*This will add event handlers that every room should have associated with the peer connection
	*_peerRTCCallbacks: JSON object {chatEnabled, dataEnabled, receivedChatMessage, receivedDataMessage}
	**/
    this.registerRoomWithPeer = function (_roomID, _peerID, _peerRTCCallbacks, _roomCallBacks) {
        if (peerConnections[_peerID]) {
            //add the chat enabled event handler to the PeerRTC object.
            peerConnections[_peerID].peerConnection.addChatEnabledEventHandler(_roomID, _peerRTCCallbacks.chatEnabled);
            peerConnections[_peerID].peerConnection.addDataEnabledEventHandler(_roomID, _peerRTCCallbacks.dataEnabled);
            peerConnections[_peerID].peerConnection.addReceivedChatMessageEventHandler(_roomID, _peerRTCCallbacks.receivedChatMessage);
            peerConnections[_peerID].peerConnection.addReceivedDataMessageEventHandler(_roomID, _peerRTCCallbacks.receivedDataMessage);
            peerConnections[_peerID].cleanup[_roomID] = _roomCallBacks.peerRTCCleanUp;
        }
        else {
            //error
        }

        //chat may be enabled by the time we register the room.
        if (peerConnections[_peerID].isChatEnabled) {
            if (typeof (_peerRTCCallbacks.chatEnabled) === "function") {
                _peerRTCCallbacks.chatEnabled({ id: _peerID });
            }
        }

        //data may be enabled by the time we register the room.
        if (peerConnections[_peerID].isDataEnabled) {
            if (typeof (_peerRTCCallbacks.dataEnabled) === "function") {
                _peerRTCCallbacks.dataEnabled({ id: _peerID });
            }
        }
    }

    /**
    *This will remove any functions associated with the provided roomID from all peer objects.
    **/
    this.removeRoomFromPeers = function (_roomID) {
        for(var key in peerConnections) {
            //remove event handlers from the PeerRTC object.
            peerConnections[key].peerConnection.removeChatEnabledEventHandler(_roomID);
            peerConnections[key].peerConnection.removeDataEnabledEventHandler(_roomID);
            peerConnections[key].peerConnection.removeReceivedChatMessageEventHandler(_roomID);
            peerConnections[key].peerConnection.removeReceivedDataMessageEventHandler(_roomID);
            if (peerConnections[key].cleanup[_roomID]) {
                delete peerConnections[key].cleanup[_roomID];
            }
        }
    }

    /**
	*This will change the event handlers related to stream updates. The room currently in the call needs to call this.
	*_eventHandlers: JSON object {receivedRemoteStream, peerRemovedStream}
	**/
    this.changeStreamEventHandlers = function (_eventHandlers) {
        for (var key in peerConnections) {
            peerConnections[key].peerConnection.onReceivedRemoteStream = _eventHandlers.receivedRemoteStream;
            peerConnections[key].peerConnection.onPeerRemovedStream = _eventHandlers.peerRemovedStream;
        }
    }

    /**
	*This returns the peer connection object 
	**/
    this.getPeerConnection = function (_peerID) {
        return peerConnections[_peerID];
    }

    /**
	*Returns every PeerRTC object we are connected to.
	**/
    this.getAllPeerConnections = function () {
        var allPeers = {};
        for (var key in peerConnections) {
            allPeers[key] = peerConnections[key].peerConnection;
        }

        return allPeers;
    }
}