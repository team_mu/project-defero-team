﻿function PeerRTC(_id, _iceServers, _peerBrowser) {
    var LOGGING = false;

    //Public variables
    this.id = _id;

    //callbacks that need to be set.
    this.onDataToSend = null;
    this.onPeerDisconnected = null;
    this.onReceivedRemoteStream = null;
    this.onPeerRemovedStream = null;

    //new set of variables for event handlers.
    var chatMessageEventHandlers = {};
    var dataMessageEventHandlers = {};
    var chatEnabledEventHandlers = {};
    var dataEnabledEventHandlers = {};

    ///////////////////////////////////
    var firefoxWorkAroundTriggered = false;
    //////////////////////////////////


    //private variables
    var peerBrowser = _peerBrowser;
    var currentIceServers = _iceServers;
    var peerConnection = null;
    var chatConnection = null;
    var dataConnection = null;
    var isInitialized = false;
    var chatEnabled = false;
    var dataEnabled = false;
    var needsNegotiation = false;
    var peerConnectionStatus = "notInitialized";

    /* Stores unsent data chunks so that they might be sent later. Items in the set are in the form
     * { id<String>: ChatDataChunkSet }
     */
    var outboundDataChunkBufferSets = {};

    /* Stores incoming data chunks that will compose a complete message. Items in the set are in the
     * form { id<String>: ChatDataChunkSet }
     * Incoming data chunks can overlap and can arrive in any order.
     */
    var inboundDataChunkBufferSets = {};

    //Start of method declarations.

    //Create a function that other methods can call when an error occurs
    var error = function (_errorData) {
        var errorObject = { object: "PeerRTC", peerId: this.id, type: _errorData.type, where: _errorData.where, message: _errorData.message };

        if (navigator.mozGetUserMedia) {
            console.log("\nError: Class: " + errorObject.object + "\nFunction: " + errorObject.where + "\nPeer ID: " + errorObject.peerId + "\nMessage: ");
            console.log(errorObject.message);
        }
        else {
            console.log("\nError: Class: " + errorObject.object + "\nFunction: " + errorObject.where + "\nPeer ID: " + errorObject.peerId + "\nMessage: " + errorObject.message);
        }
    }.bind(this);

    //Create a function that other methods can call when they want to log something.
    var log = function (_logData) {
        var logObject = { object: "PeerRTC", peerId: this.id, where: _logData.where, message: _logData.message };
        if (LOGGING) {
            console.log("\nClass: " + logObject.object + "\nFunction: " + logObject.where + "\nPeer ID: " + logObject.peerId + "\nMessage: " + logObject.message);
        }
    }.bind(this);

    /**
    *create a function that other methods will call when they need to send data to the peer before the peerconnection is setup.
    **/
    var sendToPeer = function (_data) {
        if (typeof (this.onDataToSend) === "function") {
            var message = { "destination": this.id, data: _data };
            this.onDataToSend(message);
        }
        else {
            log({ where: "sendToPeer()", message: "onDataToSend does not have function assigned to it" });
        }
    }.bind(this);

    /**
    *Function called when peerconnection receives some kind of datachannel event.
    **/
    var dataChannelEventHandler = function (_event) {
        //check what channel was created.
        if (_event.channel.label === "chat") {
            log({ where: "dataChannelEventHandler()", message: "received event for chat channel." });
            chatConnection = _event.channel;
            setupChatChannel();
        }
        else if (_event.channel.label === "data") //If not a chat, then it must be data channel
        {
            log({ where: "dataChannelEventHandler()", message: "received event for data channel." });
            dataConnection = _event.channel;
            setupDataChannel();
        }
    }.bind(this);

    /**
    *Event handler for when peerconnection needs to negotiate with other peer connections.
    *Should called after changing streams or data channels.
    **/
    var negotiationNeededEventHandler = function () {
        peerConnection.createOffer(createdOffer, error, { offerToReceiveAudio: true, offerToReceiveVideo: true, iceRestart: true });
    }

    /**
    *Call this to reinitialize the peerconnection to a readyToConnect stat
    **/
    var reinit = function () {
        this.disconnect();
        this.init();
    }.bind(this);

    /**
    *
    **/
    var syncPeerConnection = function () {
        //try and grab local stream.
        var tmpStream = peerConnection.getLocalStreams()[0];

        //reinitialize the peerconnection object.
        this.reinit();
        //if we have a stream, put it back.
        if (tmpStream) {
            peerConnection.addStream(tmpStream);
        }
        //notify peer 
        //this will obviously change to a better signal in the future.
        var message = { type: "syncPeerConnection" };
        sendToPeer(message);
    }.bind(this);

    /**
    *This is a method added so that we can get firefox working until they properly implement the specification.
    **/
    var firefoxWorkAround = function (_type, _stream) {
        reinit();
        firefoxWorkAroundTriggered = true;
    }.bind(this);

    /**
    *This is a method added so that we can get firefox working until they properly implement the specification.
    **/
    var firefoxRenegotiate = function () {
        //check if firefox workaround has already been called.
        if (firefoxWorkAroundTriggered) {
            //if it has, clear the flag for next time.
            firefoxWorkAroundTriggered = false;
        }
        else {
            //if it has not been called, then we need to do special work before calling firefox renegotiation.
            var tmpStream = peerConnection.getLocalStreams()[0];
            firefoxWorkAround();
            if (tmpStream) {
                peerConnection.addStream(tmpStream);
            }

            //clear the flag set by firefoxWorkAround
            firefoxWorkAroundTriggered = false;
        }

        var message = { type: "firefoxRenegotiate" };
        sendToPeer(message);
    }.bind(this);

    /**
    *Event hanlder for when the signaling state of peer connection changes.
    **/
    var peerConnectionSignalStateChangeEventHandler = function () {
        if (peerConnection.signalingState === "stable") {
            //log in console for debug
            log({ where: "peerConnectionSignalStateChangeEventHandler()", message: "Peerconnection signaling state changed to stable" });
            //check if we are waiting to renegotiate. If so, then start the negotiation.
            if (needsNegotiation) {
                needsNegotiation = false;
                if (peerConnection.signalingState === "stable") {
                    negotiationNeededEventHandler();
                }
            }
        }
            //else if (peerConnection.signalingState === "closed") {
            //    log({ where: "peerConnectionSignalStateChangeEventHandler()", message: "PeerRTC is closed." });
            //    this.onPeerRemovedStream({ id: this.id, stream: null });
            //}
        else {
            //log in consolse for debug....
            log({ where: "peerConnectionSignalStateChangeEventHandler()", message: "Peerconnection signaling state changed to: " + peerConnection.signalingState });
        }
    }.bind(this);

    /**
    *Event handler for when iceConnectionStateChanges.
    **/
    var onIceConnectionStateChangeEventHandler = function () {
        if (peerConnection.iceConnectionState === "disconnected") {
            log({ where: "onIceConnectionStateChangeEventHandler()", message: "Ice agent detected that peer is disconnected." });
            if (typeof (this.onPeerDisconnected) === "function") {
                this.onPeerDisconnected({ id: this.id });
            }
        }
        else if (peerConnection.iceConnectionState === "failed") {
            error({ where: "onIceConnectionStateChangeEventHandler()", message: "Ice connection agent failed, we are trying again." });
            //close all connections of old and perform cleanup.
            this.disconnect();
            //Setup like we are starting over.
            this.init();
            //send signal to peer
            var message = { type: "iceFailed" };
            sendToPeer(message);
        }
        else {
            peerConnectionStatus = "connecting";
            log({ where: "onIceConnectionStateChangeEventHandler()", message: "Ice connection state changed to: " + peerConnection.iceConnectionState });
        }
    }.bind(this);

    /**
    *Checks if onChatEnabled has been set to a function, and if so calls it providing ID of this.
    **/
    var fireChatEnabled = function () {
        peerConnectionStatus = "connected";

        //if we are here, then we have set both local and remote, same with peer, so that means chat is fully enabled.
        var noEventHandler = true;
        for (var key in chatEnabledEventHandlers) {
            if (typeof (chatEnabledEventHandlers[key]) === "function") {
                noEventHandler = false;
                chatEnabledEventHandlers[key]({ id: this.id });
            }
        }

        if (noEventHandler) {
            log({ where: "fireChatEnabled()", message: "No event handler set for onChatEnabled" });
        }

        //update the flag
        chatEnabled = true;
    }.bind(this);

    /**
    *Check if onDataEnabled has been set to a function, and if it has it calls the dunction providing a JSON object.
    **/
    var fireDataEnabled = function () {
        var noEventHandler = true;
        for (var key in dataEnabledEventHandlers) {
            if (typeof (dataEnabledEventHandlers[key]) === "function") {
                noEventHandler = false;
                dataEnabledEventHandlers[key]({ id: this.id });
            }
        }

        if (noEventHandler) {
            log({ where: "fireDataEnabled()", message: "No event handler set for onDataEnabled" });
        }

        //update the flag
        dataEnabled = true;
    }

    /**
    * Event handler for onmessage.
    * After JSON parsing, parameter _message.data will have the form
    * { setId<String>,         // unique identifier used by PeerRTC to associate the chunk with a buffer.
    *   index<int>,            // Index where the chunk will fit when the message is reconstructed.
    *   targetChunkCount<int>, // Total number of indexed chunks needed to reconstruct the message.
    *   data<String> }         // Chunk of message data.
    *
    * Notes: If chunk operations fail in this method, the chunks associated to the same message that
    * succeeded will sit in their buffer forever, waisting memory.
    **/
    var chatOnMessageEventHandler = function (_message) {
        var dataChunk = null;

        try {
            dataChunk = JSON.parse(_message.data);
        }
        catch (ex) {
            error({ where: "chatOnMessageEventHandler", message: "Failed to perform JSON.parse." });
            return;
        }

        try {
            var chunkSet = null;
            if (!(dataChunk.setId in inboundDataChunkBufferSets)) {
                // This must be the first chunk of this set.
                chunkSet = new ChatDataChunkSet();
                inboundDataChunkBufferSets[dataChunk.setId] = chunkSet;
            }
            else {
                chunkSet = inboundDataChunkBufferSets[dataChunk.setId];
            }

            chunkSet.addChunk(dataChunk.index, dataChunk.data);

            if (chunkSet.getCurrentChunkCount() >= dataChunk.targetChunkCount) {
                // We've completely reconstructed a message.
                //now send the message to every event handler we have.
                var chatClientMessage = ChatClientMessage.prototype.parse(chunkSet.getReconstructedData());

                //Determine if there is a function to call, and if so call it.
                if (chatMessageEventHandlers[chatClientMessage.roomId]) {
                    chatMessageEventHandlers[chatClientMessage.roomId]({ "source": this.id, "message": chatClientMessage });
                }
                else {
                    error({ where: "chatOnMessageEventHandler", message: "No event handlers assigned for incoming chat messages" });
                }

                //for (var i = 0; i < chatMessageEventHandlers.length; i++) {
                //    if (typeof (chatMessageEventHandlers[i]) === "function") {
                //        chatMessageEventHandlers[i]({ "source": this.id, "message": chatClientMessage });
                //    }
                //    else {
                //        error({ where: "chatOnMessageEventHandler", message: "No event handlers assigned for incoming chat messages" });
                //    }
                //}

                delete inboundDataChunkBufferSets[dataChunk.setId];
            }
        }
        catch (ex) {
            error({ where: "chatOnMessageEventHandler", message: ex.message });
        }
    }.bind(this);

    /**
    *Event handler for when chat data channel becomes open, which signifies we can now send data.
    **/
    var chatOnOpenEventHandler = function () {
        log({ where: "chatOnOpenEventHandler()", message: "chat channel with peer is open" });

        //if (!chatEnabled) {
        //    fireChatEnabled();
        //}
        //else {
        //    log({ where: "chatOnOpenEventHandler()", message: "chat already enabled with peer" });
        //}

        fireChatEnabled();

        this.sendQueuedChatMessages();
    }.bind(this);

    /**
    *Event handler for when the data channel opens.
    **/
    var dataOnOpenEventHandler = function () {
        log({ where: "dataOnOpenEventHandler()", message: "data channel with peer is open" });
        //if (!dataEnabled) {
        //    fireDataEnabled();
        //}
        //else {
        //    log({ where: "dataOnOpenEventHandler()", message: "data channel already enabled with peer" });
        //}

        fireDataEnabled();
    }

    /**
    *Adds appropriate event handlers to chat data channel.
    **/
    var setupChatChannel = function () {
        if (chatConnection) {
            log({ where: "setupChatChannel()", message: "Setting up Chat Channel" });
            //setup event handler for when data comes in on the chat data channel
            chatConnection.onmessage = chatOnMessageEventHandler;

            //setup event handler for when chat data channel opens.
            chatConnection.onopen = chatOnOpenEventHandler;
        }
        else {
            error({ where: "setupChatChannel()", message: "Tried to set event handlers for chat channel but it is null." });
        }
    }.bind(this);

    /**
    *Adds appropriate event handlers to chat data channel.
    **/
    var setupDataChannel = function () {
        if (dataConnection) {
            log({ where: "setupDataChannel()", message: "Setting up Data Channel" });
            //setup event handler for when data comes in on the data data channel
            dataConnection.onmessage = function () {
                //do something or set to another function.
            };

            //setup event handler for when data channel opens.
            dataConnection.onopen = dataOnOpenEventHandler;
        }
        else {
            error({ where: "setupDataChannel()", message: "Tried to set event handlers for data channel but it is null." });
        }
    }.bind(this);

    //Send local description to peer
    var sendDescription = function () {
        log({ where: "sendDescription()", message: "set local description" });
        log({ where: "sendDescription()", message: "sending description to peer." });
        var message = { type: "description", data: peerConnection.localDescription };
        sendToPeer(message);
    }.bind(this);

    // Success function for "createOffer".
    var createdOffer = function (_description) {
        //log({where: "createdOffer()",  message:"created offer"});
        try {
            peerConnection.setLocalDescription(_description, sendDescription, function (_error) { error({ where: "createdOffer()", message: _error }); }.bind(this));
        }
        catch (_exception) {
            error({ where: "createdOffer()", message: _exception});
            syncPeerConnection();
        }
        
    }.bind(this);

    // Success function for "createAnswer"
    var createdAnswer = function (_description) {
        //log({where: "createdAnswer()",  message:"created answer"});
        try{
            peerConnection.setLocalDescription(_description, sendDescription, function (_error) { error({ where: "createdOffer()", message: _error }); }.bind(this));
        }
        catch (_exception) {
            error({ where: "createdAnswer()", message: _exception });
            syncPeerConnection();
        }
    }.bind(this);

    //Success function for "setRemoteDescription"
    var remoteDescriptionSet = function () {
        log({ where: "remoteDescriptionSet()", message: "set remote description" });

        // if we received an offer, we need to answer
        if (peerConnection.remoteDescription.type === "offer") {
            peerConnection.createAnswer(createdAnswer, function (_error) { error({ where: "createdOffer()", message: _error }); });
        }
    }.bind(this);

    //create a datachannel that can be used for chat.
    var enableChat = function () {
        log({ where: "enableChat()", message: "Creating chat channel" });

        try {
            //we do not send any optional properties, therefore this is a reliable channel
            chatConnection = peerConnection.createDataChannel("chat");
            setupChatChannel();
        }
        catch (_exception) {
            error({ where: "enableChat()", message: "Unable to create chat channel: " + _exception });
        }
    }

    //create a datachannel that can be used for file transfers.
    var enableData = function () {
        log({ where: "enableData()", message: "Creating data channel" });

        try {
            //we do not send any optional properties, therefore this is a reliable channel
            dataConnection = peerConnection.createDataChannel("data");
            setupDataChannel();
        }
        catch (_exception) {
            error({ where: "enabledata()", message: "Unable to create data channel: " + _exception });
        }
    }

    /**
    *Adds the supplied function to our list of functions to call when we get a chat message.
    **/
    this.addReceivedChatMessageEventHandler = function (_roomID, _chatMessageEventHandler) {
        if (typeof (_chatMessageEventHandler) === "function") {
            chatMessageEventHandlers[_roomID] = _chatMessageEventHandler;
        }
        else {
            log({ where: "addReceivedChatMessageEventHandler()", message: "The supplied argument is not a function" });
        }
    }

    /**
    *Removes the function for the supplied roomID from the chatMessageEventHandlers array.
    **/
    this.removeReceivedChatMessageEventHandler = function (_roomID) {
        if (chatMessageEventHandlers[_roomID]) {
            delete chatMessageEventHandlers[_roomID];
        }
        else {
            log({ where: "removeReceivedChatMessageEventHandler()", message: "No function associated with " + _roomID });
        }
    }

    /**
    *Adds the supplied function to our list of functions to call when we get a data message.
    **/
    this.addReceivedDataMessageEventHandler = function (_roomID, _dataMessageEventHandler) {
        if (typeof (_dataMessageEventHandler) === "function") {
            dataMessageEventHandlers[_roomID] = _dataMessageEventHandler;
        }
        else {
            log({ where: "addReceivedDataMessageEventHandler()", message: "The supplied argument is not a function" });
        }
    }

    /**
    *Removes the function for the supplied roomID from the dataMessageEventHandlers array.
    **/
    this.removeReceivedDataMessageEventHandler = function (_roomID) {
        if (dataMessageEventHandlers[_roomID]) {
            delete dataMessageEventHandlers[_roomID];
        }
        else {
            log({ where: "removeReceivedDataMessageEventHandler()", message: "No function associated with " + _roomID });
        }
    }

    /**
    *Adds the supplied function to our list of functions to call when chat is enabled.
    **/
    this.addChatEnabledEventHandler = function (_roomID, _chatEnabledEventHandler) {
        if (typeof (_chatEnabledEventHandler) === "function"){
            chatEnabledEventHandlers[_roomID] = _chatEnabledEventHandler;
        }
        else {
            log({ where: "addChatEnabledEventHandler()", message: "The supplied argument is not a function" });
        }
    }

    /**
    *Removes the function for the supplied roomID from the ChatEnabledEventHandlers array.
    **/
    this.removeChatEnabledEventHandler = function (_roomID) {
        if (chatEnabledEventHandlers[_roomID]) {
            delete chatEnabledEventHandlers[_roomID];
        }
        else {
            log({ where: "removeChatEnabledEventHandler()", message: "No function associated with " +_roomID });
        }
    }

    /**
    *Adds the supplied function to our list of functions to call when data is enabled.
    **/
    this.addDataEnabledEventHandler = function (_roomID, _dataEnabledEventHandler) {
        if (typeof (_dataEnabledEventHandler) === "function") {
            dataEnabledEventHandlers[_roomID] = _dataEnabledEventHandler;
        }
        else {
            log({ where: "addDataEnabledEventHandler()", message: "The supplied argument is not a function" });
        }
    }

    /**
    *Removes the function for the supplied roomID from the DataEnabledEventHandlers array.
    **/
    this.removeDataEnabledEventHandler = function (_roomID) {
        if (dataEnabledEventHandlers[_roomID]) {
            delete dataEnabledEventHandlers[_roomID];
        }
        else {
            log({ where: "removeDataEnabledEventHandler()", message: "No function associated with " + _roomID });
        }
    }

    /**
    *The will create a data channel to be used for chat and a data channel to be used for file transfers
    **/
    this.createDataChannels = function () {
        enableChat();
        enableData();

        //manually call this since not all browsers support the onnegotiationneeded event.
        negotiationNeededEventHandler();
    }

    /**
    *Add a stream to peer connection, but don't negotiate yet.
    **/
    this.addLocalStream = function (_localStream) {
        if (_localStream) {
            if (peerConnection.getLocalStreams().length === 0) {
                //this is to make firefox work properly until they implement the standards.
                if (navigator.mozGetUserMedia || (peerBrowser === "firefox")) {
                    firefoxWorkAround();
                }

                //add the local stream to our peer connection.
                peerConnection.addStream(_localStream);
            }
            else {
                //log({ where: "addLocalStream()", message: "Already has stream, removing old and adding new one." });
                //we already have a stream, so we need to remove it before adding new one.
                if (navigator.mozGetUserMedia || (peerBrowser === "firefox")) {
                    firefoxWorkAround();
                }
                else {
                    this.removeLocalStream();
                }

                //add the local stream to our peer connection.
                peerConnection.addStream(_localStream);
            }
        }
        else {
            //send an error message.
            error({ where: "addLocalStream()", message: "The stream to be added is null or undefined" });
        }

    }

    /**
    *Remove the local stream from peer connection, but don't negotiate yet.
    **/
    this.removeLocalStream = function () {
        if (peerConnection.getLocalStreams().length > 0) {
            //since firefox currently does not all removing streams, we must check if in firefoz to perform special actions.
            if (navigator.mozGetUserMedia || (peerBrowser === "firefox")) {
                firefoxWorkAround();
            }
                //for all other browsers.
            else {
                peerConnection.removeStream(peerConnection.getLocalStreams()[0]);
            }
        }
        else {
            log({ where: "removeLocalStream()", message: "PeerConnection does not have stream to remove." });
        }
    }

    //Attaches the provided stream to peerConnection which will be streamed to the Peer.
    this.renegotiate = function () {
        if (navigator.mozGetUserMedia || (peerBrowser === "firefox")) {
            //call the method to handle renegotiation for firefox.
            firefoxRenegotiate();
        }
        else {
            //check if our connection is stable, if not then set a flag so we know we need to renegiate when it is stable.
            if (peerConnection.signalingState === "stable") {
                //this method handles renegotiating the peer connection.
                negotiationNeededEventHandler();
            }
            else {
                //we need to renegotiate. 
                needsNegotiation = true;
            }
        }
    }

    /**
    *Event handler to handle when peerConnection gets a peer stream.
    **/
    var peerAddedStreamEventHandler = function (_event) {
        //get the tracks that in the stream
        var tracks = _event.stream.getTracks();

        //set a flag
        var notify = false;

        //make sure we actually have tracks.
        if (tracks.length > 0) {
            //set the flag to true;
            notify = true;

            //loop through all the tracks and check the names
            //for (var i = 0; i < tracks.length; i++) {
            //    //if the label matches one of these, then the stream is fake, so don't fire event handler.
            //    if ((tracks[i].label === "defaulta0") || (tracks[i].label === "defaultv0")) {
            //        notify = false;
            //        break;
            //    }
            //}
        }

        //log({where: "peerAddedStreamEventHandler()",  message:"received peer stream"});
        if (notify) {
            if (typeof (this.onReceivedRemoteStream) === "function") {
                this.onReceivedRemoteStream({ id: this.id, "stream": _event.stream });
            }
        }
    }.bind(this);

    /**
    *
    **/
    var peerRemovedStreamEventHandler = function (_event) {
        var stream = _event.stream;
        //since we may have a fake stream, there will be an event trying to remove it. Since we did not add the blank
        //stream, ignore it.
        if (stream.id != "default") {
            log({ where: "peerRemovedStreamEventHandler()", message: "Peer Removed Stream." });
            if (typeof (this.onPeerRemovedStream) === "function") {
                this.onPeerRemovedStream({ id: this.id, stream: _event.stream });
            }
        }
    }.bind(this);

    /**
    *
    **/
    var onNewIceCandidateEventHandler = function (_event) {
        if (_event.candidate) {
            var message = { type: "iceCandidate", data: _event.candidate };
            sendToPeer(message);
        }
    }.bind(this);

    /**
    *Create a peerConnection object, initiate event handlers, and prepare the peerConnection object for use.
    **/
    this.init = function () {
        peerConnectionStatus = "readyToConnect";

        /**
        *webRTCConfiguration are parameters you can pass into the RTCPeerConnection constructor.
        *
        *Currently this contains:
        *   -iceServers which is the list of ICE servers to use,
        *   -bundlePolicy which tells the browser how to form messages to 
        *    other browsers. We currently chose an option providing as most compatibility as possible.
        *
        *Other parameters can be passed in, they will need to be researched though
        *
        **/
        var webRTCConfiguration = { iceServers: currentIceServers, bundlePolicy: "max-compat" };

        //create a new peerConnection object.
        peerConnection = new RTCPeerConnection(webRTCConfiguration);

        //browser fires event if a data channel is added
        peerConnection.ondatachannel = dataChannelEventHandler;

        //set event handler for when the signaling state of peer connection changes. this is fired when createoffer, createanswer, setlocaldescription
        peerConnection.onsignalingstatechange = peerConnectionSignalStateChangeEventHandler;

        //listen for when the ice connection state changes and handle appropriately.
        peerConnection.oniceconnectionstatechange = onIceConnectionStateChangeEventHandler;

        // let the "negotiationneeded" event trigger offer generation
        //peerConnection.onnegotiationneeded = negotiationNeededEventHandler;

        //set eventhandler for when remote stream arrives.
        peerConnection.onaddstream = peerAddedStreamEventHandler;

        //set an event handler for when a remote stream is removed from the connection.
        peerConnection.onremovestream = peerRemovedStreamEventHandler;

        // when ice candidate is available, send to the peer at other end.
        peerConnection.onicecandidate = onNewIceCandidateEventHandler;

        //set initialized flag
        isInitialized = true;

        log({ where: "init()", message: "PeerRTC Initialized" });
    }

    /**
    *Sets the peer broswer of this connection to the argument passed in.
    **/
    this.setPeerBrowser = function (_peerBrowser) {
        peerBrowser = _peerBrowser;
    }

    /**
    * Sends a ChatClientMessage to a ChatModel peer.
    *
    * Parameters:
    *   chatClientMessage: ChatClientMessage that will be sent over-the-wire to a
    *                      ChatModel peer recipient.
    **/
    this.sendMessage = function (chatClientMessage) {
        var chunkSet = new ChatDataChunkSet();

        try {
            chunkSet.initializeSet(chatClientMessage.stringify());
        }
        catch (ex) { }

        outboundDataChunkBufferSets[ChatUtility.getNewChatUniqueIdentifier()] = chunkSet;

        var self = this;
        setTimeout(function () {
            self.sendQueuedChatMessages();
        }, 0);

        return true;
    }

    /* Method attempts to empty all the data queues in the outbound buffer set. Message chunks
     * we fail to send will remain in the queue for another try at a later time.
     *
     * Messages are sent in chunks in the form
     * { setId<String>,          // unique identifier used by PeerRTC to associate the chunk with a buffer.
     *   index<int>,             // Index where the chunk will fit when the message is reconstructed.
     *   targetChunkCount<int>,  // Total number of indexed chunks needed to reconstruct the message.
     *   data<String> }          // Chunk of message data.
     *
     * Chunks from all the data buffers in the outbound buffer set will be sent interleaved. This
     * allows us to send large arrays of data that could take minutes to complete while still working
     * in all the little text messages in real-ish time.
     */
    this.sendQueuedChatMessages = function () {
        var isChannelReliable = true;

        /* The data channel is considered unreliable if during one complete iteration through
         * all the message buffers all data send attempts fail. If even one send attempt succeeds,
         * we will do another iteration over all the buffers.
         */
        while (isChannelReliable && !ChatUtility.isAssociativeArrayEmpty(outboundDataChunkBufferSets)) {
            var successfulMessageSendCompletions = [];

            /* For each message data buffer in the queue of buffers, send one chunk. This is
             * the interleaving of the sending of multiple message's data chunks in action.
             * This is what will allow us to keep sending chat messages out even though a large
             * file is being uploaded.
             */
            ChatUtility.iterateAssociativeArray(
                outboundDataChunkBufferSets,
                function (chunkSetKey, unsentChunkSet) {
                    var chunkIndex = unsentChunkSet.getChunkInitializationCount() - unsentChunkSet.getCurrentChunkCount();
                    var chunkedMessage = {
                        setId: chunkSetKey,
                        index: chunkIndex,
                        targetChunkCount: unsentChunkSet.getChunkInitializationCount(),
                        data: unsentChunkSet.getChunkByIndex(chunkIndex)
                    };

                    try {
                        if ((typeof chatConnection !== 'undefined' && chatConnection !== null)
                            && chatConnection.readyState === "open") {
                            chatConnection.send(JSON.stringify(chunkedMessage));
                            isChannelReliable = true;
                        }
                        else {
                            isChannelReliable = false;
                        }
                    }
                    catch (_exception) {
                        isChannelReliable = false;
                        error({ where: "sendQueuedChatMessages()", message: " Exception thrown in sendQueuedChatMessages: " + _exception.message });
                    }

                    if (isChannelReliable) {
                        // If the send operation was successful, only keep the unsent data.
                        unsentChunkSet.popChunkByIndex(chunkIndex);

                        if (unsentChunkSet.getCurrentChunkCount() <= 0) {
                            // We have successfully emptied one of the data buffers.
                            successfulMessageSendCompletions.push(chunkSetKey);
                        }
                    }
                }, null);

            for (var i = 0; i < successfulMessageSendCompletions.length; i++) {
                // Delete the buffers that are empty due to having successfully sent all their data.
                delete outboundDataChunkBufferSets[successfulMessageSendCompletions[i]];
            }
        }
    }

    //send provided data to peer
    this.sendData = function (_data) {
        //todo

        return false;
    }

    //Close all data channels and the peerConnection itself. 
    this.disconnect = function () {
        peerConnectionStatus = "disconnected";
        if (chatConnection) {
            log({ where: "disconnect()", message: "Closing chat channel." });
            chatConnection.close();
            delete chatConnection;
        }
        if (dataConnection) {
            log({ where: "disconnect()", message: "Closing data channel." });
            dataConnection.close();
            delete dataConnection
        }
        if (peerConnection) {
            log({ where: "disconnect()", message: "Closing peer connection." });
            peerConnection.close();
            delete peerConnection;
        }
    }

    //Pass a description signaling message from peer.
    var receivedDescription = function (_description) {
        //log({where: "receivedDescription()",  message:"received description."});
        try{
            peerConnection.setRemoteDescription(new RTCSessionDescription(_description), remoteDescriptionSet, function (_error) { error({ where: "receivedDescription()", message: _error }); }.bind(this));
        }
        catch (_exception) {
            error({ where: "receivedDescription()", message: _exception });
            syncPeerConnection();
        }
    }.bind(this);

    //Pass an Ice Candidate from peer that came through signaling.
    var receivedIceCandidate = function (_iceCandidate) {
        var candidate = new RTCIceCandidate(_iceCandidate);
        //log({where: "receivedIceCandidate()",  message:candidate.candidate});
        //peerConnection.addIceCandidate(candidate);
        peerConnection.addIceCandidate(candidate, function () { }, function (_error) { error({ where: "receivedIceCandidate()", message: _error }); }.bind(this));
    }.bind(this);

    /**
    *This function cleans up the old peerconnection, initiates a new one, and finally it will create the chat channel.
    **/
    var receivedIceFailed = function () {
        error({ where: "receivedIceFailed()", message: "ice Failed. Creating new peerConnection and starting negotiation." });
        var tmpStream = peerConnection.getLocalStreams()[0];
        reinit();
        if (tmpStream) {
            peerConnection.addStream(tmpStream);
        }
        this.createDataChannels();
    }.bind(this);

    /**
    *This function cleans up the old peerconnection, initiates a new one, and finally it will create the chat channel.
    **/
    var receivedSyncPeerConnection = function () {
        log({ where: "receivedSyncPeerConnection()", message: "peer " + this.id + " sent renegotiate signal" });
        var tmpLocalStream = peerConnection.getLocalStreams()[0];
        reinit();
        if (tmpLocalStream) {
            peerConnection.addStream(tmpLocalStream);
        }
        this.createDataChannels();
    }.bind(this);

    /**
    *This function cleans up the old peerconnection, initiates a new one, and finally it will create the chat channel.
    *Used because firefox does not currently support removing or adding new channels.
    **/
    var receivedFirefoxRenegotiate = function () {
        log({ where: "receivedFirefoxRenegotiate()", message: "peer using firefox sent renegotiate signal" });
        var tmpLocalStream = peerConnection.getLocalStreams()[0];
        firefoxWorkAround();
        if (tmpLocalStream) {
            peerConnection.addStream(tmpLocalStream);
        }
        this.createDataChannels();
    }.bind(this);

    //Provide a signaling message from peer to this PeerRTC. 
    this.receivedMessage = function (_data) {
        if (_data.type === "iceCandidate") {
            receivedIceCandidate(_data.data);
        }
        else if (_data.type === "description") {
            receivedDescription(_data.data);
        }
        else if (_data.type === "iceFailed") {
            receivedIceFailed();
        }
        else if (_data.type === "firefoxRenegotiate") {
            receivedFirefoxRenegotiate();
        }
        else if (_data.type === "syncPeerConnection") {
            receivedSyncPeerConnection
        }
    }

    //Returns a sequence of streams attached, should be null if none are provided.
    this.getLocalStream = function () {
        var allLocalStreams = peerConnection.getLocalStreams();
        return allLocalStreams;
    }

    this.getRemoteStream = function () {
        var allRemoteStreams = peerConnection.getRemoteStreams();
        return allRemoteStreams;
    }

    /**
    *Returns the current stats for this peer connection.
    **/
    this.getStats = function (_callback) {
        return new Promise(function (successCallback, failedCallback) {
            var peerConnectionStats = { audioSent: 0, audioReceived: 0, videoSent: 0, videoReceived: 0, totalSent: 0, totalReceived: 0, rtt: 0 };

            if (navigator.mozGetUserMedia) {

            }
            else {
                var processStats = function (_statReports) {
                    var results = _statReports.result();
                    //go through every result provided
                    for (var i = 0; i < results.length; i++) {
                        var result = results[i];
                        //get the name of the stat we are looking at.
                        var stat = result.stat("googCodecName");

                        //data for audio
                        if (stat === "opus") {
                            var received = parseInt(result.stat("bytesReceived"));
                            var sent = parseInt(result.stat("bytesSent"));
                            var roundTripTime = parseInt(result.stat("googRtt"));
                            if (received) {
                                peerConnectionStats.audioReceived += received;
                            }

                            if (sent) {
                                peerConnectionStats.audioSent += sent;
                            }

                            if (roundTripTime) {
                                peerConnectionStats.rtt = roundTripTime;
                            }
                        }

                        //data for video
                        if (stat === "VP8") {
                            var received = parseInt(result.stat("bytesReceived"));
                            var sent = parseInt(result.stat("bytesSent"));
                            var roundTripTime = parseInt(result.stat("googRtt"));
                            if (received) {
                                peerConnectionStats.videoReceived += received;
                            }

                            if (sent) {
                                peerConnectionStats.videoSent += sent;
                            }

                            if (roundTripTime) {
                                peerConnectionStats.rtt = roundTripTime;
                            }
                        }

                        //does not seem to provide transmitted stats, but info about the transmission itself.
                        if (result.type === "VideoBwe") {
                            var temp = {
                                googActualEncBitrate: result.stat("googActualEncBitrate"),
                                googAvailableSendBandwidth: result.stat("googAvailableSendBandwidth"),
                                googAvailableReceiveBandwidth: result.stat("googAvailableReceiveBandwidth"),
                                googRetransmitBitrate: result.stat("googRetransmitBitrate"),
                                googTargetEncBitrate: result.stat("googTargetEncBitrate"),
                                googBucketDelay: result.stat("googBucketDelay"),
                                googTransmitBitrate: result.stat("googTransmitBitrate")
                            };
                        }
                    }

                    //calculate the total bytes sent and received
                    peerConnectionStats.totalSent = peerConnectionStats.audioSent + peerConnectionStats.videoSent;
                    peerConnectionStats.totalReceived = peerConnectionStats.audioReceived + peerConnectionStats.videoReceived;
                    //call the provided method giving it the stats we gathered.
                    successCallback(peerConnectionStats);
                }

                //Future versions of google will change function parameters so they return the values according to specs.
                //peerConnection.getStats(peerConnection.getLocalStreams()[0].getAudioTracks()[0], function (_statReports) {
                peerConnection.getStats(processStats);
            }
        });
    }

    //Returns whether or not the object has been initialized.
    this.isInitialized = function () {
        return isInitialized;
    }

    //returns boolean value of true if this peer is disconnected.
    this.connectionStatus = function () {
        return peerConnectionStatus;
    }

    this.isChatEnabled = function () {
        return chatEnabled;
    }

    this.isDataEnabled = function () {
        return dataEnabled;
    }
}