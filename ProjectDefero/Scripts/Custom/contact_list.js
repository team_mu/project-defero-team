// contact_list

(
  function () {
      var app = angular.module('contact_list', ['user']);
      app.directive(
        'contactList',
        function () {
            return {
                restrict: 'E',
                templateUrl: '/Template/ContactList',
                controller: [
                  '$http', '$mdSidenav', '$rootScope', '$scope', '$interval', '$timeout', function ($http, $mdSidenav, $rootScope, $scope, $interval, $timeout) {
                      
                      this.searched = [];
                      var toggling = false, orig_close;

                      $scope.selectedIndex = 0;

                      $scope.onSelectSearch = function () {
                          setTimeout(function () { $('input[name=search]').focus(); });
                      };
                     
                      $timeout(function () {                     
                          $rootScope.toggle_nav = function () {
                              var el = $('.hamburger');
                              if (!$rootScope.not_guest_user()) return;
                              
                              if (el.hasClass('active')) {
                                  el.addClass('active-end');
                                  el.one('transitionend', function () {
                                      el.removeClass('active active-end');
                                  });
                                  $mdSidenav('left').close().then(function () { toggling = false; });
                              } else {
                                  el.addClass('active');
                                  $mdSidenav('left').open().then(function () {
                                      $('md-backdrop').click(
                                         function () {                                     
                                        $rootScope.toggle_nav();
                                       }
                                      );
                                      toggling = false;
                                  });                                 
                              }
                          };
                      });
                                           
                      $scope.next = function () {
                          $scope.selectedIndex = Math.min($scope.selectedIndex + 1, 1);
                      };
                      $scope.previous = function () {
                          $scope.selectedIndex = Math.max($scope.selectedIndex - 1, 0);
                      };

                      this.handle_search_keydown = function (ev) {
                          var val = ev.target.value;

                          if (val.length < 3) {
                              this.searched = [];
                              return;
                          }

                          $http.post(
                            'User/UserSearch', { query: val }
                            ).success(
                            function (result) {
                                if (val === ev.target.value) {
                                    this.searched = result.queryResult;
                                } 
                            }.bind(this));
                      }
                      this.clear_search = function () {
                          this.searched = [];
                      }.bind(this);
                  }
                ],
                controllerAs: 'contact_list'
            };
        }
      );
  }
)();