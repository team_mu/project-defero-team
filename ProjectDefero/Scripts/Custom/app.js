(
    function () {
        var app = angular.module('defero', ['ngMaterial', 'contact_list', 'online_users', 'auth', 'chat_box', 'room']);

        /*
        https://gist.github.com/thomseddon/3511330
        */
        app.filter('bytes', function () {
            return function (bytes, precision) {
                if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
                if (typeof precision === 'undefined') precision = 1;
                var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
                    number = Math.floor(Math.log(bytes) / Math.log(1024));
                return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
            }
        });

        app.controller('AppController', function ($http, $scope, $mdSidenav, $rootScope, $mdDialog, $interval, $rooms_controller, $themes, $window) {
            $rootScope.show_nav = false;
            $rootScope.selected_room_index = +localStorage.current_room || 0;
            document.title = "Defero";

            $scope.get_current_theme = function () { return $themes.current_theme; };
            $rootScope.focused = true;
            $(window).on('blur', function () { $rootScope.focused = false; })
            $(window).on('focus', function () { $rootScope.focused = true; })

            $rootScope.not_guest_user = function () {/*
						       var user = $rootScope.users.get_user($rootScope.user_id);
						       return user && !user.Guest;*/
                return !$rootScope._guest_user_tmp;
            }
            $scope.add_room = function () { $rooms_controller.add_room(); }
            $scope.get_rooms = function () { return $rooms_controller.rooms; }

            $scope.in_call = $rooms_controller.in_call;
            $scope.has_message = $rooms_controller.has_message;
            $scope.rooms_controller = $rooms_controller;

            $rootScope.setRoom = function (room_id) {
                $rootScope.current_room_id = room_id;
                $rootScope.selected_room_index = $rooms_controller.get_room_index(room_id);
                localStorage.current_room = $rootScope.selected_room_index;
                if (!$rootScope.$$phase && !$scope.$$phase) {
                    $rootScope.$apply();
                }
            }.bind(this);

            /**
             *Simply sends a message to the peer through signaling server.
             **/
            $rootScope.requestInvite = function (_id) {
                console.log("Sending connection request to: " + _id);

                //use signaling to connect to peer.
                if (!$rootScope.signal.send({
                    destination: _id, type: "invite",
                    data: $rooms_controller.get_room($rootScope.current_room_id),
                    user: $rootScope.user_id
                })) {
                    console.log("failed to send connection request to: " + _id);
                }
            }

            // Stores list of functions that need to be called after $rootScope.server_time is set.
            var serverTimeCallbackList = new ChatCallbackFunctionList();
            // Tracks whether or not the POST to /Home/GetServerTimeMillis is already in progress.
            var isGetServerTimePosting = false;

            /**
            * Asynchronously gets the Defero server time.
            **/
            $rootScope.get_server_time = function (callback) {
                serverTimeCallbackList.addCallbackFuntion(callback);

                if (!isGetServerTimePosting) {
                    isGetServerTimePosting = true;
                    var clientTimeRequest = Date.now();
                    $http.post(
                    '/Home/GetServerTimeMillis',
                    {
                        clientTime: clientTimeRequest
                    }).then(
                    function (ret) {
                        // GetServerTimeMillis request's estimated travel time to server.
                        var travelTimeToServer = (Date.now() - clientTimeRequest) / 2.0;
                        // Difference of client's time and the server's time. Client will use this as an offset to its own time.
                        ret.data.timeDifference = (ret.data.time - travelTimeToServer) - clientTimeRequest;
                        $rootScope.server_time = ret.data;
                        isGetServerTimePosting = false;
                        serverTimeCallbackList.processAndRemoveAll($rootScope.server_time);
                    });
                }
            };

            $rootScope.users = {
                users: {},
                getting: {},
                rooms: {},
                users_profiles: {},

                getUserProfilePicture: function (user_id) {
                    return 'https://google.github.io/material-design-icons/social/svg/ic_person_24px.svg';
                },

                update_users: function (users) {
                    users.forEach(function (user) {
                        this.users[user.UserId] = user;
                    }.bind(this));
                },

                update_users_room: function (users, room_id) {
                    if (!this.rooms[room_id]) this.rooms[room_id] = {};
                    users.forEach(function (user) {
                        this.rooms[room_id][user.UserId] = user;
                    }.bind(this));
                    //this was added to see if it fixes anything, which it doesn't. So we shoudl remove.
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                },

                get_user_profile: function (user_id, callback) {
                    if (this.users_profiles[user_id]) {
                        if (callback) callback(this.users_profiles[user_id]);
                        return this.users_profiles[user_id];
                    } else if (!this.getting['users_profiles']) {
                            this.getting['users_profiles'] = true;
                            $http.post(
                                '/User/GetUserProfile',
                                { userId: user_id }).
                                        then(function (ret) {
                                            this.getting['users_profiles'] = false;
                                            this.users_profiles[user_id] = ret.data.profile;
                                            if (callback) callback(this.users_profiles[user_id]);
                                        }.bind(this)
                                        );

                        }
                },

                get_user_room: function (user_id, room_id, callback) {
                    if (this.rooms[room_id] && this.rooms[room_id][user_id]) {
                        if (callback) callback(this.rooms[room_id][user_id]);
                        return this.rooms[room_id][user_id];
                    } else if (!this.getting['rooms']) {
                        this.getting['rooms'] = true;
                        $rooms_controller.update(
                                        function () {
                                            this.getting['rooms'] = false;
                                            if (callback) callback(this.rooms[room_id][user_id]);
                                        }.bind(this)
                        );

                    }
                },

                update_user_profile: function (user_id, spec, callback) {
                    this.get_user(
			user_id,
			function (user) {
			    var i;
			    for (i in spec) {
			        user[i] = spec[i];
			    }
			    $http.post(
    "/User/EditProfile",
    user
                ).then(
    function () {
        delete this.users[user_id];
        this.get_user(user_id, callback);
    }.bind(this)
                );
			}.bind(this)
                    );
                },

                get_user_settings: function (user_id, callback) {
                    this.get_user(user_id, function (user) {
                        callback(user.Settings ? JSON.parse(user.Settings) : {});
                    });
                },

                get_user: function (user_id, callback) {
                    user_id = "" + user_id;
                    if (this.users[user_id]) {
                        if (callback) {
                            callback(this.users[user_id]);
                        } else {
                            return this.users[user_id];
                        }
                    } else if (!this.getting[user_id]) {
                        this.getting[user_id] = true;
                        $http.post('/User/GetUserProfile', { userId: user_id }).
                                        then(function (ret) {
                                            this.users[user_id] = ret.data.profile;
                                            if (callback) callback(this.users[user_id]);
                                            this.getting[user_id] = false;
                                        }.bind(this));
                    }
                }
            };

            $rootScope.contact_list_data = {
                contacts: [],
                pendingContacts: [],
                contactRequests: [],
                update: function () {
                    $http.post('/User/GetContacts').success(
			        function (data) {
			            this.contacts = data.contacts || [];
			            this.pendingContacts = data.pendingContacts || [];
			            this.contactRequests = data.contactRequests || [];

			            $rootScope.users.update_users(this.contacts);
			            $rootScope.users.update_users(this.pendingContacts);
                        $rootScope.users.update_users(this.contactRequests);
			        }.bind(this)
                    );
                },

                has_pending: function () {
                    return this.pendingContacts.length;
                },

                has_requests: function () {
                    return this.contactRequests.length;
                },

                has_contacts: function () {
                    return this.contacts.length;
                },

                _arr_has: function (arr, id) {
                    var has = false;
                    arr.forEach(function (contact) {
                        if (contact.UserId == id) has = true;
                    });
                    return has;
                },

                has_contact_user: function (id) {
                    return this._arr_has(this.contacts, id);
                },

                has_pending_user: function (id) {
                    return this._arr_has(this.pendingContacts, id);
                },

                has_request_user: function (id) {
                    return this._arr_has(this.contactRequests, id);
                }
            };

            window.wjtask_on_error = function (message) {
                $http.post('/Error/LogError', { message: message });
            };


            if ("Notification" in window) {
                //Create window focus and blur events for displaying notifications
                $rootScope.windowFocus = true;

                $window.onfocus = function () {
                    $rootScope.windowFocus = true;
                }

                $window.onblur = function () {
                    $rootScope.windowFocus = false;
                }

                //Request notification permission.
                if (Notification.permission !== "granted") {
                    Notification.requestPermission();
                }
            }
        });

        if (!DEBUG) {
            app.factory('$exceptionHandler', function () {
                return function (exception, cause) {
                    exception.message += ' (caused by "' + cause + '")';
                    window.wjtask_on_error(exception.message + "\n\n" + exception.stack);
                    throw new Error(exception.message);
                };
            });
        }

        //create a text formatting service for use in the app.

        app.factory('chatFormatter', ['$sanitize', function ($sanitize) {
            return new chatFormatter($sanitize);
        }]);

        app.filter('chatFormat', ['chatFormatter', function (chatFormatter) {
            return function (text) {
                if (!text)
                    return text;

                return chatFormatter.format(text);
            }
        }]);
    }
)();
