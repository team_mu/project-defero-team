﻿// online_users
// John D. Wissler

(
  function () {
      var app = angular.module('online_users', ['user']);
      app.directive(
        'onlineUsers',
        function () {
            return {
                restrict: 'E',
                templateUrl: '/Template/OnlineUsers',
                controller: [
                  '$scope', function ($scope) {
                      
                  }
                ],
                controllerAs: 'online_users'
            };
        }
      );
  }
)();