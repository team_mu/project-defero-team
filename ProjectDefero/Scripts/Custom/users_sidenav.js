﻿
(
  function () {
     
      function mdSideNavName () {
          var ddo = {
              restrict: 'A',
              require: 'mdSidenav',
              link: function (scope, elm, attr, mdSideNav) {
                  scope.users_nav = mdSideNav;
              }
          };
          return ddo;
      }

      angular.module('users_sidenav', []).directive('mdsidenavname', mdSideNavName);
  }
)();