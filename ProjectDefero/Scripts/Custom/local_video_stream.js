﻿(
function () {
    'use strict';
    var app = angular.module('local_video_stream', []);

    app.directive('localvideostream', function () {
        return {
            restrict: 'E',
            templateUrl: '/Template/VideoStream',
            link: function (scope, element, attributes) {
                scope.$watch("stream.stream", function () {
                    var video = element[0].getElementsByTagName('video')[0];
                    if (scope.stream.stream) {
                        //video.setAttribute("autoplay", true);
                        video.volume = 0;
                        attachMediaStream(video, scope.stream.stream);
                        video.load();
                        video.play();
                    }
                    else {
                        video.pause();
                    }
                });
            },
            controller: ['$scope', '$element', '$rootScope', function ($scope, $element, $rootScope) {
                $scope.id = $rootScope.user_id;
                $scope.stream = { name: "", stream: $rootScope.local_rtc.getStream(), audioEnabled: true, videoEnabled: false };
                $rootScope.users.get_user($rootScope.user_id, function (user) { $scope.stream.name = user.Username; })
                //Check if we actually have audio in our stream. 
                if ($scope.stream.stream) {
                    if (($scope.stream.stream.getAudioTracks().length > 0) && ($scope.stream.stream.getVideoTracks().length > 0)) {
                        $scope.stream.audioEnabled = true;
                        $scope.stream.videoEnabled = true;
                    }
                    else if ($scope.stream.stream.getAudioTracks().length > 0) {
                        $scope.stream.audioEnabled = true;
                        $scope.stream.videoEnabled = false;
                    }
                    else if ($scope.stream.stream.getVideoTracks().length > 0) {
                        $scope.stream.audioEnabled = false;
                        $scope.stream.videoEnabled = true;
                    }
                    else {
                        $scope.stream.audioEnabled = false;
                        $scope.stream.videoEnabled = false;
                    }
                }
                else {
                    $scope.stream.audioEnabled = false;
                    $scope.stream.videoEnabled = false;
                }

                $scope.hasVideoStream = function () {
                    return true;
                };

                $scope.hasAudioStream = function () {
                    return true;
                };

                //Watch and make appropriate changes for when this happens.
                $scope.$watchGroup(['stream.audioEnabled', 'stream.videoEnabled'], function (newValues, oldValues, scope) {
                    var updateStream = false;
                    //make sure something changed.
                    if ((newValues[0] != oldValues[0]) || (newValues[1] != oldValues[1])) {
                        updateStream = true;
                    }

                    if (updateStream) {
                        if ($scope.stream.audioEnabled && $scope.stream.videoEnabled) {
                            $rootScope.local_rtc.getMicVideoStream().then(function (_stream) {
                                $scope.stream.stream = _stream;
                                for (var key in $scope.peersInCall) {
                                    $rootScope.peerConnections.getPeerConnection(key).peerConnection.addLocalStream($scope.stream.stream);
                                    $rootScope.peerConnections.getPeerConnection(key).peerConnection.renegotiate();
                                }
                            }, function (_error) {
                                $scope.stream.audioEnabled = oldValues[0];
                                $scope.stream.videoEnabled = oldValues[1];
                            });
                        }
                        else if ($scope.stream.videoEnabled) {
                            $rootScope.local_rtc.getVideoStream().then(function (_stream) {
                                $scope.stream.stream = _stream;
                                for (var key in $scope.peersInCall) {
                                    $rootScope.peerConnections.getPeerConnection(key).peerConnection.addLocalStream($scope.stream.stream);
                                    $rootScope.peerConnections.getPeerConnection(key).peerConnection.renegotiate();
                                }
                            }, function (_error) {
                                $scope.stream.videoEnabled = oldValues[1];
                            });
                        }
                        else if ($scope.stream.audioEnabled) {
                            $rootScope.local_rtc.getMicrophoneStream().then(function (_stream) {
                                $scope.stream.stream = _stream;
                                for (var key in $scope.peersInCall) {
                                    $rootScope.peerConnections.getPeerConnection(key).peerConnection.addLocalStream($scope.stream.stream);
                                    $rootScope.peerConnections.getPeerConnection(key).peerConnection.renegotiate();
                                }
                            }, function (_error) {
                                $scope.stream.audioEnabled = oldValues[0];
                            });
                        }
                        else {
                            for (var key in $scope.peersInCall) {
                                $rootScope.peerConnections.getPeerConnection(key).peerConnection.removeLocalStream();
                                $rootScope.peerConnections.getPeerConnection(key).peerConnection.renegotiate();
                            }
                            $rootScope.local_rtc.removeStream();
                            $scope.stream.stream = null;
                        }
                    }
                });

                /**
                *Enables the audio for the provided peer, if id is user id, then sends audio.
                **/
                $scope.enableAudio = function (_id) {
                    $scope.stream.audioEnabled = true;
                };

                /**
                *Enables the video for the provided peer, if id is user id, then sends audio.
                **/
                $scope.enableVideo = function (_id) {
                    $scope.stream.videoEnabled = true;
                };

                /**
                *Disables the audio for the provided peer, if id is user id, then sends audio.
                **/
                $scope.disableAudio = function (_id) {
                    $scope.stream.audioEnabled = false;
                }

                /**
                *Disables the video for the provided peer, if id is user id, then sends audio.
                **/
                $scope.disableVideo = function (_id) {
                    $scope.stream.videoEnabled = false;
                }
            }
            ],
            controllerAs: 'localvideostream'
        };
    }
    );
}
)();
