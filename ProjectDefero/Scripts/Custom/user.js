(
  function () {
    var app = angular.module('user', []);
    app.directive(
      'user',
      function () {
        return {
          restrict: 'E',
          templateUrl: '/Template/User',
          controller: [
            '$scope', '$http', '$element', '$rootScope', '$rooms_controller', '$mdDialog', '$themes', function ($scope, $http, $element, $rootScope, $rooms_controller, $mdDialog, $themes) {
                var id;
                var update = function () {
                    $rootScope.contact_list_data.update();
                };
                this.user_constructor = $scope.user_constructor;
                id = this.user_constructor.UserId;

                this.context_room = function () { return $scope.user_constructor.JoinTime; }
               
                this.is_contact = function () {
                   return $rootScope.contact_list_data.has_contact_user(id);
                };


                this.is_pending = function () {
                    return $rootScope.contact_list_data.has_pending_user(id);
                };

                this.is_request = function () {
                    return $rootScope.contact_list_data.has_request_user(id);
                };

                $scope.show_profile = function (ev) {
                    $mdDialog.show({
                        controller: function ($scope, $mdDialog) {
                            $scope.am_logged_in = function () {
                                return id === $rootScope.user_id;
                            };                        

                            $scope.get_user_id = function () { return id; }

                            $scope.get_username = function () {
                                var user = $rootScope.users.get_user_profile(id);
                                return (user && user.Username) || "Not set";
                            };

                            $scope.get_name = function () {
                                var user = $rootScope.users.get_user_profile(id);
                                return (user && user.RealName) || "Not set";
                            };

                            $scope.get_about = function () {
                                var user = $rootScope.users.get_user_profile(id);
                                return (user && user.AboutMe) || "Not set";
                            };
                         
                            $scope.hide = function () {
                                $mdDialog.hide();
                            };

                            $scope.cancel = function () {
                                $mdDialog.cancel();
                            };                            
                        },
                        templateUrl: '/Template/UserProfile',
                        targetEvent: ev,
                    });
                };

                this.promote = function (room) {
                    $http.post(
                        '/Room/ChangeUserRole',
                        {
                            roomId: room.roomID,
                            changedUserId: id,
                            roleName: 'Administrator'
                        }
                        ).then(function () { $rooms_controller.update(); });
                }

                this.demote = function (room) {
                    $http.post(
                      '/Room/ChangeUserRole',
                      {
                          roomId: room.roomID,
                          changedUserId: id,
                          roleName: 'User'
                      }
                      ).then(function () { $rooms_controller.update(); });
                }

                this.kick = function (room) {
                    $http.post(
                     '/Room/KickUser',
                     {
                         roomId: room.roomID,
                         kickedUserId: id
                     }
                     ).then(function () { $rooms_controller.update(); });
                }

                this.is_admin = function () {
                    return $scope.user_constructor.Role === 'Administrator';
                };

                this.in_selected_room = function () {
                    return $rooms_controller.room_has_user($rootScope.current_room_id, id);
                };

                this.is_guest = function () {
                    return $scope.user_constructor.Guest;
                };

                this.get_name = function () {
                    var user = $rootScope.users.get_user(id);
                    return user && user.Username;
                }

                this.is_online = function () {
                    var user;
                    if ($scope.user_constructor.JoinTime) {
                        user = $rootScope.users.get_user_room(id, $scope.roomID);
                    } else {
                        user = $rootScope.users.get_user(id);
                    }
                   
                    return user && user.Online;
                };
                               
                this.call = function () {
                    $rootScope.requestInvite(this.user_constructor.UserId);
                }.bind(this);

                this.request_contact = function () {
                    $http.post(
                        '/User/AddContact', { contactId: this.user_constructor.UserId }
                        ).success(update);
                }.bind(this)

                this.remove_contact = function () {
                    $http.post(
                        '/User/RemoveContact', { contactId: this.user_constructor.UserId }
                        ).success(update);
                }.bind(this)

                this.accept_contact = function () {
                    $http.post(
                        '/User/AcceptContactRequest', { contactId: this.user_constructor.UserId }
                        ).success(update);
                }.bind(this)

                this.reject_contact = function () {
                    $http.post(
                        '/User/RejectContactRequest', { contactId: this.user_constructor.UserId }
                        ).success(update);
                }.bind(this)
            }
          ],
          controllerAs: 'user'
        };
      }
    );
  })
();