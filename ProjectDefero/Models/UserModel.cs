﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDefero.Models
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool Online { get; set; }
        public bool Guest { get; set; }
    }
}