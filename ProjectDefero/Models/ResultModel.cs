﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDefero.Models
{
    public class ResultModel
    {
        public string DisplayMessage { get; set; }
        public Exception Exception { get; set; }
        public bool Success { get; set; }
    }
}