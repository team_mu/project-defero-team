﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDefero.Models
{
    public class ContactsModel
    {
        public List<UserModel> Contacts { get; set; }
        public List<UserModel> ContactRequests { get; set; }
        public List<UserModel> PendingContacts { get; set; }
    }
}