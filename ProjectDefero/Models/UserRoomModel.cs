﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDefero.Models
{
    /// <summary>
    /// A class that extends UserModels to include the User's role for the current room.
    /// </summary>
    public class UserRoomModel : UserModel
    {
        public string Role;
        public double JoinTime;
    }
}