﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDefero.Models
{
    public class RoomModel
    {
        public int RoomId { get; set; }
        public string Name { get; set; }
        public string InviteKey { get; set; }

        public List<UserRoomModel> Users { get; set; }
    }
}