﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDefero.Models
{
    /// <summary>
    /// Represents a user's full profile. Will contain more information than a UserModel.
    /// </summary>
    public class UserProfileModel : UserModel
    {
        public DateTime JoinDate { get; set; }
        public string RealName { get; set; }
        public string AboutMe { get; set; }
        public string Settings { get; set; }

    }
}