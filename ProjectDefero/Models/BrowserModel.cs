﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectDefero.Models
{
    public class BrowserModel
    {
        public string Browser { get; set; }
        public bool SignedIn { get; set; }
    }
}