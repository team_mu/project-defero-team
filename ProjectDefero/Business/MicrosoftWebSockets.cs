﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Microsoft.Web.WebSockets;
using System.Web.SessionState;
using System.Collections.Concurrent;
//using System.Web.Caching;
//using System.Web.Services;

namespace ProjectDefero
{

    class JsonMessage
    {
        public string source { get; set; }
        public string destination { get; set; }
        public string type { get; set; }
        public string room { get; set; }
        public Object data { get; set; }
    }

    public class MicrosoftWebSockets : WebSocketHandler
    {
        private static WebSocketCollection clients = new WebSocketCollection();
        private static JavaScriptSerializer serializer = new JavaScriptSerializer();

        private int id;
        private HttpSessionState currentSession;

        //custom constructor
        public MicrosoftWebSockets(HttpSessionState _currentSession) : base()
        {
            //Base the data on the authenticated user.
            currentSession = _currentSession;
        }

        //[WebMethod(EnableSession = true)]
        public override void OnOpen()
        {
            //set the id from session
            int? ID = null;

            //make sure we actually have something before trying to use it.
            if(currentSession != null)
            {
                ID = (int?)currentSession["UserId"];
            }
            else  //seems like this may never be the case...
            {
                this.Close();
            }

            //ID will be null if request is from someone not signed in.
            if (ID == null)
            {
                //If null, close the connection.
                this.Close();
            }
            else
            {
                //set our private vari
                this.id = (int)ID;

                MicrosoftWebSockets oldConnection = null;

                //clients.Single throws an exception if nothing is found.
                try
                {
                    oldConnection = (MicrosoftWebSockets)clients.Single(r => ((MicrosoftWebSockets)r).id == this.id);
                }
                catch
                {

                }

                if (oldConnection == null)
                {
                    //add the sockethandler to the collection of sockets.
                    clients.Add(this);
                }
                else
                {
                    //close the first connected socket.
                    oldConnection.Close();
                    //remove the old socket from our list.
                    clients.Remove(oldConnection);

                    //add the new socket to the collection of sockets.
                    clients.Add(this);
                }

                //Add the user to the cache of online users.
                ConcurrentDictionary<int, int> onlineUsers = (ConcurrentDictionary<int, int>)HttpRuntime.Cache.Get("OnlineUsers");
                onlineUsers.AddOrUpdate(this.id, 1, (k, v) => v + 1);

                //notify this peer that it has now connected
                this.Send(serializer.Serialize(new { type = "connected", message = "" }));
            }
        }

        public override void OnClose()
        {
            //remove this socket handler from the collection.
            clients.Remove(this);

            ////Remove this user from the cache of online users.
            ConcurrentDictionary<int, int> onlineUsers = (ConcurrentDictionary<int, int>)HttpRuntime.Cache.Get("OnlineUsers");

            //since we will be performing multiple operations that need to look lock one, lock the dictionary.
            lock (onlineUsers)
            {
                int val;
                if (onlineUsers.TryRemove(this.id, out val))
                {
                    if (val > 1)
                    {
                        onlineUsers.TryAdd(this.id, val - 1);
                    }
                }
            }
        }

        //override the OnMessage event handler so we can do some custom things when the socket receives a message.
        public override void OnMessage(string message)
        {
            JsonMessage messageObject = null;
            WebSocketHandler destinationPeer = null;

            //catch any errors when trying to deserialize message or find the destination peer.
            try
            {
                //Deserialize the message so we can read the src and destination address.
                messageObject = serializer.Deserialize<JsonMessage>(message);

                //even if this is null, it will be caught in the try catch statement.
                string destination = messageObject.destination;

                //Look throught the collection for a socket that has an id matching the destination.
               destinationPeer = clients.Single(r => ((MicrosoftWebSockets)r).id.ToString() == destination);               
            }
            catch(Exception e)
            {
                //I don't think this actually does anything in IIS....
                Console.WriteLine(e);
            }

            //Catch any errors cause when trying to send a message.
            try
            {
                //check if we have a destination.
                if (destinationPeer != null)
                {
                    destinationPeer.Send(message);
                }
                else if(messageObject.type == "stillConnected")   //If the destination peer is not found, then they re disconnected. Notify source.
                {
                    JsonMessage responseMessage = new JsonMessage();
                    responseMessage.source = "server";
                    responseMessage.destination = messageObject.source;
                    responseMessage.type = "peerNotConnected";
                    responseMessage.room = messageObject.room;
                    responseMessage.data = new { peerID = messageObject.destination};

                    this.Send(serializer.Serialize(responseMessage));
                }
            }
            catch (Exception e)
            {
                //I don't think this actually does anything in IIS....
                Console.WriteLine(e);
            }
        }
    }
}