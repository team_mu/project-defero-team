﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using DeferoEntities.Entities;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using ProjectDefero.Models;
using System.Configuration;
using ProjectDefero.Utilities;
using System.Web.Caching;
using System.Collections.Concurrent;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Hosting;
using System.IO;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectDefero.Business
{
    /// <summary>
    /// Handles all user-related functionality, such as managing an account or adding contacts.
    /// </summary>
    public class UserBusiness
    {

        private DeferoDB context;
        private int cryptoIterations;
        private static object GuestLock = new object(); //for locking to ensure uniqueness in certain fields.
        private static string LastTime = DateTime.Now.Ticks.ToString();

        public UserBusiness()
        {
            context = new DeferoDB();
            cryptoIterations = Int32.Parse(ConfigurationManager.AppSettings["CryptoIterations"]);
        }

        /// <summary>
        /// Example code: add a user to the database.
        /// </summary>
        /// <param name="username">The username of the desired user.</param>
        //public void CreateUser(string username)
        //{
        //    User user = new User()
        //    {
        //        //No need to set UserId as it's an identity (autoincrement integer), entityframework/db will handle setting it
        //        Username = username
        //    };

        //    context.Users.Add(user);

        //    //This function ends a transaction. You can make any number of changes to the context before calling it, and all of them
        //    //will be wrapped in a transaction.
        //    context.SaveChanges();

        //    //If for some reason you need a broader transaction ability, you can do so like this:
        //    //var dbTransaction = context.Database.BeginTransaction();

        //    //and when done:
        //    //dbTransaction.Commit();
        //}

        /// <summary>
        /// Searches for users whose Username or Email match the query.
        /// </summary>
        /// <param name="query">The string  we want to find</param>
        public Tuple<ResultModel, List<UserModel>> UserSearch(int? userId, string query)
        {
            //Ignore small queries.
            if (query.Length < 3)
            {
                return new Tuple<ResultModel, List<UserModel>>(
                    new ResultModel()
                    {
                        Success = false,
                        DisplayMessage = "Query was too short",
                    },
                    new List<UserModel>());
            }

            if (userId == null)
            {
                return new Tuple<ResultModel, List<UserModel>>(
                    new ResultModel()
                    {
                        Success = false,
                        DisplayMessage = "You are not signed in",
                    },
                    new List<UserModel>());
            }

            try
            {
                //Cap out at 30 users to avoid returning the whole database or something.
                //Don't return the user or their existing contacts (contact requests are OK)
                IQueryable<User> users = context.Users.Where(x => x.UserId != userId && (x.Username.Contains(query) || x.Email.Contains(query)));

                List<User> searchResults = users
                            .Where(x=> x.UserContacts
                                        .Where(y => (y.User1Id == userId && y.User1Accepted == true) ||
                                                    (y.User2Id == userId && y.User2Accepted == true))
                                            .Count() == 0
                                    &&
                                    x.UserContacts1
                                        .Where(y => (y.User1Id == userId && y.User1Accepted == true) ||
                                                    (y.User2Id == userId && y.User2Accepted == true))
                                            .Count() == 0)
                            .Take(30)
                            .ToList();

                List<UserModel> result = new List<UserModel>();

                foreach (var u in searchResults)
                {
                    result.Add(UserBusiness.UserToModel(u));
                }

                return new Tuple<ResultModel, List<UserModel>>(
                    new ResultModel()
                    {
                        Success = true,
                        DisplayMessage = "Search sucessful"
                    }, result);

            }
            catch (Exception e)
            {
                return new Tuple<ResultModel, List<UserModel>>(
                    new ResultModel()
                    {
                        Success = false,
                        DisplayMessage = "Error searching the database",
                        Exception = e
                    }, new List<UserModel>());
            }

            
        }

        /// <summary>
        /// Example code: Find all the rooms a user is in.
        /// </summary>
        /// <param name="userId">The Id of the user.</param>
        public List<Room> FindUserRooms(int userId)
        {
            User user = context.Users.Single(x => x.UserId == userId);

            List<UserRoom> userRooms = user.UserRooms.ToList();

            List<Room> rooms = new List<Room>();

            foreach (var room in userRooms)
            {
                rooms.Add(room.Room);
            }

            return rooms;
        }

        /// <summary>
        /// Validates a user's login information.
        /// </summary>
        /// <returns>A UserModel for the current User that contains information such as userId and username. 
        /// The userId will be 0 if the user does not exist or the password does not match the account.</returns>
        public UserModel ValidateUser(string email, string password)
        {
            User user = context.Users.SingleOrDefault(x => x.Email == email);

            if (user == null)
            {
                return new UserModel()
                {
                    Email = null,
                    UserId = 0,
                    Username = null,
                };
            }

            string salt = user.Salt;

            string hash = HashPassword(salt, password);

            if (user.Password != hash)
            {
                return new UserModel()
                {
                    Email = null,
                    UserId = 0,
                    Username = null
                };
            }
            else
            {
                return UserBusiness.UserToModel(user);
            }
        }

        /// <summary>
        /// Creates a new user account with the specified username, email, and password.
        /// </summary>
        /// <returns>Returns a model describing whether the creation was successful. If false, the model will contain an error message explaining what went wrong.</returns>
        public ResultModel CreateUserAccount(string username, string email, string password)
        {
            //Ensure the username is just alphanumerics and is 3-30 characters long.
            if (!Regex.IsMatch(username, @"^[a-zA-Z0-9]{3,30}$"))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "A username must be between 3 and 30 alphanumeric characters."
                };
            }

            //Ensure the email address at least looks sorta like an email.
            //Not intended to catch all bad emails or anything, just to see if it's something like xxx@yyy.zzz
            if (!Regex.IsMatch(email, @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "This email doesn't appear to be an email address."
                };
            }

            //Make sure the email does not exist
            if (EmailExists(email))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "This email already associated with an account."
                };
            }


            //Generate a salt and hash the password.
            string salt = GenerateSalt();

            string hash = HashPassword(salt, password);


            //Save the new user to the database.
            User user = new User()
            {
                Email = email,
                Password = hash,
                Salt = salt,
                Username = username,
                Guest = false,
                JoinDate = DateTime.UtcNow
            };

            context.Users.Add(user);

            try
            {
                context.SaveChanges();
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "There was an error creating the account.",
                    Exception = e
                };
            }

            return new ResultModel()
            {
                Success = true,
                DisplayMessage = "Account created.",
            };
        }

        /// <summary>
        /// Computes the salted hash for the given password, returning the result as a hex string.
        /// </summary>
        private string HashPassword(string salt, string password)
        {
            string output;

            byte[] saltBytes = GetBytes(salt);

            //Rfc2989DeriveBytes can be used to generate hashes.
            //The number of iterations can be increased even while the server is live
            //by modifying the "CryptoIterations" key in Web.config.
            //Note that doing this would break all accounts in existence, so update
            //this value only if you plan on having everyone using the service
            //reset their passwords immediately after.
            using (Rfc2898DeriveBytes hashAlg = new Rfc2898DeriveBytes(password, saltBytes, cryptoIterations))
            {
                byte[] hashedPW = hashAlg.GetBytes(64);
                output = BitConverter.ToString(hashedPW, 0).Replace("-", "");
            }

            //using ( hashAlg = SHA512.Create())
            //{
            //    UnicodeEncoding enc = new UnicodeEncoding();

            //    byte[] pwBytes = enc.GetBytes(password);

            //    bytesToHash = bytesToHash.Concat(pwBytes).ToArray();

            //    byte[] hashedPW = hashAlg.ComputeHash(bytesToHash);

            //    output = BitConverter.ToString(hashedPW, 0).Replace("-", "");
            //}

            return output;
        }

        /// <summary>
        /// Converts a Hex string to an array of bytes.
        /// </summary>
        private byte[] GetBytes(string str)
        {
            byte[] array = Enumerable.Range(0, str.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(str.Substring(x, 2), 16)).ToArray();

            return array;
        }

        /// <summary>
        /// Generate a random salt to be used for password hashing.
        /// </summary>
        /// <returns>A randomly-generated hex string to be used as a salt.</returns>
        private string GenerateSalt()
        {

            byte[] salt = new byte[8]; //64 bit salt recommended

            Random rng = new Random(unchecked((int)DateTime.Now.Ticks));

            rng.NextBytes(salt);

            return BitConverter.ToString(salt, 0).Replace("-", "");
        }

        /// <summary>
        /// Determines whether the given username already exists in the database.
        /// </summary>
        public bool UsernameExists(string username)
        {
            if (context.Users.Where(x => x.Username == username).Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether the given email already exists in the database.
        /// </summary>
        public bool EmailExists(string email)
        {
            if (context.Users.Where(x => x.Email == email).Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Obtains the username of the current user, or "Guest" if the user is not authenticated.
        /// </summary>
        public string GetCurrentUser(int? userId)
        {
            if (userId == null)
            {
                return "Guest";
            }

            try
            {
                var user = context.Users.Single(x => x.UserId == userId);

                return user.Username;
            }
            catch (Exception)
            {
                return "Guest";
            }
        }

        /// <summary>
        /// Gets a list of contacts, a list of contact requests, and a list of pending contacts 
        /// for the current user.
        /// </summary>
        public Tuple<ResultModel, ContactsModel> GetContacts(int? userId)
        {
            if (userId == null)
            {
                return new Tuple<ResultModel, ContactsModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "You are not signed in",
                        Success = false
                    },
                    new ContactsModel()
                    {
                        ContactRequests = new List<UserModel>(),
                        Contacts = new List<UserModel>(),
                        PendingContacts = new List<UserModel>()
                    });
            }

            try
            {
                User user = context.Users.Single(x => x.UserId == userId);

                List<UserModel> contacts = new List<UserModel>();
                List<UserModel> contactRequests = new List<UserModel>();
                List<UserModel> pendingContacts = new List<UserModel>();
                user.GetContacts(contacts, contactRequests, pendingContacts);

                return new Tuple<ResultModel, ContactsModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "Contact list retrieved",
                        Success = true
                    },
                    new ContactsModel() {
                        Contacts = contacts,
                        ContactRequests = contactRequests,
                        PendingContacts = pendingContacts
                    });
            }
            catch (Exception e)
            {
                return new Tuple<ResultModel, ContactsModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "There was an error retrieving your contact list",
                        Success = false,
                        Exception = e
                    },
                    new ContactsModel()
                    {
                        ContactRequests = new List<UserModel>(),
                        Contacts = new List<UserModel>(),
                        PendingContacts = new List<UserModel>()
                    });
            }
        }

        /// <summary>
        /// Add another user to the given user's contact list.
        /// The added user will see the result of this function as a 
        /// "pending contact request" that they can accept.
        /// </summary>
        public ResultModel AddContact(int? userId, int contactId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            if (userId == contactId)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You can't add yourself as a contact",
                    Success = false
                };
            }

            try
            {
                UserContact existing = context.UserContacts.SingleOrDefault(x => (x.User1Id == userId && x.User2Id == contactId)
                                                                            || (x.User2Id == userId && x.User1Id == contactId));

                if (existing != null)
                {
                    //Someone already added the other as a contact. Treat this like
                    //accepting a contact request if it was the other person.
                    string message;
                    bool success;
                    if (existing.User1Id == userId)
                    {
                        //already on this user's contact list
                        message = "This user is already on your contact list";
                        success = false;
                    }
                    else if (existing.User2Id == userId && existing.User2Accepted == false)
                    {
                        //it's basically like accepting a contact request
                        existing.User2Accepted = true;
                        message = "Added user to your contact list";
                        success = true;
                    }
                    else
                    {
                        //already accepted the contact request
                        message = "This user is already on your contact list";
                        success = false;
                    }

                    context.SaveChanges();

                    return new ResultModel()
                    {
                        DisplayMessage = message,
                        Success = success
                    };
                }
                else
                {
                    //Convention:
                    //Say we have two users, Bob and Aerith.
                    //If Bob adds Aerith as a contact, Bob's UserId should ALWAYS be User1Id
                    //and Aerith's should ALWAYS be User2Id.
                    //If Aerith adds Bob, Aerith's UserId should ALWAYS be User1Id
                    //and Bob's should ALWAYS be User2Id.
                    //The person who adds another is always User1Id,
                    //and the person who is added is always User2Id.
                    context.UserContacts.Add(new UserContact()
                    {
                        User1Id = (int)userId,
                        User2Id = contactId,
                        User1Accepted = true,
                        User2Accepted = false
                    });

                    context.SaveChanges();

                    return new ResultModel()
                    {
                        DisplayMessage = "User added to your contact list",
                        Success = true
                    };
                }
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error adding the user to your contact list",
                    Success = false,
                    Exception = e
                };
            }
        }

        /// <summary>
        /// Accept the contact request of another user.
        /// </summary>
        public ResultModel AcceptContactRequest(int? userId, int contactId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            try
            {
                //Due to how we set up contact requests, User1Id will always be the person who added the other.
                //Thus, User1Id should match the contactId in this context.
                UserContact contactRequest = context.UserContacts.Single(x => x.User2Id == userId && x.User1Id == contactId);

                contactRequest.User2Accepted = true;

                context.SaveChanges();

                return new ResultModel()
                {
                    DisplayMessage = "Accepted this user's contact request",
                    Success = true
                };

            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error accepting this user's contact request",
                    Success = false,
                    Exception = e
                };
            }
        }

        /// <summary>
        /// Reject the contact request of another user.
        /// </summary>
        public ResultModel RejectContactRequest(int? userId, int contactId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            try
            {
                //Due to how we set up contact requests, User1Id will always be the person who added the other.
                //Thus, User1Id should match the contactId in this context.
                UserContact contactRequest = context.UserContacts.Single(x => x.User2Id == userId && x.User1Id == contactId);

                context.UserContacts.Remove(contactRequest);

                context.SaveChanges();

                return new ResultModel()
                {
                    DisplayMessage = "Contact request rejected",
                    Success = true
                };

            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error rejecting this user's contact request",
                    Success = false,
                    Exception = e
                };
            }

        }

        /// <summary>
        /// Removes a contact from a user's contact list.
        /// </summary>
        public ResultModel RemoveContact(int? userId, int contactId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            try
            {
                UserContact contactRequest = context.UserContacts.Single(x => (x.User2Id == userId && x.User1Id == contactId)
                                                                         || (x.User2Id == contactId && x.User1Id == userId));

                context.UserContacts.Remove(contactRequest);

                context.SaveChanges();

                return new ResultModel()
                {
                    DisplayMessage = "User removed from your contact list",
                    Success = true
                };

            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error removing this user from your contact list",
                    Success = false,
                    Exception = e
                };
            }
        }

        /// <summary>
        /// Converts a User into its model for serving to the client.
        /// </summary>
        public static UserModel UserToModel(User user)
        {
            UserModel model = new UserModel()
            {
                Email = "This user has not made their email public",
                UserId = user.UserId,
                Username = user.Username,
                Online = ((ConcurrentDictionary<int, int>)HttpRuntime.Cache.Get("OnlineUsers")).ContainsKey(user.UserId),
                Guest = user.Guest
            };

            return model;
        }

        /// <summary>
        /// Converts a User into a UserProfileModel for serving to the client.
        /// </summary>
        public static UserProfileModel UserProfileToModel(User user)
        {
            UserProfileModel model = new UserProfileModel()
            {
                Email = "HiddenByDefault",
                UserId = user.UserId,
                Username = user.Username,
                Online = ((ConcurrentDictionary<int, int>)HttpRuntime.Cache.Get("OnlineUsers")).ContainsKey(user.UserId),
                Guest = user.Guest,
                JoinDate = user.JoinDate != null ? (DateTime)user.JoinDate : new DateTime(1970, 1, 1)
            };

            if (user.UserProfile != null)
            {
                model.AboutMe = user.UserProfile.AboutMe;
                model.RealName = user.UserProfile.RealName;
                model.Settings = user.UserProfile.Settings;
            }

            return model;
        }

        /// <summary>
        /// Returns a user's basic information for use by the frontend's business logic, as opposed to the full profile
        /// which is used for display purposes.
        /// </summary>
        public Tuple<ResultModel, UserModel> GetBasicUserInfo(int id, int? userId)
        {
            if (userId == null)
            {
                return new Tuple<ResultModel, UserModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "You are not signed in",
                        Success = false
                    },
                    null);
            }

            try
            {
                User user = context.Users.Single(x => x.UserId == id);

                UserModel model = UserBusiness.UserToModel(user);

                return new Tuple<ResultModel, UserModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "Basic info returned successfully",
                        Success = true
                    },
                    model);
            }
            catch (Exception e)
            {
                return new Tuple<ResultModel, UserModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "The requested user does not exist",
                        Exception = e,
                        Success = false
                    },
                    null);
            }


        }

        /// <summary>
        /// Returns a user's full profile.
        /// </summary>
        public Tuple<ResultModel, UserProfileModel> GetUserProfile(int id, int? userId)
        {
            if (userId == null)
            {
                return new Tuple<ResultModel, UserProfileModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "You are not signed in",
                        Success = false
                    },
                    null);
            }

            try
            {
                User user = context.Users.Single(x => x.UserId == id);

                UserProfileModel model = UserBusiness.UserProfileToModel(user);

                return new Tuple<ResultModel, UserProfileModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "Profile returned successfully",
                        Success = true
                    },
                    model);
            }
            catch (Exception e)
            {
                return new Tuple<ResultModel, UserProfileModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "The requested user does not exist",
                        Exception = e,
                        Success = false
                    },
                    null);
            }

        }

        /// <summary>
        /// Uploads a user's profile picture to the server.
        /// </summary>
        public ResultModel UploadProfilePicture(HttpPostedFileBase postedImage, int? userId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            if (postedImage == null || !postedImage.ContentType.StartsWith("image/"))
            {
                return new ResultModel()
                {
                    DisplayMessage = "File is not an image",
                    Success = false
                };
            }

            try 
            {
                Image image = Image.FromStream(postedImage.InputStream);

                //Determine that the image matches our profile image criteria.
                if (image.Height > 200 || image.Width > 200)
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "The file must be 200x200 pixels at the largest",
                        Success = false
                    };
                }

                string imageFolder = HostingEnvironment.MapPath("~/Images/profile");

                

                //We're going to save in png format to normalize the various formats we'll get, also lossless etc.
                image.Save(imageFolder + "/" + userId + ".png", ImageFormat.Png);

                return new ResultModel()
                {
                    DisplayMessage = "Profile image uploaded successfully",
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error uploading your profile image",
                    Exception = e,
                    Success = false
                };
            }

            
        }

        /// <summary>
        /// Obtains the profile picture for the given userId, or
        /// a default profile picture if they haven't yet uploaded one.
        /// </summary>
        public FileStream GetProfilePicture(int userId, int? requesterId)
        {
            string filePath = HostingEnvironment.MapPath("~/Images/profile");

            string userImagePath = filePath + "/" + userId + ".png";

            //Show a default image if not signed in or the user hasn't uploaded an image
            if (requesterId == null || !File.Exists(userImagePath))
            {
                return File.OpenRead(filePath + "/default.png");
            }
            else
            {
                return File.OpenRead(userImagePath);
            }
        }

        /// <summary>
        /// Edits the provided user's profile, updating it with the provided information.
        /// </summary>
        public ResultModel EditProfile(string username, string realName, string aboutMe, string settings, int? userId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }
            try
            {
                User user = context.Users.Single(x => x.UserId == userId);

                UserProfile profile = user.UserProfile;

                if (profile == null)
                {
                    profile = new UserProfile();
                    user.UserProfile = profile;
                }

                user.Username = username;
                profile.RealName = realName;
                profile.AboutMe = aboutMe;
                profile.Settings = settings;

                context.SaveChanges();

                return new ResultModel()
                {
                    DisplayMessage = "Profile updated",
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "Error updating profile",
                    Success = false,
                    Exception = e
                };
            }
            
        }


        public class UserComparer : IEqualityComparer<User>
        {
            public bool Equals(User x, User y)
            {
                return x.UserId == y.UserId;
            }

            public int GetHashCode(User obj)
            {
                return obj.UserId.GetHashCode();
            }
        }

        /// <summary>
        /// Signs in a user as a guest user. Saves a temporary entry into the database which will be cleaned up when
        /// the session expires.
        /// </summary>
        public UserModel ValidateGuest(string username)
        {
            username = "Guest-" + username;

            UserModel result = new UserModel()
            {
                Username = username
            };

            //Get something to put into required fields.
            string time;
            lock (GuestLock)
            {
                time = DateTime.Now.Ticks.ToString();

                while (time == LastTime)
                {
                    time = DateTime.Now.Ticks.ToString();
                }

                LastTime = time;
            }

            //Save the guest user to the database for just this session.
            //When the session expires, it will be deleted.
            User user = new User()
            {
                Email = time,
                Password = time,
                Salt = time,
                Username = username,
                Guest = true,
                JoinDate = DateTime.UtcNow
            };

            context.Users.Add(user);
            context.SaveChanges();

            result = UserBusiness.UserToModel(user);

            return result;
        }

        /// <summary>
        /// Changes the specified user's password from oldpassword to newpassword.
        /// </summary>
        public ResultModel ChangePassword(string oldpassword, string newpassword, int? userId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            try
            {
                User user = context.Users.Single(x => x.UserId == userId);

                string salt = user.Salt;

                string hash = HashPassword(salt, oldpassword);

                if (hash != user.Password)
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "Your old password seems to be incorrect",
                        Success = false
                    };
                }

                string newSalt = GenerateSalt();

                string newHash = HashPassword(newSalt, newpassword);

                user.Salt = newSalt;
                user.Password = newHash;

                context.SaveChanges();

                return new ResultModel()
                {
                    DisplayMessage = "Password changed",
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error changing your password",
                    Exception = e,
                    Success = false
                };
            }
        }

        /// <summary>
        /// If there is a user account tied to the given email address, sends a Forgot Password email to them.
        /// </summary>
        public ResultModel SendForgotPasswordEmail(string email, UrlHelper Url)
        {
            try 
            {
                User user = context.Users.SingleOrDefault(x => x.Email == email);
                if (user == null)
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "There doesn't seem to be a user with this email address",
                        Success = false
                    };
                }

                //Generate the URL
                string passwordResetKeyPath = HostingEnvironment.MapPath("~/App_Data/ResetKey.crypt");
                string hashString = user.Email + "|" + user.Salt + "|" + user.Password + "|" + DateTime.Today.ToString("yyyy/MM/dd"); //include today's date to make the link valid for 3 days.

                string hash = Crypto.ComputeHMAC(hashString, passwordResetKeyPath);

                string host = HttpContext.Current.Request.Url.Host;
                int port = HttpContext.Current.Request.Url.Port;
                string protocol = HttpContext.Current.Request.Url.Scheme; //http/https

                string url = Url.Action("", "", new RouteValueDictionary(new { email = email, passwordReset = hash }), protocol, host);

                //Set up the message
                string smtpUsername = ConfigurationManager.AppSettings["SMTPUsername"];
                MailMessage message = new MailMessage(smtpUsername, email);

                message.Subject = "Password Reset";
                message.Body = "<p>A password reset was requested for your account at " + host + ". If it wasn't you, you can ignore this email.</p>" +
                                "<p>Otherwise, you can follow this link to reset your password: " + url + " </p>" +
                                "<p>This link is valid for 3 days or until your password changes.</p>";
                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient();

                client.Send(message);


                ////Set up the SMTP client object
                //string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
                //string smtpUsername = ConfigurationManager.AppSettings["SMTPUsername"];
                //string smtpPassword = ConfigurationManager.AppSettings["SMTPPassword"];
                //int smtpPort = Int32.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
                //bool useSSL = Boolean.Parse(ConfigurationManager.AppSettings["SMTPUseSSL"]);

                //SmtpClient client = new SmtpClient(smtpServer, smtpPort)
                //{
                //    Credentials = new NetworkCredential(smtpUsername, smtpPassword),
                //    EnableSsl = useSSL,
                //    DeliveryMethod  = SmtpDeliveryMethod.Network
                //};

                //client.Send("projectdefero@gmail.com", email, "Password Reset", 
                //    "A password reset was requested for your account at projectdefero.com. If it wasn't you, you can ignore this email.<br/><br/>" +
                //    "Otherwise, you can follow this link to reset your password: " + url + " <br/><br/>" + 
                //    "This link is valid for 3 days or until your password changes."
                //    );

                return new ResultModel()
                {
                    DisplayMessage = "Password Reset email sent",
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error sending a Password Reset email",
                    Success = false,
                    Exception = e
                };
            }
        }

        /// <summary>
        /// Verifies that the given email/password reset parameters are valid (that is, they can be used to change the password
        /// for the account tied to the given email).
        /// </summary>
        public  ResultModel VerifyForgotPasswordLink(string email, string passwordReset)
        {
            try
            {
                User user = context.Users.SingleOrDefault(x => x.Email == email);

                if (user == null)
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "There is not an account tied to this email address",
                        Success = false
                    };
                }

                //Might want to make this spin somehow to prevent an attacker
                //querying the server as fast as possible with random hashes trying to find a valid link.
                if (IsValidPasswordLink(user, passwordReset))
                {
                        return new ResultModel()
                        {
                            DisplayMessage = "Valid",
                            Success = true
                        };
                }
                else
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "This password reset link is not valid",
                        Success = false
                    };
                }
                
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error verifying the password reset link",
                    Exception = e,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Changes the password for the user tied to the given email to the provided password,
        /// if the email/passwordReset combo is valid.
        /// </summary>
        public ResultModel ChangeForgottenPassword(string email, string passwordReset, string newPassword)
        {
            try
            {
                User user = context.Users.SingleOrDefault(x => x.Email == email);

                if (user == null)
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "There is not an account tied to this email address",
                        Success = false
                    };
                }

                //Might want to make this spin somehow to prevent an attacker
                //querying the server as fast as possible with random hashes trying to find a valid link.
                if (!IsValidPasswordLink(user, passwordReset))
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "This password reset link is not valid",
                        Success = false
                    };
                }
                else
                {
                    string newSalt = GenerateSalt();

                    string newHash = HashPassword(newSalt, newPassword);

                    user.Salt = newSalt;
                    user.Password = newHash;

                    context.SaveChanges();

                    return new ResultModel()
                    {
                        DisplayMessage = "Password changed",
                        Success = true
                    };
                }

            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error changing your password",
                    Exception = e,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Determines whether the provided email/password combo is valid.
        /// </summary>
        private bool IsValidPasswordLink(User user, string passwordReset)
        {
            string passwordResetKeyPath = HostingEnvironment.MapPath("~/App_Data/ResetKey.crypt");

            string hashString = user.Email + "|" + user.Salt + "|" + user.Password + "|";

            DateTime date = DateTime.Today;

            //Reset links are only valid for 3 days, so we need to try today, yesterday, and the day before.
            string serverHash;
            int iterations = 0;
            do
            {
                serverHash = hashString + date.ToString("yyyy/MM/dd");

                serverHash = Crypto.ComputeHMAC(serverHash, passwordResetKeyPath);

                if (serverHash == passwordReset)
                {
                    return true;
                }

                date = date.AddDays(-1);
                iterations++;
            }
            while (iterations < 3);

            return false;
        }
    }
}