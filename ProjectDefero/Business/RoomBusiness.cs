﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using ProjectDefero.Utilities;

using DeferoEntities.Entities;
using ProjectDefero.Models;
using System.IO;
using System.Web.Hosting;
using System.Web.Caching;
using System.Web.Routing;

namespace ProjectDefero.Business
{
    public class RoomBusiness
    {
        private DeferoDB context;
        private UserBusiness userBus;

        /// <summary>
        /// Constructor for RoomBusiness.
        /// </summary>
        public RoomBusiness() 
        {
            context = new DeferoDB();
            userBus = new UserBusiness();
        }

        /// <summary>
        /// Method creates a new Room if the input new Room name is not already in use.
        /// </summary>
        /// <param name="newRoomName">
        /// New Room name.
        /// </param>
        /// <returns>
        /// True if Room was created successfully, false otherwise.
        /// </returns>
        public Tuple<ResultModel, RoomModel> CreateRoom(int? userId, String newRoomName)
        {
            if (userId == null)
            {
                return new Tuple<ResultModel,RoomModel>(new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "You are not signed in."
                },
                null);
            }

            if (newRoomName.Length > context.GetMaxLength<Room>(x => x.Name))
            {
                return new Tuple<ResultModel,RoomModel>(new ResultModel() 
                {
                    Success = false,
                    DisplayMessage = "Room name is too long."
                },
                null);
            }

            Room room = new Room()
            {
                Name = newRoomName
            };

            context.Rooms.Add(room);

            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return new Tuple<ResultModel,RoomModel>(new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "There was an error creating the room.",
                    Exception = ex
                },
                null);
            }

            //RoomId is implicitly set after SaveChanges(), so this model will have the appropriate info.
            RoomModel model = RoomToModel(room);

            return new Tuple<ResultModel,RoomModel>(new ResultModel()
                {
                    Success = true,
                    DisplayMessage = "Room created."
                },
                model);
        }

        /// <summary>
        /// Method adds an existing User to an existing Room. This method should only be used
        /// when a user creates a room to add that user to their own room. Use the overload
        /// with the invite key for users who join via invites from other users.
        /// </summary>
        /// <param name="username">
        /// Username of User who is to join the Room.
        /// </param>
        /// <param name="roomName">
        /// Name of Room the User is to join.
        /// </param>
        /// <param name="asAdmin">
        /// Whether the user should join as admin. False by default.
        /// </param>
        /// <returns>
        /// True if the User joined the room successfully, false otherwise.
        /// </returns>
        public ResultModel JoinRoom(int? userId, int roomId, bool asAdmin = false)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "You are not signed in"
                };
            }

            
            User user = context.Users.SingleOrDefault(u => u.UserId == userId);

            if (ReferenceEquals(null, user))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "User not found"
                };
            }

            Room room = context.Rooms.SingleOrDefault(r => r.RoomId == roomId);

            if (ReferenceEquals(null, room))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "Room not found"
                };
            }

            //Check if the user already is in this room
            if (room.UserRooms.Any(x => x.UserId == userId)) 
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "You are already in this room"
                };
            }


            VTRoomRole roomRole;
            if (asAdmin)
            {
                roomRole = context.VTRoomRoles.Single(x => x.Name == "Administrator");
            }
            else
            {
                roomRole = context.VTRoomRoles.Single(x => x.Name == "User");
            }

            UserRoom userRoom = new UserRoom()
            {
                UserId = user.UserId,
                RoomId = room.RoomId,
                RoomRoleId = roomRole.RoomRoleId,
                JoinDate = DateTime.UtcNow
            };

            context.UserRooms.Add(userRoom);

            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "There was an error joining the room",
                    Exception = ex
                };
            }
            

            return new ResultModel()
            {
                Success = true,
                DisplayMessage = "Successfully joined the room"
            };
        }

        /// <summary>
        /// Joins a room using the room's InviteKey.
        /// </summary>
        public ResultModel JoinRoom(int? userId, string inviteKey)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "You are not signed in"
                };
            }

            User user = context.Users.SingleOrDefault(u => u.UserId == userId);

            if (ReferenceEquals(null, user))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "User not found"
                };
            }

            //Get the RoomId from the InviteKey.
            string[] idHmac = inviteKey.Split('_');
            string roomIdString = idHmac[0]; 

            //Verify that the HMAC from the invite key is correct for this room.
            string hmac = Crypto.ComputeHMAC(roomIdString, HostingEnvironment.MapPath("~/App_Data/InviteKey.crypt"));

            if (idHmac.Length != 2 || hmac != idHmac[1])
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "Invalid invite key"
                };
            }

            int roomId = Int32.Parse(roomIdString);

            Room room = context.Rooms.SingleOrDefault(r => r.RoomId == roomId);

            if (ReferenceEquals(null, room))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "Room not found"
                };
            }

            //Check if the user already is in this room
            if (room.UserRooms.Any(x => x.UserId == userId))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "You are already in this room"
                };
            }

            //Default to User role for invited users.
            VTRoomRole roomRole = context.VTRoomRoles.Single(x => x.Name == "User");
            

            UserRoom userRoom = new UserRoom()
            {
                UserId = user.UserId,
                RoomId = room.RoomId,
                RoomRoleId = roomRole.RoomRoleId,
                JoinDate = DateTime.UtcNow
            };

            context.UserRooms.Add(userRoom);

            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "There was an error joining the room",
                    Exception = ex
                };
            }


            return new ResultModel()
            {
                Success = true,
                DisplayMessage = "Successfully joined the room"
            };

        }

        /// <summary>
        /// Joins a room using the room's Url Key.
        /// </summary>
        public ResultModel JoinRoomByRoomKey(int? userId, string roomKey)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "You are not signed in"
                };
            }

            User user = context.Users.SingleOrDefault(u => u.UserId == userId);

            if (ReferenceEquals(null, user))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "User not found"
                };
            }

            //Get the RoomId from the InviteKey.
            string[] idHmac = roomKey.Split('_');

            string roomIdString = idHmac[0]; //Crypto.DecryptString(roomKey, HostingEnvironment.MapPath("~/App_Data/RoomKey.crypt"));

            //Verify that the hmac is correct for this id
            string hmac = Crypto.ComputeHMAC(roomIdString, HostingEnvironment.MapPath("~/App_Data/RoomKey.crypt"));

            if (idHmac.Length != 2 || hmac != idHmac[1])
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "Invalid room URL"
                };
            }

            int roomId = Int32.Parse(roomIdString);

            Room room = context.Rooms.SingleOrDefault(r => r.RoomId == roomId);

            if (ReferenceEquals(null, room))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "Room not found"
                };
            }

            //Check if the user already is in this room
            if (room.UserRooms.Any(x => x.UserId == userId))
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "You are already in this room"
                };
            }

            //Default to User role for invited users.
            VTRoomRole roomRole = context.VTRoomRoles.Single(x => x.Name == "User");


            UserRoom userRoom = new UserRoom()
            {
                UserId = user.UserId,
                RoomId = room.RoomId,
                RoomRoleId = roomRole.RoomRoleId,
                JoinDate = DateTime.UtcNow
            };

            context.UserRooms.Add(userRoom);

            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return new ResultModel()
                {
                    Success = false,
                    DisplayMessage = "There was an error joining the room",
                    Exception = ex
                };
            }


            return new ResultModel()
            {
                Success = true,
                DisplayMessage = "Successfully joined the room"
            };

        }

        /// <summary>
        /// Method returns Boolean representing whether or not method input is the name
        /// of an existing Room.
        /// </summary>
        /// <param name="roomName">
        /// Room name that may already be in use by a Room.
        /// </param>
        /// <returns>
        /// True if the input is a match for an existing Room name,
        /// false otherwise.
        /// </returns>
        public bool RoomExists(String roomName)
        {
            return context.Rooms
                .Where(r => r.Name == roomName)
                .Count() > 0;
        }

        /// <summary>
        /// Obtains a list of rooms the current user is in.
        /// Returns an empty list if the user is not logged in.
        /// </summary>
        public Tuple<ResultModel, List<RoomModel>> GetRooms(int? userId)
        {
            if (userId == null)
            {
                return new Tuple<ResultModel, List<RoomModel>>(
                    new ResultModel()
                    {
                        Success = false,
                        DisplayMessage = "You are not signed in"
                    },
                    new List<RoomModel>());
            }

            try
            {
                List<RoomModel> rooms = new List<RoomModel>();

                User user = context.Users.Single(x => x.UserId == userId);

                List<UserRoom> r = user.UserRooms.ToList();

                foreach (var room in r)
                {
                    rooms.Add(RoomToModel(room.Room));
                }

                return new Tuple<ResultModel, List<RoomModel>>(
                    new ResultModel()
                    {
                        Success = true,
                        DisplayMessage = "Rooms retrieved"
                    }, 
                    rooms);
            }
            catch (Exception e)
            {
                return new Tuple<ResultModel, List<RoomModel>>(
                    new ResultModel()
                    {
                        Success = false,
                        DisplayMessage = "There was an error retrieving your rooms from the database",
                        Exception = e
                    },
                    new List<RoomModel>());
            }
        }

        /// <summary>
        /// Converts a Room into a RoomModel for serving to the client.
        /// </summary>
        public RoomModel RoomToModel(Room room, bool includeInviteKey = true)
        {
            RoomModel model = new RoomModel()
            {
                Name = room.Name,
                RoomId = room.RoomId,
                Users = new List<UserRoomModel>()
            };

            if (includeInviteKey)
            {
                //Get the Invite Key for this room.
                string inviteKey = (string)HttpRuntime.Cache.Get("Room:" + model.RoomId);

                //Generate it if it doesn't exist in the cache.
                //Invite Keys are intended to prevent people from
                //sending a URL and joining other peoples' rooms at any time.
                //Since this code reuses a crypto key and IV for every room to generate invite keys,
                //it is not necessarily the best way to go about this.
                //However, for now, it will work.
                if (inviteKey == null)
                {
                    string keyPath = HostingEnvironment.MapPath("~/App_Data/InviteKey.crypt");

                    inviteKey = model.RoomId + "_" + Crypto.ComputeHMAC(model.RoomId.ToString(), keyPath);

                    //Put in cache for future reference. Have it expire if the keyfile changes.
                    HttpRuntime.Cache.Add("Room:" + model.RoomId, inviteKey,
                        new CacheDependency(keyPath),
                        Cache.NoAbsoluteExpiration,
                        TimeSpan.FromMinutes(10),
                        CacheItemPriority.Normal,
                        null);
                }

                model.InviteKey = inviteKey;
            }

            List<UserRoom> urs = room.UserRooms.ToList();

            //Add all the users currently in the room to the list.
            foreach (var userRoom in urs)
            {
                model.Users.Add(RoomBusiness.UserRoomToModel(userRoom.User, userRoom.VTRoomRole.Name, userRoom.JoinDate));
            }

            return model;
        }

        /// <summary>
        /// Removes a user from a room they are currently in.
        /// </summary>
        public ResultModel LeaveRoom(int? userId, int roomId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            try
            {
                Room room = context.Rooms.Single(x => x.RoomId == roomId);

                UserRoom ur = room.UserRooms.Single(x => x.UserId == userId);

                context.UserRooms.Remove(ur);

                //If the room is now empty, just delete it as nobody can join it now.
                if (room.UserRooms.Count == 0)
                {
                    context.Rooms.Remove(room);
                }

                context.SaveChanges();

                return new ResultModel()
                {
                    DisplayMessage = "You have left the room",
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error leaving the room",
                    Exception = e,
                    Success = false
                };
            }
            
        }

        /// <summary>
        /// Kicks a User from a room if the caller is an Admin for that room. 
        /// Otherwise, the kick attempt will fail.
        /// </summary>
        public ResultModel KickUser(int roomId, int kickedUserId, int? userId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            try
            {
                Room room = context.Rooms.Single(x => x.RoomId == roomId);

                if (room.UserRooms.Any(x => x.UserId == userId && x.VTRoomRole.Name == "Administrator")) 
                {
                    //The caller is an admin and is allowed to kick the given user.
                    UserRoom kickedUser = room.UserRooms.Single(x => x.UserId == kickedUserId);

                    context.UserRooms.Remove(kickedUser);

                    context.SaveChanges();

                    return new ResultModel()
                    {
                        DisplayMessage = "User was kicked from the room",
                        Success = true
                    };
                }
                else
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "You are not an administrator for this room",
                        Success = false
                    };
                }
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error kicking this user",
                    Exception = e,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Changes the User's role for the room if the caller is an Admin for that room.
        /// Otherwise, the change attempt will fail.
        /// </summary>
        public ResultModel ChangeUserRole(int roomId, int changedUserId, int? userId, string newRole)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            if (userId == changedUserId)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You can't change your own role",
                    Success = false
                };
            }

            try
            {
                Room room = context.Rooms.Single(x => x.RoomId == roomId);

                if (room.UserRooms.Any(x => x.UserId == userId && x.VTRoomRole.Name == "Administrator"))
                {
                    //Caller is an admin for this room and can change other users' roles for this room
                    UserRoom user = room.UserRooms.Single(x => x.UserId == changedUserId);

                    int roleId = context.VTRoomRoles.Single(x => x.Name == newRole).RoomRoleId;

                    user.RoomRoleId = roleId;

                    context.SaveChanges();

                    return new ResultModel()
                    {
                        DisplayMessage = "User's role changed to " + newRole,
                        Success = true
                    };
                }
                else
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "You are not an administrator for this room",
                        Success = false
                    };
                }

            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "There was an error changing this user's role",
                    Exception = e,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Returns a Url which, when visited by a user, will allow them to join the specified room.
        /// </summary>
        public Tuple<ResultModel, string> GetRoomUrl(int roomId, UrlHelper Url, int? userId)
        {
            if (userId == null)
            {
                return new Tuple<ResultModel, string>(
                    new ResultModel()
                    {
                        DisplayMessage = "You are not signed in",
                        Success = false
                    },
                    "");
            }

            User user = context.Users.Single(x => x.UserId == userId);

            if (user.UserRooms.Where(x => x.RoomId == roomId).Count() == 0)
            {
                return new Tuple<ResultModel, string>(
                    new ResultModel()
                    {
                        DisplayMessage = "You are not in the specified room",
                        Success = false
                    },
                    "");
            }

            string keyPath = HostingEnvironment.MapPath("~/App_Data/RoomKey.crypt");

            string roomKey = roomId + "_" + Crypto.ComputeHMAC(roomId.ToString(), keyPath);

            string host = HttpContext.Current.Request.Url.Host;
            int port = HttpContext.Current.Request.Url.Port;
            string protocol = HttpContext.Current.Request.Url.Scheme; //http/https

            string url = Url.Action("", "", new RouteValueDictionary(new { roomKey = roomKey }), protocol, host);


            return new Tuple<ResultModel, string>(
                new ResultModel()
                {
                    DisplayMessage = "Room Url obtained",
                    Success = true
                },
                url);
        }

        /// <summary>
        /// Returns information about a room by decoding the provided key.
        /// </summary>
        public Tuple<ResultModel, RoomModel> GetRoomInfoByKey(string roomKey, int? userId)
        {
            if (userId == null)
            {
                return new Tuple<ResultModel, RoomModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "You are not signed in",
                        Success = false
                    },
                    null);
            }

            try
            {
                string keyPath = HostingEnvironment.MapPath("~/App_Data/RoomKey.crypt");

                string[] idHmac = roomKey.Split('_');

                string roomIdString = idHmac[0];

                //Verify the hmac is correct for this room

                string hmac = Crypto.ComputeHMAC(roomIdString, keyPath);

                if (idHmac.Length != 2 || hmac != idHmac[1])
                {
                    return new Tuple<ResultModel, RoomModel>(
                        new ResultModel()
                        {
                            DisplayMessage = "Invalid room URL",
                            Success = false
                        },
                        null);
                }

                int roomId = Int32.Parse(roomIdString);

                Room room = context.Rooms.SingleOrDefault(x => x.RoomId == roomId);

                if (room == null)
                {
                    return new Tuple<ResultModel, RoomModel>(
                        new ResultModel()
                        {
                            DisplayMessage = "Room not found",
                            Success = false
                        },
                        null);
                }

                //Get the room info, making sure not to hand out the invite key to the user.
                RoomModel model = RoomToModel(room, includeInviteKey: false);

                return new Tuple<ResultModel, RoomModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "Room info returned successfully",
                        Success = true
                    },
                    model);
            }
            catch (Exception e)
            {
                return new Tuple<ResultModel,RoomModel>(
                    new ResultModel()
                    {
                        DisplayMessage = "Error retrieving room info",
                        Success = false,
                        Exception = e
                    }, 
                    null);
            }
            
        }



        /// <summary>
        /// Converts a User into a UserRoomModel that contains UserModel information plus information on the User's role for the current room.
        /// </summary>
        public static UserRoomModel UserRoomToModel(User user, string roleName, DateTime joinDate)
        {
            UserModel basicModel = UserBusiness.UserToModel(user);

            return new UserRoomModel()
            {
                Email = basicModel.Email,
                Online = basicModel.Online,
                UserId = basicModel.UserId,
                Username = basicModel.Username,
                Role = roleName,
                JoinTime = (joinDate - new DateTime(1970, 1, 1)).TotalMilliseconds,
                Guest = basicModel.Guest
            };
        }


        /// <summary>
        /// Edits the room information for the specified room.
        /// Currently just the room's name.
        /// </summary>
        public ResultModel EditRoom(int roomId, string name, int? userId)
        {
            if (userId == null)
            {
                return new ResultModel()
                {
                    DisplayMessage = "You are not signed in",
                    Success = false
                };
            }

            //Ensure the user is in the specified room with an Administrator role.
            try
            {
                User user = context.Users.Single(x => x.UserId == userId);

                UserRoom userRoom = user.UserRooms.SingleOrDefault(x => x.RoomId == roomId);

                if (userRoom == null)
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "You are not a member of this room",
                        Success = false
                    };
                }

                if (userRoom.VTRoomRole.Name != "Administrator")
                {
                    return new ResultModel()
                    {
                        DisplayMessage = "You are not an Administrator of this room",
                        Success = false
                    };
                }

                //Now we can edit the room.
                Room room = userRoom.Room;

                room.Name = name;

                context.SaveChanges();

                return new ResultModel()
                {
                    DisplayMessage = "Room info updated",
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new ResultModel()
                {
                    DisplayMessage = "Error updating room info",
                    Exception = e,
                    Success = false
                };
            }
        }
    }
}
