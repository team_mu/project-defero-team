﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ProjectDefero.Business
{
    public class ErrorBusiness
    {
        private static object ErrorLogLock = new object();

        public ErrorBusiness()
        {

        }

        /// <summary>
        /// Logs an error to a file.
        /// </summary>
        public void LogErrorToFile(string logFilePath, string logMessage)
        {
            lock (ErrorLogLock)
            {
                if (!File.Exists(logFilePath))
                {
                    File.Create(logFilePath).Close();
                }

                //Consider logging additional information like userId, and requiring the user to be logged in, in an effort
                //to stop malicious users from flooding the logs with fake error messages.
                using (StreamWriter w = File.AppendText(logFilePath))
                {
                    w.Write("Error Log - ");
                    w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                                            DateTime.Now.ToLongDateString());
                    w.WriteLine("  IP: {0}", HttpContext.Current.Request.UserHostAddress);
                    w.WriteLine("  Message: {0}", logMessage);
                    w.WriteLine("-------------------------------");
                }
            }
        }

        /// <summary>
        /// Sends a basic post HTTP request to the address 'url' with the post data 'post_data'
        /// </summary>
        /// <param name="url"></param>
        /// <param name="post_data"></param>
        private string getHTTPResponse(string url, string post_data)
        {
            // WTF???? http://stackoverflow.com/a/566847
            System.Net.ServicePointManager.Expect100Continue = false;

            byte[] byte_data = Encoding.ASCII.GetBytes(post_data);
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentLength = byte_data.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byte_data, 0, byte_data.Length); 
            dataStream.Close();

            WebResponse response = request.GetResponse();
            System.Diagnostics.Debug.WriteLine(((HttpWebResponse)response).StatusDescription);
            Stream data = response.GetResponseStream();
            StreamReader reader = new StreamReader(data);
            string text = reader.ReadToEnd();
            System.Diagnostics.Debug.WriteLine(text);
            response.Close();
            return text;
        }

        /// <summary>
        /// Sends a message to beta.wjtask.com. Which will create a task under https://beta.wjtask.com/0x17000012c2
        /// </summary>
        /// <param name="message"></param>
        public void LogErrorToWjtask(string message)
        {
            string api_token = getHTTPResponse("https://beta.wjtask.com/:api_token", "");

            char param = (char) 2, method = (char) 1;

            string DOMAIN_ID = "23", SUBTASKS_OID = "0x17000012c8", PERSON_OID = "0x17000013c3",
                   data = ":AL" + method + ":AD" + param + DOMAIN_ID + param + PERSON_OID +
                          method + ":s" + param + SUBTASKS_OID +
                          method + "new" + param + message + param + "0"; 

            getHTTPResponse("https://beta.wjtask.com/" + api_token, data);            
        }
    }
}