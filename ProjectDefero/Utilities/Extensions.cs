﻿using DeferoEntities.Entities;
using ProjectDefero.Business;
using ProjectDefero.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectDefero.Utilities
{
    public static class Extensions
    {
        /// <summary>
        /// Obtains the maximum length of a string field in an entity in the current context.
        /// Usage: context.GetMaxLength&lt;User&gt;(x => x.Name);
        /// </summary>
        /// <source>http://stackoverflow.com/questions/12378186/entity-framework-5-maxlength/12964634#12964634</source>
        /// <typeparam name="T">The type of an Entity in the context</typeparam>
        /// <param name="column">A function which describes the name of the column</param>
        /// <returns>The maximum length of the given column, or null if the column isn't of a type that has a maximum length.</returns>
        public static int? GetMaxLength<T>(this DeferoDB context, Expression<Func<T, string>> column)
        {
            int? result = null;
            
            var entType = typeof(T);
            var columnName = ((MemberExpression)column.Body).Member.Name;

            var objectContext = ((IObjectContextAdapter)context).ObjectContext;
            var test = objectContext.MetadataWorkspace.GetItems(DataSpace.CSpace);

            if (test == null)
                return null;

            var q = test
                .Where(m => m.BuiltInTypeKind == BuiltInTypeKind.EntityType)
                .SelectMany(meta => ((EntityType)meta).Properties
                .Where(p => p.Name == columnName && p.TypeUsage.EdmType.Name == "String"));

            var queryResult = q.Where(p =>
            {
                var match = p.DeclaringType.Name == entType.Name;
                if (!match)
                    match = entType.Name == p.DeclaringType.Name;

                return match;

            })
                .Select(sel => sel.TypeUsage.Facets["MaxLength"].Value)
                .ToList();

            if (queryResult.Any())
                result = Convert.ToInt32(queryResult.First());

            return result;
            
        }

        /// <summary>
        /// Fills in the "contacts", "contactRequests", and "pendingContacts" lists with 
        /// this user's contacts,
        /// a list of people who want to be added to this user's contact list, and
        /// a list of people this user has added to his/her contact list but that have not added this user to theirs.
        /// </summary>
        public static void GetContacts(this User user, List<UserModel> contacts, List<UserModel> contactRequests, List<UserModel> pendingContacts)
        {
            List<UserContact> userContacts = user.UserContacts.ToList();
            List<UserContact> userContacts1 = user.UserContacts1.ToList();
            //c.User is this user, c.User1 is the other user. Blame EntityFramework.
            foreach (var c in userContacts)
            {
                if (c.User1Accepted == true && c.User2Accepted == true)
                {
                    contacts.Add(UserBusiness.UserToModel(c.User1));
                }
                else if (c.User1Accepted == true && c.User2Accepted == false)
                {
                    pendingContacts.Add(UserBusiness.UserToModel(c.User1));
                }
                else if (c.User1Accepted == false && c.User2Accepted == true)
                {
                    contactRequests.Add(UserBusiness.UserToModel(c.User1));
                }
            }

            //c.User1 is this user, c.User is the other user. Blame EntityFramework.
            foreach (var c in user.UserContacts1.ToList())
            {
                if (c.User2Accepted == true && c.User1Accepted == true)
                {
                    contacts.Add(UserBusiness.UserToModel(c.User));
                }
                else if (c.User1Accepted == true && c.User2Accepted == false)
                {
                    contactRequests.Add(UserBusiness.UserToModel(c.User));
                }
                else if (c.User2Accepted == true && c.User1Accepted == false)
                {
                    pendingContacts.Add(UserBusiness.UserToModel(c.User));
                }
                
            }

            
        }


        /// <summary>
        /// Fills in the "contacts" and "contactRequests" lists with this user's contacts and
        /// a list of people who want to be added to this user's contact list.
        /// </summary>
        public static void GetContactsAsUsers(this User user, List<User> contacts, List<User> contactRequests)
        {

            //Users in this collection will always be in the contactSet by convention.
            //User1Id == user.UserId in this collection.
            foreach (var c in user.UserContacts.ToList())
            {
                contacts.Add(c.User1);
            }

            //Users in this collection *might* be in the contactSet depending on
            //if both sides have accepted. User2Id == user.UserId in this collection.
            foreach (var c in user.UserContacts1.ToList())
            {
                if (c.User2Accepted)
                {
                    contacts.Add(c.User);
                }
                else
                {
                    contactRequests.Add(c.User);
                }
            }


        }
    }
}