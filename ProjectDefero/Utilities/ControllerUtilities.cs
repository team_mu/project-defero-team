﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectDefero.Utilities
{
    public static class ControllerUtilities
    {
        /// <summary>
        /// Returns a JsonResult in a similar fashion to a controller's built-in Json method.
        /// If in debug mode, forces JsonRequestBehavior to allow GET requests.
        /// If in release mode, it will by default deny GET requests unless otherwise specified.
        /// </summary>
        public static JsonResult Json(object data, JsonRequestBehavior jsonRequestBehavior = JsonRequestBehavior.DenyGet)
        {
            JsonResult result;

            if (System.Diagnostics.Debugger.IsAttached)
            {
                result = new JsonResult()
                {
                    ContentType = "application/json",
                    Data = data,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                result = new JsonResult()
                {
                    ContentType = "application/json",
                    Data = data,
                    JsonRequestBehavior = jsonRequestBehavior
                };
            }

            return result;
        }
    }
}