﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ProjectDefero.Utilities
{
    /// <summary>
    /// A class for generating and using encryption keys
    /// for various purposes throughout the project.
    /// </summary>
    public class Crypto
    {
        /// <summary>
        /// Generates an AES key and saves it
        /// to the file denoted by filePath.
        /// </summary>
        public static void GenerateKey(string filePath)
        {
            byte[] key = new byte[64];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(key);

                SaveKey(key, filePath);
            }
        }

        /// <summary>
        /// Computes the HMAC of the given string using the key at the given location, 
        /// returning the result as a hex-encoded string.
        /// </summary>
        public static string ComputeHMAC(string text, string filePath)
        {
            byte[] key;

            LoadKey(out key, filePath);

            using (HMACSHA256 hmac = new HMACSHA256(key))
            {

                byte[] result = hmac.ComputeHash(Encoding.UTF8.GetBytes(text));

                return BitConverter.ToString(result).Replace("-", String.Empty);
            }
        }

       
        /// <summary>
        /// Converts a Hex string to an array of bytes.
        /// </summary>
        private static byte[] GetHexBytes(string str)
        {
            byte[] array = Enumerable.Range(0, str.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(str.Substring(x, 2), 16)).ToArray();

            return array;
        }

        /// <summary>
        /// Saves the given key and IV to a file.
        /// </summary>
        private static void SaveKey(byte[] key, string filePath)
        {
            using (FileStream stream = File.Create(filePath))
            {
                stream.Write(key, 0, 64);
            }
        }


        /// <summary>
        /// Reads in an IV and key from a file and returns them as arrays of bytes.
        /// </summary>
        private static void LoadKey(out byte[] key, string filePath)
        {
            key = new byte[64]; 

            using (FileStream stream = File.OpenRead(filePath))
            {
                stream.Read(key, 0, 64);
            }
        }
    }
}