﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Collections.Concurrent;
using System.Web.Security;
using ProjectDefero.Utilities;
using System.Web.Hosting;
using System.IO;
using DeferoEntities.Entities;

namespace ProjectDefero
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            //Set up the cache for online users.
            HttpRuntime.Cache.Add("OnlineUsers", new ConcurrentDictionary<int, int>(), null, Cache.NoAbsoluteExpiration,
                Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, OnlineUserCacheCallback);


            //Initialize the database with any necessary values such as room roles.
            Initialization.InitializeRoomRoles();

            //Create keys used for room invites and urls.
            string inviteKeyPath = HostingEnvironment.MapPath("~/App_Data/InviteKey.crypt");
            string roomUrlKeyPath = HostingEnvironment.MapPath("~/App_Data/RoomKey.crypt");
            string passwordResetKeyPath = HostingEnvironment.MapPath("~/App_Data/ResetKey.crypt");

            if (!File.Exists(inviteKeyPath))
            {
                Crypto.GenerateKey(inviteKeyPath);
            }

            if (!File.Exists(roomUrlKeyPath))
            {
                Crypto.GenerateKey(roomUrlKeyPath);
            }

            if (!File.Exists(passwordResetKeyPath))
            {
                Crypto.GenerateKey(passwordResetKeyPath);
            }

            //Delete guest users in case of weird crashing or something 
            //(since the server is starting this shouldn't break any sessions)
            DeferoDB context = new DeferoDB();

            var guestUsers = context.Users.Where(x => x.Guest == true).ToList();

            foreach (var guest in guestUsers)
            {
                var rooms = guest.UserRooms.ToList();

                foreach (var room in rooms)
                {
                    context.UserRooms.Remove(room);
                }

                context.SaveChanges();

                //context.Users.Remove(guest); //don't delete the user to preserve names in chat history.
            }

            context.SaveChanges();
        }

        /// <summary>
        /// Attempts to reinsert the online user cache if it is somehow evicted (it shouldn't be).
        /// </summary>
        private void OnlineUserCacheCallback(String key, Object cache, CacheItemRemovedReason r)
        {
            Console.Error.WriteLine("The user cache was evicted for some reason: " + r.ToString());
            //Attempt to reinsert the cache.
            try
            {
                HttpRuntime.Cache.Add("OnlineUsers", cache, null, Cache.NoAbsoluteExpiration,
                                Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, OnlineUserCacheCallback);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
            }

        }

        /// <summary>
        /// Fires on the first access to the session via HttpContext.Session["key"].
        /// Reads in the user's authentication cookie if it is available and sets the UserId.
        //  Does nothing if the user has no authentication cookie.
        /// </summary>
        void Session_Start()
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

                int userId = Int32.Parse(ticket.UserData);
                string username = ticket.Name;

                HttpContext.Current.Session["UserId"] = userId;
                HttpContext.Current.Session["Username"] = username;
            }
        }

        /// <summary>
        /// Fires on the expiration of the session.
        /// Cleans up the user from the database if they are a guest user.
        /// </summary>
        void Session_End()
        {
            DeferoDB context = new DeferoDB();

            int? userId = (int?)Session["UserId"];

            if (userId != null)
            {
                User user = context.Users.Single(x => x.UserId == userId);

                if (user.Guest)
                {
                    //Clear out any associations with this guest user in the database.
                    var rooms = user.UserRooms.ToList();

                    foreach (var room in rooms)
                    {
                        context.UserRooms.Remove(room);
                    }

                    //context.Users.Remove(user); //don't delete the user to preserve names in chat history.

                    context.SaveChanges();
                }
            }
        }
    }
}