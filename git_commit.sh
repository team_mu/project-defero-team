#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "ERROR: Message for git commit -m \"MESSAGE\" required"
else
    git add . -A
    git commit -m "$1"
    git push -u origin mvcproject
fi


