﻿CREATE TABLE [dbo].[VTRoomRole] (
    [RoomRoleId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_VTRoomRole] PRIMARY KEY CLUSTERED ([RoomRoleId] ASC)
);

