﻿CREATE TABLE [dbo].[UserProfile] (
    [UserId]   INT            NOT NULL,
    [RealName] NVARCHAR (50)  NULL,
    [AboutMe]  NVARCHAR (500) NULL,
    [Settings] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_UserProfile_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

