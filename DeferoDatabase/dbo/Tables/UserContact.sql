﻿CREATE TABLE [dbo].[UserContact] (
    [UserContactId] INT IDENTITY (1, 1) NOT NULL,
    [User1Id]       INT NOT NULL,
    [User2Id]       INT NOT NULL,
    [User1Accepted] BIT NOT NULL,
    [User2Accepted] BIT NOT NULL,
    CONSTRAINT [PK_UserContact] PRIMARY KEY CLUSTERED ([UserContactId] ASC),
    CONSTRAINT [FK_UserContact_User] FOREIGN KEY ([User1Id]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_UserContact_User1] FOREIGN KEY ([User2Id]) REFERENCES [dbo].[User] ([UserId])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserContact]
    ON [dbo].[UserContact]([User1Id] ASC, [User2Id] ASC);

