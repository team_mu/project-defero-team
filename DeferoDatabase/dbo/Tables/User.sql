﻿CREATE TABLE [dbo].[User] (
    [UserId]   INT            IDENTITY (1, 1) NOT NULL,
    [Username] NVARCHAR (30)  NOT NULL,
    [Email]    NVARCHAR (30)  NOT NULL,
    [Password] NVARCHAR (128) NOT NULL,
    [Salt]     NVARCHAR (50)  NOT NULL,
    [Guest]    BIT            CONSTRAINT [DF_User_Guest] DEFAULT ((0)) NOT NULL,
    [JoinDate] DATETIME       NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserId] ASC)
);


















GO



GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Email]
    ON [dbo].[User]([Email] ASC);

