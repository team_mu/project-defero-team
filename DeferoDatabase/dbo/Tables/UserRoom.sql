﻿CREATE TABLE [dbo].[UserRoom] (
    [UserRoomId] INT      IDENTITY (1, 1) NOT NULL,
    [UserId]     INT      NOT NULL,
    [RoomId]     INT      NOT NULL,
    [RoomRoleId] INT      NOT NULL,
    [JoinDate]   DATETIME CONSTRAINT [DF_UserRoom_JoinDate] DEFAULT ('1/1/1970 00:00:00') NOT NULL,
    CONSTRAINT [PK_UserRoom] PRIMARY KEY CLUSTERED ([UserRoomId] ASC),
    CONSTRAINT [FK_UserRoom_Room] FOREIGN KEY ([RoomId]) REFERENCES [dbo].[Room] ([RoomId]),
    CONSTRAINT [FK_UserRoom_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_UserRoom_VTRoomRole] FOREIGN KEY ([RoomRoleId]) REFERENCES [dbo].[VTRoomRole] ([RoomRoleId])
);












GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserRoom]
    ON [dbo].[UserRoom]([UserId] ASC, [RoomId] ASC);

