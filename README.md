# Project Defero #

Project Defero is a web-based communication platform. With Chromium or Firefox based browser, there is no 
need to download and install any software. Defero provides the ability to create rooms, and within 
these rooms you can send: text, voice, video and even files to other members in the room.

## Technologies: ##
### Back End ###
* ASP.NET/C#
* SQL Server

### Front End ###
* HTML 5
* JavaScript
* WebRTC
* LocalForage
