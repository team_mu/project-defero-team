//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DeferoEntities.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class VTRoomRole
    {
        public VTRoomRole()
        {
            this.UserRooms = new HashSet<UserRoom>();
        }
    
        public int RoomRoleId { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<UserRoom> UserRooms { get; set; }
    }
}
