(
  function () {
    'use strict';
    var app = angular.module('rooms', ['room']);    

    app.directive(
      'rooms',
      function () {
        return {
          restrict: 'E',
          templateUrl: 'templates/rooms.html',
          controller: [
            '$http', function ($http) {            
              this.rooms = [];
              $http.get('dummy_data/rooms.json').success(                
                function (data) {     
                  this.rooms = data; 
                }.bind(this)
              );
            }
          ],
          controllerAs: 'rooms'
        };
      }
    );
  }
)();