(
  function () {
    var app = angular.module('user', []);
    app.directive(
      'user',
      function () {
        return {
          restrict: 'E',
          templateUrl: 'templates/user.html',
/*          controller: [
            function () {            
//              this.users = [];
              this.user = {};/*
              $http.get('dummy_data/users.json').success(                
                function (data) {     
                  this.users = data; 
                }.bind(this)
              );*/
 //           }
//          ],
          controllerAs: 'user'
        };
      }
    );
  })
();