(
  function () {
    var app = angular.module('defero', ['contact_list', 'rooms']);    
    
    window.toggle_rooms = function () {
      document.getElementsByTagName('contact-list')[0].style.display = 'none';
      document.getElementsByTagName('rooms-list')[0].style.display = 'block';
      document.getElementById('contacts_tab').classList.remove('tab_active');
      document.getElementById('rooms_tab').classList.add('tab_active');      
    };
  }
)();