(
  function () {
    'use strict';
    var app = angular.module('room', []);    

    app.directive(
      'room',
      function () {
        return {
          restrict: 'E',
          templateUrl: 'templates/room.html',
          controller: [
            '$http', function ($http) {            
              this.room = {};/*
              $http.get('dummy_data/room.json').success(                
                function (data) {     
                  this.room = data; 
                }.bind(this)
              );*/
            }
          ],
          controllerAs: 'room'
        };
      }
    );
  }
)();