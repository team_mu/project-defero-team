// contact_list

(
  function () {
    var app = angular.module('contact_list', ['user']);
    app.directive(
      'contactList',
      function () {
        return {
          restrict: 'E',
          templateUrl: 'templates/contact_list.html',
          controller: [
            '$http', function ($http) {            
              this.contacts = [];
              $http.get('dummy_data/contact_list.json').success(                
                function (data) {     
                  this.contacts = data; 
                }.bind(this)
              );
            }
          ],
          controllerAs: 'contact_list'
        };
      }
    );
  }
)();