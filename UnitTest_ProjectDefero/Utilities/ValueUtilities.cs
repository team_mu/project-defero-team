﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjectDefero.Business;
using DeferoEntities.Entities;
using ProjectDefero.Models;

namespace UnitTest_ProjectDefero.Utilities
{
    /// <summary>
    /// Class utilities for generating unique values used in unit tests.
    /// </summary>
    class ValueUtilities
    {
        //// Used for guaranteeing uniqueness of strings by incrementing before concatenating.
        //private static int uniqueValueCounter = 0;

        ///// <summary>
        ///// Method returns a Room name that is not in use yet.
        ///// </summary>
        ///// <returns>
        ///// Room name that is not in use yet.
        ///// </returns>
        //public static String GetNextUniqueRoomName()
        //{
        //    return DateTime.Now.ToString("yyyy-MM-ddHH:mm:ss:ffff") + "_" + uniqueValueCounter++;
        //}

        ///// <summary>
        ///// Method returns a Username name that is not in use yet.
        ///// </summary>
        ///// <returns>
        ///// Username name that is not in use yet.
        ///// </returns>
        //public static String GetNextUniqueUserName()
        //{
        //    Random rand = new Random();
        //    char[] validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        //    char[] nameChars = new char[rand.Next(3, 30)];

        //    for (int i = 0; i < nameChars.Length; i++)
        //    {
        //        nameChars[i] = validChars[rand.Next(0, validChars.Length - 1)];
        //    }
            
        //    return new String(nameChars);
        //}

        ///// <summary>
        ///// Method returns an email address name that is not in use yet.
        ///// </summary>
        ///// <returns>
        ///// Email address that is not in use yet.
        ///// </returns>
        //public static String GetNextUniqueEmail()
        //{
        //    Random rand = new Random();
        //    String emailDomain = "@example.com";
        //    char[] validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        //    char[] nameChars = new char[rand.Next(3, 30 - emailDomain.Length)];

        //    for (int i = 0; i < nameChars.Length; i++)
        //    {
        //        nameChars[i] = validChars[rand.Next(0, validChars.Length - 1)];
        //    }

        //    return new String(nameChars) + emailDomain;
        //}

        ///// <summary>
        ///// Method returns a valid User password.
        ///// </summary>
        ///// <returns>
        ///// Valid User password.
        ///// </returns>
        //public static String GetUniqueValidUserPassword()
        //{
        //    return DateTime.Now.ToString("F").Replace(" ", String.Empty) + "_" + uniqueValueCounter++;
        //}

        //public static Room GetNewRoom()
        //{
        //    RoomBusiness roomBiz = new RoomBusiness();

        //    Room room = new Room()
        //    {
        //        Name = GetNextUniqueRoomName()
        //    };

        //    ResultModel result = roomBiz.CreateRoom(1, room.Name);

        //    if(result.Success == false)
        //    {
        //        Trace.WriteLine("Failed to create new Room: " + result.DisplayMessage);
        //        Assert.Fail();
        //    }

        //    return room;
        //}

        ///// <summary>
        ///// Method creates and returns a new User.
        ///// </summary>
        ///// <returns>
        ///// New User.
        ///// </returns>
        //public static User GetNewUser()
        //{
        //    UserBusiness userBiz = new UserBusiness();

        //    User user = new User()
        //    {
        //        Username = GetNextUniqueUserName(),
        //        Email = GetNextUniqueEmail(),
        //        Password = GetUniqueValidUserPassword()
        //    };

        //    ResultModel result
        //        = userBiz.CreateUserAccount(
        //            user.Username
        //            , user.Email
        //            , user.Password);

        //    if(result.Success == false)
        //    {
        //        Trace.WriteLine("Failed to create new User: " + result.DisplayMessage);
        //        Assert.Fail();
        //    }
            
        //    return user;
        //}
    }
}
