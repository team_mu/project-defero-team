﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjectDefero.Business;
using ProjectDefero.Controllers;
using DeferoEntities.Entities;

using UnitTest_ProjectDefero.Utilities;

namespace UnitTest_ProjectDefero.Controllers
{
    /// <summary>
    /// Class contains unit tests for ProjectDefero.Controllers.RoomController.
    /// </summary>
    [TestClass]
    public class RoomController_UnitTest
    {
        ///// <summary>
        ///// Verify Room is created successfully.
        ///// </summary>
        //[TestMethod, TestCategory("RoomController")]
        //public void CreateRoom1()
        //{
        //    String roomName = ValueUtilities.GetNextUniqueRoomName();
        //    Trace.WriteLine("Room name used: " + roomName);

        //    RoomController rc = new RoomController();
        //    RouteValueDictionary result = new RouteValueDictionary(rc.CreateRoom(roomName).Data);

        //    Assert.AreEqual(true, result["success"], result["message"] as String);
        //}

        ///// <summary>
        ///// Verify Room is not created if Room name is already in use.
        ///// </summary>
        //[TestMethod, TestCategory("RoomController")]
        //public void CreateRoom2()
        //{
        //    Room room = ValueUtilities.GetNewRoom();
        //    Trace.WriteLine("Room name used: " + room.Name);

        //    RoomController rc = new RoomController();
        //    RouteValueDictionary result = new RouteValueDictionary(rc.CreateRoom(room.Name).Data);

        //    // Verify Room was not created.
        //    Assert.AreEqual(false, result["success"], result["message"] as String);
        //}

        ///// <summary>
        ///// Verify User can join Room successfully.
        ///// </summary>
        //[TestMethod, TestCategory("RoomController")]
        //public void UserJoinRoom1()
        //{
        //    Room room = ValueUtilities.GetNewRoom();
        //    User user = ValueUtilities.GetNewUser();

        //    Trace.WriteLine("Room name used: " + room.Name);
        //    Trace.WriteLine("Username used: " + user.Username);

        //    RoomController rc = new RoomController();
        //    RouteValueDictionary result = new RouteValueDictionary(rc.UserJoinRoom(room.Name).Data);

        //    Assert.AreEqual(true, result["success"], result["message"] as String);
        //}

        ///// <summary>
        ///// Verify non-existant User cannot join a Room.
        ///// </summary>
        //[TestMethod, TestCategory("RoomController")]
        //public void UserJoinRoom2()
        //{
        //    Room room = ValueUtilities.GetNewRoom();
        //    String nonExistantUserName = ValueUtilities.GetNextUniqueUserName();

        //    Trace.WriteLine("Room name used: " + room.Name);
        //    Trace.WriteLine("Username used: " + nonExistantUserName);

        //    RoomController rc = new RoomController();
        //    RouteValueDictionary result = new RouteValueDictionary(rc.UserJoinRoom(room.Name).Data);

        //    Assert.AreEqual(false, result["success"], result["message"] as String);
        //}

        ///// <summary>
        ///// Verify User cannot join non-existant Room.
        ///// </summary>
        //[TestMethod, TestCategory("RoomController")]
        //public void UserJoinRoom3()
        //{
        //    String nonExistantRoom = ValueUtilities.GetNextUniqueRoomName();
        //    User user = ValueUtilities.GetNewUser();

        //    Trace.WriteLine("Room name used: " + nonExistantRoom);
        //    Trace.WriteLine("Username used: " + user.Username);

        //    RoomController rc = new RoomController();
        //    RouteValueDictionary result = new RouteValueDictionary(rc.UserJoinRoom(nonExistantRoom).Data);

        //    Assert.AreEqual(false, result["success"], result["message"] as String);
        //}
    }
}
