﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Diagnostics;
using System.Collections.Generic;

using ProjectDefero.Business;
using DeferoEntities.Entities;

using UnitTest_ProjectDefero.Utilities;
using ProjectDefero.Models;

namespace UnitTest_ProjectDefero.Business
{
    /// <summary>
    /// Class contains unit tests for ProjectDefero.Business.RoomBusiness.
    /// </summary>
    [TestClass]
    public class RoomBusiness_UnitTest
    {
        ///// <summary>
        ///// Verify Room is created successfully.
        ///// </summary>
        //[TestMethod, TestCategory("RoomBusiness")]
        //public void CreateRoom1()
        //{
        //    String roomName = ValueUtilities.GetNextUniqueRoomName();
        //    Trace.WriteLine("Room name used: " + roomName);

        //    RoomBusiness room = new RoomBusiness();

        //    ResultModel myResult = room.CreateRoom(1, roomName);

        //    Assert.AreEqual(true, myResult.Success, myResult.DisplayMessage);
        //}

        ///// <summary>
        ///// Verify existing Rooms are detected.
        ///// </summary>
        //[TestMethod, TestCategory("RoomBusiness")]
        //public void CreateRoom2()
        //{
        //    String roomName = ValueUtilities.GetNextUniqueRoomName();
        //    Trace.WriteLine("Room name used: " + roomName);

        //    RoomBusiness room = new RoomBusiness();

        //    // Verify Room is not detected.
        //    Assert.AreEqual(room.RoomExists(roomName), false);

        //    ResultModel myResult = room.CreateRoom(1, roomName);

        //    if (myResult.Success == false)
        //    {
        //        Trace.WriteLine("Room creation failed: " + myResult.DisplayMessage);
        //        Assert.Fail(myResult.DisplayMessage);
        //    }

        //    // Verify Room is detected.
        //    Assert.AreEqual(true, room.RoomExists(roomName));
        //}

        ///// <summary>
        ///// Verify Room is not created if Room name is already in use.
        ///// </summary>
        //[TestMethod, TestCategory("RoomBusiness")]
        //public void CreateRoom3()
        //{
        //    String roomName = ValueUtilities.GetNextUniqueRoomName();
        //    Trace.WriteLine("Room name used: " + roomName);

        //    RoomBusiness room = new RoomBusiness();
        //    ResultModel myResult = room.CreateRoom(1, roomName);

        //    if (myResult.Success == false)
        //    {
        //        Trace.WriteLine("Room creation failed: " + myResult.DisplayMessage);
        //        Assert.Fail(myResult.DisplayMessage);
        //    }

        //    myResult = room.CreateRoom(1, roomName);
        //    Trace.WriteLine("Room creation message: " + myResult.DisplayMessage);

        //    Assert.AreEqual(false, myResult.Success);
        //}

        ///// <summary>
        ///// Verify User can join a Room.
        ///// </summary>
        //[TestMethod, TestCategory("RoomBusiness")]
        //public void UserJoinRoom1()
        //{
        //    Room room = ValueUtilities.GetNewRoom();
        //    User user = ValueUtilities.GetNewUser();

        //    Trace.WriteLine("Room name used: " + room.Name);
        //    Trace.WriteLine("Username used: " + user.Username);

        //    RoomBusiness roomBiz = new RoomBusiness();
        //    ResultModel myResult = roomBiz.JoinRoom(user.UserId, room.Name);

        //    Assert.AreEqual(true, myResult.Success, myResult.DisplayMessage);
        //}

        ///// <summary>
        ///// Verify non-existant User cannot join a Room.
        ///// </summary>
        //[TestMethod, TestCategory("RoomBusiness")]
        //public void UserJoinRoom2()
        //{
        //    Room room = ValueUtilities.GetNewRoom();
        //    String nonExistantUserName = ValueUtilities.GetNextUniqueUserName();

        //    Trace.WriteLine("Room name used: " + room.Name);
        //    Trace.WriteLine("Username used: " + nonExistantUserName);

        //    RoomBusiness roomBiz = new RoomBusiness();
        //    ResultModel myResult = roomBiz.JoinRoom(null, room.Name);

        //    Assert.AreEqual(false, myResult.Success, myResult.DisplayMessage);
        //}

        ///// <summary>
        ///// Verify User cannot join non-existant Room.
        ///// </summary>
        //[TestMethod, TestCategory("RoomBusiness")]
        //public void UserJoinRoom3()
        //{
        //    String nonExistantRoom = ValueUtilities.GetNextUniqueRoomName();
        //    User user = ValueUtilities.GetNewUser();

        //    Trace.WriteLine("Room name used: " + nonExistantRoom);
        //    Trace.WriteLine("Username used: " + user.Username);

        //    RoomBusiness roomBiz = new RoomBusiness();
        //    ResultModel myResult = roomBiz.JoinRoom(user.UserId, nonExistantRoom);

        //    Assert.AreEqual(false, myResult.Success, myResult.DisplayMessage);
        //}
    }
}
